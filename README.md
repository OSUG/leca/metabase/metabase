# Metabase Project

## Definition
The project MetaBase is a database containing many information about laboratory manipulation (sampling, extraction, amplification) and sequencing. The database is managed and browsed by a web interface developped in Django.

The web interface is composed of four modules:
- Project: manage the project information and the data of laboratory manipulation.
- sequencing information: manage the sequencing information.
- Browser: browse data with graphics and interactive tables.
- API: browse data with an application programming interface.

The project is deploy in a docker-compose environment containing:
- database server
- html server
- application server

## Dependencies
The project has need two dependencies:
- docker
- docker-compose

## Installation
The installation is described for ubuntu system. To install the project you should open a terminal, copy the gitlab project in your favorite directory and go into the project directory:

```
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/leca/metabase.git
cd metaBase
```

### Automatic
To install the project you can run the script `setup_metabase_project.sh` which process all installations and initialisations:
- install dependencies
- add docker to os group
- assign group docker to the user
- copy the docker daemon file to configure dns for internet connexion
- restart docker service
- clone the git repository of metaBase project
- give execution permission to the script `manage.py`
- create required directories for migration, database backup and database system
- write user and group id in secret environment file
- build the docker images of the application metaBase
- copy Django static files into production directory
- initialize database
- create django superuser

Command:
```
bash setup_metabase_project.sh
```

### Manual
To install the project you can process installations and initialisations manually by following the steps below:
1. install dependencies:

	```
	sudo apt install -y docker
	sudo apt install -y docker-compose
	```

2. add docker to os group:

	```
	sudo groupadd docker
	```

3. assign group docker to the current user:

	```
	sudo usermod -aG docker ${USER}
	```

4. copy the docker daemon file to configure dns for internet connexion:

	```
	sudo cp docker/daemon.json /etc/docker/
	```

5. restart docker service to consider deamon file:

	```
	sudo service docker restart
	```

6. clone the git repository of metaBase project:

	```
	git clone https://git.metabarcoding.org/lionnetc/metaBase.git docker/djangoApp
	```

7. give execution permission to the script manage.py:

	```
	sudo chmod +x docker/djangoApp/manage.py
	```

8. create required directories for migration, database backup and database:

	```
	mkdir docker/migration
	mkdir docker/db-data
	mkdir docker/backup
	```

9. write user id in secret environment file:

	```
	echo "UID="$(id -u) >> docker/.env.secret.metaBase
	```

10. write group id in secret environment file:

	```
	echo "GID="$(id -g) >> docker/.env.secret.metaBase
	```

11. build the docker images of application metaBase:

	```
	sudo docker-compose -f docker/docker-compose.prod.yml build
	```

12. copy the static files into directories of production server:

	```
	sudo docker-compose -f docker/docker-compose.prod.yml run web /app/djangoApp/manage.py collectstatic --noinput
	```

13. stop and remove docker-compose service:

	```
	sudo docker-compose -f docker/docker-compose.prod.yml down
	```

14. (optinal) initialize database if a file is provided in argument:

	```
	bash init_metabase_db.sh dump_data_file.json
	```

15. add a Django superuser:

	```
	sudo docker-compose -f docker/docker-compose.prod.yml run web /app/djangoApp/manage.py createsuperuser
	sudo docker-compose -f docker/docker-compose.prod.yml down
	```

## Execution
All interactions with the system are processed through docker-compose command.

## Create member accounts
Django has an admin interface that allow you to add new member account: http://0.0.0.0:80/admin

## Export and Import data
The data sould be exported and imported :


And after the export or import, you can down the docker-compose environment:

```
docker-compose -f docker/docker-compose.prod.yml
```

To export data, you should run the following commands:

```
docker-compose -f docker/docker-compose.prod.yml run --rm web python3 /app/djangoApp/manage.py dumpdata > /app/backup/backup_file.json
```


You can export specific table with the following commands:

```
docker-compose -f docker/docker-compose.prod.yml run --rm web python3 /app/djangoApp/manage.py dumpdata auth.user > /app/backup/backup_file_user.json
```


To import data, you should run the following commands:

```
docker-compose -f docker/docker-compose.prod.yml run --rm web python3 /app/djangoApp/manage.py loaddata /app/migration/imported_data.json
```


### Development environment
#### Automatic
To run the system with the development environment, you can execute the following command:
```
bash run_docker_compose.sh dev
```

#### Manual
To run the system with the development environment manually, you should execute in the project directory the following commands:
- if you are in the group 'docker':

	```
	docker-compose -f docker/docker-compose.dev.yml up
	docker-compose -f docker/docker-compose.dev.yml down
	```

- else you should execute command with sudoer permission:

	```
	sudo docker-compose -f docker/docker-compose.dev.yml up
	sudo docker-compose -f docker/docker-compose.dev.yml down
	```

### Production environment
#### Automatic
To run the system with the production environment, you can execute the following command:
```
bash run_docker_compose.sh prod
```

#### Manual
To run the system with the production environment manually, you should execute in the project directory the following commands:
- if you are in the group 'docker':

	```
	docker-compose -f docker/docker-compose.prod.yml up
	docker-compose -f docker/docker-compose.prod.yml down
	```

- else you should execute command with sudoer permission:

	```
	sudo docker-compose -f docker/docker-compose.prod.yml up
	sudo docker-compose -f docker/docker-compose.prod.yml down
	```

## Uninstall project
The uninstallation is described for ubuntu system. You should open a terminal into the project directory.

### Automatic
To uninstall the project, you can run the script `uninstall_project.sh`:
```
bash uninstall_project.sh all
```
The argument `all` allow you to remove os group docker and uninstall docker and docker-compose.
If you want to keep docker and docker-compose you run the script without argument:
```
bash uninstall_project.sh
```

### Manual
To uninstall manually the project, you should processs the following steps:
1. shutdown the docker-compose if the system is running:

	```
	sudo docker-compose -f docker/docker-compose.prod.yml down
	```

2. remove the docker images of project (posgres, nginx, python and docker\_web):

	```
	sudo docker rmi -f $(sudo docker images -q docker_web:latest)
	sudo docker rmi -f $(sudo docker images -q postgres:11.1-alpine)
	sudo docker rmi -f $(sudo docker images -q nginx:1.15-alpine)
	sudo docker rmi -f $(sudo docker images -q python:3.6-alpine)
	```

3. remove docker volume containing the static files:

	```
	sudo docker volume rm docker_static-file-volume
	```

4. remove the docker daemon file:

	```
	sudo rm /etc/docker/daemon.json
	```

5. remove required directories:

	```
	sudo rm -r docker/migration
	sudo rm -r docker/db-data
	sudo rm -r docker/backup
	```

6. remove the group docker:

	```
	sudo deluser ${USER} docker
	sudo groupdel docker
	```

7. uninstall docker and docker-compose:

	```
	sudo apt autoremove docker docker-compose
