"""
Django development settings for metaBase project.
"""

from .common import *
import os

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'dlw%j(#x+!v=j@pmu&ch+*v0^wf40_xi)($ceiiwqa2s&9y193'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Allowed host to run application
ALLOWED_HOSTS = ["*"]

INTERNAL_IPS = ('127.0.0.1')

# Application definition

INSTALLED_APPS.append('debug_toolbar')

MIDDLEWARE.append('debug_toolbar.middleware.DebugToolbarMiddleware')

STATIC_ROOT="/static"

LOGGING = {
    'version': 1,
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django.db.backends': {
            'level': 'DEBUG',
            'handlers': ['console']
        },
    },
}
