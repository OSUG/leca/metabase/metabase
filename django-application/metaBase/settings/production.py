"""
Django production settings for metaBase project.
"""

from .common import *
import base64
import os

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

if 'DJANGO_SECRET_KEY' in os.environ:
    SECRET_KEY = base64.b64decode(os.getenv('DJANGO_SECRET_KEY'))
else:
    raise Exception('Please set DJANGO_SECRET_KEY')

if 'DJANGO_ALLOWED_HOSTS' in os.environ:
    ALLOWED_HOSTS = os.getenv('DJANGO_ALLOWED_HOSTS').split(',')
else:
    ALLOWED_HOSTS = ['0.0.0.0']
