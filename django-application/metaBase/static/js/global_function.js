/******************************************/
/* global function for apps metabase      */
/******************************************/

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie != "") {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) == name + "=") {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return /^(GET|HEAD|OPTIONS|TRACE)$/.test(method);
}

function initCSRFToken() {
  var csrftoken = getCookie("csrftoken");
  $.ajaxSetup({
    beforeSend: function(xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    }
  });
}

function user_authentication(responseJSON) {
  if (responseJSON.not_authenticated) {
    location.href = responseJSON.redirect_url;
  }
}

function displayFormError(
  feedbackSelector,
  inputSelector,
  messageDivSeletor,
  button,
  responseJSON
) {
  if (responseJSON.errorType == "validator") {
    // django widget error messsage
    responseJSON.validatorErrors.forEach(function(error) {
      $(feedbackSelector).append(error);
    });
    $(inputSelector).addClass("is-invalid");
  } else {
    // cards error message
    $(messageDivSeletor).html(responseJSON.html);
  }
  button.prop("disabled", true);
}

// write message in div 'message' to give general information
function writeMessages(message, messageType){
  messageType = messageType === "error" ? "danger" : messageType;
  $("#messages").html("<div class=\"alert alert-" +
    messageType + " alert-dismissible\">" +
    message +
    "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
    "<span aria-hidden=\"true\">&times;</span>" +
    "</button></div>");
}

// write message in div 'message' to give general information
function ErrorModalMsg(messageDivSeletor, message, title=""){
  $(messageDivSeletor).html("<div class=\"card text-white bg-danger mb-3\">" +
    "<h5 class=\"card-header\">Error "+ title +"</h5>" +
    "<div class=\"card-body\">" +
    message +
    "</div></div>");
}
