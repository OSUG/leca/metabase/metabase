from django import template
from metaBase import __version__

register = template.Library()

@register.simple_tag
def get_version():
    if __version__ == "CI_COMMIT_REF_NAME":
        return None
    return __version__
