from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import include, re_path, path
from django.conf import settings
from django.views.generic.base import RedirectView
from django.views import defaults


urlpatterns = [
    re_path(r'^$',
        RedirectView.as_view(pattern_name='metabaseHome:index',
                             permanent=False)),
    re_path(r'^metabaseEditor/',
        include('metabaseEditor.urls')),
    re_path(r'^home/',
        include('metabaseHome.urls')),
    re_path(r'^browser/',
        include('metabaseBrowser.urls')),
    re_path(r'^api/',
        include('metabaseAPI.urls')),

    re_path(r'^login/$',
        auth_views.LoginView.as_view(), name='login'),
    re_path(r'^logout/$',
        auth_views.LogoutView.as_view(),
        {'next_page': '/login/'},
        name='logout'),

    re_path(r'^account/password_change/$',
        auth_views.PasswordChangeView.as_view(),
        name='password_change'),
    re_path(r'^account/password_change/done/$',
        auth_views.PasswordChangeDoneView.as_view(),
        name='password_change_done'),
    re_path(r'^account/password_reset/$',
        auth_views.PasswordResetView.as_view(),
        name='password_reset'),
    re_path(r'^account/password_reset/done/$',
        auth_views.PasswordResetDoneView.as_view(),
        name='password_reset_done'),
    path('account/reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm'),
    re_path(r'^account/reset/done/$',
        auth_views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete'),

    re_path(r'^admin/', admin.site.urls),
    re_path(r'^admin/doc/', include('django.contrib.admindocs.urls'))
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        re_path(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

def handler404(request, exception):
    return defaults.page_not_found(request, exception, template_name='error/404.html')

def handler500(request):
    return defaults.server_error(request, template_name='error/500.html')

def handler403(request, exception):
    return defaults.permission_denied(request, exception, template_name='error/403.html')

def handler400(request, exception):
    return defaults.bad_request(request, exception, template_name='error/400.html')
