from django.apps import AppConfig


class MetabaseapiConfig(AppConfig):
    name = 'metabaseAPI'
