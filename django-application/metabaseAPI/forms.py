from django import forms
from django.db.models import Q

from metabaseEditor.models import Project, Tube_mix


class ProjectForm(forms.Form):
    project = forms.ModelChoiceField(Project.objects.filter(available=True))

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(ProjectForm, self).__init__(*args, **kwargs)

        if user:
            filtered_project = Project.objects.filter(Q(available=True) |
                Q(users__id=user.id)).distinct().order_by('name')
            self.fields['project'].queryset = filtered_project


class TubeNameForm(forms.Form):
    tube = forms.ModelChoiceField(
        Tube_mix.objects.filter(project__available=True).distinct().values_list(
            'name', flat=True
        ).order_by('name'))

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(TubeNameForm, self).__init__(*args, **kwargs)

        if user:
            filtered_project = Tube_mix.objects.filter(Q(project__available=True) |
            Q(project__users__id=user.id)).order_by('name')
            self.fields['tube'].queryset = filtered_project
