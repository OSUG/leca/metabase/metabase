$(document).ready(function() {
  initCSRFToken();
  $('.collapse').on('show.bs.collapse', function(e) {
  var card = $(this).closest('.card');
  $('html,body').animate({
    scrollTop: card.offset().top
  }, 500);
});

  $('input[name="projectUrl"]').val(window.location.origin + '/api/project');

  $('#getprojectList').on('click', function(e){
    location.href = '/api/project/get_list';
  });

  $("#NgsFilter")
    .off("shown.bs.collapse")
    .on("shown.bs.collapse", function() {
      var tubeTable = buildTable(
        "#tubeTable",
        "/api/tube/get-list",
        [
          {'data': 'name'},
          {'data': 'project', 'defaultContent': 'NA'},
          {'data': 'libraries', 'orderable': false, 'defaultContent': 'NA'}
        ]
      );

        tubeTable.on( 'select', function ( e, dt, type, indexes ) {
          $('#getNgsFilter').prop('disabled', false);
          if ( type === 'row' ) {
              var selectedRow = tubeTable.row( indexes ).node();
              var tubeId = $(selectedRow).attr('data-id');
              $('input[name="ngsFilterUrl"]').val(window.location.origin + '/api/tube/' + tubeId + '/ngs_filter');
          }
        }).on("deselect", function(e, dt, type, indexes) {
          $('#getNgsFilter').prop('disabled', true);
          $('input[name="ngsFilterUrl"]').val('');
        });

        $('#getNgsFilter').on('click', function(e){
          var selectedRow = tubeTable.row('.selected').node();
          var tubeId = $(selectedRow).attr('data-id');
          location.href = '/api/tube/' + tubeId + '/download_ngs_filter';
        });
    });

  $('#selectProject').on('change', function(){
    var projectName = $('#selectProject').val().trim();
    if(projectName.length == 0){
      $('#getProjectTubes').prop('disabled', true);
      $('input[name="projectTubesUrl"]').val('');
    }
    else{
      $('#getProjectTubes').prop('disabled', false);
      var uri = encodeURI(window.location.origin + '/api/project/' + projectName + '/tube_list');
      $('input[name="projectTubesUrl"]').val(uri);
    }
  });

  $('#getProjectTubes').on('click', function(e){
    var projectName = $('#selectProject').val().trim();
    location.href = '/api/project/' + projectName + '/download_tube_list';
  });

  $('#selectTubeName').on('change', function(){
    var tubeName = $('#selectTubeName').val().trim();
    if(tubeName.length == 0){
      $('#getTubeId').prop('disabled', true);
      $('input[name="tubeNameUrl"]').val('');
    }
    else{
      $('#getTubeId').prop('disabled', false);
      var uri = encodeURI(window.location.origin + '/api/tube/' + tubeName + '/tube_list');
      $('input[name="tubeNameUrl"]').val(uri);
    }
  });

  $('#getTubeId').on('click', function(e){
    var tubeName = $('#selectTubeName').val().trim();
    location.href = '/api/tube/' + tubeName + '/download_tube_list';
  });

  $('#selectProjectSample').on('change', function(){
    var projectName = $('#selectProjectSample').val().trim();
    if(projectName.length == 0){
      $('#getProjectSample').prop('disabled', true);
      $('input[name="projectSampleUrl"]').val('');
    }
    else{
      $('#getProjectSample').prop('disabled', false);
      var uri = encodeURI(window.location.origin + '/api/project/' + projectName + '/sample_list');
      $('input[name="projectSampleUrl"]').val(uri);
    }
  });

  $('#getProjectSample').on('click', function(e){
    var projectName = $('#selectProjectSample').val().trim();
    location.href = encodeURI('/api/project/' + projectName + '/download_project_samples_list');
  });

  $("#tubeSamples")
    .off("shown.bs.collapse")
    .on("shown.bs.collapse", function() {
      var tubeSampleTable = buildTable(
        "#tubeSampleTable",
        "/api/tube/get-list",
        [
          {'data': 'name'},
          {'data': 'project', 'defaultContent': 'NA'},
          {'data': 'libraries', 'orderable': false, 'defaultContent': 'NA'}
        ]
      );

      tubeSampleTable.on( 'select', function ( e, dt, type, indexes ) {
        $('#getTubeSample').prop('disabled', false);
        if ( type === 'row' ) {
          var selectedRow = tubeSampleTable.row( indexes ).node();
          var tubeId = $(selectedRow).attr('data-id');
          $('input[name="tubeSampleUrl"]').val(window.location.origin + '/api/tube/' + tubeId + '/sample_list');
        }
      }).on("deselect", function(e, dt, type, indexes) {
        $('#getTubeSample').prop('disabled', true);
        $('input[name="tubeSampleUrl"]').val('');
      });

      $('#getTubeSample').on('click', function(e){
        var selectedRow = tubeSampleTable.row('.selected').node();
        var tubeId = $(selectedRow).attr('data-id');
        location.href = '/api/tube/' + tubeId + '/download_samples_list';
      });
    });

  $("#sampleQualifiers")
    .off("shown.bs.collapse")
    .on("shown.bs.collapse", function() {
      var sampleTable = buildTable(
        "#sampleTable",
        "/api/sample/get-list",
        [
          {'data': 'name'},
          {'data': 'project'}
        ]
      );

      sampleTable.on( 'select', function ( e, dt, type, indexes ) {
        $('#getSampleQualifiers').prop('disabled', false);
        if ( type === 'row' ) {
          var selectedRow = sampleTable.row( indexes ).node();
          var sampleId = $(selectedRow).attr('data-id');
          $('input[name="sampleQualifierUrl"]').val(window.location.origin + '/api/sample/' + sampleId + '/qualifiers');
        }
      }).on("deselect", function(e, dt, type, indexes) {
        $('#getSampleQualifiers').prop('disabled', true);
        $('input[name="sampleQualifierUrl"]').val('');
      });

      $('#getSampleQualifiers').on('click', function(e){
        var selectedRow = sampleTable.row('.selected').node();
        var sampleId = $(selectedRow).attr('data-id');
        location.href = '/api/sample/' + sampleId + '/download_qualifiers_list';
      });
    });
});

function buildTable(selectorId, url, columns) {
  if ($.fn.DataTable.isDataTable(selectorId)) {
    return $(selectorId).DataTable();
  }
  else{
    var table = $(selectorId).DataTable({
      columns: columns,
      pageLength: 6,
      autoWidth: true,
      select: 'single',
      processing: true,
      language: {
        emptyTable: "No data available !",
        processing: '<i class="fa fa-spinner fa-spin fa-5x text-success"></i><span class="sr-only">Loading...</span>'
      },
      serverSide: true,
      ajax: {
        url: url,
        type: "POST",
        error: function(jqXHR){
          if (jqXHR.responseJSON){
            user_authentication(jqXHR.responseJSON);
            if (jqXHR.responseJSON.message){
              writeMessages(jqXHR.responseJSON.message, 'error');
            }
          }
          else{
            writeMessages(
              'An error occur: the data can not be loaded!',
              'error');
          }
        }
      },
      lengthChange: false,
      deferRender: true,
      initComplete: function() {
        var $sb = $(selectorId + "_wrapper .dataTables_filter input[type='search']");
        // remove current handler
        $sb.off();
        // Add key hander
        $sb.on("keypress", function (evtObj) {
            if (evtObj.keyCode == 13) {
                table.search($sb.val()).draw();
            }
        });
        // add button and button handler
        var btn = $("<button type='button' class='btn btn-success btn-sm'>Go</button>");
        $sb.after(btn);
        btn.on("click", function (evtObj) {
            table.search($sb.val()).draw();
        });
      },
      createdRow: function(row, data) {
        // add submission file id to the row
        $(row).attr({
          "data-id": data.id
        });
      },
      order: [0, "asc"]
    });
    return table;
  }
}
