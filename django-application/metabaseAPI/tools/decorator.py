from django.shortcuts import redirect
from django.contrib import messages
from django.http import JsonResponse
from django.conf import settings
from django.db.models import Q

from functools import wraps

from metabaseEditor.models import Project


def check_user_permission(view_function):
    @wraps(view_function)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            if 'project_id' in kwargs:
                filter_request = Q(id=kwargs['project_id'])
            elif "tube_id" in kwargs:
                filter_request = Q(tube_mix__id=kwargs['tube_id'])
            elif "sample_id" in kwargs:
                filter_request = (Q(tube_mix__pcr__plate__well__extract__sample__id=kwargs['sample_id']) |
                Q(tube_mix__submission_file__unused_sample__sample__id=kwargs['sample_id']) |
                Q(tube_mix__submission_file__unused_extract__extract__sample__id=kwargs['sample_id']))
            if Project.objects.filter(filter_request, Q(available=True) | Q(
            available=False, users__username=request.user.username)).exists():
                return view_function(request, *args, **kwargs)
        messages.warning(request,
            "Your account doesn't have access to this page "
            + "or your session has expired. "
            + "To proceed, please login with an account that has access.")
        from django.contrib.auth.views import redirect_to_login
        return redirect_to_login("/api", settings.LOGIN_URL)
    return wrapper


def check_user_login(view_function):
    @wraps(view_function)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            return view_function(request, *args, **kwargs)
        messages.warning(request,
            "Your account doesn't have access to this page "
            + "or your session has expired. "
            + "To proceed, please login with an account that has access.")
        from django.contrib.auth.views import redirect_to_login
        return redirect_to_login("/api", settings.LOGIN_URL)
    return wrapper
