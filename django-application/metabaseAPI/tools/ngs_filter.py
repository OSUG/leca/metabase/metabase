from metabaseEditor.models import Well, PrimerPair, Plate_96, Tube_mix
import re

def make_primer_tag_pairs(plate_id):
    primer_tag_pairs = {'forward': list(), 'reverse': list()}
    tag_forward = Well.objects.filter(
        plate__id=plate_id
    ).distinct().values_list('barcode_forward', flat=True)
    for tag in tag_forward:
        primer_forward = PrimerPair.objects.filter(
            well__plate__id=plate_id, well__barcode_forward=tag
        ).distinct().values_list('forward_sequence', flat=True)
        for primer in list(primer_forward):
            primer_tag_pairs['forward'].append(
                {'tag':tag, 'primer':primer, 'plate': plate_id})

    tag_reverse = Well.objects.filter(
        plate__id=plate_id
    ).distinct().values_list('barcode_reverse', flat=True)
    for tag in tag_reverse:
        primer_reverse = PrimerPair.objects.filter(
            well__plate__id=plate_id, well__barcode_reverse=tag
        ).distinct().values_list('reverse_sequence', flat=True)
        for primer in list(primer_reverse):
            primer_tag_pairs['reverse'].append(
                {'tag':tag, 'primer':primer, 'plate': plate_id})

    return primer_tag_pairs


def define_tag_orientation_column(plate_id):
    """
        Define the orientation of barcode in the column.

        Check if the columns contains the same reverse barcode or the same
        forward barcode and return the barcode orientation: 'classic',
        'reverse' or 'unorder'. If the reverse barcode are defined in columns
        the orientation is 'classic'. If the forward barcode are defined
        in columns the orientation is 'reverse'. Else the function return
        'unorder'.

        :param plate_id: the id of the plate in the database
        :return: the column tag orientation (string)
    """
    orientation = {'classic':0, 'reverse':0}
    columns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    rows = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    for column in columns:
        # get the number of reverse tag for the column 'column'
        ctag_reverse = Well.objects.filter(
            plate__id=plate_id, column=column
        ).distinct().values_list('barcode_reverse', flat=True).count()
        if ctag_reverse <= 1:
            orientation['classic'] += 1
        elif ctag_reverse > 1:
            # if many reverse tag are detected for the column 'column'
            ctag_forward = Well.objects.filter(
                plate__id=plate_id, column=column
            ).distinct().values_list('barcode_forward', flat=True).count()
            if ctag_forward <= 1:
                orientation['reverse'] += 1
    # check for the entire plate
    if orientation['classic'] == len(columns):
        return 'classic'
    elif orientation['reverse'] == len(columns):
        return 'reverse'
    return 'unorder'


def define_tag_orientation_row(plate_id):
    """
        Define the orientation of barcode in the rows.

        Check if the row contains the same reverse barcode or the same
        forward barcode and return the barcode orientation: 'classic',
        'reverse' or 'unorder'. If the forward barcode are defined in rows
        the orientation is 'classic'. If the reverse barcode are defined
        in rows the orientation is 'reverse'. Else the function return
        'unorder'.

        :param plate_id: the id of the plate in the database
        :return: the row tag orientation (string)
    """
    orientation = {'classic':0, 'reverse':0}
    rows = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    for row in rows:
        # get the number of reverse tag for the row 'row'
        ctag_forward = Well.objects.filter(
            plate__id=plate_id, row=row
        ).distinct().values_list('barcode_forward', flat=True).count()
        if ctag_forward <= 1:
            orientation['classic'] += 1
        elif ctag_forward > 1:
            # if many reverse tag are detected for the row 'row'
            ctag_reverse = Well.objects.filter(
                plate__id=plate_id, row=row
            ).distinct().values_list('barcode_reverse', flat=True).count()
            if ctag_reverse <= 1:
                orientation['reverse'] += 1
    # check for the entire plate
    if orientation['classic'] == len(rows):
        return 'classic'
    elif orientation['reverse'] == len(rows):
        return 'reverse'
    return 'unorder'


def define_tag_orientation(plate_id):
    """
        Define the orientation of barcode in the plate.

        For each row and column check if the barcodes are identical and return
        the barcode orientation: 'classic', 'reverse' or 'unorder'.

        :param plate_id: the id of the plate in the database
        :return: the tag orientation (string)
    """
    row_orientation = define_tag_orientation_row(plate_id)
    column_orientation = define_tag_orientation_column(plate_id)
    if row_orientation == 'classic' and column_orientation == 'classic':
        return 'classic'
    elif row_orientation == 'reverse' and column_orientation == 'reverse':
        return 'reverse'
    return 'unorder'


def get_well_coordinate(plate_id, forward_pair, reverse_pair):
    column = 'undef'
    row = 'undef'
    orientation = define_tag_orientation(plate_id)
    if orientation == 'classic':
        row = Well.objects.filter(
            plate__id=plate_id,
            barcode_forward=forward_pair['tag'],
            primer__forward_sequence=forward_pair['primer']
        ).distinct().values_list('row', flat=True).first()
        column = Well.objects.filter(
            plate__id=plate_id,
            barcode_reverse=reverse_pair['tag'],
            primer__reverse_sequence=reverse_pair['primer']
        ).distinct().values_list('column', flat=True).first()
    elif orientation == 'reverse':
        row = Well.objects.filter(
            plate__id=plate_id,
            barcode_reverse=reverse_pair['tag'],
            primer__reverse_sequence=reverse_pair['primer']
        ).distinct().values_list('row', flat=True).first()
        column = Well.objects.filter(
            plate__id=plate_id,
            barcode_forward=forward_pair['tag'],
            primer__forward_sequence=forward_pair['primer']
        ).distinct().values_list('column', flat=True).first()
    return {'column':column, 'row':row}


def add_unexisted_well_with_tag_primer(plate_id):
    unexist_wells = list()
    primer_tag_pairs = make_primer_tag_pairs(plate_id)
    replicate = 1
    for forward_pair in primer_tag_pairs['forward']:
        for reverse_pair in primer_tag_pairs['reverse']:
            well = Well.objects.filter(
                plate__id=plate_id,
                barcode_forward=forward_pair['tag'],
                barcode_reverse=reverse_pair['tag'],
                primer__forward_sequence=forward_pair['primer'],
                primer__reverse_sequence=reverse_pair['primer'])
            if not well.exists():
                well_coordinate = get_well_coordinate(plate_id,
                                                      forward_pair,
                                                      reverse_pair)
                unexist_well = {
                    'well_name': 'blank',
                    'replicat_number': replicate,
                    'well_type': 'blank',
                    'tag_forward': forward_pair['tag'],
                    'tag_reverse': reverse_pair['tag'],
                    'primer_forward': forward_pair['primer'],
                    'primer_reverse': reverse_pair['primer'],
                    'position': {
                        'column': str(well_coordinate['column']).zfill(2),
                        'row': str(well_coordinate['row'])
                    }
                }
                unexist_wells.append(unexist_well)
                replicate += 1
    return unexist_wells


def get_tag(plate_id, row, column):
    tags = {
        'forward': 'undef',
        'reverse': 'undef'
    }
    orientation = define_tag_orientation(plate_id)
    if orientation == 'classic':
        reverse_tag = Well.objects.filter(
            plate__id=plate_id,
            column=column
        ).distinct().values_list('barcode_reverse', flat=True)
        if reverse_tag.count() == 1:
            tags['reverse'] = reverse_tag.first()
        forward_tag = Well.objects.filter(
            plate__id=plate_id,
            row=row
        ).distinct().values_list('barcode_forward', flat=True)
        if forward_tag.count() == 1:
            tags['forward'] = forward_tag.first()
    elif orientation == 'reverse':
        reverse_tag = Well.objects.filter(
            plate__id=plate_id,
            row=row
        ).distinct().values_list('barcode_reverse', flat=True)
        if reverse_tag.count() == 1:
            tags['reverse'] = reverse_tag.first()
        forward_tag = Well.objects.filter(
            plate__id=plate_id,
            column=column
        ).distinct().values_list('barcode_forward', flat=True)
        if forward_tag.count() == 1:
            tags['forward'] = forward_tag.first()
    return tags


def get_primer(plate_id, row, column):
    primers = {
        'forward': 'undef',
        'reverse': 'undef'
    }
    orientation = define_tag_orientation(plate_id)
    if orientation == 'classic':
        reverse_primer = PrimerPair.objects.filter(
            well__plate__id=plate_id,
            well__column=column
        ).distinct().values_list('reverse_sequence', flat=True)
        if reverse_primer.count() == 1:
            primers['reverse'] = reverse_primer.first()
        forward_primer = PrimerPair.objects.filter(
            well__plate__id=plate_id,
            well__row=row
        ).distinct().values_list('forward_sequence', flat=True)
        if forward_primer.count() == 1:
            primers['forward'] = forward_primer.first()
    elif orientation == 'reverse':
        reverse_primer = PrimerPair.objects.filter(
            well__plate__id=plate_id,
            well__row=row
        ).distinct().values_list('reverse_sequence', flat=True)
        if reverse_primer.count() == 1:
            primers['reverse'] = reverse_primer.first()
        forward_primer = PrimerPair.objects.filter(
            well__plate__id=plate_id,
            well__column=column
        ).distinct().values_list('forward_sequence', flat=True)
        if forward_primer.count() == 1:
            primers['forward'] = forward_primer.first()
    return primers


def add_unexisted_well(plate_id):
    unexist_wells = list()
    rows = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    columns = range(1, 13)
    replicate = 1
    for column in columns:
        for row in rows:
            well = Well.objects.filter(
                plate__id=plate_id,
                column=column,
                row=row
            )
            if not well.exists():
                well_tag = get_tag(plate_id, row, column)
                well_primer = get_primer(plate_id, row, column)
                unexist_well = {
                    'well_name': 'blank',
                    'replicat_number': replicate,
                    'well_type': 'blank',
                    'tag_forward': well_tag['forward'],
                    'tag_reverse': well_tag['reverse'],
                    'primer_forward': well_primer['forward'],
                    'primer_reverse': well_primer['reverse'],
                    'position': {
                        'column': str(column).zfill(2),
                        'row': str(row)
                    }
                }
                unexist_wells.append(unexist_well)
                replicate += 1
    return unexist_wells


def get_tube_primer_tag_pair(tube_id):
    primer_tag_pairs = { 'forward': list(), 'reverse': list() }
    plates = Plate_96.objects.filter(
        pcr__tube__id=tube_id).values_list('id',flat=True)
    for plate_id in plates:
        plate_primer_tag_pairs = make_primer_tag_pairs(plate_id)
        primer_tag_pairs['forward'].extend(plate_primer_tag_pairs['forward'])
        primer_tag_pairs['reverse'].extend(plate_primer_tag_pairs['reverse'])
    return primer_tag_pairs


def get_plate_design(plate_id):
    """
        Get the plate design in MetaBase with the plate id.

        Get the plate design in the database with the plate id.

        :param plate_id: the id of the plate in the database
        :return: list of list [tube id, project name, library name(s)]
    """
    plate = list()
    # get the well information for the plate id
    wells = list(Well.objects.filter(
        plate__id=plate_id
    ).select_related(
        'extract', 'primer', 'plate').distinct().order_by('column', 'row'))
    # initiate dictionnary which contains the number of extract replicate
    extract_count = dict()
    for well in wells:
        well_type = well.wtype
        # generate the name of the well content
        if well.extract:
            # if well.extract.etype == 'sample':
                # if is an extract
            well_name = well.extract.name
            # else:
            #     # if is a pcrPos or extneg
            #     well_name = well.extract.etype
            well_type = well.extract.etype
        else:
            # if is a blank or a prcNeg
            well_name = well.wtype
        # add extract in the dict if it's not exist to initiate replicate counter
        if well_name not in extract_count:
            extract_count[well_name] = 0
        extract_count[well_name] += 1
        # generate a line of ngs filter file in a list.
        json_well = {
            'well_name': re.sub(r"\s+", "_", str(well_name).rstrip()),
            'replicat_number': extract_count[well_name],
            'well_type': well_type,
            'tag_forward': well.barcode_forward,
            'tag_reverse': well.barcode_reverse,
            'primer_forward': well.primer.forward_sequence,
            'primer_reverse': well.primer.reverse_sequence,
            'position': {
                'column': str(well.column).zfill(2),
                'row': str(well.row)
            }
        }
        plate.append(json_well)
    return plate


def get_ngs_filter_information(tube_id):
    # get the tube information
    tube = Tube_mix.objects.get(id=tube_id)
    # get plate in the database
    plates = Plate_96.objects.filter(pcr__tube__id=tube_id).distinct()
    json_structure = {
        'tube_name': tube.name,
        'tube_id': tube_id,
        'plates':[]
    }
    for plate in plates:
        plate_output = {'name': re.sub(r"\s+", "_", str(plate.name).rstrip())}
        # get the plate design
        wells = get_plate_design(plate.id)
        if len(wells) < 96:
            # wells.extend(add_unexisted_well_with_tag_primer(plate.id))
            wells.extend(add_unexisted_well(plate.id))
        plate_output['wells'] = wells
        json_structure['plates'].append(plate_output)
    return json_structure
