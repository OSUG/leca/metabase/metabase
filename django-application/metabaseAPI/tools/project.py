from django.http import JsonResponse
from django.db.models import Q

from metabaseEditor.models import Project, Tube_mix, Library


def is_parent_project(project_id):
    return Project.objects.filter(parentProject__id=project_id).exists()


def get_project(project_id):
    if Project.objects.filter(id=project_id).exists():
        return Project.objects.get(id=project_id)
    return None


def get_project_list(user):
    project_list = list()
    # get tubes list
    projects = list(Project.objects.filter(
            Q(available=True) | Q(
                available=False,
                users__username=user
            )).distinct())
    for project in projects:
        json_project = {
            'name': project.name,
            'description': project.description,
            'email': project.email,
            'leader': project.leader,
            'is_parent_project': is_parent_project(project.id),
            'tubes': []
        }

        tubes = Tube_mix.objects.filter(
            project__id=project.id
        ).distinct()
        for tube in tubes:
            json_tube = {
                'id': tube.id,
                'name': tube.name
            }
            json_project['tubes'].append(json_tube)

        json_project['libraries'] = list(Library.objects.filter(
            tube__project__id=project.id
        ).values_list('name', flat=True))
        project_list.append(json_project)
    return project_list
