from django.db.models import Q, Prefetch, Subquery, OuterRef, CharField

from metabaseEditor.models import Sample_qualifier, Sample, Project, Tube_mix, Extract

from metabaseBrowser.tools.sample_list import get_samples_list

from .project import is_parent_project

def get_project_sample_list(project_id):
    project = Project.objects.get(id=project_id)
    is_parent = is_parent_project(project_id)
    project_json = {
        'project_name': project.name,
        'project_id': project_id,
        'is_parent_project': is_parent,
        'samples': []
    }
    # get sample list
    samples = list(get_samples_list(project.id).prefetch_related(
        Prefetch('sample_qualifiers',
            queryset=Sample_qualifier.objects.all(),
            to_attr='qualifiers'),
        Prefetch('extract_set',
            queryset=Extract.objects.only("name"),
            to_attr='extracts')).annotate(
                project=Subquery(
                    Project.objects.filter(
                        Q(tube_mix__pcr__plate__well__extract__sample__id=OuterRef('id')) |
                        Q(tube_mix__submission_file__unused_sample__sample__id=OuterRef('id')) |
                        Q(tube_mix__submission_file__unused_extract__extract__sample__id=OuterRef('id'))
                    ).distinct().values('name'), output_field=CharField())))
    for sample in samples:
        json_sample = {
            'sample_id': sample.id,
            'sample_name': sample.name,
            'extract_name': get_extract_name(sample.extracts),
            'qualifiers': get_qualifier(sample.qualifiers)
        }
        if is_parent:
            json_sample['project'] = sample.project
        project_json['samples'].append(json_sample)
    return project_json


def get_qualifier(qualifiers):
    qualifiers_list = list()
    for qualifier in qualifiers:
        json_qualifier = {
            'key': qualifier.key,
            'value': qualifier.value
        }
        qualifiers_list.append(json_qualifier)
    return qualifiers_list


def get_extract_name(extracts):
    extracts_list = list()
    for extract in extracts:
        extracts_list.append(extract.name)
    return extracts_list


def get_tube_samples(tube_id):
    return Sample.objects.filter(
        Q(extract__well__plate__pcr__tube__id=tube_id) |
        Q(unused_sample__submission_file__tube_mix__id=tube_id) |
        Q(extract__unused_extract__submission_file__tube_mix__id=tube_id)
    ).distinct()


def get_sample_information(sample_id):
    sample = Sample.objects.prefetch_related(
        Prefetch('sample_qualifiers',
            queryset=Sample_qualifier.objects.all(),
            to_attr='qualifiers')).get(id=sample_id)
    return {
        'sample_id': sample_id,
        'sample_name': sample.name,
        'qualifiers': get_qualifier(sample.qualifiers)
    }


def get_tube_sample_list(tube_id):
    tube = Tube_mix.objects.get(id=tube_id)
    tube_json = {
        'tube_id': tube.id,
        'tube_name': tube.name,
        'project_name': tube.project.name if tube.project else None,
        'samples': []
    }
    # get sample list
    samples = get_tube_samples(tube_id).prefetch_related(
        Prefetch('sample_qualifiers',
            queryset=Sample_qualifier.objects.all(),
            to_attr='qualifiers'),
        Prefetch('extract_set',
            queryset=Extract.objects.only("name"),
            to_attr='extracts'))
    for sample in list(samples):
        json_sample = {
            'sample_id': sample.id,
            'sample_name': sample.name,
            'extract_name': get_extract_name(sample.extracts),
            'qualifiers': get_qualifier(sample.qualifiers)
        }
        tube_json['samples'].append(json_sample)
    return tube_json


def get_sample_tubes(sample_id):
    return Tube_mix.objects.filter(
        Q(pcr__plate__well__extract__sample__id=sample_id) |
        Q(submission_file__unused_sample__sample__id=sample_id) |
        Q(submission_file__unused_extract__extract__sample__id=sample_id)
    ).distinct()
