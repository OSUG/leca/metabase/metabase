from django.db.models import Prefetch
from metabaseEditor.models import Tube_mix, Project

from .project import is_parent_project


def get_tube(tube_id):
    if Tube_mix.objects.filter(id=tube_id).exists():
        return Tube_mix.objects.get(id=tube_id)
    return None

def get_in_sub_project(project_id, function):
    tube_list = Tube_mix.objects.none()
    sub_projects = Project.objects.filter(
        parentProject__id=project_id).distinct()
    for sub_project in sub_projects:
        tube_list = (tube_list | function(sub_project.id))
    return tube_list


def get_all_tube_list(project_id):
    tube_list = Tube_mix.objects.filter(
        project_id=project_id
    ).distinct()
    if tube_list.count() > 0:
        return tube_list
    return get_in_sub_project(project_id, get_all_tube_list)


def get_project_tubes_list(project_id):
    project = Project.objects.get(id=project_id)
    is_parent = is_parent_project(project.id)
    project_json = {
        'project_name': project.name,
        'project_id': project_id,
        'is_parent_project': is_parent,
        'tubes': []
    }
    # get tubes list
    tubes = get_all_tube_list(project.id).prefetch_related('libraries')
    for tube in tubes:
        json_tube = {
            'id': tube.id,
            'name': tube.name,
            'libraries': list(tube.libraries.values_list('name', flat=True))
        }
        if is_parent:
            json_tube['project'] = tube.project.name
        project_json['tubes'].append(json_tube)
    return project_json


def get_tubes_list(tube_name):
    # get tubes list
    tubes = Tube_mix.objects.filter(
        name=tube_name
    ).select_related('project').prefetch_related('libraries').distinct()
    json_tubes = {
        'tube_name': tube_name,
        'tubes': []
    }
    for tube in tubes:
        json_tube = {
            'id': tube.id,
            'project': tube.project.name if tube.project else None,
            'libraries': list(tube.libraries.values_list('name', flat=True))
        }
        json_tubes['tubes'].append(json_tube)
    return json_tubes
