from django.urls import re_path
from rest_framework.authtoken.views import obtain_auth_token
from . import views

app_name = 'metabaseAPI'

urlpatterns = [
    re_path(r'^$',
    views.api,
    name='api'),

    re_path(r'^project$',
        views.get_project_list_json.as_view(),
        name='get_project_list_json'),

    re_path(r'^project/get_list$',
        views.download_project_list,
        name='download_project_list'),

    re_path(r'^project/(?P<project_id>[0-9]+)/tube_list$',
        views.get_tube_id_from_project_name.as_view(),
        name='get_tube_id_from_project_name'),
    re_path(r'^project/(?P<project_id>[0-9]+)/sample_list$',
        views.get_project_samples_json.as_view(),
        name='get_project_samples_json'),
    re_path(r'^project/(?P<project_id>[0-9]+)/download_tube_list$',
        views.download_project_tubes,
        name='download_tube_list'),
    re_path(r'^project/(?P<project_id>[0-9]+)/download_project_samples_list$',
        views.download_project_samples_list,
        name='download_project_samples_list'),

    re_path(r'^tube/get-list$',
    views.TubeSamplesView.as_view(),
    name='get_samples_list'),
    re_path(r'^tube/(?P<tube_name>[^/]+)/tube_list$',
        views.get_tube_id_from_name.as_view(),
        name='get_tube_id_from_name'),
    re_path(r'^tube/(?P<tube_id>[0-9]+)/download_tube_list$',
        views.download_tubes_list,
        name='download_tubes_list'),
    re_path(r'^tube/(?P<tube_id>[0-9]+)/sample_list$',
        views.get_tube_samples_list_json.as_view(),
        name='get_tube_samples_list_json'),
    re_path(r'^tube/(?P<tube_id>[0-9]+)/download_samples_list$',
        views.download_tube_samples_list,
        name='download_tube_samples_list'),
    re_path(r'^tube/(?P<tube_id>[0-9]+)/ngs_filter$',
        views.ngs_filter.as_view(),
        name='ngs_filter_file'),
    re_path(r'^tube/(?P<tube_id>[0-9]+)/download_ngs_filter$',
        views.download_ngs_filter,
        name='download_ngs_filter'),

    re_path(r'^sample/get-list$',
    views.SampleQualifiersView.as_view(),
    name='get_samples_list'),
    re_path(r'^sample/(?P<sample_id>[0-9]+)/qualifiers',
        views.get_sample_qualifier.as_view(),
        name='get_sample_qualifier'),
    re_path(r'^sample/(?P<sample_id>[0-9]+)/download_qualifiers_list',
        views.download_qualifiers_list,
        name='download_qualifiers_list'),

    re_path(r'^get-auth-token/$', obtain_auth_token, name='get-auth-token')
    ]
