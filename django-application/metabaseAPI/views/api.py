from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required

from metabaseEditor.models import Tube_mix

from ..forms import *

from ..tools.tube import *
from ..tools.project import *
from ..tools.samples import *
from ..tools.ngs_filter import get_ngs_filter_information
from ..tools.decorator import check_user_login, check_user_permission


@require_http_methods(["GET"])
@login_required
def api(request):
    return render(request, 'api.html', {
        'project_form': ProjectForm(user=request.user),
        'tube_name_form': TubeNameForm(user=request.user)
    })


@require_http_methods(["GET"])
@check_user_login
@check_user_permission
def download_ngs_filter(request, tube_id):
    tube = Tube_mix.objects.get(id=tube_id)
    json_structure = get_ngs_filter_information(tube_id)
    json_file = JsonResponse(json_structure, content_type='application/json',
                             json_dumps_params={'ensure_ascii':False})
    response = HttpResponse(json_file, content_type='application/json')
    file_name = 'ngs_filter_tube_' + tube.name + '.json'
    response['Content-Disposition'] = ('attachment; filename="'
                                       + file_name + '"')
    return response


@require_http_methods(["GET"])
@check_user_login
@check_user_permission
def download_project_tubes(request, project_id):
    project = get_project(project_id)
    json_structure = get_project_tubes_list(project_id)
    json_file = JsonResponse(json_structure, safe=False,
                             content_type='application/json',
                             json_dumps_params={'ensure_ascii':False})
    response = HttpResponse(json_file, content_type='application/json')
    file_name = 'tubes_of_project_' + project.name + '.json'
    response['Content-Disposition'] = ('attachment; filename="'
                                       + file_name + '"')
    return response


@require_http_methods(["GET"])
@check_user_login
@check_user_permission
def download_tubes_list(request, tube_id):
    tube = get_tube(tube_id)
    json_structure = get_tubes_list(tube.name)
    json_file = JsonResponse(json_structure, content_type='application/json',
                             json_dumps_params={'ensure_ascii':False})
    response = HttpResponse(json_file, content_type='application/json')
    file_name = 'tubes_list_for_tube_' + tube.name + '.json'
    response['Content-Disposition'] = ('attachment; filename="'
                                       + file_name + '"')
    return response


@require_http_methods(["GET"])
@check_user_login
def download_project_list(request):
    json_structure = get_project_list(request.user)
    json_file = JsonResponse(json_structure, safe=False,
                             content_type='application/json',
                             json_dumps_params={'ensure_ascii':False})
    response = HttpResponse(json_file, content_type='application/json')
    file_name = 'project_list.json'
    response['Content-Disposition'] = ('attachment; filename="'
                                       + file_name + '"')
    return response


@require_http_methods(["GET"])
@check_user_login
@check_user_permission
def download_project_samples_list(request, project_id):
    project = get_project(project_id)
    json_structure = get_project_sample_list(project_id)
    json_file = JsonResponse(json_structure, safe=False,
                             content_type='application/json',
                             json_dumps_params={'ensure_ascii':False})
    response = HttpResponse(json_file, content_type='application/json')
    file_name = 'samples_list_for_project_' + project.name + '.json'
    response['Content-Disposition'] = ('attachment; filename="'
                                       + file_name + '"')
    return response


@require_http_methods(["GET"])
@check_user_login
@check_user_permission
def download_tube_samples_list(request, tube_id):
    tube = Tube_mix.objects.get(id=tube_id)
    json_structure = get_tube_sample_list(tube_id)
    json_file = JsonResponse(json_structure,
                             content_type='application/json',
                             json_dumps_params={'ensure_ascii':False})
    response = HttpResponse(json_file, content_type='application/json')
    file_name = 'samples_list_for_tube_' + tube.name + '.json'
    response['Content-Disposition'] = ('attachment; filename="'
                                       + file_name + '"')
    return response


@require_http_methods(["GET"])
@check_user_login
@check_user_permission
def download_qualifiers_list(request, sample_id):
    sample = Sample.objects.get(id=sample_id)
    json_structure = get_sample_information(sample_id)
    json_file = JsonResponse(json_structure, content_type='application/json',
                             json_dumps_params={'ensure_ascii':False})
    response = HttpResponse(json_file, content_type='application/json')
    file_name = 'qualifiers_list_for_sample_' + sample.name + '.json'
    response['Content-Disposition'] = ('attachment; filename="'
                                       + file_name + '"')
    return response
