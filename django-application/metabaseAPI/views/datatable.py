from django.http import JsonResponse
from django.db.models import Q, Case, When, Subquery, OuterRef, CharField
from django.views import View
from django.urls import reverse

from metabaseEditor.models import Sample, Project

class AsyncDataTableView(View):

    entities = None
    values = []
    url_column = None
    filter_columns = []

    PARAM_LENGTH = 'length'
    PARAM_START = 'start'
    PARAM_SORT_COL_IDX = 'order[0][column]'
    PARAM_SORT_COL_NAME = 'columns[{}][data]'
    PARAM_SORT_DIR = 'order[0][dir]'
    PARAM_SEARCH = 'search[value]'

    DEFAULT_LENGTH = 6
    DEFAULT_START = 10
    DEFAULT_SORT_COL_IDX = 0
    DEFAULT_SORT_DIR = 'asc'


    def make_order_by(self, order_dir, sorted_col):
        order = [order_dir + sorted_col]
        return order


    def make_filter(self, q):
        filter_kwargs = {self.filter_columns[0] + '__icontains': q}
        filter_q = Q(**filter_kwargs)
        filter = filter_q
        for filter_field in self.filter_columns[1:]:
            filter_kwargs = {filter_field + '__icontains': q}
            filter_q = Q(**filter_kwargs)
            filter |= filter_q
        return filter


    def custom_queryset(self, order_by, start, length):
        sample_ids = list(self.entities.order_by(
            *order_by).values_list('id', flat=True)[start:start + length])
        self.entities = list(self.entities.filter(
            id__in=sample_ids).order_by(*order_by).values(*self.values))


    def post(self, request, *args, **kwargs):

        length = int(request.POST.get(AsyncDataTableView.PARAM_LENGTH, AsyncDataTableView.DEFAULT_LENGTH))
        start = int(request.POST.get(AsyncDataTableView.PARAM_START, AsyncDataTableView.DEFAULT_START))
        sort_col_idx = int(request.POST.get(AsyncDataTableView.PARAM_SORT_COL_IDX, AsyncDataTableView.DEFAULT_SORT_COL_IDX))
        sort_dir = request.POST.get(AsyncDataTableView.PARAM_SORT_DIR, AsyncDataTableView.DEFAULT_SORT_DIR)

        order_dir = ''
        if sort_dir != AsyncDataTableView.DEFAULT_SORT_DIR:
            order_dir = '-'
        sorted_col = request.POST.get(AsyncDataTableView.PARAM_SORT_COL_NAME.format(sort_col_idx),
                                     AsyncDataTableView.PARAM_SORT_COL_NAME.format(AsyncDataTableView.DEFAULT_SORT_COL_IDX))
        order_by = self.make_order_by(order_dir, sorted_col)
        records_total = self.entities.count()
        records_filtered = records_total

        #############
        # Filtering #
        #############
        q = request.POST.get(AsyncDataTableView.PARAM_SEARCH)
        if q != None and q != '':
            if len(self.filter_columns) > 0:
                filter = self.make_filter(q)
                self.entities = self.entities.filter(filter)
                records_filtered = self.entities.count()

        self.custom_queryset(order_by, start, length)

        result = {
            "recordsTotal": records_total,
            "recordsFiltered": records_filtered,
            "data": self.entities
        }
        return JsonResponse(result, safe=False)
