# from django.views.decorators.http import require_http_methods
# from django.http import JsonResponse
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.response import Response

from ..tools.ngs_filter import *

from metabaseEditor.models import Tube_mix, Plate_96


class ngs_filter(APIView):
    def get(self, request, tube_id):
        """
            Get the plate information related to the tube with the id 'tube_id'.

            Get the plate design in the database related to the tube with the id
            'tube id'.

            :param tube_id: the id of of tube mix
            :return: json structure containing the ngs_filter_data
        """
        # Check if the tube id exist in the database
        if not Tube_mix.objects.filter(Q(id=tube_id),
                                       Q(project__available=True) |
                                       Q(project__users__id=request.user.id)).exists():
            return Response(
                {'error': 'The tube id not exist in the database'})
        json_structure = get_ngs_filter_information(tube_id)
        return Response(json_structure)
