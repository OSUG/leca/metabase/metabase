# from django.views.decorators.http import require_http_methods
# from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response

from ..tools.project import *


class get_project_list_json(APIView):
    def get(self, request):
        """
            Get the project list.

            Get all project name in the database.

            :return: list of project jaune
        """
        project_list = get_project_list(request.user)
        return Response(project_list)
