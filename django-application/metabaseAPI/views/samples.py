# from django.views.decorators.http import require_http_methods
# from django.http import JsonResponse
from django.db.models import Q, Case, When
from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils.decorators import method_decorator

from metabaseEditor.models import Sample, Project
from metabaseBrowser.tools.decorator import check_user_login_js

from .datatable import AsyncDataTableView

from ..tools.samples import *


class get_project_samples_json(APIView):
    def get(self, request, project_id):
        """
            Get the sample list containing in the project name.

            Get the sample name, and metadata in the database with
            the project name.

            :param project_name: the id of project
            :return: list of objects
        """
        # Check if the project name exist in the database
        if not Project.objects.filter(Q(id=project_id),
                                      Q(available=True) |
                                      Q(users__id=request.user.id)).exists():
            return Response(
                {'error': 'The project id not exist in the database'})
        sample_list = get_project_sample_list(project_id)
        return Response(sample_list)


class get_tube_samples_list_json(APIView):
    def get(self,request, tube_id):
        """
            Get the sample list containing in the tube with id 'tube_id'.

            Get the sample name, and metadata in the database with the tube id.

            :param tube_id: the tube id
            :return: list of objects
        """
        # Check if the tube id exist in the database
        if not Tube_mix.objects.filter(Q(id=tube_id),
                                       Q(project__available=True) |
                                       Q(project__users__id=request.user.id)).exists():
            return Response(
                {'error': 'The tube id not exist in the database'})
        sample_list = get_tube_sample_list(tube_id)
        return Response(sample_list)


class get_sample_qualifier(APIView):
    def get(self, request, sample_id):
        """
            Get the metadata of sample with the 'sample id'.

            Get the metadata in the database with the sample id.

            :param sample_id: the sample id
            :return: list of objects
        """
        available_project = Project.objects.filter(Q(available=True) | Q(
            available=False,
            users__id=request.user.id
        ))
        # Check if the sample id exist in the database
        if not Sample.objects.filter(Q(id=sample_id), Q(extract__well__plate__pcr__tube__project__in=available_project) |
        Q(unused_sample__submission_file__tube_mix__project__in=available_project) |
        Q(extract__unused_extract__submission_file__tube_mix__project__in=available_project)).exists():
            return Response(
                {'error': 'The sample id not exist in the database'})
        json_sample = get_sample_information(sample_id)
        return Response(json_sample)


@method_decorator([check_user_login_js], name="dispatch")
class SampleQualifiersView(AsyncDataTableView):

    def make_filter(self, q):
        filter = Q(name__icontains=q)
        project_list = Project.objects.filter(name__icontains=q).values_list('id', flat=True)
        filtered_sample = list(Sample.objects.filter(
            Q(extract__well__plate__pcr__tube__project__id__in=project_list) |
            Q(unused_sample__submission_file__tube_mix__project__id__in=project_list) |
            Q(extract__unused_extract__submission_file__tube_mix__project__id__in=project_list)
        ).distinct().values_list('id', flat=True))
        filter |= Q(id__in=filtered_sample)
        return filter

    def make_order_by(self, order_dir, sorted_col):
        order_by = [order_dir + sorted_col]
        if sorted_col == 'project':
            projects = Project.objects.all().order_by(order_dir + 'name').values_list('name', flat=True)
            sample_order = list()
            for project in projects:
                sample_order.extend(Sample.objects.filter(
                    Q(extract__well__plate__pcr__tube__project__name=project) |
                    Q(unused_sample__submission_file__tube_mix__project__name=project) |
                    Q(extract__unused_extract__submission_file__tube_mix__project__name=project)
                ).distinct().values_list('id', flat=True))
            order_by = [Case(*[
                            When(id=id,
                                 then=pos) for pos, id in enumerate(sample_order)
                            ])
                        ]
        return order_by

    def custom_queryset(self, order_by, start, length):
        sample_ids = list(self.entities.order_by(*order_by).values_list('id', flat=True)[start:start + length])
        self.entities = list(self.entities.filter(id__in=sample_ids).annotate(
            project=Subquery(
                Project.objects.filter(
                    Q(tube_mix__pcr__plate__well__extract__sample__id=OuterRef('id')) |
                    Q(tube_mix__submission_file__unused_sample__sample__id=OuterRef('id')) |
                    Q(tube_mix__submission_file__unused_extract__extract__sample__id=OuterRef('id'))
                ).distinct().values('name'), output_field=CharField())).order_by(*order_by).values(*self.values))

    def dispatch(self, *args, **kwargs):
        user_id = self.request.user.id
        available_project = Project.objects.filter(Q(available=True) | Q(
            available=False,
            users__id=user_id
        )).distinct()
        self.entities = Sample.objects.filter(Q(extract__well__plate__pcr__tube__project__in=available_project) |
        Q(unused_sample__submission_file__tube_mix__project__in=available_project) |
        Q(extract__unused_extract__submission_file__tube_mix__project__in=available_project)).distinct()
        self.values = ['id', 'name', 'project']
        self.filter_columns = ['name', 'project']
        return super().dispatch(*args, **kwargs)
