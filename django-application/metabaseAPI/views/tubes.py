from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils.decorators import method_decorator
from django.db.models import Q

from metabaseEditor.models import Tube_mix, Project
from metabaseBrowser.tools.decorator import check_user_login_js

from .datatable import AsyncDataTableView

from ..tools.tube import *


class get_tube_id_from_project_name(APIView):
    def get(self, request, project_id):
        """
            Get the tube list containing in the project name.

            Get the tube id, tube name and library name in the database with
            the project name.

            :param project_name: the id of project
            :return: list of objects
        """
        # Check if the project name exist in the database
        if not Project.objects.filter(Q(id=project_id),
                                      Q(available=True) |
                                      Q(users__id=request.user.id)).exists():
            return Response(
                {'error': 'The project id not exist in the database'})
        tube_list = get_project_tubes_list(project_id)
        return Response(tube_list)


class get_tube_id_from_name(APIView):
    def get(self, request, tube_name):
        """
            Get the tube information in MetaBase with the tube name.

            Get the tube id, project name and library name in the database with
            the tube name.

            :param tube_name: the name of tube mix
            :return: list of objects
        """
        # Check if the tube name exist in the database
        if not Tube_mix.objects.filter(Q(name=tube_name),
                                       Q(project__available=True) |
                                       Q(project__users__id=request.user.id)
                                       ).exists():
            return Response(
                {'error': 'The tube name not exist in the database'})
        tube_list = get_tubes_list(tube_name)
        return Response(tube_list)


@method_decorator([check_user_login_js], name="dispatch")
class TubeSamplesView(AsyncDataTableView):

    def make_order_by(self, order_dir, sorted_col):
        if sorted_col == 'project':
            sorted_col = 'project__name'
        return [order_dir + sorted_col]

    def custom_queryset(self, order_by, start, length):
        # get tubes list
        tubes = list(self.entities.order_by(*order_by)[start:start+length])
        self.entities = list()
        for tube in tubes:
            libs = list()
            for lib in tube.libraries.all():
                libs.append(lib.name)
            json_tube = {
                'id': tube.id,
                'name': tube.name,
                'project': tube.project.name if tube.project else None,
                'libraries': '; '.join(libs) if len(libs) > 0 else None
            }
            self.entities.append(json_tube)

    def dispatch(self, *args, **kwargs):
        self.entities = Tube_mix.objects.filter(Q(project__available=True) | Q(project__users__id=self.request.user.id)).select_related('project').prefetch_related(
            'libraries').distinct()
        self.values = ['id', 'name', 'project__name']
        self.filter_columns = ['name', 'project__name', 'libraries__name']
        return super().dispatch(*args, **kwargs)
