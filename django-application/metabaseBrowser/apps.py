from django.apps import AppConfig


class MetabasebrowserConfig(AppConfig):
    name = 'metabaseBrowser'
