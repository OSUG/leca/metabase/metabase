$(document).ready(function() {
  initCSRFToken();

  var projectTable = $("#project-table").DataTable({
    lengthChange: false,
    scrollX: true,
    info: false,
    ajax: {
      url: "/browser/project/get_list",
      type: "POST",
      error: function(jqXHR){
        if (jqXHR.responseJSON){
          user_authentication(jqXHR.responseJSON);
          if (jqXHR.responseJSON.message){
            writeMessages(jqXHR.responseJSON.message, 'error');
          }
        }
        else{
          writeMessages(
            'An error occur: the project list can not be loaded!',
            'error');
        }
        $(".dataTables_empty").text("error, projects can not be loaded!");
      }
    },
    columns: [
      {
        data: null,
        render: function(data, type, full, meta) {
          var available = "<i>(Protected)</i>";
          if (data.available){
            available = "<i>(Available)</i>";
          }
          return (
            '<a href="project/' + data.id + '">' + data.name + "</a> " + available
          );
        }
      },
      { data: "description" },
      { data: "leader" },
      { data: "parent_project",
        render: function(data, type, full, meta) {
          return data.replace(/; /g, "<br>");
        }
      },
      {
        data: "sub-project",
        render: function(data, type, full, meta) {
          return data.replace(/; /g, "<br>");
        }
      }
    ],
    buttons: [
      {
        extend: "columnsToggle",
        columns: ":gt(0)"
      }
    ],
    initComplete: function() {
      projectTable
        .buttons()
        .container()
        .appendTo("#project-table_wrapper .col-md-6:eq(0)");
    },
    order: [0, "asc"]
  });
});
