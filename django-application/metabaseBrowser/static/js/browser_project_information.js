$(document).ready(function() {
  buildDatatables("#subProjectTable");
  buildDatatables("#parentProjectTable");
});

function buildDatatables(selector) {
  if ($.fn.DataTable.isDataTable(selector)) {
    $(selector)
      .DataTable()
      .draw();
  } else {
    $(selector).DataTable({
      scrollY: 200,
      scrollCollapse: true,
      info: false,
      searching: false,
      paging: false,
      language: {
        emptyTable: "No project available."
      },
      order: [0, "asc"]
    });
  }
}
