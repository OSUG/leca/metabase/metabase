$(document).ready(function() {
  initCSRFToken();

  var spinner = '<i class="fa fa-spinner fa-spin fa-5x text-success"></i>';

  $("#nav-samples-list-tab")
    .off("shown.bs.tab")
    .on("shown.bs.tab", function() {
      var sampleTable;
      if (!$("#samples-list").children().length) {
        $("#samples-list").html(
          "<div class='text-center'>" + spinner + "</div>"
        );
        $.ajax({
          url: "/browser/project/" + projectId + "/sample/get-header",
          type: "POST",
          success: function(data) {
            $("#samples-list").html(data.html);
            var nbFixColumn = (data.column.length >= 2) ? 2 : 0;
            sampleTable = buildBrowserSampleTable(projectId, nbFixColumn, data.column);
          },
          error: function(jqXHR){
            if (jqXHR.responseJSON){
              user_authentication(jqXHR.responseJSON);
              if (jqXHR.responseJSON.message){
                writeMessages(jqXHR.responseJSON.message, 'error');
              }
            }
            else{
              writeMessages(
                'An error occur: the sample list can not be loaded!',
                'error');
            }
          },
          complete: function(data) {
          }
        });
      }
    });

  $("#samples-list").on("click", ".sample-tree-view", function(e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr("href"),
      type: "POST",
      success: function(data) {
        show_sample_tree(data.sample.id, data.sample.name);
      },
      error: function(jqXHR) {
        if (jqXHR.responseJSON){
          user_authentication(jqXHR.responseJSON);
          if (jqXHR.responseJSON.message){
            writeMessages(jqXHR.responseJSON.message, 'error');
          }
        }
        else{
          writeMessages(
            'An error occur: the sample tree view can not be loaded!',
            'error');
        }
      }
    });
  });

  $(".nav-tabs").on("click", "span", function() {
    var tab = $(this).parent();
    $(tab.attr("href")).remove();
    $("#nav-samples-list-tab").tab("show");
    tab.tab("dispose");
    tab.remove();
  });
});

function show_sample_tree(sampleId, sampleName){
  var spinner = '<i class="fa fa-spinner fa-spin fa-5x text-success"></i>';
  if (!$("#nav-sample-" + sampleId + "-tree-view").length) {
    $("#nav-tab").append(
      '<a class="nav-item nav-link" id="nav-sample-' +
        sampleId +
        '-tree-view" data-toggle="tab" href="#sample-' +
        sampleId +
        '-tree-view" role="tab" aria-controls="sample-' +
        sampleId +
        '-tree-view" aria-selected="true">' +
        sampleName +
        '<span class="close ml-1">&times;</span></a>'
    );
    $("#nav-tabContent").append(
      '<div class="tab-pane fade" id="sample-' +
        sampleId +
        '-tree-view" role="tabpanel" aria-labelledby="nav-sample-' +
        sampleId +
        '-tree-view"></div>'
    );
  }
  $("#nav-sample-" + sampleId + "-tree-view")
    .off("shown.bs.tab")
    .on("shown.bs.tab", function() {
      var selector = "#sample-" + sampleId + "-tree-view";
      if (!$(selector).children().length) {
        $(selector).html(
          "<div class='text-center'>" + spinner + "</div>"
        );
        setupTreeView(
          "/browser/project/" +
            projectId +
            "/sample/" +
            sampleId +
            "/init-tree",
            "#sample-" + sampleId + "-tree-view"
        );
      }
    });
  $("#nav-sample-" + sampleId + "-tree-view").tab("show");
}

function buildBrowserSampleTable(projectId, nbFixColumn, columns) {
  var samplesTable = $("#samplesTable").DataTable({
    columns: columns,
    processing: true,
    language: {
      processing: '<i class="fa fa-spinner fa-spin fa-5x text-success"></i><span class="sr-only">Loading...</span>'
    },
    serverSide: true,
    ajax: {
      url: "/browser/project/" + projectId + "/sample/get-list",
      type: "POST",
      error: function(jqXHR){
        if (jqXHR.responseJSON){
          user_authentication(jqXHR.responseJSON);
          if (jqXHR.responseJSON.message){
            writeMessages(jqXHR.responseJSON.message, 'error');
          }
        }
        else{
          writeMessages(
            'An error occur: the sample list can not be loaded!',
            'error');
        }
        $("#samplesTable .dataTables_empty").text("error, samples can not been load!");
      }
    },
    lengthChange: false,
    scrollX: true,
    deferRender: true,
    fixedColumns: {
      leftColumns: nbFixColumn
    },
    buttons: [
      {
        extend: "colvis",
        className: "btn-sm",
        columns: ":gt(1)"
      }
    ],
    columnDefs: [
      {
        targets: 0,
        data: "id",
        orderable: false,
        render: function(data, type, full, meta) {
          return (
            '<a class="btn btn-secondary btn-sm sample-tree-view"' +
            'href="/browser/project/' +
            projectId +
            "/sample/" +
            data +
            '/tree-view"> tree view </a>'
          );
        }
      }
    ],
    initComplete: function() {
      $("#samplesTable").DataTable()
        .buttons()
        .container()
        .appendTo("#samplesTable_wrapper .col-md-6:eq(0)");

        var $sb = $(".dataTables_filter input[type='search']");
        // remove current handler
        $sb.off();
        // Add key hander
        $sb.on("keypress", function (evtObj) {
            if (evtObj.keyCode == 13) {
                samplesTable.search($sb.val()).draw();
            }
        });
        // add button and button handler
        var btn = $("<button type='button' class='btn btn-success btn-sm'>Go</button>");
        $sb.after(btn);
        btn.on("click", function (evtObj) {
            samplesTable.search($sb.val()).draw();
        });
    },
    order: [1, "asc"]
  });

  $("#samplesTable").on("column-visibility.dt", function(
    e,
    settings,
    column,
    state
  ) {
    samplesTable.columns.adjust();
  });

  return samplesTable;
}
