$(document).ready(function() {
  initCSRFToken();

  var spinner = '<i class="fa fa-spinner fa-spin fa-5x text-success"></i>';

  $("#nav-project-tree-view-tab")
    .off("shown.bs.tab")
    .on("shown.bs.tab", function() {
      if (!$("#project-tree-view").children().length) {
        $("#project-tree-view").html(
          "<div class='text-center'>" + spinner + "</div>"
        );
        setupTreeView(
          "/browser/project/" + projectId + "/init-tree",
          "#project-tree-view"
        );
      }
    });
});
