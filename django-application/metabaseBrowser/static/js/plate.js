function setupPlateView(url, target) {
  function build_plate(plateData) {
    var containerName = "plate-container-" + plateData.id;
    $(target).html("<div id=" + containerName + " style='overflow-x:scroll'></div>");
    var width = $(target).width() - 30;
    if(width < 742){
      width = 742;
    }
    var maxHeight = Math.round($(document).height() / 2 / 100) * 100;
    if(maxHeight < 393){
      maxHeight = 393;
    }

    var margin = {
      top: 10,
      right: 40,
      bottom: 10,
      left: 40
    };

    // define space between circle
    var wellWidth = (width - margin.right - margin.left) / 13;
    var well_height = (maxHeight - margin.top - margin.bottom) / 9;
    wellWidth = well_height = Math.min(wellWidth, well_height);

    var tooltip = d3
      .tip()
      .offset([-10, 0])
      .attr("class", "d3-tip")
      .html(function(d) {
        var metadata = '<ul class="tooltipList">';
        if(d.wtype === "extract"){
          metadata += "<li> type: " + d.extract.etype + "</li>";
          metadata += "<li> extract: " + d.extract.name + "</li>";
          if (d.extract.etype === 'PCRPos'){
            d3.keys(d.extract.pcrpos).forEach(function(d1) {
              metadata += "<li> " + d1 + " : " + d.extract.pcrpos[d1] + "</li>";
            });
          }
          else if(d.extract.etype !== "extneg"){
            metadata += "<li> sample: " + d.extract.sample.name + "</li>";
          }
        }
        else {
          metadata += "<li> type: " + d.wtype + "</li>";
        }
        metadata += "<li> dilution: " + d.dilution + "</li>";
        metadata += "<li> forward primer: " + d.forward_primer + "</li>";
        metadata += "<li> reverse primer: " + d.reverse_primer + "</li>";
        metadata += "<li> forward tag: " + d.barcode_forward + "</li>";
        metadata += "<li> reverse tag: " + d.barcode_reverse + "</li>";
        metadata += "</ul>";
        return metadata;
      });

    var zoom = d3.zoom().scaleExtent([1, 2])
      .translateExtent([[0, 0], [width, maxHeight]])
      .on("zoom", function () {
        gWell.attr("transform", d3.event.transform);
        gRowLabel.attr("transform", d3.event.transform);
        gColLabel.attr("transform", d3.event.transform);
      });

    var svg = d3
      .select("#" + containerName)
      .append("svg")
      .attr("width", width)
      .attr("height", maxHeight)
      .attr("viewBox", [
        -(wellWidth / 2),
        -(well_height / 2),
        width,
        maxHeight
      ])
      .style("font", "10px sans-serif")
      .style("user-select", "none")
      .call(zoom);

    svg.call(tooltip);

    var gRowLabel = svg
      .append("g")
      .attr("class", "rowLabel");

    var rowLabel = gRowLabel
      .selectAll("g")
      .data(plateData.rows)
      .enter()
      .append("g")
      .attr("transform", function(d, i) {
        return "translate(" + 0 + "," + well_height * (i + 1) + ")";
      })
      .append("text")
      .attr("dy", "0.31em")
      .attr("dx", "-0.5em")
      .attr("text-anchor", "middle")
      .style("font", "14px sans-serif")
      .text(function(d) {
        return d;
      });

    var gColLabel = svg
      .append("g")
      .attr("class", "colLabel");

    var colLabel = gColLabel
      .selectAll("g")
      .data(plateData.columns)
      .enter()
      .append("g")
      .attr("transform", function(d, i) {
        return "translate(" + wellWidth * (i + 1) + "," + 0 + ")";
      })
      .append("text")
      .attr("text-anchor", "middle")
      .style("font", "14px sans-serif")
      .text(function(d) {
        return d;
      });

    var gWell = svg
      .append("g")
      .attr("class", "well")
      .attr("cursor", "pointer");

    var well = gWell
      .selectAll("g")
      .data(plateData.data)
      .enter()
      .append("g")
      .attr("transform", function(d, i) {
        return (
          "translate(" +
          wellWidth * (d.column + 1) +
          "," +
          well_height * (d.row + 1) +
          ")"
        );
      })
      .on("mouseover", function(d) {
        tooltip.show(d);
      })
      .on("mouseout", function(d) {
        tooltip.hide(d);
      })
      .on("click", function(d) {
        if (d.extract && d.extract.etype === 'extract'){
          show_sample_tree(d.extract.sample.id, d.extract.sample.name);
        }
      });

    well.append("circle")
    .attr("r", 20)
    .attr("fill", function(d) {
      if (d.wtype === "blank") {
        return "Cyan";
      } else if (d.wtype === "PCRNeg") {
        return "Red";
      } else if (d.extract.etype === "PCRPos") {
        return "Chartreuse";
      } else if (d.extract.etype === "extneg") {
        return "Yellow";
      } else {
        return "#fff";
      }
    })
    .attr("stroke", "steelblue")
    .attr("stroke-width", "1.5px");

    well.append("text")
    .attr("text-anchor", "middle")
    .text(function(d){
      if(d.wtype === 'extract' && d.extract.etype === 'extract'){
        return d.extract.name;
      }
    })
    .style("font-size", function(d) {
      return Math.min(2 * 20, (2 * 20 - 8) / this.getComputedTextLength() * 10) + "px"; })
    .attr("dy", ".35em");

    return true;
  }
  $.ajax({
    url: url,
    type: "GET",
    success: build_plate,
    error: function(jqXHR){
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        if(jqXHR.responseJSON.message) {
          writeMessages(jqXHR.responseJSON.message, 'error');
        }
      }
      else{
        writeMessages(
            'An error occur: the children data can not be loaded!',
            'error');
      }
    }
  });
}

function show_plate(id, name, url) {
  var spinner = '<i class="fa fa-spinner fa-spin fa-5x text-success"></i>';
  if (!$("#nav-plate-" + id).length) {
    $("#nav-tab").append(
      '<a class="nav-item nav-link" id="nav-plate-' +
        id +
        '" data-toggle="tab" href="#plate-' +
        id +
        '" role="tab" aria-controls="plate-' +
        id +
        '" aria-selected="true">' +
        name +
        '<span class="close ml-1">&times;</span></a>'
    );
    $("#nav-tabContent").append(
      '<div class="tab-pane fade" id="plate-' +
        id +
        '" role="tabpanel" aria-labelledby="nav-plate-' +
        id +
        '"></div>'
    );
  }

  $("#nav-plate-" + id)
    .off("shown.bs.tab")
    .on("shown.bs.tab", function() {
      if (!$("#plate-" + id).children().length) {
        $("#plate-" + id).html(
          '<div class="text-center">' + spinner + '</div>'
        );
        setupPlateView(url, "#plate-" + id);
      }
    });

  $("#nav-plate-" + id).tab("show");
}
