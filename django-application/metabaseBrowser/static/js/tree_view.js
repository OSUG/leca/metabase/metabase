function setupTreeView(url, target) {
  /**
   * Compute the best size for {@code length} so that text is fully displayed
   *
   * @param svg
   *          SVG part to add item and compute size
   * @param length
   *          the value to adapt
   * @param text
   *          the text to display within
   */
  function ensureSizeForText(svg, length, text) {
    var tt = svg
      .append("text")
      .attr("dy", -100)
      .text(text);
    var l = 12 + tt.node().getComputedTextLength();
    var best = Math.max(length, l);
    tt.remove();
    return best;
  }

  function computeSVGHeight(maxHeight, right, left, margin) {
    var treeHeight = right.x - left.x + margin.top + margin.bottom;
    return Math.min(maxHeight, treeHeight);
  }

  function build_tree(treeData) {
    var containerName = "tree-container-" + treeData.name.replace(/ /g, "_").replace(/\'/g, "_");
    $(target).html(
      "<div id=" + containerName + ' style="overflow-x:scroll"></div>'
    );
    var width = $(target).width() - 30;
    if (width < 860) {
      width = 860;
    }
    var maxHeight = $(document).height() / 1.60;
    var dx = maxHeight / treeData.children.length;
    var max_depth = 7;
    var dy = width / max_depth;
    var rootNameLength = treeData.name.length;

    var tree = d3
      .tree()
      .nodeSize([dx, dy])
      .separation(function(a, b) {
        var brother_sep = 1 / a.depth;
        if(brother_sep < 0.6){
          brother_sep = 0.6;
        }
        return a.parent == b.parent ? brother_sep : 0.8;
      });

    var root = d3.hierarchy(treeData);

    root.x0 = dy / 2;
    root.y0 = 0;
    root.descendants().forEach(function(d, i) {
      d.id = i;
    });

    var tooltip = d3
      .tip()
      .offset([-10, 0])
      .attr("class", "d3-tip")
      .html(function(d) {
        if (d.data.metadata) {
          var metadata = '<ul class="tooltipList">';
          d.data.metadata.forEach(function(d) {
            d3.keys(d).forEach(function(key) {
              if(d[key] instanceof Array){
                metadata += "<li>" + key + ": " + d[key].join(" | ") + "</li>";
              }
              else{
                metadata += "<li>" + key + ": " + d[key] + "</li>";
              }
            });
          });
          metadata += "</ul>";
          return metadata;
        }
      });

    var diagonal = d3
      .linkHorizontal()
      .x(function(d) {
        return d.y;
      })
      .y(function(d) {
        return d.x;
      });

    var zoom = d3
      .zoom()
      .scaleExtent([1, 1.5])
      .filter(function() {
        return !d3.event.button && d3.event.type !== "dblclick";
      })
      .on("zoom", function() {
        gNode.attr("transform", d3.event.transform);
        gLink.attr("transform", d3.event.transform);
      });


    var svg = d3
      .select("#" + containerName)
      .append("svg")
      .attr("width", width)
      .attr("height", maxHeight)
      .attr("viewBox", [0, 0, width, dx])
      .style("font", "10px sans-serif")
      .style("user-select", "none")
      .call(zoom);

    svg.call(tooltip);

    var margin = {
      top: 10,
      right: 80,
      bottom: 10,
      left: ensureSizeForText(svg, 40, treeData.name)
    };

    var gLink = svg
      .append("g")
      .attr("fill", "none")
      .attr("stroke", "#555")
      .attr("stroke-opacity", 0.4)
      .attr("stroke-width", 1.5);

    var gNode = svg.append("g").attr("cursor", "pointer");

    function update(source) {
      var duration = 750;
      var nodes = root.descendants().reverse();
      var links = root.links();

      margin.left = ensureSizeForText(svg, 40, treeData.name);
      // define max depth to calculate the path length between node
      var leaves_max_depth = 0;
      root.leaves().forEach(function(d){
        if (d.depth <= 7){
          if (max_depth < d.depth + 2){
            max_depth = d.depth + 2;
          }
          if (leaves_max_depth < d.depth){
            leaves_max_depth = d.depth;
          }
        }
      });
      if (leaves_max_depth == source.depth || (leaves_max_depth + 2 <= 7 && max_depth > 7)) {
        max_depth = (source.depth > 4) ? source.depth + 2 : 7;
      }

      // Compute the new tree layout.
      var children = root.children ? root.children.length : 1;
      root.descendants().forEach(function(d){
        if(d.children && d.children.length > children){
          children = d.children.length;
        }
      });
      dx = maxHeight / children;
      if(dx < 25){
        dx = 25;
      }
      dy = width / max_depth;
      tree.nodeSize([dx, dy]);
      tree(root);

      var left = root;
      var right = root;
      var adjust_width = 0;
      root.leaves().forEach(function(d){
        var right_node_position = d.y + ensureSizeForText(svg, 40, d.data.name) + margin.right;
        if (width < right_node_position && adjust_width < (right_node_position - width)){
          adjust_width = right_node_position - width;
        }
      });
      root.eachBefore(function(node) {
        node.y -= adjust_width;
        if (node.x < left.x) left = node;
        if (node.x > right.x) right = node;
      });

      height = computeSVGHeight(maxHeight, right, left, margin);

      // Update the nodes…
      var node = gNode.selectAll("g").data(nodes, function(d) {
        return d.id;
      });

      // Enter any new nodes at the parent's previous position.
      var nodeEnter = node
        .enter()
        .append("g")
        .attr("transform", function(d) {
          return "translate(" + source.y0 + "," + source.x0 + ")";
        })
        .attr("fill-opacity", 0)
        .attr("stroke-opacity", 0)
        .on("mouseover", function(d) {
          if (d.data.metadata) {
            tooltip.show(d);
          }
        })
        .on("mouseout", function(d) {
          if (d.data.metadata) {
            tooltip.hide(d);
          }
        })
        .on("click", function(d) {
          if (d.children) {
            if (d.data.type !== "tube") {
              d.name = d.name + " (" + d.children.length + ")";
              d.data.name = d.data.name + " (" + d.children.length + ")";
            }
            d.children = null;
            update(d);
          } else if (d.data.type === "plate") {
            show_plate(d.data.id, d.data.name, d.data.url);
          } else if (d.data.url) {
            get_children(d, root);
          }
        });

      nodeEnter
        .append("circle")
        .attr("r", 4.5)
        .attr("stroke", "steelblue")
        .attr("stroke-width", "1.5px");

      nodeEnter
        .append("text")
        .attr("dy", "0.31em")
        .attr("x", function(d) {
          return d.children ? -10 : 10;
        })
        .attr("text-anchor", function(d) {
          return d.children ? "end" : "start";
        })
        .text(function(d) {
          return d.data.name;
        })
        .clone(true)
        .lower()
        .attr("stroke-linejoin", "round")
        .attr("stroke-width", 3)
        .attr("stroke", "white");

      var transition = svg
        .transition()
        .duration(duration)
        .attr("height", maxHeight)
        // .attr("viewBox", [-margin.left, left.x - margin.top, width + adjust_width, height])
        .attr("viewBox", [-margin.left, left.x - margin.top, width, height])
        .tween(
          "resize",
          window.ResizeObserver ? null : function() {
                return function() {
                  svg.dispatch("toggle");
                };
              }
        );

      // Transition nodes to their new position.
      var nodeUpdate = node
        .merge(nodeEnter)
        .transition(transition)
        .attr("transform", function(d) {
          return "translate(" + d.y + "," + d.x + ")";
        })
        .attr("fill-opacity", 1)
        .attr("stroke-opacity", 1);

      nodeUpdate.selectAll("circle").attr("fill", function(d) {
        return d.data.url && d.data.type != "plate" ? "lightsteelblue" : "#fff";
      });

      nodeUpdate.selectAll("text").text(function(d) {
        return d.data.name;
      });

      // Transition exiting nodes to the parent's new position.
      var nodeExit = node
        .exit()
        .transition(transition)
        .remove()
        .attr("transform", function(d) {
          return "translate(" + source.y + "," + source.x + ")";
        })
        .attr("fill-opacity", 0)
        .attr("stroke-opacity", 0);

      // Update the links…
      var link = gLink.selectAll("path").data(links, function(d) {
        return d.target.id;
      });

      // Enter any new links at the parent's previous position.
      var linkEnter = link
        .enter()
        .append("path")
        .attr("d", function(d) {
          var o = {
            x: source.x0,
            y: source.y0
          };
          return diagonal({
            source: o,
            target: o
          });
        });

      // Transition links to their new position.
      link
        .merge(linkEnter)
        .transition(transition)
        .attr("d", diagonal);

      // Transition exiting nodes to the parent's new position.
      link
        .exit()
        .transition(transition)
        .remove()
        .attr("d", function(d) {
          var o = {
            x: source.x,
            y: source.y
          };
          return diagonal({
            source: o,
            target: o
          });
        });

      // Stash the old positions for transition.
      root.eachBefore(function(d) {
        d.x0 = d.x;
        d.y0 = d.y;
      });
    }

    function get_children(d, root){
      $.ajax({
        url: d.data.url,
        type: "GET",
        success: function(data) {
          root.descendants().forEach(function(d1) {
            d1.height += 1;
          });
          data.data.forEach(function(childData, i) {
            var child = d3.hierarchy(childData);
            child.depth = d.depth + 1;
            child.parent = d;
            child.id = d.id + "_" + i;
            if (!d.children) {
              d.children = [];
              d.data.children = [];
            }
            d.children.push(child);
          });
          d.data.name = data.name;
          update(d);
        },
        error: function(jqXHR){
          if (jqXHR.responseJSON){
            user_authentication(jqXHR.responseJSON);
            if(jqXHR.responseJSON.message) {
              writeMessages(jqXHR.responseJSON.message, 'error');
            }
          }
          else{
            writeMessages(
                'An error occur: the children data can not be loaded!',
                'error');
          }
        }
      });
    }
    update(root);
    return svg.node();
  }
  $.ajax({
    url: url,
    type: "GET",
    success: build_tree,
    error: function(jqXHR){
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        if(jqXHR.responseJSON.message) {
          writeMessages(jqXHR.responseJSON.message, 'error');
        }
      }
      else{
        writeMessages(
            'An error occur: the children data can not be loaded!',
            'error');
      }
    }
  });
}
