from django.db.models import Q

from metabaseEditor.models import *


def count_in_sub_project(project_id, function):
    submit_file_count = 0
    sub_projects = Project.objects.filter(
        parentProject__id=project_id).distinct()
    for sub_project in sub_projects:
        submit_file_count += function(sub_project.id)
    return submit_file_count


def count_submission_file(project_id):
    submit_file_count = Submission_file.objects.filter(
        tube_mix__project__id=project_id
    ).distinct().count()
    if submit_file_count > 0:
        return submit_file_count
    return count_in_sub_project(project_id, count_submission_file)


def count_tube(project_id):
    tube_count = Tube_mix.objects.filter(
        project__id=project_id
    ).distinct().count()
    if tube_count > 0:
        return tube_count
    return count_in_sub_project(project_id, count_tube)


def count_plate(project_id):
    plate_count = Plate_96.objects.filter(
        pcr__tube__project__id=project_id
    ).distinct().count()
    if plate_count > 0:
        return plate_count
    return count_in_sub_project(project_id, count_plate)


def count_blank(project_id):
    blank_count = Well.objects.filter(
        plate__pcr__tube__project__id=project_id,
        wtype='blank').distinct().count()
    if blank_count > 0:
        return blank_count
    return count_in_sub_project(project_id, count_blank)


def count_pcrNeg(project_id):
    pcrNeg_count = Well.objects.filter(
        plate__pcr__tube__project__id=project_id,
        wtype='PCRNeg').distinct().count()
    if pcrNeg_count > 0:
        return pcrNeg_count
    return count_in_sub_project(project_id, count_pcrNeg)


def get_used_extract(project_id, type):
    return Extract.objects.filter(
        well__plate__pcr__tube__project__id=project_id,
        etype=type
    ).distinct()


def count_all_extract(project_id):
    extract_count = Extract.objects.filter(
        Q(well__plate__pcr__tube__project__id=project_id) |
        Q(unused_extract__submission_file__tube_mix__project__id=project_id),
        etype="sample"
    ).distinct().count()
    if extract_count > 0:
        return extract_count
    return count_in_sub_project(project_id, count_all_extract)


def count_used_extract(project_id):
    extract_count = get_used_extract(project_id, "sample").count()
    if extract_count > 0:
        return extract_count
    return count_in_sub_project(project_id, count_used_extract)


def count_unused_extract(project_id):
    used_extract_id = get_used_extract(
        project_id, "sample").values_list('id',flat=True)
    extract_count = Extract.objects.filter(
        unused_extract__submission_file__tube_mix__project__id=project_id,
        etype="sample"
    ).exclude(
        id__in=used_extract_id).distinct().count()
    if extract_count > 0:
        return extract_count
    return count_in_sub_project(project_id, count_unused_extract)


def count_all_extNeg(project_id):
    extract_count = Extract.objects.filter(
        Q(well__plate__pcr__tube__project__id=project_id) |
        Q(unused_extract__submission_file__tube_mix__project__id=project_id),
        etype="extneg"
    ).distinct().count()
    if extract_count > 0:
        return extract_count
    return count_in_sub_project(project_id, count_all_extNeg)


def count_used_extNeg(project_id):
    extract_count = get_used_extract(project_id, "extneg").count()
    if extract_count > 0:
        return extract_count
    return count_in_sub_project(project_id, count_used_extNeg)


def count_unused_extNeg(project_id):
    used_extract_id = get_used_extract(
        project_id, "extneg").values_list('id',flat=True)
    extract_count = Extract.objects.filter(
        unused_extract__submission_file__tube_mix__project__id=project_id,
        etype="extneg"
    ).exclude(
        id__in=used_extract_id).distinct().count()
    if extract_count > 0:
        return extract_count
    return count_in_sub_project(project_id, count_unused_extNeg)


def count_all_pcrPos(project_id):
    extract_count = Extract.objects.filter(
        Q(well__plate__pcr__tube__project__id=project_id) |
        Q(unused_extract__submission_file__tube_mix__project__id=project_id),
        etype="PCRPos"
    ).distinct().count()
    if extract_count > 0:
        return extract_count
    return count_in_sub_project(project_id, count_all_pcrPos)


def count_used_pcrPos(project_id):
    extract_count = get_used_extract(project_id, "PCRPos").count()
    if extract_count > 0:
        return extract_count
    return count_in_sub_project(project_id, count_used_pcrPos)


def count_unused_pcrPos(project_id):
    used_extract_id = get_used_extract(
        project_id, "PCRPos").values_list('id',flat=True)
    extract_count = Extract.objects.filter(
        unused_extract__submission_file__tube_mix__project__id=project_id,
        etype="PCRPos"
    ).exclude(
        id__in=used_extract_id).distinct().count()
    if extract_count > 0:
        return extract_count
    return count_in_sub_project(project_id, count_unused_pcrPos)


def get_used_sample(project_id):
    return Sample.objects.filter(
        Q(extract__well__plate__pcr__tube__project__id=project_id) |
        Q(extract__unused_extract__submission_file__tube_mix__project__id=project_id)
    ).distinct()


def count_all_sample(project_id):
    sample_count = Sample.objects.filter(
        Q(id__in=get_used_sample(project_id).values_list('id', flat=True)) |
        Q(unused_sample__submission_file__tube_mix__project__id=project_id)
    ).distinct().count()
    if sample_count > 0:
        return sample_count
    return count_in_sub_project(project_id, count_all_sample)


def count_used_sample(project_id):
    sample_count = get_used_sample(project_id).count()
    if sample_count > 0:
        return sample_count
    return count_in_sub_project(project_id, count_used_sample)


def count_unused_sample(project_id):
    used_sample_id = get_used_sample(project_id).values_list('id',flat=True)
    sample_count = Sample.objects.filter(
        unused_sample__submission_file__tube_mix__project__id=project_id
    ).exclude(
        id__in=used_sample_id
    ).distinct().count()
    if sample_count > 0:
        return sample_count
    return count_in_sub_project(project_id, count_unused_sample)


def count_libraries(project_id):
    library_count = Library.objects.filter(
        tube__project__id=project_id
    ).distinct().count()
    if library_count > 0:
        return library_count
    return count_in_sub_project(project_id, count_libraries)


def count_sequencing_file(project_id):
    sequencing_file_count = Sequencing_file.objects.filter(
        library_name__tube__project__id=project_id
    ).distinct().count()
    if sequencing_file_count > 0:
        return sequencing_file_count
    return count_in_sub_project(project_id, count_sequencing_file)


def get_project_information(project_id):
    project = Project.objects.get(id=project_id)
    project_information = dict()
    # get project information
    project_information['name'] = project.name
    project_information['description'] = project.description
    project_information['available'] = project.available
    project_information['leader'] = project.leader
    project_information['email'] = project.email
    project_information['parentProject'] = project.parentProject.all()
    sub_project = list(Project.objects.filter(
        parentProject=project.id))
    project_information['subProject'] = sub_project
    project_information[
        'submission_file_count'] = count_submission_file(project_id)
    project_information['tube_count'] = count_tube(project_id)
    project_information['plate_count'] = count_plate(project_id)
    project_information['blank_count'] = count_blank(project_id)
    project_information['pcrNeg_count'] = count_pcrNeg(project_id)
    # get the count of used and unused extract
    project_information['extract_count'] = count_all_extract(project_id)
    project_information['used_extract_count'] = count_used_extract(project_id)
    project_information[
        'unused_extract_count'] = count_unused_extract(project_id)
    # get the count of used and unused negative pcr control
    project_information['extneg_count'] = count_all_extNeg(project_id)
    project_information['used_extneg_count'] = count_used_extNeg(project_id)
    project_information[
        'unused_extneg_count'] = count_unused_extNeg(project_id)
    # get the count of used and unused positive pcr control
    project_information['pcrPos_count'] = count_all_pcrPos(project_id)
    project_information['used_pcrPos_count'] = count_used_pcrPos(project_id)
    project_information[
        'unused_pcrPos_count'] = count_unused_pcrPos(project_id)
    # get the count of used and unused sample
    project_information['sample_count'] = count_all_sample(project_id)
    project_information['used_sample_count'] = count_used_sample(project_id)
    project_information[
        'unused_sample_count'] = count_unused_sample(project_id)
    # get the count of libraries
    project_information['library_count'] = count_libraries(project_id)
    # get the count of sequencing files
    project_information[
        'sequencing_file_count'] = count_sequencing_file(project_id)
    return project_information
