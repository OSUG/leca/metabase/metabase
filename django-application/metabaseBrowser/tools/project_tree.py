from metabaseEditor.models import PCR

from metabaseEditor.models import Project, Tube_mix

def populate_project_children(project_id):
    project_tubes = Tube_mix.objects.filter(
        project__id=project_id
    ).distinct().order_by("name")
    if project_tubes.exists():
        return populate_tube_nodes(list(project_tubes))
    return populate_sub_project_nodes(project_id)


def populate_sub_project_nodes(project_id):
    children = list()
    sub_projects = Project.objects.filter(
        parentProject__id=project_id
    ).distinct()
    for sub_project in sub_projects:
        project_tubes = Tube_mix.objects.filter(
            project__id=sub_project.id
        ).distinct().count()
        sub_sub_projects = Project.objects.filter(
            parentProject__id=sub_project.id
        ).distinct().count()
        sub_object = project_tubes + sub_sub_projects
        get_children_url = ("/browser/tree-view/project/"
                            + str(sub_project.id) + "/get-children")
        sub_project_node = {"name": sub_project.name
                            + " (" + str(sub_object) +")",
                            "type": 'sub-project',
                            "url": get_children_url if sub_object > 0 else None
        }
        children.append(sub_project_node)
    return children


def populate_tube_nodes(project_tubes):
    children = list()
    for tube in project_tubes:
        metadata = list()
        if tube.submission_file:
            metadata.append(
                {"Submission_file": tube.submission_file.file_name})
        if tube.concentration > 0:
            metadata.append({"Concentration": str(tube.concentration) + " µl"})
        if tube.quantity > 0:
            metadata.append({"Quantity": str(tube.quantity) + " ng/µl"})
        tube_node = {
            "name": tube.name,
            "type": 'tube',
            "url": "/browser/tree-view/tube/"
            + str(tube.id) + "/get-children"
        }
        if len(metadata) > 0:
            tube_node['metadata'] = metadata
        children.append(tube_node)
    return children


def populate_plate_nodes(project_plates):
    plates = list()
    for plate in project_plates:
        if PCR.objects.filter(plate__id=plate.id).count() == 1:
            pcr_info = PCR.objects.get(plate__id=plate.id)
            metadata = [
                {"Run id": pcr_info.run},
                {"Run position": pcr_info.run_position},
                {"Machine id": pcr_info.machine_id},
                {"Cycles": pcr_info.cycles},
                {"Denaturation time": str(pcr_info.denaturation_time) + " s"},
                {"Hybridation time": str(pcr_info.hybridation_time) + " s"},
                {"Synthesis time": str(pcr_info.synthesis_time) + " s"},
                {"Final synthesis time":
                 str(pcr_info.final_synthesis_time) + " min"},
                {"Denaturation temperature":
                 str(pcr_info.denaturation_temperature) + "°C"},
                {"Hybridation temperature":
                 str(pcr_info.hybridation_temperature) + "°C"},
                {"Synthesis temperature":
                 str(pcr_info.synthesis_temperature) + "°C"},
                {"Final synthesis temperature":
                 str(pcr_info.final_synthesis_temperature) + "°C"},
            ]
        else:
            metadata = [
                {"Run id": list()},
                {"Run position": list()},
                {"Machine id": list()},
                {"Cycles": list()},
                {"Denaturation time": list()},
                {"Hybridation time": list()},
                {"Synthesis time": list()},
                {"Final synthesis time": list()},
                {"Denaturation temperature": list()},
                {"Hybridation temperature": list()},
                {"Synthesis temperature": list()},
                {"Final synthesis temperature": list()},
            ]
            for pcr_info in PCR.objects.filter(plate__id=plate.id).order_by("run"):
                metadata[0]["Run id"].append(pcr_info.run)
                metadata[1]["Run position"].append(pcr_info.run_position)
                metadata[2]["Machine id"].append(pcr_info.machine_id)
                metadata[3]["Cycles"].append(pcr_info.cycles)
                metadata[4]["Denaturation time"].append(
                        str(pcr_info.denaturation_time) + " s")
                metadata[5]["Hybridation time"].append(
                        str(pcr_info.hybridation_time) + " s")
                metadata[6]["Synthesis time"].append(
                        str(pcr_info.synthesis_time) + " s")
                metadata[7]["Final synthesis time"].append(
                        str(pcr_info.final_synthesis_time) + " min")
                metadata[8]["Denaturation temperature"].append(
                        str(pcr_info.denaturation_temperature) + "°C")
                metadata[9]["Hybridation temperature"].append(
                        str(pcr_info.hybridation_temperature) + "°C")
                metadata[10]["Synthesis temperature"].append(
                        str(pcr_info.synthesis_temperature) + "°C")
                metadata[11]["Final synthesis temperature"].append(
                        str(pcr_info.final_synthesis_temperature) + "°C")
        plates.append(
        {
            "id": plate.id,
            "name": plate.name,
            "type": 'plate',
            "url": "/browser/plate/"
            + str(plate.id) + "/init-plate",
            "metadata": metadata
         })
    return plates
