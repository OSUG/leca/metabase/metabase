from django.db.models import Q, Prefetch
from django.core.exceptions import MultipleObjectsReturned

from metabaseEditor.models import Sample, Sample_qualifier, Project

from .project_information import get_used_sample


def get_sample_project(sample_id):
    project = Project.objects.filter(
        Q(tube_mix__pcr__plate__well__extract__sample__id=sample_id) |
        Q(tube_mix__submission_file__unused_sample__sample__id=sample_id) |
        Q(tube_mix__submission_file__unused_extract__extract__sample__id=sample_id)
    ).distinct()
    if project.count() > 1:
        raise MultipleObjectsReturned('Too many projects are found!')
    return project.first()


def get_in_sub_project(project_id, function):
    sample_list = Sample.objects.none()
    sub_projects = Project.objects.filter(
        parentProject__id=project_id).distinct()
    for sub_project in sub_projects:
        sample_list = (sample_list | function(sub_project.id))
    return sample_list


def get_samples_list(project_id):
    sample_list = Sample.objects.filter(
        Q(id__in=get_used_sample(project_id).values_list('id', flat=True)) |
        Q(unused_sample__submission_file__tube_mix__project__id=project_id)
    ).distinct()
    if sample_list.count() > 0:
        return sample_list
    return get_in_sub_project(project_id, get_samples_list)


def is_parent_project(project_id):
    return Project.objects.filter(parentProject__id=project_id).exists()


def get_qualifier_name(project_id):
    project_sample = get_samples_list(project_id).values_list('id', flat=True)
    qualifiers = list(Sample_qualifier.objects.filter(
        sample__id__in=project_sample
    ).distinct().order_by('key').values_list('key', flat=True))
    if is_parent_project(project_id):
        qualifiers.insert(0,'Project')
    return qualifiers


def format_qualifiers_to_columns(project_id, qualifiers):
    columns = [None, {'data': 'name', 'defaultContent': 'NA'}]
    for qualifier in qualifiers:
        columns.append({'data': qualifier, 'defaultContent': 'NA'})
    return columns


def get_sub_project_list(project_id, filter=None):
    sub_projects_list = list()
    full_filter = Q(parentProject__id=project_id)
    if filter is not None:
        full_filter &= Q(name__icontains=filter)
    sub_projects = Project.objects.filter(full_filter)
    for sub_project in sub_projects:
        if is_parent_project(sub_project.id):
            sub_projects_list.extend(get_sub_project_list(sub_project.id))
        else:
            sub_projects_list.append(sub_project.name)
    return sub_projects_list
