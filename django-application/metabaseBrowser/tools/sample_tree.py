from metabaseEditor.models import Plate_96, Extract_qualifier


def populate_sample_node(sample_extracts):
    extracts = list()
    for extract in sample_extracts:
        children_count = Plate_96.objects.filter(
            well__extract__id=extract.id).distinct().count()
        qualifiers = list(Extract_qualifier.objects.filter(
            extract__id=extract.id).distinct())
        metadata = list()
        for qualifier in qualifiers:
            metadata.append({qualifier.key: qualifier.value})
        extracts.append(
        {
            "name": extract.name + " (" + str(children_count) + ")",
            "type": 'extract',
            "url": "/browser/tree-view/extract/"
            + str(extract.id) + "/get-children",
            "metadata": metadata
         })
    return extracts
