from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.contrib import messages
from django.db.models import Q
from django.contrib.auth.decorators import login_required

from metabaseEditor.models import Project
from ..tools.project_information import get_project_information
from ..tools.decorator import check_user_permission, check_user_login_js


@require_http_methods(["GET"])
@login_required
def browser(request):
  return render(request, 'browser.html')


@require_http_methods(["POST"])
@check_user_login_js
def get_project_list(request):
    project_list = list()
    try:
        projects = list(Project.objects.filter(
                Q(available=True) | Q(
                    available=False,
                    users__username=request.user.username
                )).distinct())
        for project in projects:
            pjt = dict()
            pjt['id'] = project.id
            pjt['name'] = project.name
            pjt['description'] = project.description
            pjt['available'] = project.available
            pjt['leader'] = project.leader
            parent_project = list(project.parentProject.all().values_list(
                'name', flat=True))
            pjt['parent_project'] = ' ; '.join(parent_project)
            sub_project = list(Project.objects.filter(
                parentProject=project.id).values_list('name', flat=True))
            pjt['sub-project'] = ' ; '.join(sub_project)
            project_list.append(pjt)
        return JsonResponse({'data': project_list},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An error occur: the project list can not be loaded!'
               + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return JsonResponse({'message': msg}, status=500)


@require_http_methods(["GET"])
@login_required
@check_user_permission
def browse_project(request, project_id=None):
    if not project_id:
        return redirect('metabaseBrowser:browser')
    try:
        project_information = get_project_information(project_id)
        context = {
            "project": Project.objects.get(id=project_id),
            "project_information": project_information
        }
        return render(request, 'project_browser.html', context)
    except Exception as e:
        error_name = type(e).__name__
        messages.error(request, 'An error occur: the project can not be load!'
                       + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return redirect('metabaseBrowser:browser')
