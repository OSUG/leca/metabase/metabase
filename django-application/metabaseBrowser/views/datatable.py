from django.http import JsonResponse
from django.db.models import Q, Case, When, Count
from django.views import View

from metabaseEditor.models import Project, Sample

from ..tools.sample_list import *


class AsyncDataTableView(View):

    entities = None
    values = []

    PARAM_LENGTH = 'length'
    PARAM_START = 'start'
    PARAM_SORT_COL_IDX = 'order[0][column]'
    PARAM_SORT_COL_NAME = 'columns[{}][data]'
    PARAM_SORT_DIR = 'order[0][dir]'
    PARAM_SEARCH = 'search[value]'

    DEFAULT_LENGTH = 10
    DEFAULT_START = 10
    DEFAULT_SORT_COL_IDX = 0
    DEFAULT_SORT_DIR = 'asc'
    DEFAULT_START = 10

    def post(self, request, *args, **kwargs):

        length = int(request.POST.get(AsyncDataTableView.PARAM_LENGTH,
                                      AsyncDataTableView.DEFAULT_LENGTH))
        start = int(request.POST.get(AsyncDataTableView.PARAM_START,
                                     AsyncDataTableView.DEFAULT_START))
        sort_col_idx = int(request.POST.get(AsyncDataTableView.PARAM_SORT_COL_IDX,
                                            AsyncDataTableView.DEFAULT_SORT_COL_IDX))
        sort_dir = request.POST.get(AsyncDataTableView.PARAM_SORT_DIR,
                                    AsyncDataTableView.DEFAULT_SORT_DIR)

        order_dir = ''
        reverse = False
        if sort_dir != AsyncDataTableView.DEFAULT_SORT_DIR:
            order_dir = '-'
            reverse = True
        sorted_col = request.POST.get(AsyncDataTableView.PARAM_SORT_COL_NAME.format(sort_col_idx),
                                     AsyncDataTableView.PARAM_SORT_COL_NAME.format(AsyncDataTableView.DEFAULT_SORT_COL_IDX))
        order_by = [order_dir + sorted_col]
        if sorted_col == 'Project':
            sub_projects = get_sub_project_list(kwargs['project_id'])
            sample_order = list()
            for sub_project in sorted(sub_projects, reverse=reverse):
                sample_order.extend(Sample.objects.filter(
                    Q(extract__well__plate__pcr__tube__project__name=sub_project) |
                    Q(unused_sample__submission_file__tube_mix__project__name=sub_project) |
                    Q(extract__unused_extract__submission_file__tube_mix__project__name=sub_project)
                ).distinct().values_list('id', flat=True))
            order_by = [Case(*[
                            When(id=id,
                                 then=pos) for pos, id in enumerate(sample_order)
                            ])
                        ]
        elif sorted_col != 'name':
            sort_sample = self.entities.filter(sample_qualifiers__key=sorted_col).order_by(order_dir + "sample_qualifiers__value").values_list('id', flat=True)
            self.entities = self.entities.annotate(
                sorted_count=Count('sample_qualifiers__key',
                                   filter=Q(sample_qualifiers__key=sorted_col),
                                   distinct=True))
            order_by = [order_dir + 'sorted_count',
                        Case(*[
                            When(id=id,
                                 then=pos) for pos, id in enumerate(sort_sample)
                            ])
                        ]

        records_total = self.entities.count()
        records_filtered = records_total

        #############
        # Filtering #
        #############
        q = request.POST.get(AsyncDataTableView.PARAM_SEARCH)

        if q != None and q != '':
            filter = (Q(sample_qualifiers__value__icontains=q) |
                      Q(name__icontains=q))
            if is_parent_project(kwargs['project_id']):
                sub_project_list = get_sub_project_list(kwargs['project_id'], q)
                filtered_sample = list(Sample.objects.filter(
                    Q(extract__well__plate__pcr__tube__project__name__in=sub_project_list) |
                    Q(unused_sample__submission_file__tube_mix__project__name__in=sub_project_list) |
                    Q(extract__unused_extract__submission_file__tube_mix__project__name__in=sub_project_list)
                ).distinct().values_list('id', flat=True))
                filter |= Q(id__in=filtered_sample)

            self.entities = self.entities.filter(filter)
            records_filtered = self.entities.count()

        samples = list(self.entities.order_by(*order_by)[start:start+length])
        self.entities = list()
        for sample in samples:
            project = get_sample_project(sample.id)
            qualifiers = { 'id': sample.id,
                          'name': sample.name,
                          'Project': project.name }
            for sq in sample.sample_qualifiers.all():
                qualifiers[sq.key] = sq.value
            self.entities.append(qualifiers)

        result = {
            "recordsTotal": records_total,
            "recordsFiltered": records_filtered,
            "data": self.entities
        }

        return JsonResponse(result, safe=False)
