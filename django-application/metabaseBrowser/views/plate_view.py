from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.forms.models import model_to_dict

from metabaseEditor.models import Plate_96, Well, Extract_qualifier
from ..tools.decorator import check_user_permission_js, check_user_login_js


@require_http_methods(["GET"])
@check_user_login_js
@check_user_permission_js
def init_plate_view(request, plate_id):
    try:
        plate = get_object_or_404(Plate_96, id=plate_id)
        wells_list = list()
        rows = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
        columns = range(1, 13)
        wells = list(Well.objects.filter(
            plate__id=plate_id).distinct().order_by(
                'row', 'column').select_related('extract', 'primer',
                                                'extract__sample'))
        for well in wells:
            well_obj = model_to_dict(well)
            well_obj['row'] = rows.index(well.row)
            well_obj['column'] = columns.index(well.column)
            if well.wtype == 'extract':
                extract = {
                    'name': well.extract.name,
                }
                if well.extract.etype == 'sample':
                    extract['etype'] = 'extract'
                    sample = {
                        'name': well.extract.sample.name,
                        'id': well.extract.sample.id
                    }
                    extract['sample'] = sample
                else:
                    extract['etype'] = well.extract.etype
                if well.extract.etype == 'PCRPos':
                    qualifiers = Extract_qualifier.objects.filter(
                        extract__id=well.extract.id).distinct()
                    pcrpos = dict()
                    for qualifier in qualifiers:
                        pcrpos[qualifier.key] = qualifier.value
                    extract['pcrpos'] = pcrpos
                well_obj['extract'] = extract
            del well_obj['primer']
            well_obj['forward_primer'] = well.primer.forward_sequence
            well_obj['reverse_primer'] = well.primer.reverse_sequence
            wells_list.append(well_obj)
        return JsonResponse({'data': wells_list, 'id': plate.id,
                             'rows': rows, 'columns': list(columns)
                             }, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An error occur: the plate view can not be loaded!'
               + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return JsonResponse({'message': msg}, status=500)
