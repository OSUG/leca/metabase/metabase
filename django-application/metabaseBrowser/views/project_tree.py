from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.db.models import Q

from metabaseEditor.models import (Project, Tube_mix, Plate_96,
                                   Library, Sequencing_file)

from ..tools.project_tree import *
from ..tools.decorator import check_user_permission_js, check_user_login_js


@require_http_methods(["GET"])
@check_user_login_js
@check_user_permission_js
def init_project_tree_view(request, project_id):
    try:
        project = Project.objects.get(id=project_id)
        tubes = list()
        get_children_url = ("/browser/tree-view/project/"
                            + str(project.id) + "/get-children")
        children = populate_project_children(project_id)
        tree = {"name": project.name,
                "type": 'project',
                "url": get_children_url if len(children) > 0 else None,
                "children": children}
        return JsonResponse(tree, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An error occur: the project tree view can not be loaded!'
               + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return JsonResponse({'message': msg}, status=500)


@require_http_methods(["GET"])
@check_user_login_js
@check_user_permission_js
def get_project_children(request, project_id):
    try:
        project = Project.objects.get(id=project_id)
        children = populate_project_children(project_id)
        return JsonResponse({'data': children, 'name': project.name},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An error occur: '
            + 'the project children in the project tree view can not be loaded!'
            + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return JsonResponse({'message': msg}, status=500)


@require_http_methods(["GET"])
@check_user_login_js
@check_user_permission_js
def get_tubes_children(request, tube_id):
    try:
        tube = Tube_mix.objects.get(id=tube_id)
        libraries_count = Library.objects.filter(
            tube__id=tube_id).distinct().count()
        plates_count = Plate_96.objects.filter(
            pcr__tube__id=tube_id).distinct().count()
        children = list()
        if libraries_count > 0:
            children.append({
                "name": "libraries (" + str(libraries_count) +")",
                "type": 'libraries',
                "url": "/browser/tree-view/tube/"
                + str(tube.id) + "/get-libraries"
            })
        if plates_count > 0:
            children.append({
                "name": "plates (" + str(plates_count) +")",
                "type": 'plates',
                "url": "/browser/tree-view/tube/"
                + str(tube.id) + "/get-plates"
            })
        return JsonResponse({"data": children, 'name': tube.name},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An error occur: '
            + 'the tube children in the project tree view can not be loaded!'
            + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return JsonResponse({'message': msg}, status=500)


@require_http_methods(["GET"])
@check_user_login_js
@check_user_permission_js
def get_libraries(request, tube_id):
    try:
        libraries = list()
        project_libraries = list(Library.objects.filter(
            tube__id=tube_id
        ).distinct())
        for library in project_libraries:
            library_children_count = Sequencing_file.objects.filter(
                library_name__name=library.name
            ).distinct().count()
            libraries.append(
                {
                    "name": library.name + " ("+ str(library_children_count) +")",
                    "type": 'library',
                    "url": "/browser/tree-view/library/"
                    + str(library.name) + "/get-children"
                 })
        return JsonResponse({'data': libraries, 'name': 'libraries'},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An error occur: '
            + 'the libraries in the project tree view can not be loaded!'
            + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return JsonResponse({'message': msg}, status=500)


@require_http_methods(["GET"])
@check_user_login_js
@check_user_permission_js
def get_library_children(request, library_id):
    try:
        sequencing_files = list()
        library_sequencing_file = list(Sequencing_file.objects.filter(
            library_name__name=library_id
        ).distinct())
        for sequencing_file in library_sequencing_file:
            sequencing_files.append(
                {
                    "name": sequencing_file.file_name + "."
                    + sequencing_file.file_type,
                    "type": 'sequencing_file',
                    "metadata":[
                        {"Sequencing technology":
                            sequencing_file.sequencingTechnology},
                        {"Sequencing instrument":
                            sequencing_file.sequencingInstrument},
                        {"Xml file": sequencing_file.information.file_name}
                    ]
                 })
        return JsonResponse({'data': sequencing_files, 'name': library_id},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An error occur: '
            + 'the library children in the project tree view can not be loaded!'
            + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return JsonResponse({'message': msg}, status=500)


@require_http_methods(["GET"])
@check_user_login_js
@check_user_permission_js
def get_plates(request, tube_id):
    try:
        project_plates = list(Plate_96.objects.filter(
            pcr__tube__id=tube_id
        ).distinct())
        plates = populate_plate_nodes(project_plates)
        return JsonResponse({'data': plates, 'name': 'plates'},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An error occur: '
            + 'the plates in the project tree view can not be loaded!'
            + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return JsonResponse({'message': msg}, status=500)
