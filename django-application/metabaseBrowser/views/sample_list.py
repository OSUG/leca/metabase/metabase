from django.template.loader import render_to_string
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.utils.decorators import method_decorator

from ..tools.sample_list import *
from ..tools.decorator import check_user_permission_js, check_user_login_js

from .datatable import AsyncDataTableView

@require_http_methods(["POST"])
@check_user_login_js
@check_user_permission_js
def get_qualifiers(request, project_id):
    try:
        qualifiers = get_qualifier_name(project_id)
        columns = format_qualifiers_to_columns(project_id, qualifiers)
        html = render_to_string( 'parts/samples_list.html',
                                { "qualifiers": qualifiers },
                                request=request)
        return JsonResponse({'html': html, 'column': columns})
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An error occur: the sample list can not be loaded!'
               + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return JsonResponse({'message': msg}, status=500)


@method_decorator([check_user_login_js, check_user_permission_js], name="dispatch")
class SampleListView(AsyncDataTableView):
    def dispatch(self, *args, **kwargs):
        try:
            self.entities = get_samples_list(
                kwargs['project_id']).prefetch_related(
                    Prefetch('sample_qualifiers',
                             queryset=Sample_qualifier.objects.order_by('key'))
                )
            return super().dispatch(*args, **kwargs)
        except Exception as e:
            error_name = type(e).__name__
            msg = ('An error occur: the sample list can not be loaded!'
                   + ' (Error: ' + error_name + ': ' + e.args[0] +')')
            return JsonResponse({'message': msg}, status=500)
