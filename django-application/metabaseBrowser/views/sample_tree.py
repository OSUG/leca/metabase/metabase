from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.forms.models import model_to_dict

from metabaseEditor.models import Sample, Plate_96, Extract

from ..tools.sample_tree import *
from ..tools.project_tree import populate_plate_nodes
from ..tools.decorator import check_user_permission_js, check_user_login_js

@require_http_methods(["POST"])
@check_user_login_js
@check_user_permission_js
def add_sample_tree_view(request, sample_id):
    try:
        sample = Sample.objects.get(id=sample_id)
        return JsonResponse({'sample': model_to_dict(sample)},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An error occur: the sample tree view can not be loaded!'
               + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return JsonResponse({'message': msg}, status=500)


@require_http_methods(["GET"])
@check_user_login_js
@check_user_permission_js
def init_sample_tree_view(request, sample_id):
    try:
        sample = Sample.objects.get(id=sample_id)
        get_children_url = ("/browser/tree-view/sample/"
                            + str(sample.id) + "/get-children")
        sample_extracts = Extract.objects.filter(
            sample__id=sample_id
        ).distinct().order_by("name")
        extracts = populate_sample_node(list(sample_extracts))
        tree = {
            "name": sample.name,
            "type": 'sample',
            "url": get_children_url if sample_extracts.count() > 0 else None,
            "children": extracts
        }
        return JsonResponse(tree, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An error occur: the sample tree view can not be loaded!'
               + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return JsonResponse({'message': msg}, status=500)


@require_http_methods(["GET"])
@check_user_login_js
@check_user_permission_js
def get_sample_children(request, sample_id):
    try:
        sample = Sample.objects.get(id=sample_id)
        extracts = list()
        sample_extracts = Extract.objects.filter(
            sample__id=sample_id
        ).distinct().order_by("name")
        extracts = populate_sample_node(list(sample_extracts))
        return JsonResponse({'data': extracts, 'name': sample.name},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An error occur: '
            + 'the sample children in the sample tree view can not be loaded!'
            + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return JsonResponse({'message': msg}, status=500)


@require_http_methods(["GET"])
@check_user_login_js
@check_user_permission_js
def get_extract_children(request, extract_id):
    try:
        extract = Extract.objects.get(id=extract_id)
        extract_plates = list(Plate_96.objects.filter(
            well__extract__id=extract_id).distinct().order_by("name"))
        plates = populate_plate_nodes(extract_plates)
        return JsonResponse({"data": plates, 'name': extract.name},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An error occur: '
            + 'the extract children in the sample tree view can not be loaded!'
            + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        return JsonResponse({'message': msg}, status=500)
