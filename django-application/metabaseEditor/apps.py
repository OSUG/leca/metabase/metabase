from django.apps import AppConfig


class metabaseEditorConfig(AppConfig):
    name = 'metabaseEditor'
