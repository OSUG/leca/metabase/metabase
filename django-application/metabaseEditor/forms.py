from django.forms import ModelForm
from django import forms

from django.db.models import Count, Q

from django.contrib.auth.models import User
from .models import Project, Tube_mix, Sequencing_information


class ProjectForm(ModelForm):

    class Meta:
        model = Project
        fields = ['name', 'description', 'email', 'leader', 'parentProject',
                  'users', 'available']
        exclude = ['id']

    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        self.fields["users"].widget = forms.CheckboxSelectMultiple()
        self.fields["parentProject"].widget = forms.CheckboxSelectMultiple()
        if self.data:
            self.data = self.data.copy()
            all_users = self.data.getlist('users')
            for superuser in User.objects.filter(is_superuser=True):
                all_users.append(str(superuser.id))
            self.data.setlist('users', all_users)
        else:
            users = User.objects.filter(is_superuser=False)
            self.fields['users'].queryset = users

        if self.instance.id is not None:
            if Project.objects.filter(parentProject__id=self.instance.id).exists():
                self.fields['parentProject'].queryset = Project.objects.none()
            else:
                parent_project = Project.objects.exclude(
                    Q(id=self.instance.id) |
                    Q(parentProject__isnull=False) |
                    Q(tube_mix__isnull=False)).distinct()
                self.fields['parentProject'].queryset = parent_project
        else:
            parent_project = Project.objects.exclude(
                Q(tube_mix__isnull=False) |
                Q(parentProject__isnull=False)).distinct()
            self.fields['parentProject'].queryset = parent_project


class ExperimentForm(forms.Form):
    excelFilePaths = forms.FileField(
        widget=forms.ClearableFileInput(attrs={'multiple': True}))


class SequencingForm(forms.Form):
    sequencingFile = forms.FileField(
        widget=forms.ClearableFileInput(attrs={'multiple': True}))


class UpdateForm(forms.Form):
    project = forms.ModelChoiceField(Project.objects.none())

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(UpdateForm, self).__init__(*args, **kwargs)

        if user:
            filtered_project = Project.objects.filter(
                users__id=user.id).order_by('name')
            self.fields['project'].queryset = filtered_project


class selectSequencingForm(forms.Form):
    tube_count = Count('sequencing_file__library_name__tube', filter=Q(
        sequencing_file__library_name__tube__project__isnull=True),
        distinct=True)
    sequencingInfoFile = forms.ModelChoiceField(
        Sequencing_information.objects.annotate(
            tube_count=tube_count
            ).exclude(tube_count=0).order_by('file_name'))

    def __init__(self, *args, **kwargs):
        super(selectSequencingForm, self).__init__(*args, **kwargs)

        tube_count = Count('sequencing_file__library_name__tube', filter=Q(
            sequencing_file__library_name__tube__project__isnull=True),
            distinct=True)
        selected_files = Sequencing_information.objects.annotate(
            tube_count=tube_count
        ).exclude(tube_count=0).order_by('file_name')
        self.fields['sequencingInfoFile'].queryset = selected_files
