from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q, Count

from metabaseEditor.models import *
from metabaseEditor.tools.imported_data import *
from metabaseEditor.tools.deleted_data import *
from metabaseEditor.tools.submission_file import *

class Command(BaseCommand):
    help = 'Check if data has to be deleted from the database'

    def handle(self, *args, **options):
        try:
            submit_files = list()
            imported_file = get_ID_submission_files()
            if imported_file is not None:
                submit_files.extend(imported_file)

            imported_data_count = Imported_data_tmp.objects.all().count()
            print("Number of object in imported_data_count : "
                  + str(imported_data_count))

            deleted_file = get_delete_ID_submission_files()
            if deleted_file is not None:
                submit_files.extend(deleted_file)

            delete_data = list(Deleted_data_tmp.objects.filter(
                    type="submission_file").values_list("uniq_id", flat=True))
            submit_files.extend(
                    list(Submission_file.objects.filter(
                            tube_mix__isnull=True).exclude(id__in=delete_data)))

            print("Number of submission files that need to be deleted: "
                  + str(len(submit_files)))

            # deleted data table
            deleted_samples = list(Deleted_data_tmp.objects.filter(
                       type="samples").values_list("uniq_id", flat=True))
            delete_sample_count = Sample.objects.filter(
                    id__in=deleted_samples).count()
            print("Number of deleted samples that need to be remove: "
                  + str(delete_sample_count))

            deleted_controls = list(Deleted_data_tmp.objects.filter(
                   type="controls").values_list("uniq_id", flat=True))
            delete_ctl_count = Extract.objects.filter(
                    id__in=deleted_controls).count()
            print("Number of deleted controls that need to be remove: "
                  + str(delete_ctl_count))

            deleted_plates = list(Deleted_data_tmp.objects.filter(
                   type="plates").values_list("uniq_id", flat=True))
            delete_plate_count = Plate_96.objects.filter(
                    id__in=deleted_plates).count()
            print("Number of deleted plates that need to be remove: "
                  + str(delete_plate_count))

            deleted_primers = list(Deleted_data_tmp.objects.filter(
                   type="primers").values_list("uniq_id", flat=True))
            delete_primer_count = PrimerPair.objects.filter(
                    id__in=deleted_primers).count()
            print("Number of deleted primers that need to be remove: "
                  + str(delete_primer_count))

            deleted_tubes = list(Deleted_data_tmp.objects.filter(
                   type="tubes").values_list("uniq_id", flat=True))
            delete_tube_count = Tube_mix.objects.filter(
                    id__in=deleted_tubes).count()
            print("Number of deleted tube that need to be remove: "
                  + str(delete_tube_count))

            # Clean db table
            tube_count = Tube_mix.objects.annotate(
                            library_count=Count("libraries")
                            ).filter(library_count=0,
                                     submission_file__isnull=True
                                     ).exclude(id__in=deleted_tubes).count()
            print("Number of tube that need to be deleted: "
                  + str(tube_count))

            primer_count = PrimerPair.objects.annotate(
                    well_count=Count("well")).filter(
                            well_count=0
                            ).exclude(id__in=deleted_primers).count()
            print("Number of primer pairs that need to be deleted: "
                  + str(primer_count))

            pcr_count = PCR.objects.annotate(tube_count=Count("tube")
                                             ).filter(Q(plate__isnull=True) |
                                                      Q(tube_count=0)).count()
            print("Number of pcr information that need to be deleted: "
                  + str(pcr_count))

            plate_count = Plate_96.objects.annotate(
                    well_count=Count("well"),
                    pcr_count=Count("pcr")).filter(
                            Q(well_count=0) |
                            Q(pcr_count=0)
                            ).exclude(id__in=deleted_plates).count()
            print("Number of pcr plate that need to be deleted: "
                  + str(plate_count))

            well_count = Well.objects.filter(Q(plate__isnull=True) |
                                             Q(primer__isnull=True) |
                                             Q(plate__in=deleted_plates)).count()
            print("Number of well that need to be deleted: " + str(well_count))

            ue_count = Unused_extract.objects.filter(
                    Q(submission_file__isnull=True) |
                    Q(extract__isnull=True)).count()
            print("Number of unused extract that need to be deleted: "
                  + str(ue_count))

            extract_count = Extract.objects.annotate(
                            well_count=Count("well"),
                            unused_extract_count=Count("unused_extract")
                            ).filter(
                                    well_count=0,
                                    unused_extract_count=0
                                    ).exclude(id__in=deleted_controls).count()
            print("Number of extract that need to be deleted: "
                  + str(extract_count))

            eq_count = Extract_qualifier.objects.filter(
                    extract__isnull=True).count()
            print("Number of extract qualifier that need to be deleted: "
                  + str(eq_count))

            us_count = Unused_sample.objects.filter(
                    Q(submission_file__isnull=True) |
                    Q(sample__isnull=True)).count()
            print("Number of unused sample that need to be deleted: "
                  + str(us_count))

            sample_count = Sample.objects.annotate(
                    extract_count=Count("extract"),
                    unused_sample_count=Count("unused_sample")
                    ).filter(
                            extract_count=0,
                            unused_sample_count=0
                            ).exclude(id__in=deleted_samples).count()
            print("Number of sample that need to be deleted: "
                  + str(sample_count))

            sq_count = Sample_qualifier.objects.filter(
                    sample__isnull=True).count()
            print("Number of sample qualifier that need to be deleted: "
                  + str(sq_count))

            sf_count = Sequencing_file.objects.filter(
                    Q(library_name__isnull=True) |
                    Q(information__isnull=True)).count()
            print("Number of sequencing file that need to be deleted: "
                  + str(sf_count))

            library_count = Library.objects.annotate(
                    sequencing_file_count=Count("sequencing_file")
                    ).filter(sequencing_file_count=0).count()
            print("Number of library that need to be deleted: "
                  + str(library_count))

            si_count = Sequencing_information.objects.annotate(
                    sequencing_file_count=Count("sequencing_file")).filter(
                            sequencing_file_count=0).count()
            print("Number of sequencing information that need to be deleted: "
                  + str(si_count))

        except Exception as e:
            error_name = type(e).__name__
            msg = ("An error occur on the checking database integrity command! "
                   + error_name + ": "
                   + e.args[0])
            raise CommandError(msg)
