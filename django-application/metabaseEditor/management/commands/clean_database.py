from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q, Count

from metabaseEditor.models import *
from metabaseEditor.tools.imported_data import *
from metabaseEditor.tools.deleted_data import *
from metabaseEditor.tools.submission_file import *

class Command(BaseCommand):
    help = 'Clean the database, remove bad imported data and unused data'

    def handle(self, *args, **options):
        try:
            imported_file = get_ID_submission_files()
            if imported_file is not None:
                for imf in imported_file:
                    remove_submission_file(imf.value)
                    imf.delete()

            Imported_data_tmp.objects.all().delete()

            deleted_sample = Deleted_data_tmp.objects.filter(
                    type="samples").values_list("uniq_id", flat=True)
            Sample.objects.filter(id__in=list(deleted_sample)).delete()

            deleted_controls = Deleted_data_tmp.objects.filter(
                    type="controls").values_list("uniq_id", flat=True)
            Extract.objects.filter(id__in=list(deleted_controls)).delete()

            deleted_plates = Deleted_data_tmp.objects.filter(
                    type="plates").values_list("uniq_id", flat=True)
            Plate_96.objects.filter(id__in=list(deleted_plates)).delete()

            deleted_primers = Deleted_data_tmp.objects.filter(
                    type="primers").values_list("uniq_id", flat=True)
            PrimerPair.objects.filter(id__in=list(deleted_primers)).delete()

            deleted_tube = list(Deleted_data_tmp.objects.filter(
                    type="tubes").values_list("uniq_id", flat=True))
            Tube_mix.objects.filter(id__in=deleted_tube).update(
                    submission_file=None, concentration=0, quantity=0,
                    mtype='None')

            deleted_project_tube = list(Deleted_data_tmp.objects.filter(
                            type="tubes",
                            parent__parent__type="project"
                            ).values_list("uniq_id", flat=True))
            Tube_mix.objects.filter(
                            id__in=deleted_project_tube).update(project=None)

            deleted_file = get_delete_ID_submission_files()
            if deleted_file is not None:
                for df in deleted_file:
                    remove_submission_file(df.uniq_id)
                    df.delete()

            Submission_file.objects.filter(tube_mix__isnull=True).delete()

            deleted_project = list(Deleted_data_tmp.objects.filter(
                    type="project").values_list("uniq_id", flat=True))
            Project.objects.filter(id__in=deleted_project).delete()
            remove_deleted_data("project", deleted_project)

            Tube_mix.objects.annotate(
                    library_count=Count("libraries")
                    ).filter(library_count=0,
                             submission_file__isnull=True).delete()

            PrimerPair.objects.annotate(
                    well_count=Count("well")).filter(well_count=0).delete()

            PCR.objects.annotate(
                    tube_count=Count("tube")).filter(Q(plate__isnull=True) |
                                                     Q(tube_count=0)).delete()

            Plate_96.objects.annotate(
                    well_count=Count("well"),
                    pcr_count=Count("pcr")).filter(Q(well_count=0)|
                                                   Q(pcr_count=0)).delete()

            Well.objects.filter(Q(plate__isnull=True) |
                                Q(primer__isnull=True)).delete()

            Unused_extract.objects.filter(Q(submission_file__isnull=True) |
                                          Q(extract__isnull=True)).delete()

            Extract.objects.annotate(
                            well_count=Count("well"),
                            unused_extract_count=Count("unused_extract")
                            ).filter(well_count=0,
                                     unused_extract_count=0).delete()

            Extract_qualifier.objects.filter(extract__isnull=True).delete()
            Unused_sample.objects.filter(Q(submission_file__isnull=True) |
                                          Q(sample__isnull=True)).delete()

            Sample.objects.annotate(
                    extract_count=Count("extract"),
                    unused_sample_count=Count("unused_sample")
                    ).filter(extract_count=0,
                             unused_sample_count=0).delete()

            Sample_qualifier.objects.filter(sample__isnull=True).delete()

            Sequencing_file.objects.filter(Q(library_name__isnull=True) |
                                           Q(information__isnull=True)).delete()

            Library.objects.annotate(
                            sequencing_file_count=Count("sequencing_file")
                            ).filter(sequencing_file_count=0).delete()

            Sequencing_information.objects.annotate(
                    sequencing_file_count=Count("sequencing_file")).filter(
                            sequencing_file_count=0).delete()

        except Exception as e:
            error_name = type(e).__name__
            msg = ("An error occur on the cleaning database command! "
                   + error_name + ": "
                   + e.args[0])
            raise CommandError(msg)

        self.stdout.write(self.style.SUCCESS('Successfully database cleaning'))
