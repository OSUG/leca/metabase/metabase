from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


###############
# model class
###############

class Project(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False, blank=False,
                            unique=True)
    available = models.BooleanField(default=True)
    description = models.TextField(max_length=1000)
    email = models.EmailField(max_length=255)
    leader = models.CharField(max_length=255)
    parentProject = models.ManyToManyField('self', blank=True, default=None,
                                           symmetrical=False)
    users = models.ManyToManyField(User, blank=True)

    def __str__(self):
        return self.name


class Submission_file(models.Model):
    id = models.AutoField(primary_key=True)
    file_name = models.CharField(max_length=255, null=False, blank=False)
    submission_date = models.DateTimeField(auto_now=True)
    excel_file = models.BinaryField(blank=False)

    def __str__(self):
        return self.file_name


class Tube_mix(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False, blank=False)
    mtype = models.CharField(max_length=255, null=False, blank=False)
    quantity = models.IntegerField(default=0)
    concentration = models.IntegerField(default=0)
    submission_file = models.ForeignKey(Submission_file,
                                        on_delete=models.CASCADE, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name


class Plate_96(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=250)


class PCR(models.Model):
    id = models.AutoField(primary_key=True)
    run = models.CharField(max_length=255, null=True, blank=True)
    run_position = models.IntegerField(null=True, blank=True)
    cycles = models.IntegerField()
    denaturation_time = models.IntegerField()
    hybridation_time = models.IntegerField()
    synthesis_time = models.IntegerField()
    final_synthesis_time = models.IntegerField()
    denaturation_temperature = models.IntegerField()
    hybridation_temperature = models.IntegerField()
    synthesis_temperature = models.IntegerField()
    final_synthesis_temperature = models.IntegerField()
    machine_id = models.CharField(max_length=255, null=True, blank=True)
    tube = models.ManyToManyField(Tube_mix, blank=False)
    plate = models.ForeignKey(Plate_96, on_delete=models.CASCADE)


class PrimerPair(models.Model):
    id = models.AutoField(primary_key=True)
    forward_sequence = models.CharField(max_length=255, null=False,
                                        blank=False)
    reverse_sequence = models.CharField(max_length=255, null=False,
                                        blank=False)

    class Meta:
        unique_together = (('forward_sequence', 'reverse_sequence'),)


class Sample(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False, blank=False)

    def __str__(self):
        return self.name


class Sample_qualifier(models.Model):
    id = models.AutoField(primary_key=True)
    key = models.CharField(max_length=255, null=False, blank=False)
    value = models.CharField(max_length=255, null=False, blank=False)
    sample = models.ForeignKey(Sample, related_name='sample_qualifiers', on_delete=models.CASCADE)

    class Meta:
        unique_together = (('key', 'value', 'sample'),)


class Unused_sample(models.Model):
    id = models.AutoField(primary_key=True)
    sample = models.ForeignKey(Sample, on_delete=models.CASCADE)
    submission_file = models.ForeignKey(Submission_file,
                                        on_delete=models.CASCADE)

    class Meta:
        unique_together = (('sample', 'submission_file'),)


class Extract(models.Model):
    extractTypes = (
        ('pcrpos', 'PCRPos'),
        ('sample', 'sample'),
        ('extneg', 'negative extraction')
    )
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False, blank=False)
    etype = models.CharField(max_length=7, choices=extractTypes)
    sample = models.ForeignKey(Sample, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name


class Extract_qualifier(models.Model):
    id = models.AutoField(primary_key=True)
    key = models.CharField(max_length=255, null=False, blank=False)
    value = models.CharField(max_length=255, null=False, blank=False)
    extract = models.ForeignKey(Extract, on_delete=models.CASCADE)

    class Meta:
        unique_together = (('key', 'value', 'extract'),)


class Unused_extract(models.Model):
    id = models.AutoField(primary_key=True)
    extract = models.ForeignKey(Extract, on_delete=models.CASCADE)
    submission_file = models.ForeignKey(Submission_file,
                                        on_delete=models.CASCADE)

    class Meta:
        unique_together = (('extract', 'submission_file'),)


class Well(models.Model):
    wellTypes = (
        ('pcrneq', 'PCRNeg'),
        ('blank', 'blank'),
        ('extract', 'extract'),
    )
    plate = models.ForeignKey(Plate_96, on_delete=models.CASCADE)
    primer = models.ForeignKey(PrimerPair, on_delete=models.CASCADE)
    extract = models.ForeignKey(Extract, on_delete=models.CASCADE, null=True)
    column = models.IntegerField(null=False, blank=False)
    row = models.CharField(max_length=1, null=False, blank=False)
    barcode_forward = models.CharField(max_length=255, null=False, blank=False)
    barcode_reverse = models.CharField(max_length=255, null=False, blank=False)
    wtype = models.CharField(max_length=7, choices=wellTypes)
    dilution = models.FloatField(default=1)

    class Meta:
        unique_together = (('plate', 'primer',
                            'barcode_forward', 'barcode_reverse'),)


class Library(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False,
                            primary_key=True)
    tube = models.ForeignKey(Tube_mix, related_name='libraries', on_delete=models.CASCADE)


class Sequencing_information(models.Model):
    id = models.AutoField(primary_key=True)
    info_file = models.BinaryField(blank=False)
    file_name = models.CharField(max_length=255, null=False, blank=False)
    file_type = models.CharField(max_length=255, null=False, blank=False)
    provider = models.CharField(max_length=255, null=False, blank=False)
    contact = models.CharField(max_length=255, null=False, blank=False)
    sequencing_date = models.DateTimeField(auto_now=False)

    def __str__(self):
        return self.file_name


class Sequencing_file(models.Model):
    id = models.AutoField(primary_key=True)
    library_name = models.ForeignKey(Library, on_delete=models.CASCADE)
    file_name = models.CharField(max_length=255, null=False, blank=False)
    file_type = models.CharField(max_length=255, null=False, blank=False)
    sequencingTechnology = models.CharField(max_length=255, null=False,
                                            blank=False)
    sequencingInstrument = models.CharField(max_length=255, null=False,
                                            blank=False)
    information = models.ForeignKey(Sequencing_information,
                                    on_delete=models.CASCADE)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class Imported_data_tmp(models.Model):
    data_type = (
        ('samples', 'samples'),
        ('extracts', 'extracts'),
        ('tubes', 'tubes'),
        ('plates', 'plates'),
        ('pcrs', 'pcrs'),
        ('project', 'project'),
        ('submission_file', 'submission_file'),
        ('controls', 'controls'),
    )
    id = models.CharField(max_length=255, primary_key=True)
    type = models.CharField(max_length=255, choices=data_type)
    key = models.CharField(max_length=255, null=False, blank=False)
    value = models.CharField(max_length=255, null=False, blank=False)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, default=None,
                               blank=True, null=True)


class Deleted_data_tmp(models.Model):
    data_type = (
        ('samples', 'samples'),
        ('controls', 'controls'),
        ('tubes', 'tubes'),
        ('plates', 'plates'),
        ('project', 'project'),
        ('primers', 'primers'),
        ('submission_file', 'submission_file'),
    )
    id = models.CharField(max_length=255, primary_key=True)
    type = models.CharField(max_length=255, choices=data_type)
    uniq_id = models.CharField(max_length=255, null=False, blank=False)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, default=None,
                               blank=True, null=True)
