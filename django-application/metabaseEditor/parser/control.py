from django.db import InternalError
from .header_utils import get_header_length

"""
    The ``control`` module
    ======================

    Use to parse the excel sheet 'control'.
"""


def check_control_label(control_sheet):
    """
        Check label of excel sheet 'control'.

        This function check if the excel sheet 'control' contains 4 fixed label
        in position A3, A5, A7 and A9. Raise a exception if a label is missing.

        :param control_sheet: openpyxl sheet containing control data
        :raises InternalError: if the labels are not correct
    """
    label_control = {
        'A3': 'negative extraction controls:',
        'A5': 'negative pcr controls:',
        'A7': 'blank pcr controls:',
        'A9': 'positive controls with their expected taxonomic abundance (0 to 100):',
        'A10': 'taxid'
    }
    for k, v in label_control.items():
        if not control_sheet[k].value:
            raise InternalError('Missing header "' + v + '" in cell '+ k + '.')

        if str(control_sheet[k].value).lower() != v:
            raise InternalError('The cell ' + k + ' ("'
                                + str(control_sheet[k].value)
                                + '") not contains "' + v + '".')


def check_control_value(control_sheet):
    """
        Check value of excel sheet 'control'.

        This function checks if the values of 3 labels are filled. Three values
        are checked in position C3, C5 and C7.

        :param control_sheet: openpyxl sheet containing control data
        :raises InternalError: if the values are not correct or missing
    """
    if not control_sheet['C3'].value:
        raise InternalError('Missing the negative extraction controls value.')

    if not control_sheet['C5'].value:
        raise InternalError('Missing negative pcr controls value.')

    if not control_sheet['C7'].value:
        raise InternalError('Missing blank pcr controls.')


def check_positive_control_abundance(control_sheet, ctl_pos):
    """
    Check the taxids and their abundance.

    Two verifications are performed for positif control: first, check if
    the taxid is not duplicated. Second, the sum of abundance
    value is calculed for each positive control and must be between 99.95
    and 100.05.

    :param control_sheet: openpyxl sheet containing control data
    :param ctl_pos: the list containing the positive control name
    :raises InternalError: if the values are not correct or missing
    """
    empty_cell = False
    max_row = 10
    nb_empty_row = 0
    taxid_list = list()
    abundance = list()
    for ctl in ctl_pos:
        abundance.append(0)
    for row in control_sheet.iter_rows(min_row=11):
        if not row[0].value:
            empty_cell = True
            if nb_empty_row > 10000:
                break
            nb_empty_row += 1
        elif empty_cell:
            raise InternalError('The row '
                                + str(max_row + 1)
                                + ' missing taxid.')
        else:
            taxid = str(row[0].value)
            if taxid in taxid_list:
                raise InternalError('The taxid ' + taxid
                                    + ' already exist in the submission file!')
            taxid_list.append(taxid)
            max_row += 1
        for ctl_idx in range(1, len(ctl_pos) + 1):
            if row[ctl_idx].value:
                abundance[ctl_idx-1] += float(row[ctl_idx].value)

    for val in abundance:
        if val <= 99.95 or val >= 100.05:
            raise InternalError('The total abundance of positive control: "'
                                + str(ctl_pos[abundance.index(val)])
                                + '" (' + str(val) + ') '
                                + 'must be equal to 100.')
    return "Taxid loaded: " + str(len(taxid_list))


def check_positive_control(control_sheet):
    """
        Check the positive control.

        This function checks if the values of positive control are valided.
        First, the uniqueness of positive control name is checked. Second the
        uniqueness of taxids is checked. Third, the wholeness of the abundance
        is checked: the sum of abundance value is calculed for each positive
        control and must be between 99.95 and 100.05.

        :param control_sheet: openpyxl sheet containing control data
        :raises InternalError: if the values are not correct or missing
        :return: string indicating the number of control positive read
    """
    ctl_pos = list()
    msg = list()
    empty_cell = False
    column = 0
    # check control positive unicity
    for cell in control_sheet[10][1:]:
        if not cell.value:
            empty_cell = True
        elif empty_cell:
            raise InternalError('The column '
                                + str(column + 1)
                                + ' missing positive control.')
        else:
            if cell.value in ctl_pos:
                raise InternalError('The positif control "'
                                    + str(cell.value)
                                    + '" already exist in the '
                                    + 'submission file!')
            ctl_pos.append(cell.value)
            column += 1
    # check abundance
    msg.append("Positive control loaded: " + str(column))
    msg.append(check_positive_control_abundance(control_sheet, ctl_pos))
    return msg


def excel_parsing_control(control_sheet):
    """
        Parse the excel sheet 'control'.

        This function parse the excel sheet 'control' to get information
        about experiement control.

        :param control_sheet: openpyxl sheet containing control data
        :return: dictionnary containing information of experiment control
    """
    max_col = get_header_length(control_sheet[10])
    control_list = dict()
    # get fixed values and fill returned structure
    control_list['negCtl'] = control_sheet['C3'].value
    control_list['negPCRctl'] = control_sheet['C5'].value
    control_list['blkPCRctl'] = control_sheet['C7'].value
    control_list['ctlPos'] = list()
    # get positive control name and set structure to stock abundance value
    for cell in control_sheet['10'][1:max_col]:
        ctl_pos = dict()
        ctl_pos['name'] = cell.value
        ctl_pos['abundance'] = dict()
        control_list['ctlPos'].append(ctl_pos)
    # fill abundance value for each taxid and each positive control name
    for row in control_sheet.iter_rows(min_row=11):
        taxid = str(row[0].value)
        for cell_index in range(1, max_col):
            # get abundance value
            if row[cell_index].value:
                tax_qty = float(row[cell_index].value)
                # check if value exist and is not null
                if tax_qty > 0:
                    # get index of positive control name
                    ctl_index = cell_index - 1
                    control_list['ctlPos'][
                            ctl_index]['abundance'][taxid] = tax_qty
    return control_list


def excel_parsing_control_checking(control_sheet):
    """
        Process all checking method and parse excel sheet 'control'.

        This function uses all checking methods describe above and parse the
        excel sheet 'control'.

        :param: control_sheet: openpyxl sheet containing control data
        :return: dictionnary containing information of experiment control
        :raises: InternalError: if the sheet is not valid
    """
    output = dict()
    check_control_label(control_sheet)
    check_control_value(control_sheet)
    output["info"] = check_positive_control(control_sheet)
    output["data"] = excel_parsing_control(control_sheet)

    return output


def excel_parsing_get_control(control_sheet):
    """
        Parse the excel sheet 'control'.

        This function parse the excel sheet 'control' to get the extraction
        control in the sheet control.

        :param control_sheet: openpyxl sheet containing control data
        :return: extraction control value
    """
    control = {
            'negCtl' : None,
            'negPCRctl' : None,
            'blkPCRctl' : None
    }
    if not control_sheet['C3'].value:
        raise InternalError('Missing the negative extraction controls value.')
    if not control_sheet['C5'].value:
        raise InternalError('Missing negative pcr controls value.')
    if not control_sheet['C7'].value:
        raise InternalError('Missing blank pcr controls.')

    control['negCtl'] = control_sheet['C3'].value
    control['negPCRctl'] = control_sheet['C5'].value
    control['blkPCRctl'] = control_sheet['C7'].value

    return control


def excel_parsing_get_positif_control(control_sheet):
    """
        Parse the excel sheet 'control'.

        This function parse the excel sheet 'control' to get information
        about experiement control.

        :param control_sheet: openpyxl sheet containing control data
        :return: dictionnary containing information of experiment control
    """
    max_col = get_header_length(control_sheet[10])
    # get fixed values and fill returned structure
    control_pos_list = list()
    # get positive control name and set structure to stock abundance value
    for cell in control_sheet['10'][1:max_col]:
        control_pos_list.append(cell.value)
    return control_pos_list
