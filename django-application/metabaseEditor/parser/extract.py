from django.db import InternalError
from openpyxl.utils import get_column_letter

from .header_utils import *

"""
    The ``extract`` module
    ======================

    Use to parse the excel sheet 'extraction'.
"""


def check_extract_row(row, header):
    """
        Check the wholeness of row in the excel sheet 'extract'.

        This function check if the row contains the required value and if
        each value has a related label.

        :param row: a row containing the extract data
        :param header: array containing fixed labels.
        default: extract id, sample id, batch)
        :raises InternalError: if a value or label is missing
    """
    max_col = len(header)
    # get index of fixed column
    sample_index = header.index('sample id')
    batch_index = header.index('batch')

    if not row[sample_index].value:
        raise InternalError('Missing sample ID (row '
                            + str(row[sample_index].row)+ ').')

    if row[batch_index].value == None:
        raise InternalError('Missing batch number (row '
                            + str(row[batch_index].row)+ ').')
    # check qualifier integrity
    for cell in row:
        if cell.value and str(cell.value).strip():
            if cell.column > max_col:
                raise InternalError(
                    'Missing header name for the column '
                    + str(cell.column) + '.')



def excel_parsing_extract(extract_sheet):
    """
        Parse the excel sheet 'extract'.

        This function parse the excel sheet 'extract' to get information
        about extraction.

        :param extract_sheet: openpyxl sheet containing extraction data
        :return: dictionnary containing information of extraction
    """
    max_col = get_header_length(extract_sheet[2])
    qualifier = list()
    extract_list = list()
    header = get_header_label(extract_sheet[2], 'Extraction')
    fixed_header = ['extract id', 'sample id', 'batch']     # required column
    # fill qualifier list
    for header_index in range(1, max_col + 1):
        header_label = str(extract_sheet.cell(row=2,
                                              column=header_index).value)
        if header_label.lower() not in fixed_header:
            qualifier.append(header_label)

    for row in extract_sheet.iter_rows(min_row=3):
        if row[header.index('extract id')].value is None:
            break
        # fill returned structure
        extract = dict()
        extract['name'] = str(row[header.index('extract id')].value).rstrip()
        extract['sample'] = str(row[header.index('sample id')].value).rstrip()
        extract['batch'] = row[header.index('batch')].value
        extract['qualifier'] = dict()
        # fill qualifier list
        for qualifier_name in qualifier:
            qualifier_value = row[header.index(qualifier_name.lower())].value
            if qualifier_value is not None:
                extract['qualifier'][qualifier_name] = qualifier_value
        extract_list.append(extract)
    return extract_list


def excel_parsing_extract_checking(extract_sheet):
    """
        Process all checking method and parse excel sheet 'extract'.

        This function uses all checking methods describe above and parse the
        excel sheet 'extract'.

        :param: extract_sheet: openpyxl sheet containing extraction data
        :return: dictionnary containing information of extraction
        :raises: InternalError: if the sheet is not valid
    """
    output = dict()
    header = get_header_label(extract_sheet[2], 'Extraction')
    extract_id_idx = header.index('extract id')
    checked_header = ['extract id', 'sample id', 'batch']   # required column
    check_header(header, checked_header, 'extraction')
    extract_list = list()
    empty_cell = False
    max_row = 2
    nb_empty_row = 0
    for row in extract_sheet.iter_rows(min_row=3):
        if not row[extract_id_idx].value:
            empty_cell = True
            if nb_empty_row > 10000:
                break
            nb_empty_row += 1
        elif empty_cell:
            raise InternalError('The row '
                                + str(max_row + 1)
                                + ' missing extract id.')
        else:
            check_extract_row(row, header)
            if row[extract_id_idx].value in extract_list:
                raise InternalError('line '+ str(row[extract_id_idx].row)
                                    + ': The extract id '
                                    + str(row[extract_id_idx].value)
                                    + ' already exist in the submission file!')
            extract_list.append(row[extract_id_idx].value)
            max_row += 1

    output['info'] = "Extract ids loaded: " + str(len(extract_list))
    output["data"] = excel_parsing_extract(extract_sheet)
    return output


def excel_parsing_get_extract(extract_sheet):
    """
        Parse the excel sheet 'extract'.

        This function parse the excel sheet 'extract' to get only
        the sample id.

        :param extract_sheet: openpyxl sheet containing extraction data
        :return: list containing sample id of extraction sheet
    """
    extract_list = list()
    header = get_header_label(extract_sheet[2], 'Extraction')

    for row in extract_sheet.iter_rows(min_row=3):
        if row[header.index('extract id')].value is None:
            break
        extract_id = str(row[header.index('extract id')].value).rstrip()
        if extract_id not in extract_list:
            extract_list.append(extract_id)
    return extract_list
