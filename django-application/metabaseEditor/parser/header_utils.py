from django.db import InternalError

"""
    The ``header_utils`` module
    ======================

    Use manipulate header in the excel sheet.
"""


def get_header_length(header):
    """
        Get the length of header.

        Browse header and return his length.

        :param header: row of openpyxl sheet containing labels and qualifiers
        :return: the header length
    """
    header_length = 0
    for header_cell in header:
        if header_cell.value:
            header_length += 1
    return header_length


def get_header_label(sheet_header, sheet_name):
    """
        Get the label contained in the header.

        Get label and qualifier contained in the header.

        :param sheet_header: row of openpyxl sheet containing labels
        :param sheet_name: name of sheet
        :return: list containing labels and qualifiers
        :raises InternalError: if header is not correct
    """
    header = list()
    empty_cell = False
    for header_cell in sheet_header:
        if header_cell.value:
            # check if the header is a contiguous array
            if empty_cell:
                raise InternalError('Missing header label in '
                                    + str(sheet_name) + ' sheet.')
            # check duplicate
            if str(header_cell.value).lower() in header:
                raise InternalError('The ' + str(sheet_name) + ' header label "'
                                    + str(header_cell.value) + '" already exist '
                                    + 'in the submission file.')
            header.append(str(header_cell.value).lower())
        else:
            empty_cell = True
    return header


def check_header(header, checked_header, sheet_name):
    """
        Check the header integrity.

        Check if the header contains required labels

        :param header: row of openpyxl sheet containing labels and qualifiers
        :param checked_header: list of required labels
        : sheet_name: the name of openpyxl sheet
    """
    for header_value in checked_header:
        if header_value not in header:
            raise InternalError('Missing the label ' + str(header_value)
                                + ' in ' + str(sheet_name) + ' sheet.')
