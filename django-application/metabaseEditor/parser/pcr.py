from django.db import InternalError

from .header_utils import get_header_label, check_header

"""
    The ``pcr`` module
    ======================

    Use to parse the excel sheet 'PCR'.
"""

def check_pcr_value(row, check_values):
    """
        Check the value of excel sheet 'pcr'.

        Check the integrity of pcr value for one row. Check if the row not
        contains empty cell.

        :param row: line of excel sheet corresponding to pcr information
        :param check_values: list containing header labels
        :raises InternalError: if cell is empty
    """
    for i in range(0,len(check_values)):
        if not row[i].value:
            raise InternalError('Row ' + str(row[i].row)
                                     + ': missing ' + str(check_values[i]) + '!')


def check_run_unicity(row, runs, header):
    """
        Check the uniqueness of run id and position.

        Check if the pcr run are not duplicated.

        :param row: line of excel sheet corresponding to pcr information
        :param runs: dictionnary where  pcr run id and position are stored
        :param header: list containing header labels
        :raises InternalError: if pcr run are duplicated
    """
    run_id = row[header.index('run')].value
    machin_id = row[header.index('machine id')].value
    run_id = str(run_id) + "-" + str(machin_id)
    if run_id not in runs:
        runs[run_id] = list()

    run_position = row[header.index('position')].value
    if run_position in runs[run_id]:
        raise InternalError('The run position '
                            + str(run_position)
                            + ' already exist in the run '
                            + str(run_id) + '!')
    runs[run_id].append(run_position)


def check_integer_value(row, header):
    """
        Check the integrity of value which must be an integer.

        For many column the value must be an integer and positive, this
        function check if this value are integer.

        :param row: line of excel sheet corresponding to pcr information
        :param header: list containing header labels
        :raises InternalError: if value is not an integer or negative
    """
    checked_header = ['cycles', 'denaturation time (s)', 'hybridation time (s)',
                      'synthesis time (s)', 'final synthesis time (min)',
                      'denaturation temp', 'hybridation temp',
                      'synthesis temp', 'final synthesis temp']
    for header_cell in checked_header:
        header_index = header.index(header_cell)
        # check if value is an integer
        if not isinstance(row[header_index].value, int):
            raise InternalError('The value row ' + str(row[header_index].row)
                                + ' column ' + str(row[header_index].column)
                                + ' must be an integer!')
        # check if the value is positive
        if row[header_index].value < 0:
            raise InternalError('The value row ' + str(row[header_index].row)
                                + ' column ' + str(row[header_index].column)
                                + ' must be positive!')


def excel_parsing_pcr_checking(pcr_sheet):
    """
        Process all checking method and parse excel sheet 'PCR'.

        This function uses all checking methods describes above and parse the
        excel sheet 'PCR'. Check if the data in sheet 'PCR' are usabled
        for the database insertion.

        :param pcr_sheet: openpyxl sheet containing PCR information
        :return: dictionnary containing info of PCR for checking view
    """
    # output dictionnary
    pcr = {
        'runs': dict(),
        'plates': list(),
        'warnings': list()
    }
    runs = dict()

    header = get_header_label(pcr_sheet[2], 'pcr')
    # required header labels
    checked_header = ['run', 'position', 'plate id', 'cycles',
                      'denaturation time (s)', 'hybridation time (s)',
                      'synthesis time (s)', 'final synthesis time (min)',
                      'denaturation temp', 'hybridation temp',
                      'synthesis temp', 'final synthesis temp', 'machine id']

    check_header(header, checked_header, 'pcr')
    empty_cell = False
    max_row = 2
    nb_empty_row = 0
    for row in pcr_sheet.iter_rows(min_row=3, max_col=13):
        if all(cell.value is None for cell in row[0:len(header)]):
            empty_cell = True
            if nb_empty_row > 10000:
                break
            nb_empty_row += 1
        elif empty_cell:
            raise InternalError('The row '
                                + str(max_row + 1)
                                + ' missing pcr program.')
        else:
            plate_id = row[header.index('plate id')].value
            if any(c.isspace() for c in row[header.index('plate id')].value):
                pcr['warnings'].append('Row '
                                       + str(row[header.index('plate id')].row)
                                       +' : the plate id '
                                       + str(plate_id) + ' contains spaces!)')
            check_pcr_value(row, header)
            check_run_unicity(row, runs, header)
            check_integer_value(row, header)
            run_id = row[header.index('run')].value
            machin_id = row[header.index('machine id')].value
            run_id = str(run_id) + "-" + str(machin_id)
            run_position = row[header.index('position')].value
            # store pcr run id for the next checking step (in view function)
            if run_id not in pcr['runs']:
                pcr['runs'][run_id] = list()
            # store run position for the next checking step (in view function)
            if run_position not in pcr['runs'][run_id]:
                pcr['runs'][run_id].append(run_position)
            # store plate id for the next checking step (in view function)
            if plate_id not in pcr['plates']:
                pcr['plates'].append(plate_id)
            else:
                # add warning message for the user
                pcr['warnings'].append('The plate "' + str(plate_id)
                                     + '" already exist in the submission file!')
            max_row += 1
    pcr['info'] = "PCR program loaded: " + str(max_row - 2)
    return pcr


def excel_parsing_pcr(pcr_sheet):
    """
        Parse the excel sheet 'PCR'.

        This function parse the excel sheet 'PCR' to get information
        about content of plate 96 well and primer pairs.

        :param plate_sheet: openpyxl sheet containing PCR information
        :return: dictionnary containing information of PCR
    """
    runs = dict()
    header = get_header_label(pcr_sheet[2], 'pcr')

    for row in pcr_sheet.iter_rows(min_row=3, max_col=13):
        if all(cell.value is None for cell in row[0:len(header)]):
            break
        run_id = row[header.index('run')].value
        machin_id = row[header.index('machine id')].value
        run_id = str(run_id) + "-" + str(machin_id)
        run_position = row[header.index('position')].value
        # set the sturture of output dictionnary
        if run_id not in runs:
            runs[run_id] = dict()
        run = runs[run_id][run_position] = dict()
        # fill the output dictionnary with PCR value
        run['plate'] = str(row[header.index('plate id')].value).rstrip()
        run['cycle'] = row[header.index('cycles')].value
        run['denatTime'] = row[header.index('denaturation time (s)')].value
        run['hybridTime'] = row[header.index('hybridation time (s)')].value
        run['synthTime'] = row[header.index('synthesis time (s)')].value
        run['finalSynthTime'] = row[
            header.index('final synthesis time (min)')].value
        run['denatTemp'] = row[header.index('denaturation temp')].value
        run['hybridTemp'] = row[header.index('hybridation temp')].value
        run['synthTemp'] = row[header.index('synthesis temp')].value
        run['finalSynthTemp'] = row[
            header.index('final synthesis temp')].value
        run['machineId'] = row[header.index('machine id')].value
    return runs


def excel_parsing_get_pcr(pcr_sheet):
    """
        Parse the excel sheet 'PCR'.

        This function parse the excel sheet 'PCR' to get the plate id.

        :param plate_sheet: openpyxl sheet containing PCR information
        :return: list containing plate id of pcrs sheet
    """
    plates_list = list()
    header = get_header_label(pcr_sheet[2], 'pcr')
    for row in pcr_sheet.iter_rows(min_row=3, max_col=13):
        if row[header.index('plate id')].value is None:
            break
        # fill the output dictionnary with PCR value
        plates_list.append(str(row[header.index('plate id')].value).rstrip())
    return plates_list
