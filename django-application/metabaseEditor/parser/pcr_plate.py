from django.db import InternalError

from .header_utils import get_header_label, check_header
"""
    The ``pcr_plate`` module
    ======================

    Use to parse the excel sheet 'pcr_plate'.
"""

def check_well_coordinate(row, column_ids, row_ids, header):
    """
        Check the well position well in plate 96.

        Check if the row and column position of well are include in the
        plate 96 coordinate.

        :param row: line of excel sheet corresponding to a well in pcr plate
        :param column_ids: list of column ids
        :param row_ids: list of row ids
        :param header: list containing header labels
        :raises InternalError: if well position is not in plate 96
    """
    # check column
    if not int(row[header.index('column')].value) in column_ids:
        raise InternalError('Row ' + str(row[1].row)
                             + ': the column id (' + str(row[1].value)
                             + ') must be include between '
                             + str(column_ids[0]) + ' and '
                             + str(column_ids[-1]) + '!')
    # check row
    if not row[header.index('row')].value in row_ids:
        raise InternalError('Row ' + str(row[2].row)
                             + ': the row id (' + str(row[2].value)
                             + ') must be include between '
                             + str(row_ids[0])
                             + ' and ' + str(row_ids[-1]) + '!')


def check_well_unicity(row, plate_dict, header):
    """
        Check the uniqueness of well.

        This function check if the well are not duplicated.

        :param row: line of excel sheet corresponding to a well in pcr plate
        :param plate_dict: dictionnary using to store already shown well
        :param header: list containing header labels
        :raises InternalError: if well are duplicated
    """
    plate_id = row[header.index('plate id')].value
    column_id = int(row[header.index('column')].value)
    row_id = row[header.index('row')].value
    if plate_id not in plate_dict:
        plate_dict[plate_id] = dict()
    if column_id not in plate_dict[plate_id]:
        plate_dict[plate_id][column_id] = list()
    # check duplicate
    if str(row_id).lower() in plate_dict[plate_id][column_id]:
        raise InternalError('The well already exist in '
                            + 'the plate ' + str(plate_id)
                            + ' at column ' + str(column_id)
                            + ' and row '+ str(row_id) + '!')
    else:
        # fill dictionnary plate_dict for the next well
        plate_dict[plate_id][column_id].append(str(row_id).lower())


def check_tag_unicity(row, plate_dict, header):
    """
        Check the uniqueness of pcr tag.

        Check for the row and column that the tags are identical.

        :param row: line of excel sheet corresponding to a well in pcr plate
        :param plate_dict: dictionnary using to store tag by row and column
        :param header: list containing header labels
        :return: list of warning messages
    """
    warning_message = list()
    plate_id = row[header.index('plate id')].value
    column_id = int(row[header.index('column')].value)
    row_id = row[header.index('row')].value
    row_tag = str(row[header.index('tag forward')].value)
    row_number = str(row[header.index('tag forward')].row)
    column_tag = str(row[header.index('tag reverse')].value)
    column_number = str(row[header.index('tag reverse')].row)
    # check the tag of row
    if plate_id not in plate_dict['row']:
        plate_dict['row'][plate_id] = dict()
    if row_id not in plate_dict['row'][plate_id]:
        # fill dictionnary plate_dict for the next row tag
        plate_dict['row'][plate_id][row_id] = row_tag.lower()
    # check if the tag is the same than the other tag in the row
    elif plate_dict['row'][plate_id][row_id] != row_tag.lower():
        warning_message.append("Row " + row_number + ": the tag "
                               + row_tag
                               + " not corespond to the other tag in row "
                               + str(row_id) + "!")
    # check the tag of column
    if plate_id not in plate_dict['column']:
        plate_dict['column'][plate_id] = dict()
    if column_id not in plate_dict['column'][plate_id]:
        # fill dictionnary plate_dict for the next column tag
        plate_dict['column'][plate_id][column_id] = column_tag.lower()
    # check if the tag is the same than the other tag in the column
    elif plate_dict['column'][plate_id][column_id] != column_tag.lower():
        warning_message.append("Row " + column_number + ": the tag "
                               + column_tag
                               + " not corespond to the other tag in column "
                               + str(column_id) + "!")
    return warning_message


def check_plate_value(row, check_values):
    """
        Check the value of plate 96.

        Check the integrity of plate 96 value for one row. Check if the row
        not contains empty cell.

        :param row: line of excel sheet corresponding to a well in pcr plate
        :param check_values: list containing header labels
        :raises InternalError: if cell is empty
    """
    for i in range(0,len(check_values)):
        if not row[i].value and row[i].value != 0:
            raise InternalError('Row ' + str(row[i].row)
                                     + ': missing ' + str(check_values[i]) + '!')


def excel_parsing_pcr_plate_checking(plate_sheet):
    """
        Process all checking method and parse excel sheet 'PCR plate'.

        This function uses all checking methods describes above and parse the
        excel sheet 'PCR plate'. Check if the data in sheet 'PCR plate' are
        usabled for the database insertion.

        :param plate_sheet: openpyxl sheet containing plate 96 information
        :return: dictionnary containing info of plate 96 for checking view
    """
    plate = {
        'plates': dict(),
        'extracts': list(),
        'warnings': list()
    }
    checking_dict = {
        'row': dict(),
        'column': dict(),
        'count': dict()
    }
    # row and column notation for plate 96
    row_ids = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    column_ids = range(1, 13)

    header = get_header_label(plate_sheet[2], 'plate')
    # required header labels
    checked_header = ['plate id', 'column', 'row', 'extract id', 'dilution',
              'tag forward', 'tag reverse', 'primer forward', 'primer reverse']

    empty_cell = False
    max_row = 2
    nb_empty_row = 0

    check_header(header, checked_header, 'plate')
    for row in plate_sheet.iter_rows(min_row=3):
        if all(cell.value is None for cell in row[0:len(header)]):
            empty_cell = True
            if nb_empty_row > 10000:
                break
            nb_empty_row += 1
        elif empty_cell:
            raise InternalError('The row '
                                + str(max_row + 1)
                                + ' missing pcr plate information.')
        else:
            check_well_coordinate(row, column_ids, row_ids, header)
            check_well_unicity(row, checking_dict, header)
            check_plate_value(row, header)
            warning_message = check_tag_unicity(row, checking_dict, header)
            plate['warnings'].extend(warning_message)
            # get well value
            plate_id = row[header.index('plate id')].value
            column_id = int(row[header.index('column')].value)
            row_id = row[header.index('row')].value
            extract_id = str(row[header.index('extract id')].value).rstrip()
            # count the number of well for each plate
            if plate_id not in checking_dict['count']:
                checking_dict['count'][plate_id] = 1
            else:
                checking_dict['count'][plate_id] += 1
            # store extract id for the next checking step (in view function)
            if extract_id not in plate['extracts']:
                plate['extracts'].append(extract_id)
            # store plate id for the next checking step (in view function)
            if plate_id not in plate['plates']:
                plate['plates'][plate_id] = dict()
            if column_id not in plate['plates'][plate_id]:
                plate['plates'][plate_id][column_id] = dict()
            if row_id not in plate['plates'][plate_id][column_id]:
                well = plate['plates'][plate_id][column_id][row_id] = dict()
            # store well information for the next checking step (in view function)
            well['extractId'] = extract_id
            well['tforward'] = row[header.index('tag forward')].value
            well['treverse'] = row[header.index('tag reverse')].value
            well['pforward'] = row[header.index('primer forward')].value
            well['preverse'] = row[header.index('primer reverse')].value
            max_row += 1
    # check number of well for each plate
    for plate_id, well_count in checking_dict['count'].items():
        if well_count != 96:
            # add warning message for the user
            plate['warnings'].append('Plate ' + str(plate_id) + ': '
                                     + str(well_count)
                                     + ' wells instead of 96!')
    plate['info'] = "PCR plate loaded: " + str(len(plate['plates'].keys()))
    return plate

def excel_parsing_pcr_plate(plate_sheet):
    """
        Parse the excel sheet 'PCR plate'.

        This function parse the excel sheet 'PCR plate' to get information
        about content of plate 96 well and primer pairs.

        :param plate_sheet: openpyxl sheet containing plate 96 information
        :return: dictionnary containing information of plate 96
    """
    plate_list = dict()
    plate_list['primers'] = dict()
    header = get_header_label(plate_sheet[2], 'plate')

    for row in plate_sheet.iter_rows(min_row=3):
        if all(cell.value is None for cell in row[0:len(header)]):
            break
        plate_id = str(row[header.index('plate id')].value).rstrip()
        column_id = int(row[header.index('column')].value)
        row_id = row[header.index('row')].value
        # set the sturture of output dictionnary
        if plate_id not in plate_list:
            plate_list[plate_id] = dict()
        if column_id not in plate_list[plate_id]:
            plate_list[plate_id][column_id] = dict()
        if row_id not in plate_list[plate_id][column_id]:
            plate = plate_list[plate_id][column_id][row_id] = dict()
        # fill the output dictionnary with PCR plate value
        plate['extractId'] = str(row[header.index('extract id')].value).rstrip()
        plate['dilution'] = row[header.index('dilution')].value
        plate['tforward'] = row[header.index('tag forward')].value
        plate['treverse'] = row[header.index('tag reverse')].value
        # get primer information
        p_forward = row[header.index('primer forward')].value
        p_reverse = row[header.index('primer reverse')].value
        primers = plate_list['primers']
        # add primer to the output dictionnary
        if p_forward not in primers:
            primers[p_forward] = dict()
        if p_reverse not in primers[p_forward]:
            primers[p_forward][p_reverse] = dict()
            primers[p_forward][p_reverse]['pforward'] = p_forward
            primers[p_forward][p_reverse]['preverse'] = p_reverse
        plate['primer'] = primers[p_forward][p_reverse]
    return plate_list


def excel_parsing_get_pcr_plate(plate_sheet):
    """
        Parse the excel sheet 'PCR plate'.

        This function parse the excel sheet 'PCR plate' to get information
        about content of plate 96 well and primer pairs.

        :param plate_sheet: openpyxl sheet containing plate 96 information
        :return: dictionnary containing information of plate 96
    """
    plate_list = list()
    header = get_header_label(plate_sheet[2], 'plate')

    for row in plate_sheet.iter_rows(min_row=3):
        if row[header.index('plate id')].value is None:
            break
        plate_id = str(row[header.index('plate id')].value).rstrip()
        plate_list.append(plate_id)
    return plate_list
