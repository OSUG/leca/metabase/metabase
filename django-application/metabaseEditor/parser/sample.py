from django.db import InternalError
from openpyxl.utils import get_column_letter

from .header_utils import *

"""
    The ``sample`` module
    ======================

    Use to parse the excel sheet 'sample'.
"""

def check_sample_id_unicity(sample_id_column):
    """
        Check the uniqueness of sample id.

        Check if sample id are not duplicated.

        :param sample_id_column: the column containing sample id
        :raises InternalError: If sample id are duplicated or missing
    """
    sample_list = list()
    for sample_cell in sample_id_column:
        sample_id = sample_cell.value
        # check empty cell
        if not sample_id:
            raise InternalError('Row ' + str(sample_cell.row)
                                + ': missing sample ID.')
        # check duplicate sample
        if str(sample_id).lower() in sample_list:
            raise InternalError('The sample '
                                + str(sample_id)
                                + ' already exist in the submission file.')
        sample_list.append(str(sample_id).lower())


def check_sample_data_integrity(row, max_col):
    """
        Check the wholeness of each row in the excel sheet 'sample'.

        This function check if each value has a related label.

        :param sample_data: part of excel sheet containing the extract data
        :param max_col: the maximal number of column
        :raises InternalError: if a label is missing
    """
    for cell in row:
        if cell.value:
            if cell.column > max_col:
                raise InternalError(
                    'Missing header name for the column '
                    + get_column_letter(cell.column) + '.')


def excel_parsing_sample(sample_sheet):
    """
        Parse the excel sheet 'sample'.

        This function parse the excel sheet 'sample' to get information
        about sampling.

        :param sample_sheet: openpyxl sheet containing sampling data
        :return: dictionnary containing information of sampling
    """
    max_col = get_header_length(sample_sheet[2])
    qualifier = list()
    sample_list = dict()
    header = get_header_label(sample_sheet[2], 'Samples')

    # get qualifier name in the header
    for header_index in range(1, max_col+1):
        header_label = str(sample_sheet.cell(row=2, column=header_index).value)
        if header_label.lower() != 'sample id':
            qualifier.append(header_label)

    # add qualifier for each sample
    for row in sample_sheet.iter_rows(min_row=3):
        if row[header.index('sample id')].value is None:
            break
        sample_id = str(row[header.index('sample id')].value).rstrip()
        sample_list[sample_id] = dict()
        # add qualifier
        for qualifier_name in qualifier:
            qualifier_value = row[header.index(qualifier_name.lower())].value
            if qualifier_value is not None:
                sample_list[sample_id][qualifier_name] = qualifier_value
    return sample_list

def excel_parsing_sample_checking(sample_sheet):
    """
        Process all checking method and parse excel sheet 'sample'.

        This function uses all checking methods describe above and parse the
        excel sheet 'sample'.

        :param: extract_sheet: openpyxl sheet containing sampling data
        :return: dictionnary containing information of sampling and info message
        :raises: InternalError: if the sheet is not valid
    """
    output = dict()
    max_col = get_header_length(sample_sheet[2])
    header = get_header_label(sample_sheet[2], 'Samples')
    sample_id_idx = header.index('sample id')
    sample_list = list()
    check_header(header, ['sample id'], 'Samples')
    empty_cell = False
    max_row = 2
    nb_empty_row = 0
    for row in sample_sheet.iter_rows(min_row=3):
        if not row[sample_id_idx].value:
            empty_cell = True
            if nb_empty_row > 10000:
                break
            nb_empty_row += 1
        elif empty_cell:
            raise InternalError('The row '
                                + str(max_row + 1)
                                + ' missing sample id.')
        else:
            sample_id = str(row[sample_id_idx].value).rstrip()
            if sample_id in sample_list:
                raise InternalError('The sample '
                                    + str(sample_id)
                                    + ' already exist in the submission file.')
            sample_list.append(sample_id)
            check_sample_data_integrity(row, max_col)
            max_row += 1
    output['info'] = "Sample ids loaded: " + str(max_row - 2)
    output["data"] = excel_parsing_sample(sample_sheet)
    return output


def excel_parsing_get_sample(sample_sheet):
    """
        Parse the excel sheet 'sample'.

        This function parse the excel sheet 'sample' to get information
        about sampling.

        :param sample_sheet: openpyxl sheet containing sampling data
        :return: dictionnary containing information of sampling
    """
    sample_list = list()
    header = get_header_label(sample_sheet[2], 'Samples')

    for row in sample_sheet.iter_rows(min_row=3):
        if row[header.index('sample id')].value is None:
            break
        sample_id = str(row[header.index('sample id')].value).rstrip()
        sample_list.append(sample_id)
    return sample_list
