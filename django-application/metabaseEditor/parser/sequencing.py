from xml.parsers.expat import *
import openpyxl
from .sequencing_xml_file import *
from .sequencing_excel_file import *


"""
    The ``sequencing`` module
    ======================

    Use to parse the sequencing information file.
"""

def sequencing_parsing(sequencing_file):
    """
        Parse sequencing file to get sequencing information.

        Get information about sequencing like sequencing provider,
        email contact, sequencing date, libraries, sequencing files .....

        :param sequencing_file: xml or excel file containing sequencing info
        :return: dictionnary containing sequencing information
    """
    parser_information = dict()
    try:
        # if the sequencing file is a xml file
        buff = sequencing_file.read()
        # get sequencing project information
        parser_information['project'] = project_infomation_xml_parsing(buff)
        # get sequencing run information
        parser_information['run'] = run_information_xml_parsing(buff)
        parser_information['type'] = 'xml'
    except ExpatError as ee:
        # if the sequencing file is a excel file
        excel_file = openpyxl.load_workbook(sequencing_file)
        # get sequencing project information
        parser_information['project'] = sequencing_infomation_excel_parsing(
            excel_file)
        # get sequencing run information
        parser_information['run'] = run_information_excel_parsing(
            excel_file, parser_information['project'])
        parser_information['type'] = 'excel'
    return parser_information
