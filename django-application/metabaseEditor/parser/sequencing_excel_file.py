from django.db import InternalError
import re

"""
    The ``sequencing excel file`` module
    ===================================

    Use to parse the excel file containing the sequencing information.
"""

def check_sequencing_information_label(sequencing_info_sheet):
    """
        Check label of excel sheet 'Sequencing information'.

        This function check if the excel sheet 'Sequencing information'
        contains 5 fixed labels in position A3, A5, A7, A9 and A11.
        Raise a exception if a label is missing.

        :param sequencing_info_sheet: openpyxl sheet containing sequencing
                                      information data
        :raises InternalError: if the labels are not correct
    """
    sequencing_info_label = {
        'A3': 'provider:',
        'A5': 'contact:',
        'A7': 'sequencing date:',
        'A9': 'sequencing technology:',
        'A11': 'sequencing instrument:'

    }
    for k, v in sequencing_info_label.items():
        if not sequencing_info_sheet[k].value:
            # check if the cell is not empty
            raise InternalError('Missing header "' + v + '" in cell '+ k + '.')

        if str(sequencing_info_sheet[k].value).lower() != v:
            # check if the the cell value match with the required label
            raise InternalError('The cell ' + k + ' ("'
                                + str(sequencing_info_sheet[k].value)
                                + '") not contains "' + v + '".')


def check_sequencing_date(date_value):
    """
        Check sequencing date format for excel sheet 'Sequencing information'.

        This function check if the format of sequencing date in the
        excel sheet 'Sequencing information' is good.
        Raise a exception if a format is not good.

        :param date_value:  sequencing date
        :raises InternalError: if the format is not good.
    """
    m = re.search(r"(?P<dateYear>\d{4})\-"
                  r"(?P<dateMonth>\d{2})\-"
                  r"(?P<dateDay>\d{2})",str(date_value))
    if m is None:
        raise InternalError("The date format is not valid. "
                            + "It's not correspond to the format: YYYY-MM-DD")


def get_data_sheet_sequencing_info(sequencing_info_sheet):
    """
        Get data in the excel sheet 'Sequencing information'.

        This function parse the excel sheet 'Sequencing information' to
        get information about sequencing.

        :param sequencing_info_sheet: openpyxl sheet containing sequencing
                                      information data
        :return: dictionnary containing information of sequencing
    """
    sequencing_info = dict()
    # get fixed values and fill returned structure
    sequencing_info['facility_institution'] = sequencing_info_sheet['B3'].value
    sequencing_info['customer_email'] = sequencing_info_sheet['B5'].value
    sequencing_info['sequencing_date'] = sequencing_info_sheet['B7'].value
    sequencing_info['sequencingTechnology'] = sequencing_info_sheet['B9'].value
    sequencing_info['sequencingInstrument'] = sequencing_info_sheet['B11'].value
    return sequencing_info


def sequencing_infomation_excel_parsing(excel_file):
    """
        Check and parse the excel sheet 'Sequencing information'.

        This function check the data consistency in the excel sheet
        'Sequencing information' and get information about sequencing.

        :param excel_file: openpyxl workbook
        :return: dictionnary containing information of sequencing
    """
    # check the label field
    check_sequencing_information_label(excel_file['Sequencing information'])
    # check sequencing date
    check_sequencing_date(excel_file['Sequencing information']['B7'].value)
    return get_data_sheet_sequencing_info(excel_file['Sequencing information'])


def check_libraries_header(libraries_sheet):
    """
        Check the header of excel sheet 'Libraries'.

        This function check if the excel sheet 'Libraries'
        contains 2 required labels in position A2, B2.
        Raise a exception if a label is missing.

        :param libraries_sheet: openpyxl sheet containing libraries data
        :raises InternalError: if the header is not valid
    """
    # required header label
    required_label = {
        'A2': 'Libraries',
        'B2': 'Tube'
    }
    for key, value in required_label.items():
        if not libraries_sheet[key].value:
            # check if the cell is not empty
            raise InternalError('Missing header "' + value
                                + '" in cell '+ key + '.')
        if libraries_sheet[key].value != value:
            # check if the the cell value match with the required label
            raise InternalError('The cell ' + key + ' ("'
                                + str(libraries_sheet[key].value)
                                + '") not contains "' + value + '".')


def check_libraries_tubes_unicity(libraries_sheet):
    """
        Check the data consistency of excel sheet 'Libraries'.

        This function check if the data is not duplicated.
        Raise a exception if the data is duplicated.

        :param libraries_sheet: openpyxl sheet containing libraries data
        :raises InternalError: if the data is duplicated
    """
    check_values = dict()
    max_row = libraries_sheet.max_row
    for line in libraries_sheet.iter_rows(min_row=3, max_row=max_row):
        # get cell values
        library = line[0].value
        tube = line[1].value
        if not library:
            # check if the cell is not empty
            raise InternalError('Row ' + str(line[0].row)
                                + ': missing library.')
        if not tube:
            # check if the cell is not empty
            raise InternalError('Row ' + str(line[1].row)
                                + ': missing tube.')
        if library in check_values:
            if tube in check_values[library]:
                # check duplicate information
                raise InternalError('The library ' + str(library)
                                    + ' with the tube ' + str(tube)
                                    + ' already exist in the excel file.')
            else:
                check_values[library].append(tube)
        else:
            check_values[library] = list()
            check_values[library].append(tube)


def check_sequencing_files_duplicate(sequencing_files_column):
    """
        Check the data consistency of excel sheet 'Sequencing files'.

        This function check if the data is valid and not duplicated.
        Raise a exception if a data is missing or duplicating.

        :param libraries_sheet: openpyxl sheet containing sequencing files data
        :raises InternalError: if the data is missing or duplicating
    """
    check_value = list()
    for sequencing_file in sequencing_files_column:
        if not sequencing_file.value:
            # check if the cell is not empty
            raise InternalError('Row ' + str(sequencing_file.row)
                                + ': missing sequencing file.')
        if sequencing_file.value in check_value:
            # check duplicate sequencing file
            raise InternalError('The sequencing file '
                                + str(sequencing_file.value)
                                + '.fastq.gz'
                                + ' already exist in the excel file.')
        check_value.append(sequencing_file.value)


def check_libraries_unicity(sequencing_file_sheet, libraries):
    """
        Check the libraries unicity of excel sheet 'Sequencing files'.

        This function check if the libraries is not duplicated in the sheet
        'Sequencing files'. Raise a exception if a library is duplicated.

        :param libraries_sheet: openpyxl sheet containing sequencing files data
        :raises InternalError: if a library is duplicated
    """
    check_libraries = list()
    max_row = sequencing_file_sheet.max_row
    for line in sequencing_file_sheet.iter_rows(min_row=3, max_row=max_row):
        library = line[1].value
        if not library:
            # check if the cell is not empty
            raise InternalError('Row ' + str(line[1].row)
                                + ': missing library.')
        if library not in libraries:
            # check if all library are used in the sheet libraries
            raise InternalError('The library ' + str(library)
                                + ' is not used in the sheet Libraries')
        check_libraries.append(library)
    for library in libraries:
        if library not in check_libraries:
            # check if all library are used in the sheet sequencing files
            raise InternalError('The library ' + str(library)
                                + ' is not used in the sheet Sequencing files')


def excel_parsing_libraries(libraries_sheet):
    """
        Get data in the excel sheet 'Libraries'.

        This function parse the excel sheet 'Libraries' to get libraries
        and tubes links.

        :param libraries_sheet: openpyxl sheet containing libraries and tubes
        :return: dictionnary containing libraries and corresponding tubes
    """
    libraries = dict()
    max_row = libraries_sheet.max_row
    for line in libraries_sheet.iter_rows(min_row=3, max_row=max_row):
        library = line[0].value
        tube = line[1].value
        libraries[library] = list()
        libraries[library].append(tube)
    return libraries


def excel_parsing_sequencing_files(sequencing_files_sheet):
    """
        Get data in the excel sheet 'Sequencing files'.

        This function parse the excel sheet 'Sequencing files' to get
        sequencing files and libraries links.

        :param sequencing_files_sheet: openpyxl sheet containing sequencing
        files information
        :return: dictionnary containing sequencing files and libraries
    """
    sequencing_files = dict()
    max_row = sequencing_files_sheet.max_row
    for line in sequencing_files_sheet.iter_rows(min_row=3, max_row=max_row):
        sequencing_file = line[0].value
        library = line[1].value
        if library not in sequencing_files:
            sequencing_files[library] = list()
        sequencing_files[library].append(sequencing_file)
    return sequencing_files


def make_xml_parser_output(informations, libraries, sequencing_files):
    """
        Generate a same output structure of xml parser with excel data.

        This function use data get by the excel parser to build a object like
        xml parser output of run information.

        :param informations: dictionnary of sequencing project information
        :param libraries: dictionnary of libraries and corresponding tubes
        :param sequencing_files: dictionnary of libraries and corresponding
        sequencing files
        :return: list containing run informations like xml parser output of
        run information
    """
    output = list()
    run = dict()
    output.append(run)
    run['sequencingTechnology'] = informations['sequencingTechnology']
    run['sequencingInstrument'] = informations['sequencingInstrument']
    run['libs'] = list()
    for library, tube_list in libraries.items():
        lib = dict()
        lib['id'] = library
        for tube in tube_list:
            lib['sampleName'] = tube
        lib['fileName'] = sequencing_files[library]
        run['libs'].append(lib)
    return output


def run_information_excel_parsing(excel_file, sequencing_information):
    """
        Parse the sheet 'Libraries' and 'Sequencing files'.

        This function parse the sheet 'Libraries' and 'Sequencing files' to
        build a object like xml parser output of run information.

        :param excel_file: openpyxl workbook
        :param informations: dictionnary containing sequencing project info
        :return: list containing run informations like xml parser output of
        run information
    """
    check_libraries_tubes_unicity(excel_file['Libraries'])
    libraries = excel_parsing_libraries(excel_file['Libraries'])
    check_sequencing_files_duplicate(excel_file['Sequencing files']['A'])
    check_libraries_unicity(excel_file['Sequencing files'], libraries.keys())
    sequencing_files = excel_parsing_sequencing_files(
        excel_file['Sequencing files'])
    return make_xml_parser_output(sequencing_information, libraries,
                                  sequencing_files)
