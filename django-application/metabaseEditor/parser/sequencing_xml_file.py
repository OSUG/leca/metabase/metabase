from xml.dom.minidom import *

"""
    The ``sequencing xml file`` module
    ===================================

    Use to parse the XML file provide by Fasteris.
"""


def project_infomation_xml_parsing(xml_string):
    """
        Parse xml file to get project information.

        Get information about project like, email, contact....

        :param xml_string: file content (xml_string=file.read())
        :return: dictionnary containing project information
    """
    xml_file = parseString(xml_string)
    resultsNode = xml_file.getElementsByTagName('results')[0]
    projectNode = resultsNode.getElementsByTagName('project')[0]
    projectInformation = dict()
    for node in projectNode.childNodes:
        for child in node.childNodes:
            if child.nodeType == Node.ELEMENT_NODE:
                if child.firstChild.nodeType == Node.TEXT_NODE:
                    projectInformation[node.nodeName
                                        + "_"
                                        + child.nodeName
                                       ] = child.firstChild.nodeValue.strip()
    return projectInformation


def run_information_xml_parsing(xml_string):
    """
        Parse xml file to get sequencing run information.

        Get sequencing run information like library name, tube name,
        sequencing instrument....;

        :param xml_string: file content (xml_string=file.read())
        :return: dictionnary containing sequencing run information
    """
    xml_file = parseString(xml_string)
    run_information = list()
    for run_node in xml_file.getElementsByTagName('run'):
        run_dict = dict()
        run_information.append(run_dict)
        if run_node.nodeType == Node.ELEMENT_NODE:
            if run_node.hasAttributes():
                # get run id to build sequencing file name
                run_id = run_node.getAttribute('id')
            # get sequencer information
            instrument_node = run_node.getElementsByTagName('instrument')[0]
            if instrument_node.nodeType == Node.ELEMENT_NODE:
                sequencing_technology = instrument_node.getElementsByTagName(
                        'manufacturer'
                    )[0].firstChild.nodeValue.strip()
                run_dict['sequencingTechnology'] = sequencing_technology
                if instrument_node.hasAttributes():
                    serial_number = instrument_node.getAttribute(
                        'serialNumber')
                instrument_model = instrument_node.getElementsByTagName(
                    'model')[0]
                if (instrument_model.nodeType == Node.ELEMENT_NODE and
                    instrument_model.hasAttributes()):
                    sequencing_instrument = (instrument_model
                                             .getAttribute('name'))
                    run_dict['sequencingInstrument'] = (sequencing_instrument
                                                       + " "
                                                       + serial_number)
            run_dict['libs'] = list()
            # get library information
            for lib_node in run_node.getElementsByTagName('lib'):
                if lib_node.nodeType == Node.ELEMENT_NODE:
                    lib = dict()
                    run_dict['libs'].append(lib)
                    if lib_node.hasAttributes():
                        lib_id = lib_node.getAttribute('id')
                        lib['id'] = lib_id
                    sample_node = lib_node.getElementsByTagName(
                        'sampleName')[0]
                    if sample_node.nodeType == Node.ELEMENT_NODE:
                        sample_name = sample_node.firstChild.nodeValue.strip()
                        lib['sampleName'] = sample_name
                    lib['fileName'] = list()
                    # build sequencing file name
                    for lane_node in lib_node.getElementsByTagName('lane'):
                        if (lane_node.nodeType == Node.ELEMENT_NODE and
                            lane_node.hasAttributes()):
                            lane_id = lane_node.getAttribute('id')
                            try:
                                lane_id = "L%03d"%(int(lane_id), )
                            except ValueError as ve:
                                lane_id = "L" + str(lane_id)
                            lib['fileName'].append("%s_%s_%s" % (
                                run_id,
                                lane_id,
                                lib_id, )
                            )
    return run_information
