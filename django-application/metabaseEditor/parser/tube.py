from django.db import InternalError

from .header_utils import get_header_label, check_header

"""
    The ``tube`` module
    ======================

    Use to parse the excel sheet 'mix (tube)'.
"""

def check_tube_value(row, check_values):
    """
        Check the value of excel sheet 'mix (tube)'.

        Check the integrity of tube value for one row. Check if the row not
        contains empty cell.

        :param row: line of excel sheet corresponding to tube information
        :param check_values: list containing header labels
        :raises InternalError: if cell is empty
    """
    if any(row[idx].value is None for idx in range(0,len(row))):
        row_number = 0
        for i in range(0,len(row)):
            if row[i].value != None:
                row_number = row[i].row
        raise InternalError('Row ' + str(row_number)
                                 + ': missing required values: '
                                 + str(check_values[i]) + '!')


def check_plate_tube_unicity(row, checked_plates, header):
    """
        Check the uniqueness of plate id.

        Check if the plate ids are not duplicated.

        :param row: line of excel sheet corresponding to tube information
        :param checked_plates: dictionnary where plate ids are stored
        :param header: list containing header labels
        :raises InternalError: if plate ids are duplicated
    """
    plate_id = row[header.index('plate id')].value
    if plate_id in checked_plates:
        tube = row[header.index('mix (tube)')].value
        return ('The plate id ' + str(plate_id)
                            + ' already exist with another tube '
                            + str(tube) + '!')
    return None


def excel_parsing_tube_checking(tube_sheet):
    """
        Process all checking method and parse excel sheet 'mix (tube)'.

        This function uses all checking methods describes above and parse the
        excel sheet 'mix (tube)'. Check if the data in sheet ''mix (tube)'
        are usabled for the database insertion.

        :param tube_sheet: openpyxl sheet containing tubes information
        :return: dictionnary containing plate and tube info for checking view
    """
    tubes = {
        'tubes': list(),
        'plates': list(),
        'warnings': list()
    }
    header = get_header_label(tube_sheet[2], 'tube')
    checked_header = ['mix (tube)', 'plate id', 'type',
                      'volume (µl)', 'concentration (ng/µl)']

    empty_cell = False
    max_row = 2
    nb_empty_row = 0
    check_header(header, checked_header, 'tube')
    for label in checked_header:
        if label not in header[0:5]:
            raise InternalError('The label "' + label
                                + '" is not in the first 5 columns of '
                                + ' mix (tube) labels.')

    for row in tube_sheet.iter_rows(min_row=3, max_col=5):
        if all(cell.value is None for cell in row[0:len(row)]):
            empty_cell = True
            if nb_empty_row > 10000:
                break
            nb_empty_row += 1
        elif empty_cell:
            raise InternalError('The row '
                                + str(max_row + 1)
                                + ' missing tube information.')
        else:
            check_tube_value(row, header)
            warnings = check_plate_tube_unicity(row, tubes['plates'], header)
            if warnings is not None:
                tubes['warnings'].append(warnings)

            plate_id = row[header.index('plate id')].value
            tubes_name = row[header.index('mix (tube)')].value
            # store tube name for the next checking step (in view function)
            if tubes_name not in tubes['tubes']:
                tubes['tubes'].append(tubes_name)
            # store plate id for the next checking step (in view function)
            if plate_id not in tubes['plates']:
                tubes['plates'].append(plate_id)
            max_row += 1
    if not tubes['tubes'] and not tubes['plates']:
        raise InternalError("Error, the sheet mix(Tube) is empty")
    tubes['info'] = 'Tube loaded: ' + str(len(tubes['tubes']))
    return tubes


def excel_parsing_tube(tube_sheet):
    """
        Parse the excel sheet 'mix (tube)'.

        This function parse the excel sheet 'mix (tube)' to get information
        about tube.

        :param tube_sheet: openpyxl sheet containing tube information
        :return: dictionnary containing information of tube
    """
    tubes = dict()
    header = get_header_label(tube_sheet[2], 'tube')

    for row in tube_sheet.iter_rows(min_row=3, max_col=5):
        if all(cell.value is None for cell in row[0:len(row)]):
            break
        tube_name = row[header.index('mix (tube)')].value
        plate_id = str(row[header.index('plate id')].value).rstrip()
        type = row[header.index('type')].value
        volume = row[header.index('volume (µl)')].value
        concentration = row[header.index('concentration (ng/µl)')].value

        if plate_id not in tubes:
            tubes[plate_id] = list()
        tubes[plate_id].append({
                'tubeName' : tube_name,
                'type' : type,
                'quantity' : volume,
                'concentration' : concentration,
        })
    return tubes
