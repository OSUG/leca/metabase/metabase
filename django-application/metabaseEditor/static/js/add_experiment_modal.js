$(document).ready(function() {
  initCSRFToken();

  $("#show-modal-add-experiment").click(function() {
    $("#import-experiment").prop("hidden", false);
    $("#import-experiment").prop("disabled", true);
    $("#check-experiment").prop("disabled", true);
    $("#ok-modal-import").prop("hidden", true);
    $(".modal-footer", "#checkExperimentModal")
      .find("button")
      .not("#import-experiment").prop("disabled", false);
  });

  $("#excelFilePaths").change(function() {
    $("#checking-message").html("");
    $("#check-experiment").prop("disabled", false);
    $("#check-experiment-modal-error").html("");
    $("#excelFilePaths").removeClass("is-invalid");
    $("#import-experiment").prop("disabled", true);
    $("#import-experiment").prop("hidden", false);
    $("#check_progress_div").prop("hidden", true);
    $("#import_progress_div").prop("hidden", true);
    $("#ok-modal-import").prop("hidden", true);
  });

  $("#check-experiment-modal-body").on(
    "click",
    "#check-experiment",
    function() {
      $('#check_progress_div').empty();
      $('#import_progress_div').prop('hidden', true);
      $('#import_progress_div').empty();
      init_progress_bar('#check_progress_div', 'check_progress_bar');
      var projectId = $("#selectProject").val();
      var data = new FormData($("#addExperimentForm").get(0));
      var files_list = $("#addExperimentForm input[name=excelFilePaths]").get(0).files;
      var inputField = $("#addExperimentForm").find("input");
      var cancelButton = $(".modal-footer", "#checkExperimentModal")
        .find("button")
        .not("#import-experiment");
      cancelButton.prop("disabled", true);
      inputField.prop("disabled", true);
      var btn = $(this);
      btn.prop("disabled", true);
      btn.html(btn.attr("data-loading-text"));
      $('#check_progress_div').prop('hidden', false);
      var percent = (100/files_list.length) / 7;
      var chain = $.when();
      var decal = 1;
      var messages = {};
      $.each(files_list, function(index, elm){
        messages[files_list[index].name] = {'infos': [], 'warnings': []};
        chain = chain.then(function(){
          var progress_val = percent * (decal + index);
          return make_check_import_request(
            "/metabaseEditor/project/" + projectId + "/file/import/check",
            data, index, progress_val, btn, messages[files_list[index].name]);
        }).then(function(){
          decal = decal + 1;
          var progress_val = percent * (decal + index);
          return make_check_import_request(
            "/metabaseEditor/project/" + projectId + "/samples/import/check",
            data, index, progress_val, btn, messages[files_list[index].name]);
        }).then(function(){
          decal = decal + 1;
          var progress_val = percent * (decal + index);
          return make_check_import_request(
            "/metabaseEditor/project/" + projectId + "/controls/import/check",
            data, index, progress_val, btn, messages[files_list[index].name]);
        }).then(function(){
          decal = decal + 1;
          var progress_val = percent * (decal + index);
          return make_check_import_request(
            "/metabaseEditor/project/" + projectId + "/extracts/import/check",
            data, index, progress_val, btn, messages[files_list[index].name]);
        }).then(function(){
          decal = decal + 1;
          var progress_val = percent * (decal + index);
          return make_check_import_request(
            "/metabaseEditor/project/" + projectId + "/pcrs/import/check",
            data, index, progress_val, btn, messages[files_list[index].name]);
        }).then(function(){
          decal = decal + 1;
          var progress_val = percent * (decal + index);
          return make_check_import_request(
            "/metabaseEditor/project/" + projectId + "/plates/import/check",
            data, index, progress_val, btn, messages[files_list[index].name]);
        }).then(function(){
          decal = decal + 1;
          var progress_val = percent * (decal + index);
          return make_check_import_request(
            "/metabaseEditor/project/" + projectId + "/tubes/import/check",
            data, index, progress_val, btn, messages[files_list[index].name]);
        });
      });
      chain.done(function(){
        write_checking_messages(projectId, messages, btn);
        $("#import-experiment").prop("disabled", false);
      }).fail(function(){
        var percentage = 100;
        $('#check_progress_div').children().attr("aria-valuenow", function(i, w){
          percentage -= w;
        });
        $('#import_progress_div').prop('hidden', false);
        $("#check_progress_div").append('<div id="check_progress_bar_clear" class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
        $("#import_progress_div").append('<div class="row" style="align-items:center"><div class="col-5"><small>'+ "Clear database" +'</small></div><div class="col-6"><div id="import_progress_file_clear" class="progress"></div></div><div id="step_clear" class="col">0/1</div></div>');
        $('#import_progress_file_clear').append('<div id="import_progress_bar_file_clear" class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
        $.when().then(function(){
          return make_clean_request(
            "/metabaseEditor/project/" + projectId + "/import/check/clear",
            data, 100, percentage, btn);
        }).always(function(){
          inputField.prop("disabled", false);
          cancelButton.prop("disabled", false);
          btn.html("check");
          $("#check_progress_bar_clear").removeClass('progress-bar-animated');
          $("#import_progress_bar_file_clear").removeClass('progress-bar-animated');
        });
      }).always(function(){
        $(".progress-bar").removeClass('progress-bar-animated');
        inputField.prop("disabled", false);
        cancelButton.prop("disabled", false);
        btn.html("check");
      });
    }
  );

  $("#addExperimentForm").submit(function(e) {
    $('#checking-message').empty();
    $('#check_progress_div').empty();
    $('#import_progress_div').empty();
    var experimentTable = $("#experiment-table").DataTable();
    var projectSequencingTable = $("#project-sequencing-table").DataTable();
    var projectId = $("#selectProject").val();
    var data = new FormData($(this).get(0));
    var files_list = $("#addExperimentForm input[name=excelFilePaths]").get(0).files;
    var inputField = $(this).find("input");
    var cancelButton = $(".modal-footer", "#checkExperimentModal")
      .find("button")
      .not("#import-experiment");
    cancelButton.prop("disabled", true);
    inputField.prop("disabled", true);
    var btn = $("#import-experiment");
    btn.html(btn.attr("data-loading-text"));
    btn.prop("disabled", true);
    e.preventDefault();
    $('#import_progress_div').prop('hidden', false);
    var percent_tot = (100 / files_list.length)/ 4;
    var percent = 100 / 4;
    var decal = 1;
    var chain = $.when();
    $.each(files_list, function(index, elm){
      $("#check_progress_div").append('<div id="check_progress_bar'+index+'" class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
      $("#import_progress_div").append('<div class="row" style="align-items:center"><div class="col-5"><small>'+ elm.name +'</small></div><div class="col-6"><div id="import_progress_file'+index+'" class="progress"></div></div><div id="step'+index+'" class="col">0/4</div></div>');
      $('#import_progress_file'+index).append('<div id="import_progress_bar_file'+index+'" class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
      chain = chain.then(function(){
        decal = 1;
        var progress_val = percent * decal;
        var progress_val_tot = percent_tot * decal;
        return make_import_request(
          "/metabaseEditor/project/" + projectId + "/fileAndTube/import",
          data, index, progress_val, progress_val_tot, btn);
      }).then(function(){
        decal = decal + 1;
        var progress_val = percent * decal;
        var progress_val_tot = percent_tot * decal;
        return make_import_request(
          "/metabaseEditor/project/" + projectId + "/samples/import",
          data, index, progress_val, progress_val_tot, btn);
      }).then(function(){
        decal = decal + 1;
        var progress_val = percent * decal;
        var progress_val_tot = percent_tot * decal;
        return make_import_request(
          "/metabaseEditor/project/" + projectId + "/extracts/import",
          data, index, progress_val, progress_val_tot, btn);
      }).then(function(){
        decal = decal + 1;
        var progress_val = percent * decal;
        var progress_val_tot = percent_tot * decal;
        return make_import_request(
          "/metabaseEditor/project/" + projectId + "/pcr_plates/import",
          data, index, progress_val, progress_val_tot, btn);
      });
    });
    chain.done(function(){
      $("#ok-modal-import").prop("hidden", false);
      $("#ok-modal-import").prop("disabled", false);
      btn.html("Import");
      btn.prop("disabled", false);
      write_checking_messages(projectId, {'infos': ['All file are imported with success!']}, btn);
      btn.prop("hidden", true);
      projectSequencingTable.ajax.reload();
      experimentTable.ajax.reload();
      $("#import-experiment").prop("disabled", false);
      inputField.prop("disabled", false);
      $(".progress-bar").removeClass('progress-bar-animated');
    }).fail(function() {
      var percentage = 100;
      $('#check_progress_div').children().attr("aria-valuenow", function(i, w){
        percentage -= w;
      });
      var percentage_part = percentage/4;
      $("#check_progress_div").append('<div id="check_progress_bar_clear" class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
      $("#import_progress_div").append('<div class="row" style="align-items:center"><div class="col-5"><small>'+ "Clear database" +'</small></div><div class="col-6"><div id="import_progress_file_clear" class="progress"></div></div><div id="step_clear" class="col">0/4</div></div>');
      $('#import_progress_file_clear').append('<div id="import_progress_bar_file_clear" class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
      $.when().then(function(){
        return make_clean_request(
          "/metabaseEditor/project/" + projectId + "/samples/import/remove",
          data, 25, percentage_part, btn);
      }).then(function(){
        return make_clean_request(
          "/metabaseEditor/project/" + projectId + "/extracts/import/remove",
          data, 50, percentage_part*2, btn);
      }).then(function(){
        return make_clean_request(
          "/metabaseEditor/project/" + projectId + "/pcr_plates/import/remove",
          data, 75, percentage_part*3, btn);
      }).then(function(){
        return make_clean_request(
          "/metabaseEditor/project/" + projectId + "/fileAndTube/import/remove",
           data, 100, percentage_part*4, btn);
      }).always(function(){
        experimentTable.ajax.reload();
        inputField.prop("disabled", false);
        btn.prop("disabled", false);
        btn.html("Import");
        btn.prop("hidden", true);
        $("#ok-modal-import").prop("hidden", false);
        $("#ok-modal-import").prop("disabled", false);
        $("#check_progress_bar_clear").removeClass('progress-bar-animated');
        $("#import_progress_bar_file_clear").removeClass('progress-bar-animated');
      });
    }).always(function(){
      $(".progress-bar").not("#check_progress_bar_clear")
      .not("#import_progress_bar_file_clear")
      .removeClass('progress-bar-animated');
    });
  });

  $("#checkExperimentModal").on("hidden.bs.modal", function() {
    $("#checking-message").html("");
    $("#check-experiment").prop("disabled", false);
    $("#import-experiment").html("import");
    $("#import-experiment").prop("disabled", true);
    $("#check_progress_div").prop("hidden", true);
    $("#import_progress_div").prop("hidden", true);
    $("#excelFilePaths")
      .wrap("<form>")
      .closest("form")
      .get(0)
      .reset();
    $("#excelFilePaths").unwrap();
  });
});


function make_check_import_request(url, data, index, percent, btn, messages){
  data.set("file_index", index);
  return $.ajax({
    url: url,
    type: "POST",
    data: data,
    async: true,
    cache: false,
    processData: false,
    contentType: false,
    success: function(data) {
      if (data.warnings){
        data.warnings.forEach(function(item, i) {
          messages.warnings.push(item);
        });
      }
      if (data.infos){
        data.infos.forEach(function(item, i) {
          messages.infos.push(item);
        });
      }
    },
    error: function(jqXHR) {
      $('#check_progress_bar').removeClass("bg-success");
      $('#check_progress_bar').addClass("bg-danger");
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        displayFormError(
          "#check-experiment-modal-error",
          "#excelFilePaths",
          "#checking-message",
          btn,
          jqXHR.responseJSON
        );
      }
      else if (jqXHR.status && jqXHR.statusText){
        var msg = "An error occur: ";
        msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
        ErrorModalMsg("#checking-message", msg);
        btn.prop("disabled", true);
      }
      else{
        ErrorModalMsg(
          "#checking-message",
          "An error occur: the excel file can not be imported !");
        btn.prop("disabled", true);
      }
      $("#import-experiment").prop("disabled", true);
    },
    complete: function(){
      $('#check_progress_bar').css("width", percent + "%");
      $('#check_progress_bar').attr("aria-valuenow", percent);
    }
  });
}


function make_import_request(url, data, index, percent, percent_tot, btn){
  data.set("file_index", index);
  return $.ajax({
    url: url,
    type: "POST",
    data: data,
    async: true,
    cache: false,
    processData: false,
    contentType: false,
    success: function(data) {
      $('#check_progress_bar' + index).css("width", percent_tot + "%");
      $('#import_progress_bar_file'+ index).css('width', percent + "%");
      $('#step'+index).html(data.step_number);
      $('#check_progress_bar' + index).attr("aria-valuenow", percent_tot);
    },
    error: function(jqXHR) {
      $('#check_progress_bar'+ index).removeClass("bg-success");
      $('#check_progress_bar'+ index).addClass("bg-danger");
      $('#import_progress_bar_file'+ index).removeClass("bg-success");
      $('#import_progress_bar_file'+ index).addClass("bg-danger");
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        displayFormError(
          "#check-experiment-modal-error",
          "#excelFilePaths",
          "#checking-message",
          btn,
          jqXHR.responseJSON
        );
      }
      else {
        var msg = "An error occur: ";
        if (jqXHR.status && jqXHR.statusText){
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
        }
        else{
            msg = msg + "the excel file can not be imported !";
        }
        ErrorModalMsg("#checking-message", msg);
        btn.prop("disabled", true);
      }
      $("#import-experiment").prop("disabled", true);
    }
  });
}


function make_clean_request(url, data, percent, percent_tot, btn){
  return $.ajax({
    url: url,
    type: "POST",
    data: data,
    async: true,
    cache: false,
    processData: false,
    contentType: false,
    success: function(data) {
      $('#import_progress_bar_file_clear').css('width', percent + "%");
      $('#step_clear').html(data.step_number);
      $('#check_progress_bar_clear').css('width', percent_tot + "%");
    },
    error: function(jqXHR) {
      $('#check_progress_bar_clear').css('width', "100%");
      $('#import_progress_bar_file_clear').css('width', "100%");
      $('#import_progress_bar_file_clear').removeClass("bg-warning");
      $('#import_progress_bar_file_clear').addClass("bg-danger");
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        displayFormError(
          "#check-experiment-modal-error",
          "#excelFilePaths",
          "#checking-message",
          btn,
          jqXHR.responseJSON
        );
      }
      else {
        var msg = "An error occur: ";
        if (jqXHR.status && jqXHR.statusText){
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
        }
        else{
            msg = msg + "the excel file can not be imported !";
        }
        ErrorModalMsg("#checking-message", msg);
        btn.prop("disabled", true);
      }
      $("#import-experiment").prop("disabled", true);
    }
  });
}


function init_progress_bar(selector, progress_id){
  $(selector).append('<div id="' + progress_id + '" class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
}


function write_checking_messages(projectId, messages, btn){
  $.ajax({
    url: "/metabaseEditor/project/" + projectId + "/write_messages",
    type: "POST",
    data: { files: JSON.stringify(messages) },
    cache: false,
    success: function(data) {
      $("#checking-message").html(data);
    },
    error: function(jqXHR) {
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        displayFormError(
          "#check-experiment-modal-error",
          "#excelFilePaths",
          "#checking-message",
          btn,
          jqXHR.responseJSON
        );
      }
      else if (jqXHR.status && jqXHR.statusText){
        var msg = "An error occur: ";
        msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
        ErrorModalMsg("#checking-message", msg);
        btn.prop("disabled", true);
      }
      else{
        ErrorModalMsg(
          "#checking-message",
          "An error occur: the excel file can not be imported !");
      }
    }
  });
}
