$(document).ready(function() {
  initCSRFToken();

  $("#show-modal-add-library").click(function() {
    $("#import-sequencing").prop("disabled", true);
    $("#check-sequencing").prop("disabled", false);
    $("#add-library").prop("disabled", true);
  });

  $("#add-library-modal-body").on("change", "#sequencingFile", function() {
    $("#checking-sequencing-message").html("");
    $("#check-sequencing").prop("disabled", false);
    $("#check-sequencing-modal-error").html("");
    $("#sequencingFile").removeClass("is-invalid");
    $("#import-sequencing").prop("disabled", true);
  });

  $("#add-library-modal-body").on("click", "#check-sequencing", function() {
    var data = new FormData($("#addSequencingForm").get(0));
    var inputField = $("#addSequencingForm").find("input");
    var cancelButton = $(".modal-footer", "#addLibraryModal")
      .find("button")
      .not("#add-library");
    cancelButton.prop("disabled", true);
    inputField.prop("disabled", true);
    var btn = $(this);
    btn.html(btn.attr("data-loading-text"));
    $.ajax({
      url: "/metabaseEditor/sequencing/import/check",
      type: "POST",
      data: data,
      cache: false,
      processData: false,
      contentType: false,
      success: function(data) {
        $("#checking-sequencing-message").html(data);
        btn.prop("disabled", true);
        $("#import-sequencing").prop("disabled", false);
      },
      error: function(jqXHR) {
        if(jqXHR.responseJSON){
          user_authentication(jqXHR.responseJSON);
          displayFormError(
            "#check-sequencing-modal-error",
            "#sequencingFile",
            "#checking-sequencing-message",
            btn,
            jqXHR.responseJSON
          );
        }
        else if (jqXHR.status && jqXHR.statusText){
          var msg = "An error occur: ";
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
          ErrorModalMsg("#checking-sequencing-message", msg);
          btn.prop("disabled", true);
        }
        else{
          ErrorModalMsg(
            "#checking-sequencing-message",
            "An error occur: the excel file can not be imported");
          btn.prop("disabled", true);
        }
        $("#import-sequencing").prop("disabled", true);
      },
      complete: function() {
        inputField.prop("disabled", false);
        cancelButton.prop("disabled", false);
        btn.html("check");
      }
    });
  });

  $("#add-library-modal-body").on("submit", "#addSequencingForm", function(e) {
    var projectId = $("#selectProject").val();
    var data = new FormData($(this).get(0));
    var inputField = $(this).find("input");
    var cancelButton = $(".modal-footer", this)
      .find("button")
      .not("#import-sequencing");
    cancelButton.prop("disabled", true);
    inputField.prop("disabled", true);
    var btn = $("#import-sequencing");
    btn.html(btn.attr("data-loading-text"));
    e.preventDefault();
    $.ajax({
      url: "/metabaseEditor/project/" + projectId + "/sequencing/import",
      type: "POST",
      data: data,
      cache: false,
      processData: false,
      contentType: false,
      success: function(data) {
        buildSequencingDatatable(data.data);
        $("#messages").append(data.html_message);
      },
      error: function(jqXHR) {
        if(jqXHR.responseJSON){
          user_authentication(jqXHR.responseJSON);
          displayFormError(
            "#check-sequencing-modal-error",
            "#sequencingFile",
            "#checking-sequencing-message",
            btn,
            jqXHR.responseJSON
          );
        }
        else if (jqXHR.status && jqXHR.statusText){
          var msg = "An error occur: ";
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
          ErrorModalMsg("#checking-sequencing-message", msg);
          btn.prop("disabled", true);
        }
        else{
          ErrorModalMsg(
            "#checking-sequencing-message",
            "An error occur: the sequencing file can not be imported !");
          btn.prop("disabled", true);
        }
        $("#check-sequencing").prop("disabled", true);
      },
      complete: function() {
        inputField.prop("disabled", false);
        cancelButton.prop("disabled", false);
        btn.html("import");
      }
    });
  });

  $("#addLibraryModal").on("hidden.bs.modal", function() {
    initialiseSequencingSelection();
    $("#nav-sequencing-selection").tab("show");
    $("#nav-link-tubes-libraries").addClass("disabled");
  });

  $("#add-library-modal-body").on(
    "hidden.bs.tab",
    "#nav-link-tubes-libraries",
    function() {
      $("#add-library").prop("disabled", true);
      $("#nav-link-tubes-libraries").addClass("disabled");
    }
  );

  $("#add-library-modal-body").on(
    "shown.bs.tab",
    "#nav-link-tubes-libraries",
    function() {
      initialiseSequencingSelection();
      $("#select_all").prop("checked", true);
      $("#error-library-link-message").html("");
    }
  );

  $("#add-library-modal-body").on("submit", "#selectSequencingForm", function(
    e
  ) {
    var projectId = $("#selectProject").val();
    var data = new FormData($(this).get(0));
    var cancelButton = $(".modal-footer", this)
      .find("button")
      .not("#add-library");
    cancelButton.prop("disabled", true);
    var btn = $("#load-sequencing");
    btn.html(btn.attr("data-loading-text"));
    e.preventDefault();
    $.ajax({
      url:
        "/metabaseEditor/project/" +
        projectId +
        "/get_library_and_tube_information",
      type: "POST",
      data: data,
      cache: false,
      processData: false,
      contentType: false,
      success: buildSequencingDatatable,
      error: function(jqXHR) {
        if(jqXHR.responseJSON){
          user_authentication(jqXHR.responseJSON);
          displayFormError(
            "#load-sequencing-file-error",
            "#sequencingInfoFile",
            "#loading-sequencing-message",
            btn,
            jqXHR.responseJSON
          );
        }
        else if (jqXHR.status && jqXHR.statusText){
          var msg = "An error occur: ";
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
          ErrorModalMsg("#loading-sequencing-message", msg);
          btn.prop("disabled", true);
        }
        else{
          ErrorModalMsg(
            "#loading-sequencing-message",
            "An error occur: the library can not be loaded !");
          btn.prop("disabled", true);
        }
      },
      complete: function() {
        cancelButton.prop("disabled", false);
        btn.html("load");
      }
    });
  });

  $("#add-library-modal-body").on("change", "#sequencingInfoFile", function() {
    $("#loading-sequencing-message").html("");
    $("#load-sequencing").prop("disabled", false);
    $("#load-sequencing-file-error").html("");
    $("#sequencingInfoFile").removeClass("is-invalid");
  });

  $("#add-library").on("click", function() {
    var projectId = $("#selectProject").val();
    var projectSequencingTable = $("#project-sequencing-table").DataTable();
    var dataTableData = $("#sequencing-information-table")
      .DataTable()
      .rows(".selected")
      .data();
    var tubeId = $.map(dataTableData, function(item) {
      var lib = {
        id: item.id,
        library: item.library_name,
        tube: item.tube_name
      };
      return lib;
    });
    var cancelButton = $(".modal-footer", this)
      .find("button")
      .not("#add-library");
    cancelButton.prop("disabled", true);
    $(this).html($(this).attr("data-loading-text"));
    $.ajax({
      url: "/metabaseEditor/project/" + projectId + "/sequencing/library/link",
      type: "POST",
      dataType: "json",
      data: { rows: JSON.stringify(tubeId) },
      success: function(data) {
        data.data.forEach(function(d) {
          projectSequencingTable.row.add(d).draw(false);
        });
        projectSequencingTable.columns.adjust().draw();
        $("#messages").append(data.html_message);
        $("#sequencingSelection > .card-body").html(data.html_form);
        $("#addLibraryModal").modal("toggle");
      },
      error: function(jqXHR) {
        if(jqXHR.responseJSON){
          user_authentication(jqXHR.responseJSON);
          $("#error-library-link-message").html(jqXHR.responseJSON.html);
        }
        else if (jqXHR.status && jqXHR.statusText){
          var msg = "An error occur: ";
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
          ErrorModalMsg("#error-library-link-message", msg);
        }
        else{
          ErrorModalMsg(
            "#error-library-link-message",
            "An error occur: the library can not be linked !");
        }
      },
      complete: function() {
        $("#add-library").prop("disabled", true);
        cancelButton.prop("disabled", false);
        $("#add-library").html("add");
      }
    });
  });

  $("#add-library-modal-body").on("click", "#select_all", function() {
    if ($("#select_all").is(":checked")) {
      $("#sequencing-information-table")
        .DataTable()
        .rows()
        .select();
    } else {
      $("#sequencing-information-table")
        .DataTable()
        .rows()
        .deselect();
    }
  });
});

function initialiseSequencingSelection() {
  $("#sequencingSelection").collapse("show");
  $("#sequencingImport").collapse("hide");
  $("#checking-sequencing-message").html("");
  $("#loading-sequencing-message").html("");
  $("#check-sequencing").prop("disabled", false);
  $("#load-sequencing").prop("disabled", false);
  $("#import-sequencing").prop("disabled", true);
  $("#sequencingFile")
    .wrap("<form>")
    .closest("form")
    .get(0)
    .reset();
  $("#sequencingFile").unwrap();
  $("#sequencingInfoFile")
    .wrap("<form>")
    .closest("form")
    .get(0)
    .reset();
  $("#sequencingInfoFile").unwrap();
}

function buildSequencingDatatable(data) {
  if ($.fn.DataTable.isDataTable("#sequencing-information-table")) {
    $("#sequencing-information-table")
      .DataTable()
      .destroy();
  }
  var seqInfoTab = $("#nav-link-tubes-libraries");
  seqInfoTab.closest(".disabled").removeClass("disabled");
  seqInfoTab.off("shown.bs.tab").on("shown.bs.tab", function() {
    var libraryTable = $("#sequencing-information-table").DataTable({
      data: data.data,
      scrollY: 200,
      scrollCollapse: true,
      paging: false,
      columns: [
        { defaultContent: "" },
        { data: "library_name" },
        { data: "tube_name" }
      ],
      columnDefs: [
        {
          orderable: false,
          className: "select-checkbox text-center",
          targets: 0
        }
      ],
      select: { style: "multi" },
      createdRow: function(row, data) {
        $(row).attr({
          "data-id": data.id
        });
      },
      initComplete: function() {
        this.api()
          .rows()
          .select();
      },
      order: [1, "asc"]
    });

    libraryTable
      .on("select", function(e, dt, type, indexes) {
        var selectedRows = libraryTable.rows(".selected").count();
        if (selectedRows > 0) {
          $("#add-library").prop("disabled", false);
        }
      })
      .on("deselect", function(e, dt, type, indexes) {
        var selectedRows = libraryTable.rows(".selected").count();
        if (selectedRows == 0) {
          $("#add-library").prop("disabled", true);
        }
      });
  });
  seqInfoTab.tab("show");
  if (data.data.length > 0) {
    $("#add-library").prop("disabled", false);
  }
}
