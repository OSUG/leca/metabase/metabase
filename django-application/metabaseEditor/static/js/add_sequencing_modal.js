$(document).ready(function() {
  // get and initialize the CSRF token for the ajax request
  initCSRFToken();

  $("#sequencingFile").change(function() {
    $("#checking-sequencing-message").html("");
    $("#check-sequencing").prop("disabled", false);
    $("#check-sequencing-modal-error").html("");
    $("#sequencingFile").removeClass("is-invalid");
    $("#import-sequencing").prop("disabled", true);
  });

  $("#check-sequencing-modal-body").on(
    "click",
    "#check-sequencing",
    function() {
      var data = new FormData($("#addSequencingForm").get(0));
      var inputField = $("#addSequencingForm").find("input");
      var cancelButton = $(".modal-footer", "#addSequencingModal")
        .find("button")
        .not("#import-sequencing");
      cancelButton.prop("disabled", true);
      inputField.prop("disabled", true);
      var btn = $(this);
      btn.html(btn.attr("data-loading-text"));
      $.ajax({
        url: "/metabaseEditor/sequencing/import/check",
        type: "POST",
        data: data,
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
          $("#checking-sequencing-message").html(data);
          btn.prop("disabled", true);
          $("#import-sequencing").prop("disabled", false);
        },
        error: function(jqXHR) {
          if(jqXHR.responseJSON){
            user_authentication(jqXHR.responseJSON);
            displayFormError(
              "#check-sequencing-modal-error",
              "#sequencingFile",
              "#checking-sequencing-message",
              btn,
              jqXHR.responseJSON
            );
          }
          else if (jqXHR.status && jqXHR.statusText){
            var msg = "An error occur: ";
            msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
            ErrorModalMsg("#checking-sequencing-message", msg);
            btn.prop("disabled", true);
          }
          else{
            ErrorModalMsg(
              "#checking-sequencing-message",
              "An error occur: the sequencing file can not be imported !");
            btn.prop("disabled", true);
          }
          $("#import-sequencing").prop("disabled", true);
        },
        complete: function() {
          inputField.prop("disabled", false);
          cancelButton.prop("disabled", false);
          btn.html("check");
        }
      });
    }
  );

  $("#addSequencingForm").submit(function(e) {
    var data = new FormData($(this).get(0));
    var inputField = $(this).find("input");
    var cancelButton = $(".modal-footer", this)
      .find("button")
      .not("#import-sequencing");
    cancelButton.prop("disabled", true);
    inputField.prop("disabled", true);
    var btn = $("#import-sequencing");
    btn.html(btn.attr("data-loading-text"));
    e.preventDefault();
    $.ajax({
      url: "/metabaseEditor/sequencing/import",
      type: "POST",
      data: data,
      cache: false,
      processData: false,
      contentType: false,
      success: function(data) {
        data.data.forEach(function(d) {
          $("#sequencing-table").DataTable().row.add(d).draw(false);
        });
        $("#messages").append(data.html);
        $("#addSequencingModal").modal("toggle");
      },
      error: function(jqXHR) {
        if(jqXHR.responseJSON){
          user_authentication(jqXHR.responseJSON);
          displayFormError(
            "#check-sequencing-modal-error",
            "#sequencingFile",
            "#checking-sequencing-message",
            btn,
            jqXHR.responseJSON
          );
        }
        else if (jqXHR.status && jqXHR.statusText){
          var msg = "An error occur: ";
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
          ErrorModalMsg("#checking-sequencing-message", msg);
          btn.prop("disabled", true);
        }
        else{
          ErrorModalMsg(
            "#checking-sequencing-message",
            "An error occur: the sequencing file can not be imported !");
          btn.prop("disabled", true);
        }
        $("#check-sequencing").prop("disabled", true);
      },
      complete: function() {
        inputField.prop("disabled", false);
        cancelButton.prop("disabled", false);
        btn.html("Import");
      }
    });
  });

  $("#addSequencingModal").on("hidden.bs.modal", function() {
    $("#checking-sequencing-message").html("");
    $("#check-sequencing").prop("disabled", false);
    $("#import-sequencing").prop("disabled", true);
    $("#sequencingFile")
      .wrap("<form>")
      .closest("form")
      .get(0)
      .reset();
    $("#sequencingFile").unwrap();
  });

});
