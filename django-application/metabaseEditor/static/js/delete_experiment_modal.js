$(document).ready(function() {
  initCSRFToken();

  var spinner = "<i class=\"fa fa-spinner fa-spin fa-4x text-success\"></i>";

  $("#deleteExperimentModal").on("show.bs.modal", function() {
    $('#del_info_progress_div').empty();
    $('#delete_progress_div').empty();
    init_progress_bar('#del_info_progress_div', 'del_info_progress_bar');
    var experimentTable=$("#experiment-table").DataTable();
    var projectId = $("#selectProject").val();
    var experimentId = experimentTable.row(".selected").data().id;
    var experimentFile = experimentTable.row(".selected").data().file_name;
    $("#delete-experiment-question").html(
      "<p>Are you sure you want to delete the experiment file " +
        experimentFile +
        "?</p>"
    );
    var btn = $("#delete-experiment");
    var cancelBtn = $("#cancel-modal-delete");
    btn.prop("disabled", true);
    cancelBtn.prop("disabled", true);
    btn.html(btn.attr("data-loading-text"));
    $('#del_info_progress_div').prop('hidden', false);
    var percent = 100 / 5;
    var chain = $.when();
    var decal = 1;
    var messages = {'warnings': []};
    chain = chain.then(function(){
      var progress_val = percent * decal;
      return make_info_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/libraries/delete/information",
        progress_val, messages);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      return make_info_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/tubes/delete/information",
        progress_val, messages);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      return make_info_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/plates/delete/information",
        progress_val, messages);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      return make_info_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/samples/delete/information",
        progress_val, messages);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      return make_info_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/extracts/delete/information",
        progress_val, messages);
    });
    chain.done(function(){
      write_delete_messages(projectId, messages, btn);
      btn.prop("disabled", false);
      cancelBtn.prop("disabled", false);
    }).always(function(){
      $("#del_info_progress_bar").removeClass('progress-bar-animated');
      btn.html("Delete");
    });
  });

  $("#delete-experiment").click(function() {
    $('#del_info_progress_div').empty();
    init_progress_bar('#del_info_progress_div', 'del_info_progress_bar');
    $('#delete-experiment-message').empty();
    $('#delete_progress_div').prop('hidden', false);
    var experimentTable=$("#experiment-table").DataTable();
    var projectId = $("#selectProject").val();
    var experimentId = experimentTable.row(".selected").data().id;
    var btn = $(this);
    var cancelBtn =$("#cancel-modal-delete");
    btn.prop("disabled", true);
    btn.html(btn.attr("data-loading-text"));
    cancelBtn.prop("disabled", true);

    var percent_identify = 100 / 5;
    var percent_delete = 100 / 6;
    var percent_tot = 100 / 11;
    var decal = 0;
    var chain = $.when();
    var file_name = experimentTable.row('.selected').data().file_name;
    $("#delete_progress_div").append('<div class="row" style="align-items:center"><div class="col-5"><small>'+ "Deletion preparation" +'</small></div><div class="col-6"><div id="prepare_delete_progress_file" class="progress"></div></div><div id="step_identify" class="col">0/5</div></div>');
    $('#prepare_delete_progress_file').append('<div id="prepare_delete_progress_bar" class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
    $("#delete_progress_div").append('<div class="row" style="align-items:center"><div class="col-5"><small>'+ "Deletion" +'</small></div><div class="col-6"><div id="delete_progress_file" class="progress"></div></div><div id="step_delete" class="col">0/6</div></div>');
    $('#delete_progress_file').append('<div id="delete_progress_bar" class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
    chain = chain.then(function(){
      decal = decal + 1;
      var progress_val = percent_identify * decal;
      var progess_tot = percent_tot * decal;
      return make_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/file/delete/identify", progress_val, progess_tot,
        "#prepare_delete_progress_bar", "#step_identify");
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent_identify * decal;
      var progess_tot = percent_tot * decal;
      return make_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/tubes/delete/identify", progress_val, progess_tot,
        "#prepare_delete_progress_bar", "#step_identify");
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent_identify * decal;
      var progess_tot = percent_tot * decal;
      return make_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/plates/delete/identify", progress_val, progess_tot,
        "#prepare_delete_progress_bar", "#step_identify");
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent_identify * decal;
      var progess_tot = percent_tot * decal;
      return make_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/samples/delete/identify", progress_val, progess_tot,
        "#prepare_delete_progress_bar", "#step_identify");
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent_identify * decal;
      var progess_tot = percent_tot * decal;
      return make_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/extracts/delete/identify", progress_val, progess_tot,
        "#prepare_delete_progress_bar", "#step_identify");
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent_delete * (decal - 5);
      var progess_tot = percent_tot * decal;
      return make_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/plates/delete", progress_val, progess_tot,
        "#delete_progress_bar", "#step_delete");
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent_delete * (decal - 5);
      var progess_tot = percent_tot * decal;
      return make_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/primers/delete", progress_val, progess_tot,
        "#delete_progress_bar", "#step_delete");
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent_delete * (decal - 5);
      var progess_tot = percent_tot * decal;
      return make_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/samples/delete", progress_val, progess_tot,
        "#delete_progress_bar", "#step_delete");
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent_delete * (decal - 5);
      var progess_tot = percent_tot * decal;
      return make_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/controls/delete", progress_val, progess_tot,
        "#delete_progress_bar", "#step_delete");
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent_delete * (decal - 5);
      var progess_tot = percent_tot * decal;
      return make_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/tubes/delete", progress_val, progess_tot,
        "#delete_progress_bar", "#step_delete");
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent_delete * (decal - 5);
      var progess_tot = percent_tot * decal;
      return make_delete_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" +
        experimentId + "/file/delete", progress_val, progess_tot,
        "#delete_progress_bar", "#step_delete");
    });
    chain.done(function(){
      $("#ok-modal-delete").prop("hidden", false);
      $("#ok-modal-delete").prop("disabled", false);
      btn.prop("hidden", true);
      write_delete_messages(projectId,
        {'infos': ['The experiment was removed with success!']}, btn);
      experimentTable.row(".selected").remove().draw(false);
      $("#project-sequencing-table").DataTable().ajax.reload();
      $(".progress-bar").removeClass('progress-bar-animated');
      $("#show-modal-delete-experiment").prop("disabled", true);
      $("#download-experiment").prop("disabled", true);
    }).fail(function() {
      var percentage = 100;
      $('#del_info_progress_div').children().attr("aria-valuenow", function(i, w){
        percentage -= w;
      });
      var percentage_part = percentage/2;
      $("#del_info_progress_div").append('<div id="del_info_progress_bar_clear" class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
      $("#delete_progress_div").append('<div class="row" style="align-items:center"><div class="col-5"><small>'+ "Clear database" +'</small></div><div class="col-6"><div id="delete_progress_file_clear" class="progress"></div></div><div id="step_clear" class="col">0/3</div></div>');
      $('#delete_progress_file_clear').append('<div id="delete_progress_bar_file_clear" class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
      $.when().then(function(){
        return make_clean_delete_request(
          "/metabaseEditor/project/" + projectId + "/experiment/" +
          experimentId + "/unlink_tube_and_pcrs",
          50, percentage_part, btn);
      }).then(function(){
        return make_clean_delete_request(
          "/metabaseEditor/project/" + projectId + "/experiment/" +
          experimentId + "/unlink_exp_tube",
          50, percentage_part, btn);
      }).then(function(){
        return make_clean_delete_request(
          "/metabaseEditor/project/" + projectId + "/experiment/" +
          experimentId + "/remove_exp_tube",
          100, percentage_part*2, btn);
      }).always(function(){
        experimentTable.ajax.reload();
        $("#project-sequencing-table").DataTable().ajax.reload();
        $("#show-modal-delete-experiment").prop("disabled", true);
        $("#download-experiment").prop("disabled", true);
        btn.prop("hidden", true);
        $("#ok-modal-delete").prop("hidden", false);
        $("#ok-modal-delete").prop("disabled", false);
        $("#del_info_progress_bar_clear").removeClass('progress-bar-animated');
        $("#delete_progress_bar_file_clear").removeClass('progress-bar-animated');
      });
    }).always(function(){
      $(".progress-bar").not("#del_info_progress_bar_clear")
      .not("#delete_progress_bar_file_clear")
      .removeClass('progress-bar-animated');
    });
  });

  $("#deleteExperimentModal").on("hidden.bs.modal", function() {
    $("#ok-modal-delete").prop("hidden", true);
    $("#ok-modal-delete").prop("disabled", true);
    $("#delete-experiment-message").html("");
    $("#delete-experiment").html("Delete");
    $("#delete-experiment").prop("hidden", false);
    $("#del_info_progress_div").prop("hidden", true);
    $("#delete_progress_div").prop("hidden", true);
  });

});

function make_info_delete_request(url, percent, messages){
  return $.ajax({
    url: url,
    type: "POST",
    async: true,
    cache: false,
    processData: false,
    contentType: false,
    success: function(data) {
      if (data.warnings){
        data.warnings.forEach(function(item, i) {
          messages.warnings.push(item);
        });
      }
      if (data.infos){
        data.infos.forEach(function(item, i) {
          messages.infos.push(item);
        });
      }
    },
    error: function(jqXHR) {
      $('#del_info_progress_bar').removeClass("bg-success");
      $('#del_info_progress_bar').addClass("bg-danger");
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        $("#delete-experiment-message").html(jqXHR.responseJSON.html);
      }
      else if (jqXHR.status && jqXHR.statusText){
        var msg = "An error occur: ";
        msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
        ErrorModalMsg("#delete-experiment-message", msg);
      }
      else{
        ErrorModalMsg(
          "#delete-experiment-message",
          "An error occur: the experiment information can not be loaded !");
      }
      $("#delete-experiment").prop("disabled", true);
    },
    complete: function(){
      $('#del_info_progress_bar').css("width", percent + "%");
    }
  });
}


function make_delete_request(url, percent, percent_tot, progress_bar_id,
step_id){
  return $.ajax({
    url: url,
    type: "POST",
    async: true,
    cache: false,
    processData: false,
    contentType: false,
    success: function(data) {
      $('#del_info_progress_bar').css("width", percent_tot + "%");
      $('#del_info_progress_bar').attr("aria-valuenow", percent_tot);
      $(progress_bar_id).css('width', percent + "%");
      $(step_id).html(data.step_number);
    },
    error: function(jqXHR) {
      $('#del_info_progress_bar').removeClass("bg-success");
      $('#del_info_progress_bar').addClass("bg-danger");
      $(progress_bar_id).removeClass("bg-success");
      $(progress_bar_id).addClass("bg-danger");
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        $("#delete-experiment-message").html(jqXHR.responseJSON.html);
      }
      else {
        var msg = "An error occur: ";
        if (jqXHR.status && jqXHR.statusText){
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
        }
        else{
            msg = msg + "the experiement file can not be delete !";
        }
        ErrorModalMsg("#delete-experiment-message", msg);
      }
      $("#delete-experiment").prop("disabled", true);
    }
  });
}


function make_clean_delete_request(url, percent, percent_tot, btn){
  return $.ajax({
    url: url,
    type: "POST",
    async: true,
    cache: false,
    processData: false,
    contentType: false,
    success: function(data) {
      $('#delete_progress_bar_file_clear').css('width', percent + "%");
      $('#step_clear').html(data.step_number);
      $('#del_info_progress_bar_clear').css('width', percent_tot + "%");
    },
    error: function(jqXHR) {
      $("#delete-experiment-message").append("<div id='clear-delete-message'></div>");
      $('#del_info_progress_bar_clear').css('width', "100%");
      $('#del_info_progress_bar_clear').removeClass("bg-warning");
      $('#del_info_progress_bar_clear').addClass("bg-danger");
      $('#delete_progress_bar_file_clear').css('width', "100%");
      $('#delete_progress_bar_file_clear').removeClass("bg-warning");
      $('#delete_progress_bar_file_clear').addClass("bg-danger");
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        $("#clear-delete-message").append(jqXHR.responseJSON.html);
      }
      else {
        var msg = "An error occur: ";
        if (jqXHR.status && jqXHR.statusText){
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
        }
        else{
            msg = msg + "the experiement file can not be delete !";
        }
        ErrorModalMsg("#clear-delete-message", msg, " - Clear database");
        btn.prop("disabled", true);
      }
      $("#delete-experiment").prop("disabled", true);
    }
  });
}


function init_progress_bar(selector, progress_id){
  $(selector).append('<div id="' + progress_id + '" class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
}

function write_delete_messages(projectId, messages, btn){
  $.ajax({
    url: "/metabaseEditor" + "/project/" +
      projectId + "/write_delete_messages",
    type: "POST",
    data: { infos: JSON.stringify(messages) },
    cache: false,
    success: function(data) {
      $("#delete-experiment-message").html(data);
    },
    error: function(jqXHR) {
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        $("#delete-experiment-message").html(jqXHR.responseJSON.html);
      }
      else if (jqXHR.status && jqXHR.statusText){
        var msg = "An error occur: ";
        msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
        ErrorModalMsg("#delete-experiment-message", msg);
        btn.prop("disabled", true);
      }
      else{
        ErrorModalMsg(
          "#delete-experiment-message",
          "An error occur: the experiment information can not be loaded !");
      }
    }
  });
}
