$(document).ready(function() {
  initCSRFToken();

  $("#show-modal-delete-sequencing").on("click", function() {
    var projectSequencingTable = $("#project-sequencing-table").DataTable();
    var dataTableData = projectSequencingTable.rows(".selected").data();
    var librariesId = $.map(dataTableData, function(item) {
      return item.id;
    });
    $("#delete-library-question").html(
      "<p>Are you sure you want to delete the libraries " +
        librariesId.join(", ") +
        "?</p>"
    );
  });

  $("#delete-library").click(function() {
    var projectId = $("#selectProject").val();
    var projectSequencingTable = $("#project-sequencing-table").DataTable();
    var dataTableData = projectSequencingTable.rows(".selected").data();
    var libraryId = $.map(dataTableData, function(item) {
      return item.id;
    });
    var btn = $(this).find("#delete-library");
    btn.html(btn.attr("data-loading-text"));
    $.ajax({
      url: "/metabaseEditor/project/" + projectId + "/library/unlink",
      type: "POST",
      dataType: "json",
      data: { libraries: JSON.stringify(libraryId) },
      success: function(data) {
        if (data.status == "SUCCESS") {
          projectSequencingTable
            .rows(".selected")
            .remove()
            .draw(false);
          projectSequencingTable.columns.adjust().draw();
          $("#sequencingSelection > .card-body").html(data.html_form);
          $("#messages").append(data.html_message);
          $("#deleteLibraryModal").modal("toggle");
        }
      },
      error: function(jqXHR) {
        $("#deleteLibraryModal > .modal-dialog").addClass("modal-lg");
        if(jqXHR.responseJSON){
          user_authentication(jqXHR.responseJSON);
          $("#delete-library-message").html(jqXHR.responseJSON.html);
        }
        else if (jqXHR.status && jqXHR.statusText){
          var msg = "An error occur: ";
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
          ErrorModalMsg("#delete-library-message", msg);
        }
        else{
          ErrorModalMsg(
            "#delete-library-message",
            "An error occur: the library can not be deleted !");
        }
        $("#delete-library").prop("disabled", true);
      },
      complete: function() {
        btn.html("Delete");
      }
    });

    $("#show-modal-delete-sequencing").prop("disabled", true);
  });

  $("#deleteLibraryModal").on("hidden.bs.modal", function() {
    $("#delete-library-message").html("");
    $(this)
      .find(".modal-dialog")
      .removeClass("modal-lg");
    $("#delete-library").prop("disabled", false);
  });

});
