$(document).ready(function() {
  initCSRFToken();

  var spinner = "<i class=\"fa fa-spinner fa-spin fa-4x text-success\"></i>";


  $("#deleteProjectModal").on("show.bs.modal", function() {
    $('#del-project-info-progress-div').empty();
    $('#delete-project-progress-div').empty();
    init_progress_bar('#del-project-info-progress-div', 'del-project-info-progress-bar');
    var projectId = $("#selectProject").val();
    var projectName = $("#selectProject option:selected").text();
    $("#delete-project-question").html(
      "<p>Are you sure you want to delete the project " + projectName + "?</p>"
    );
    $("#delete-project-message").html(
      "<div class=\"text-center\">" + spinner + "</div>"
    );
    var btn = $(".del-project");
    var cancelBtn = $("#cancel-modal-project-delete");
    btn.prop("disabled", true);
    cancelBtn.prop("disabled", true);
    btn.each(function() {
      $(this).data('original-text', $(this).text());
      $(this).html($(this).attr("data-loading-text"));
    });
    $('#del-project-info-progress-div').prop('hidden', false);
    var percent = 100 / 7;
    var chain = $.when();
    var decal = 1;
    var messages = {'warnings': []};
    chain = chain.then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      return make_info_project_delete_request(
        "/metabaseEditor/project/" + projectId +
        "/subproject/delete/information",
        progress_val, messages);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      return make_info_project_delete_request(
        "/metabaseEditor/project/" + projectId +
        "/experiment-file/delete/information",
        progress_val, messages);
    }).then(function(){
      var progress_val = percent * decal;
      return make_info_project_delete_request(
        "/metabaseEditor/project/" + projectId +
        "/libraries/delete/information",
        progress_val, messages);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      return make_info_project_delete_request(
        "/metabaseEditor/project/" + projectId + "/tubes/delete/information",
        progress_val, messages);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      return make_info_project_delete_request(
        "/metabaseEditor/project/" + projectId + "/plates/delete/information",
        progress_val, messages);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      return make_info_project_delete_request(
        "/metabaseEditor/project/" + projectId + "/samples/delete/information",
        progress_val, messages);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      return make_info_project_delete_request(
        "/metabaseEditor/project/" + projectId +
        "/extracts/delete/information",
        progress_val, messages);
    });
    chain.done(function(){
      write_delete_project_messages(projectId, messages, btn);
      btn.prop("disabled", false);
      cancelBtn.prop("disabled", false);
    }).always(function(){
      $("#del-project-info-progress-bar").removeClass('progress-bar-animated');
      btn.each(function() {
        $(this).html($(this).data('original-text'));
        $(this).removeData('original-text');
      });
      cancelBtn.prop("disabled", false);
    });
  });

  $("#delete-project").click(function() {
    $('#del-project-info-progress-div').empty();
    init_progress_bar('#del-project-info-progress-div', 'del-project-info-progress-bar');
    $('#delete-project-message').empty();
    $('#delete-project-progress-div').prop('hidden', false);
    var projectId = $("#selectProject").val();
    var btn = $(this);
    var cancelBtn = $("#cancel-modal-project-delete");
    $('.del-project').prop("disabled", true);
    btn.data('original-text', btn.text());
    btn.html(btn.attr("data-loading-text"));
    cancelBtn.prop("disabled", true);
    $.ajax({
      url: "/metabaseEditor/project/" + projectId + "/experiment_list",
      type: "POST",
      async: true,
      cache: false,
      processData: false,
      contentType: false,
      success: function(data) {
        remove_project(projectId, data, btn, cancelBtn);
      },
      error: function(jqXHR) {
        $('#del-project-info-progress-bar').removeClass("bg-success");
        $('#del-project-info-progress-bar').addClass("bg-danger");
        if (jqXHR.responseJSON){
          user_authentication(jqXHR.responseJSON);
          $("#delete-project-message").html(jqXHR.responseJSON.html);
        }
        else if (jqXHR.status && jqXHR.statusText){
          var msg = "An error occur: ";
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
          ErrorModalMsg("#delete-project-message", msg);
        }
        else{
          ErrorModalMsg(
            "#delete-project-message",
            "An error occur: the project can not be deleted !");
        }
        btn.html(btn.data('original-text'));
        btn.removeData('original-text');
        $('.del-project').prop("disabled", true);
        $('.del-project').prop("hidden", true);
        $("#ok-modal-project-delete").prop("hidden", false);
        $("#ok-modal-project-delete").prop("disabled", false);
      }
    });
  });


  $("#delete-parent-project").click(function() {
    $('#del-project-info-progress-div').empty();
    init_progress_bar('#del-project-info-progress-div', 'del-project-info-progress-bar');
    $('#delete-project-message').empty();
    $('#delete-project-progress-div').prop('hidden', false);
    var projectId = $("#selectProject").val();
    var btn = $(this);
    var cancelBtn = $("#cancel-modal-project-delete");
    $('.del-project').prop("disabled", true);
    btn.data('original-text', btn.text());
    btn.html(btn.attr("data-loading-text"));
    cancelBtn.prop("disabled", true);
    $("#delete-project-progress-div").append('<div class="row" style="align-items:center"><div class="col-5"><small>delete project</small></div><div class="col-6"><div id="delete-project-progress-parent-project"' +
    ' class="progress"></div></div><div id="delete-parent-project" class="col step-div">0/2</div></div>');
    $('#delete-project-progress-parent-project').append('<div id="delete-progress-bar-parent-project"' +
    ' class="progress-bar progress-bar-striped progress-bar-animated bg-success"' +
    ' role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
    var chain = $.when().then(function(){
      return make_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/parent-project/delete/identify",
        50, 50, "#delete-progress-bar-parent-project",
        "#delete-parent-project", 1, 2);
    }).then(function(){
      return make_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/parent-project/delete",
        100, 100, "#delete-progress-bar-parent-project",
        "#delete-parent-project", 2, 2);
    }).done(function(){
      location.href = "/metabaseEditor/project";
    }).fail(function() {
      $("#del-project-info-progress-div").append('<div id="del-info-progress-bar-clear" class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
      $("#delete-project-progress-div").append('<div class="row" style="align-items:center"><div class="col-5"><small>'+ "Clear database" +'</small></div><div class="col-6"><div id="delete-project-progress-clear" class="progress"></div></div><div id="step_project_clear" class="col step-div">0/1</div></div>');
      $('#delete-project-progress-clear').append('<div id="delete-project-progress-clear-bar" class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
      $.when(function(){
        return make_clean_delete_project_request(
          "/metabaseEditor/project/" + projectId + "/parent-project/delete",
          100, 100, btn, 1, 1);
      }).done(function(){
        $("#ok-modal-project-delete").on('click', function(){
          location.href = "/metabaseEditor/project";
        });
      }).always(function(){
        $('.del-project').prop("hidden", true);
        $("#ok-modal-project-delete").prop("hidden", false);
        $("#ok-modal-project-delete").prop("disabled", false);
        $("#del-info-progress-bar-clear").removeClass('progress-bar-animated');
        $("#delete-project-progress-clear-bar").removeClass('progress-bar-animated');
      });
    }).always(function(){
      $(".progress-bar").not("#del-info-progress-bar-clear")
      .not("#delete-project-progress-clear-bar")
      .removeClass('progress-bar-animated');
    });
  });

  $("#deleteProjectModal").on("hidden.bs.modal", function() {
    $("#delete-project-message").html("");
    $('.del-project').prop("disabled", true);
  });
});


function make_info_project_delete_request(url, percent, messages){
  return $.ajax({
    url: url,
    type: "POST",
    async: true,
    cache: false,
    processData: false,
    contentType: false,
    success: function(data) {
      if (data.warnings){
        data.warnings.forEach(function(item, i) {
          messages.warnings.push(item);
        });
      }
      if (data.infos){
        data.infos.forEach(function(item, i) {
          messages.infos.push(item);
        });
      }
    },
    error: function(jqXHR) {
      $('#del-project-info-progress-bar').removeClass("bg-success");
      $('#del-project-info-progress-bar').addClass("bg-danger");
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        $("#delete-project-message").html(jqXHR.responseJSON.html);
      }
      else if (jqXHR.status && jqXHR.statusText){
        var msg = "An error occur: ";
        msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
        ErrorModalMsg("#delete-project-message", msg);
      }
      else{
        ErrorModalMsg(
          "#delete-project-message",
          "An error occur: the project information can not be loaded !");
      }
      $(".del-project").prop("disabled", true);
    },
    complete: function(){
      $('#del-project-info-progress-bar').css("width", percent + "%");
    }
  });
}

function init_progress_bar(selector, progress_id){
  $(selector).append('<div id="' + progress_id + '" class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
}

function write_delete_project_messages(projectId, messages, btn){
  url = "/metabaseEditor" + "/project/write_delete_messages";
  if(projectId != null){
    url = "/metabaseEditor" + "/project/" +
      projectId + "/write_delete_messages";
  }
  $.ajax({
    url: url,
    type: "POST",
    data: { infos: JSON.stringify(messages) },
    cache: false,
    success: function(data) {
      $("#delete-project-message").html(data);
    },
    error: function(jqXHR) {
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        $("#delete-project-message").html(jqXHR.responseJSON.html);
      }
      else if (jqXHR.status && jqXHR.statusText){
        var msg = "An error occur: ";
        msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
        ErrorModalMsg("#delete-project-message", msg);
        btn.prop("disabled", true);
      }
      else{
        ErrorModalMsg(
          "#delete-project-message",
          "An error occur: the project information can not be loaded !");
      }
    }
  });
}


function remove_project(projectId, data, btn, cancelBtn){
  $("#delete-project-progress-div").append('<div class="row" style="align-items:center"><div class="col-5"><small>Deletion preparation</small></div><div class="col-6"><div id="delete-project-progress-id-project"' +
  ' class="progress"></div></div><div id="step-id-project" class="col step-div">0/1</div></div>');
  $('#delete-project-progress-id-project').append('<div id="delete-progress-bar-project"' +
  ' class="progress-bar progress-bar-striped progress-bar-animated bg-success"' +
  ' role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
  var percent = 100 / 11;
  var percent_tot = 100 / (11 * data.nb_files + 2);
  var progess_tot = percent_tot;
  var chain = $.when().then(function(){
    return make_delete_project_request(
      "/metabaseEditor/project/" + projectId + "/delete/identify",
      100, percent_tot, "#delete-progress-bar-project",
      "#step-id-project", 1, 1);
  });
  $.each(data.files_list, function(index, elm){
    var decal = 1 * (index + 1);
    chain = chain.then(function(){
      $("#delete-project-progress-div").append('<div class="row" style="align-items:center"><div class="col-5"><small>'+
      elm.file_name + '</small></div><div class="col-6"><div id="delete-project-progress-file' +
      index + '" class="progress"></div></div><div id="step_delete' + index + '" class="col step-div">0/11</div></div>');
      $('#delete-project-progress-file' + index).append('<div id="delete-progress-bar-file' +
      index + '" class="progress-bar progress-bar-striped progress-bar-animated bg-success"' +
      ' role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
      decal = decal + 1;
      var progress_val = percent * decal;
      progess_tot += percent_tot;
      return make_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" + elm.id +
        "/file/delete/identify", progress_val, progess_tot,
        "#delete-progress-bar-file" + index, "#step_delete" + index, 1, 11);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      progess_tot += percent_tot;
      return make_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" + elm.id +
        "/tubes/delete/identify", progress_val, progess_tot,
        "#delete-progress-bar-file" + index, "#step_delete" + index, 2, 11);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      progess_tot += percent_tot;
      return make_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" + elm.id +
        "/plates/delete/identify", progress_val, progess_tot,
        "#delete-progress-bar-file" + index, "#step_delete" + index, 3, 11);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      progess_tot += percent_tot;
      return make_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" + elm.id +
        "/samples/delete/identify", progress_val, progess_tot,
        "#delete-progress-bar-file" + index, "#step_delete" + index, 4, 11);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      progess_tot += percent_tot;
      return make_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" + elm.id +
        "/extracts/delete/identify", progress_val, progess_tot,
        "#delete-progress-bar-file" + index, "#step_delete" + index, 5, 11);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      progess_tot += percent_tot;
      return make_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" + elm.id +
        "/plates/delete", progress_val, progess_tot,
        "#delete-progress-bar-file" + index, "#step_delete" + index, 6, 11);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      progess_tot += percent_tot;
      return make_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" + elm.id +
        "/primers/delete", progress_val, progess_tot,
        "#delete-progress-bar-file" + index, "#step_delete" + index, 7, 11);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      progess_tot += percent_tot;
      return make_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" + elm.id +
        "/samples/delete", progress_val, progess_tot,
        "#delete-progress-bar-file" + index, "#step_delete" + index, 8, 11);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      progess_tot += percent_tot;
      return make_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" + elm.id +
        "/controls/delete", progress_val, progess_tot,
        "#delete-progress-bar-file" + index, "#step_delete" + index, 9, 11);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      progess_tot += percent_tot;
      return make_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" + elm.id +
        "/tubes/delete", progress_val, progess_tot,
        "#delete-progress-bar-file" + index, "#step_delete" + index, 10, 11);
    }).then(function(){
      decal = decal + 1;
      var progress_val = percent * decal;
      progess_tot += percent_tot;
      return make_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/experiment/" + elm.id +
        "/file/delete", progress_val, progess_tot,
        "#delete-progress-bar-file" + index, "#step_delete" + index, 11, 11);
    });
  });
  chain = chain.then(function(){
    $("#delete-project-progress-div").append('<div class="row" style="align-items:center"><div class="col-5"><small>delete project</small></div><div class="col-6"><div id="delete-project-progress-del-project" class="progress"></div></div><div id="step-del-project" class="col step-div">0/1</div></div>');
    $('#delete-project-progress-del-project').append('<div id="delete-progress-bar-del-project"' +
    ' class="progress-bar progress-bar-striped progress-bar-animated bg-success"' +
    ' role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
    return make_delete_project_request(
      "/metabaseEditor/project/" + projectId + "/delete",
      100, 100, "#delete-progress-bar-del-project",
      "#step-del-project", 1, 1);
  });
  chain.done(function(){
    location.href = "/metabaseEditor/project";
  }).fail(function() {
    var percentage = 100;
    $('#del-project-info-progress-div').children().attr("aria-valuenow", function(i, w){
      percentage -= w;
    });
    var percentage_part = percentage / 4;
    $("#del-project-info-progress-div").append('<div id="del-info-progress-bar-clear" class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
    $("#delete-project-progress-div").append('<div class="row" style="align-items:center"><div class="col-5"><small>'+ "Clear database" +'</small></div><div class="col-6"><div id="delete-project-progress-clear" class="progress"></div></div><div id="step_project_clear" class="col step-div">0/4</div></div>');
    $('#delete-project-progress-clear').append('<div id="delete-project-progress-clear-bar" class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
    $.when().then(function(){
      return make_clean_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/unlink_tube_and_pcrs",
        25, percentage_part, btn, 1, 4);
    }).then(function(){
      return make_clean_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/unlink_project_tube",
        50, percentage_part*2, btn, 2, 4);
    }).then(function(){
      return make_clean_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/remove_project_tube",
        75, percentage_part*3, btn, 3, 4);
    }).then(function(){
      return make_clean_delete_project_request(
        "/metabaseEditor/project/" + projectId + "/delete",
        100, percentage_part*4, btn, 4, 4);
    }).done(function(){
      $("#ok-modal-project-delete").on('click', function(){
        location.href = "/metabaseEditor/project";
      });
    }).always(function(){
      $('.del-project').prop("hidden", true);
      $("#ok-modal-project-delete").prop("hidden", false);
      $("#ok-modal-project-delete").prop("disabled", false);
      $("#del-info-progress-bar-clear").removeClass('progress-bar-animated');
      $("#delete-project-progress-clear-bar").removeClass('progress-bar-animated');
    });
  }).always(function(){
    $(".progress-bar").not("#del-info-progress-bar-clear")
    .not("#delete-project-progress-clear-bar")
    .removeClass('progress-bar-animated');
  });
}


function make_delete_project_request(url, percent, percent_tot, progress_bar_id,
step_id, current_step, step){
  return $.ajax({
    url: url,
    type: "POST",
    async: true,
    cache: false,
    processData: false,
    contentType: false,
    success: function(data) {
      $('#del-project-info-progress-bar').css("width", percent_tot + "%");
      $('#del-project-info-progress-bar').attr("aria-valuenow", percent_tot);
      $(progress_bar_id).css('width', percent + "%");
      $(step_id).html(current_step + "/" + step);
    },
    error: function(jqXHR) {
      $('#del-project-info-progress-bar').removeClass("bg-success");
      $('#del-project-info-progress-bar').addClass("bg-danger");
      $(progress_bar_id).removeClass("bg-success");
      $(progress_bar_id).addClass("bg-danger");
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        $("#delete-project-message").html(jqXHR.responseJSON.html);
      }
      else {
        var msg = "An error occur: ";
        if (jqXHR.status && jqXHR.statusText){
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
        }
        else{
            msg = msg + "the project can not be delete !";
        }
        ErrorModalMsg("#delete-project-message", msg);
      }
      $(".del-project").prop("disabled", true);
    }
  });
}

function make_clean_delete_project_request(url, percent, percent_tot, btn,
  current_step, step){
  return $.ajax({
    url: url,
    type: "POST",
    async: true,
    cache: false,
    processData: false,
    contentType: false,
    success: function(data) {
      $('#delete-project-progress-clear-bar').css('width', percent + "%");
      $('#step_project_clear').html(current_step + "/" + step);
      $('#del-info-progress-bar-clear').css('width', percent_tot + "%");
    },
    error: function(jqXHR) {
      $("#delete-project-message").append("<div id='clear-project-delete-message'></div>");
      $('#del-info-progress-bar-clear').css('width', "100%");
      $('#del-info-progress-bar-clear').removeClass("bg-warning");
      $('#del-info-progress-bar-clear').addClass("bg-danger");
      $('#delete-project-progress-clear-bar').css('width', "100%");
      $('#delete-project-progress-clear-bar').removeClass("bg-warning");
      $('#delete-project-progress-clear-bar').addClass("bg-danger");
      if (jqXHR.responseJSON){
        user_authentication(jqXHR.responseJSON);
        $("#clear-project-delete-message").append(jqXHR.responseJSON.html);
      }
      else {
        var msg = "An error occur: ";
        if (jqXHR.status && jqXHR.statusText){
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
        }
        else{
            msg = msg + "the project can not be delete !";
        }
        ErrorModalMsg("#clear-project-delete-message", msg, " - Clear database");
      }
      btn.prop("disabled", true);
    }
  });
}
