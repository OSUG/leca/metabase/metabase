$(document).ready(function() {
  // get and initialize the CSRF token for the ajax request
  initCSRFToken();

  $("#deleteSequencingModal").on("show.bs.modal", function() {
    var sequencingTable = $("#sequencing-table").DataTable();
    var sequencingId = sequencingTable.row(".selected").data().id;
    $("#delete-sequencing-question").html(
      "<p>Are you sure you want to delete the sequencing information file " +
        sequencingTable.row(".selected").data().file_name +
        "?</p>"
    );
    $.ajax({
      url: "/metabaseEditor/sequencing/" + sequencingId + "/information",
      type: "POST",
      success: function(data) {
        $("#delete-sequencing-message").html(data);
      },
      error: function(jqXHR) {
        if(jqXHR.responseJSON){
          user_authentication(jqXHR.responseJSON);
          $("#delete-sequencing-message").html(jqXHR.responseJSON.html);
        }
        else if (jqXHR.status && jqXHR.statusText){
          var msg = "An error occur: ";
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
          ErrorModalMsg("#delete-sequencing-message", msg);
        }
        else{
          ErrorModalMsg(
            "#delete-sequencing-message",
            "An error occur: the sequencing information can not be loaded !");
        }
        $("#delete-sequencing").prop("disabled", true);
      }
    });
  });

  $("#delete-sequencing").click(function() {
    var sequencingTable = $("#sequencing-table").DataTable();
    var sequencingId = sequencingTable.row(".selected").data().id;
    $.ajax({
      url: "/metabaseEditor/sequencing/" + sequencingId + "/remove",
      type: "POST",
      success: function(data) {
        if (data.status == "SUCCESS") {
          sequencingTable
            .row(".selected")
            .remove()
            .draw(false);
          $("#messages").append(data.html);
        }
        $("#deleteSequencingModal").modal("toggle");
        $("#show-modal-delete-sequencing").prop("disabled", true);
      },
      error: function(jqXHR) {
        if(jqXHR.responseJSON){
          user_authentication(jqXHR.responseJSON);
          $("#delete-sequencing-message").html(jqXHR.responseJSON.html);
        }
        else if (jqXHR.status && jqXHR.statusText){
          var msg = "An error occur: ";
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
          ErrorModalMsg("#delete-sequencing-message", msg);
        }
        else{
          ErrorModalMsg(
            "#delete-sequencing-message",
            "An error occur: the sequencing can not be deleted !");
        }
        $("#delete-sequencing").prop("disabled", true);
      }
    });
  });

  $("#deleteSequencingModal").on("hidden.bs.modal", function() {
    $("#delete-sequencing-message").html("");
    $("#delete-sequencing").prop("disabled", false);
  });

});
