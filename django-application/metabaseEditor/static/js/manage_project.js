$(document).ready(function() {
  // get and initialize the CSRF token for the ajax request
  initCSRFToken();

  // set delete and download button properties to disabled
  $(window).on("unload", function(e) {
    $("#show-modal-delete-experiment").prop("disabled", true);
    $("#download-experiment").prop("disabled", true);
    $("#show-modal-delete-sequencing").prop("disabled", true);
  });

  // build the table containing experiment submission file
  var experimentTable = $("#experiment-table").DataTable({
    ajax: {
      url: getExperimentUrl(),
      type: "POST",
      error: function(jqXHR) {
        if (jqXHR.responseJSON) {
          // redirect if the user authentication fail
          user_authentication(jqXHR.responseJSON);
          // display error messages
          $("#messages").append(jqXHR.responseJSON.html);
        }
        else if (jqXHR.status && jqXHR.statusText){
          var msg = "An error occur: ";
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
          writeMessages(msg, "error");
        }
        else{
          writeMessages(
            "An error occur: the sequencing file can not be imported !",
            "error");
        }
        $(".dataTables_empty").text("error, data can not been load!");
      }
    },
    columns: [
      { data: "file_name" },
      { data: "submission_date" },
      {
        data: "tube_list",
        render: function(data, type, full, meta) {
          return data.replace(/; /g, "<br>");
        }
      }
    ],
    createdRow: function(row, data) {
      // add submission file id to the row
      $(row).attr({
        "data-id": data.id
      });
    },
    select: { style: "single" },
    order: [1, "asc"]
  });

  // init selection and deselection event on the experiment table
  experimentTable
    .on("select", function(e, dt, type, indexes) {
      // set delete, download button to available
      $("#show-modal-delete-experiment").prop("disabled", false);
      $("#download-experiment").prop("disabled", false);
    })
    .on("deselect", function(e, dt, type, indexes) {
      // set delete, download button to disabled
      $("#show-modal-delete-experiment").prop("disabled", true);
      $("#download-experiment").prop("disabled", true);
    });

  // build the table containing libraries and tubes
  var projectSequencingTable = $("#project-sequencing-table").DataTable({
    ajax: {
      url: getProjectSequencingUrl(),
      type: "POST",
      error: function(jqXHR) {
        if (jqXHR.responseJSON) {
          // redirect if the user authentication fail
          user_authentication(jqXHR.responseJSON);
          // display error messages
          $("#messages").append(jqXHR.responseJSON.html);
        }
        else if (jqXHR.status && jqXHR.statusText){
          var msg = "An error occur: ";
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
          writeMessages(msg, "error");
        }
        else{
          writeMessages(
            "An error occur: the sequencing file can not be imported !",
            "error");
        }
        $(".dataTables_empty").text("error, data can not been load!");
      }
    },
    autoWidth: true,
    columns: [
      { data: "library_name" },
      { data: "tube_name" },
      {
        data: "experiment",
        render: function(data, type, full, meta) {
          // prettier-ignore
          return data === true ? '<i class="fa fa-check fa-2x"></i>'
            : '<i class="fa fa-times fa-2x"></i>';
        },
        className: "text-center"
      },
      { data: "file_name" }
    ],
    select: { style: "multi" },
    createdRow: function(row, data) {
      // add library id to the row
      $(row).attr({
        "data-id": data.id
      });
    },
    order: [1, "asc"]
  });

  // init selection and deselection event on the sequencing table
  projectSequencingTable
    .on("select", function(e, dt, type, indexes) {
      // set delete button to available
      var selectedRows = projectSequencingTable.rows(".selected").count();
      if (selectedRows > 0) {
        $("#show-modal-delete-sequencing").prop("disabled", false);
      }
    })
    .on("deselect", function(e, dt, type, indexes) {
      // set delete button to disabled
      var selectedRows = projectSequencingTable.rows(".selected").count();
      if (selectedRows == 0) {
        $("#show-modal-delete-sequencing").prop("disabled", true);
      }
    });

  // init event on the download button
  $("#download-experiment").click(function() {
    // get the submission file id
    var experimentId = experimentTable.row(".selected").data().id;
    // redirect to the downloaded file
    location.href = "experiment/" + experimentId + "/download";
  });
});

// generate experiment url to get data to populate experiment table
function getExperimentUrl() {
  var projectId = $("#selectProject").val();
  if (projectId !== "") {
    return "/metabaseEditor/project/" + projectId + "/experiment/get_data";
  }
  return "/metabaseEditor/project/experiment/get_data";
}

// generate sequencing url to get data to populate sequencing table
function getProjectSequencingUrl() {
  var projectId = $("#selectProject").val();
  if (projectId !== "") {
    return "/metabaseEditor/project/" + projectId + "/sequencing/get_data";
  }
  return "/metabaseEditor/project/sequencing/get_data";
}
