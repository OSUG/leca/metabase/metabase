$(document).ready(function() {
  initCSRFToken();

  buildParentProjectDatatables();
  buildUserDatatables();
  $("#nav-project-info-tab")
    .off("shown.bs.tab")
    .on("shown.bs.tab", function() {
      buildParentProjectDatatables();
      buildUserDatatables();
    });

  $("#project-information").on("click", "#select_all_user", function() {
    if ($(this).is(":checked")) {
      $("tbody", "#users-table")
        .find("input")
        .not(":input[readonly]")
        .prop("checked", true);
    } else {
      $("tbody", "#users-table")
        .find("input")
        .not(":input[readonly]")
        .prop("checked", false);
    }
  });

  $("#project-information").on("click", "#select_all_parentProject", function() {
    if ($(this).is(":checked")) {
      $("tbody", "#parentProject-table")
        .find("input")
        .not(":input[readonly]")
        .prop("checked", true);
    } else {
      $("tbody", "#parentProject-table")
        .find("input")
        .not(":input[readonly]")
        .prop("checked", false);
    }
  });

  $("#reset_modification").on("click", function() {
    var projectId = $("#selectProject").val();
    url = "/metabaseEditor/project/users/get";
    if (projectId !== "") {
      url = "/metabaseEditor/project/" + projectId + "/users/get";
    }
    $("#users-table")
      .find("input")
      .not(":input[readonly]")
      .prop("checked", false);
    $("#select_all_user").prop("checked", false);
    $.ajax({
      url: url,
      type: "POST",
      success: function(data) {
        data.forEach(function(d) {
          $("#id_users_" + d).prop("checked", true);
        });
      },
      error: function(jqXHR) {
        if(jqXHR.responseJSON){
          user_authentication(jqXHR.responseJSON);
        }
        else if (jqXHR.status && jqXHR.statusText){
          var msg = "An error occur: ";
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
          writeMessages(msg, "error");
        }
        else{
          writeMessages(
            "An error occur: the modification can not be reseted !",
            "error");
        }
      }
    });
  });

});

function buildUserDatatables() {
  if ($.fn.DataTable.isDataTable("#users-table")) {
    $("#users-table")
      .DataTable()
      .draw();
  } else {
    $("#users-table").DataTable({
      scrollY: 200,
      scrollCollapse: true,
      info: false,
      searching: false,
      paging: false,
      columnDefs: [
        {
          orderable: false,
          targets: 0
        }
      ],
      order: [1, "asc"]
    });
  }
}

function buildParentProjectDatatables() {
  if ($.fn.DataTable.isDataTable("#parentProject-table")) {
    $("#parentProject-table")
      .DataTable()
      .draw();
  } else {
    $.fn.dataTable.ext.order["dom-checkbox"] = function  ( settings, col )
    {
        return this.api().column( col, {order:"index"} ).nodes().map( function ( td, i ) {
            return $("input", td).prop("checked") ? "1" : "0";
        } );
    };
    var parentProjectTable = $("#parentProject-table").DataTable({
      scrollY: 200,
      scrollCollapse: true,
      info: false,
      paging: false,
      columnDefs: [
        {
          targets: 0,
          orderDataType: "dom-checkbox"
        }
      ],
      order: [[0, "desc"],[1, "asc"]]
    });
    $(".dataTables_filter").hide();
    $("#search_datatables").on("keyup change", function(){
      parentProjectTable.search($(this).val()).draw();
    });
  }
}
