$(document).ready(function() {
  // get and initialize the CSRF token for the ajax request
  initCSRFToken();

  $(window).on("unload", function(e) {
    $("#show-modal-delete-sequencing").prop("disabled", true);
  });

  var sequencingTable = $("#sequencing-table").DataTable({
    ajax: {
      url: "/metabaseEditor/sequencing/get_sequencing_information",
      type: "POST",
      error: function(jqXHR) {
        if (jqXHR.responseJSON) {
          user_authentication(jqXHR.responseJSON);
          $("#messages").append(jqXHR.responseJSON.html);
        }
        else if (jqXHR.status && jqXHR.statusText){
          var msg = "An error occur: ";
          msg = msg + " code " + jqXHR.status + " : " + jqXHR.statusText;
          writeMessages(msg, "error");
        }
        else{
          writeMessages(
            "An error occur: the library can not be deleted !",
            "error");
        }
        $(".dataTables_empty").text("error, data can not been load!");
      }
    },
    scrollY: 400,
    scrollX: true,
    scrollCollapse: true,
    paging: false,
    fixedColumns: {
      leftColumns: 1,
      heightMatch:"auto"
    },
    select: true,
    columns: [
      { data: "file_name" },
      { data: "sequencing_date" },
      {
        data: "tubes",
        render: function(data, type, full, meta) {
          return data.replace(/; /g, "<br>");
        }
      },
      {
        data: "libraries",
        render: function(data, type, full, meta) {
          return data.replace(/; /g, "<br>");
        }
      },
      {
        data: "sequencing_files",
        render: function(data, type, full, meta) {
          return data.replace(/; /g, "<br>");
        }
      }
    ],
    createdRow: function(row, data) {
      $(row).attr({
        "data-id": data.id
      });
    },
    buttons: [
      {
        extend: "columnsToggle",
        columns: ":gt(0)"
      }
    ],
    initComplete: function() {
      sequencingTable
        .buttons()
        .container()
        .appendTo("#sequencing-table_wrapper .col-md-6:eq(0)");
    },
    order: [0, "asc"]
  });

  sequencingTable
    .on("select", function(e, dt, type, indexes) {
      $("#show-modal-delete-sequencing").prop("disabled", false);
    })
    .on("deselect", function(e, dt, type, indexes) {
      $("#show-modal-delete-sequencing").prop("disabled", true);
    });

});
