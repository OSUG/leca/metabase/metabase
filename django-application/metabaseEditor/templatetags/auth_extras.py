from django import template
from django.contrib.auth.models import Group

register = template.Library()

@register.filter(name='has_permission')
def has_permission(user, group_name):
    if user.is_superuser:
        return True
    group = Group.objects.filter(name=group_name)
    if group.exists():
        return group.first() in user.groups.all()
    else:
        return False
