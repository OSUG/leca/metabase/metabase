from django.shortcuts import redirect
from django.contrib import messages
from django.http import JsonResponse
from django.conf import settings
from django.db.models import Q

from functools import wraps

from ..models import Project


def check_user_auth_js(view_function):
    @wraps(view_function)
    def wrapper(request, *args, **kwargs):
        if request.user.is_superuser or "editor" in request.user.groups.values_list("name",flat=True):
            return view_function(request, *args, **kwargs)
        messages.warning(request,
            "Your account doesn't have access to this page. "
            + "To proceed, please login with an account that has access.")
        return JsonResponse({'status': 'FAIL', 'not_authenticated': True,
                             'redirect_url': settings.LOGIN_URL, 'data':[]},
                            status=401)
    return wrapper


def check_user_auth(view_function):
    @wraps(view_function)
    def wrapper(request, *args, **kwargs):
        if request.user.is_superuser or "editor" in request.user.groups.values_list("name",flat=True):
            return view_function(request, *args, **kwargs)
        messages.warning(request,
            "Your account doesn't have access to this page. "
            + "To proceed, please login with an account that has access.")
        return redirect(settings.LOGIN_URL)
    return wrapper


def check_user_right(view_function):
    @wraps(view_function)
    def wrapper(request, *args, **kwargs):
        if 'project_id' in kwargs:
            project_id = kwargs['project_id']
            if project_id:
                user = request.user
                check_user_auth = Project.objects.filter(id=project_id, users=user)
                if not check_user_auth.exists():
                    messages.warning(request,
                        "Your account doesn't have access to this page. "
                        + "To proceed, please login with an account that has access.")
                    return redirect(settings.LOGIN_URL)
        return view_function(request, *args, **kwargs)
    return wrapper


def check_user_right_exp(view_function):
    @wraps(view_function)
    def wrapper(request, *args, **kwargs):
        if 'submission_file_id' in kwargs:
            submit_file_id = kwargs['submission_file_id']
            user = request.user
            check_user_auth = Project.objects.filter(
                    tube_mix__submission_file__id=submit_file_id,
                    users=user)
            if check_user_auth.exists():
                return view_function(request, *args, **kwargs)
        messages.warning(request,
            "Your account doesn't have access to this page. "
            + "To proceed, please login with an account that has access.")
        return redirect(settings.LOGIN_URL)
    return wrapper


def check_user_right_exp_js(view_function):
    @wraps(view_function)
    def wrapper(request, *args, **kwargs):
        if 'submission_file_id' in kwargs:
            submit_file_id = kwargs['submission_file_id']
            user = request.user
            check_user_auth = Project.objects.filter(
                    tube_mix__submission_file__id=submit_file_id,
                    users=user)
            if check_user_auth.exists():
                return view_function(request, *args, **kwargs)
        messages.warning(request,
            "Your account doesn't have access to this page. "
            + "To proceed, please login with an account that has access.")
        return JsonResponse({'status': 'FAIL', 'not_authenticated': True,
                             'redirect_url': settings.LOGIN_URL, 'data':[]},
                            status=401)
    return wrapper


def check_user_right_js(view_function):
    @wraps(view_function)
    def wrapper(request, *args, **kwargs):
        if 'project_id' in kwargs:
            project_id = kwargs['project_id']
            if project_id:
                user = request.user
                check_user_auth = Project.objects.filter(id=project_id, users=user)
                if not check_user_auth.exists():
                    messages.warning(request,
                        "Your account doesn't have access to this page. "
                        + "To proceed, please login with an account that has access.")
                    return JsonResponse({'status': 'FAIL', 'not_authenticated': True,
                                         'redirect_url': settings.LOGIN_URL, 'data':[]},
                                        status=401)
        return view_function(request, *args, **kwargs)
    return wrapper


def check_user_login(view_function):
    @wraps(view_function)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            return view_function(request, *args, **kwargs)
        messages.warning(request,
            "Your account doesn't have access to this page "
            + "or your session has expired. "
            + "To proceed, please login with an account that has access.")
        return JsonResponse({'status': 'FAIL', 'not_authenticated': True,
                             'redirect_url': settings.LOGIN_URL, 'data':[]},
                            status=500)
    return wrapper
