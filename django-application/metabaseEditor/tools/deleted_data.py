from ..models import Deleted_data_tmp
import time


def get_deleted_id_by_type(submission_file_id, types):
    deleted_data = Deleted_data_tmp.objects.filter(type=types,
            parent__uniq_id=submission_file_id)
    return deleted_data.values_list("uniq_id", flat=True)


def get_project_deleted_id_by_type(project_id, types):
    project_list = [project_id]
    project_list.append(list(Deleted_data_tmp.objects.filter(
            type="project", parent__uniq_id=project_id
            ).values_list('uniq_id', flat=True)))
    deleted_data = Deleted_data_tmp.objects.filter(type=types,
            parent__parent__uniq_id__in=project_list)
    return deleted_data.values_list("uniq_id", flat=True)


def instanciate_deleted_data(type, uniq_id, parent=None):
    delete_data = Deleted_data_tmp()
    delete_data.type = type
    delete_data.uniq_id = uniq_id
    if parent is not None:
        delete_data.parent = parent
        delete_data.id = str(parent.uniq_id) + type + str(uniq_id)
    else:
        delete_data.id = type + str(uniq_id)
    return delete_data


def get_project_deleted_data(project_id):
    delete_id = "project" + str(project_id)
    if Deleted_data_tmp.objects.filter(id=delete_id).exists():
        return Deleted_data_tmp.objects.get(id=delete_id)
    return None


def project_deleted_data(project_id, parent=None):
    delete_id = "project" + str(project_id)
    if Deleted_data_tmp.objects.filter(id=delete_id).exists():
        return Deleted_data_tmp.objects.get(id=delete_id)
    deleted_project = instanciate_deleted_data("project", project_id)
    if parent is not None:
        deleted_project.parent = parent
    deleted_project.save()
    return deleted_project


def submit_file_deleted_data(submission_file_id, deleted_project=None):
    delete_id = "submission_file" + str(submission_file_id)
    if Deleted_data_tmp.objects.filter(id=delete_id).exists():
        return Deleted_data_tmp.objects.get(id=delete_id)
    submit_file = instanciate_deleted_data("submission_file",
                                           submission_file_id)
    if deleted_project is not None:
        submit_file.parent = deleted_project
    submit_file.save()
    return submit_file


def get_delete_submission_file(submission_file_id):
    delete_id = "submission_file" + str(submission_file_id)
    if Deleted_data_tmp.objects.filter(id=delete_id).exists():
        return Deleted_data_tmp.objects.get(id=delete_id)
    return None


def get_delete_ID_submission_files():
    if Deleted_data_tmp.objects.filter(type="submission_file").count() > 0:
        return list(Deleted_data_tmp.objects.filter(type="submission_file"))
    return None


def push_deleted_data(delete_data_list):
    Deleted_data_tmp.objects.bulk_create(delete_data_list)

def clear_deleted_table(submission_file_id):
    delete_id = "submission_file" + str(submission_file_id)
    Deleted_data_tmp.objects.filter(id=str(delete_id)).delete()


def clear_deleted_table_type(submission_file_id, type):
    delete_id = "submission_file" + str(submission_file_id)
    Deleted_data_tmp.objects.filter(type=type,
                                     parent__id=str(delete_id)).delete()
    if Deleted_data_tmp.objects.filter(parent__id=str(delete_id)).count() < 1:
        Deleted_data_tmp.objects.filter(id=str(delete_id)).delete()


def remove_deleted_data(type, uniq_ids):
    if isinstance(uniq_ids, list):
        Deleted_data_tmp.objects.filter(type=type, uniq_id__in=uniq_ids).delete()
    else:
        Deleted_data_tmp.objects.filter(type=type, uniq_id=uniq_ids).delete()
