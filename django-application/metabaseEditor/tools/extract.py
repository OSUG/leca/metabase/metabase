from django.db import IntegrityError, InternalError
from django.db.models import Count, Q

from ..models import (Extract, Extract_qualifier, Unused_extract,
                      Sample, Unused_sample, Submission_file)
from .imported_data import (instanciate_imported_data, add_qualifier,
                            push_qualifiers)

def check_extract_unicity(extract, existed_extract_list, control):
    if extract['name'] in existed_extract_list:
        existed_extract = existed_extract_list[extract['name']]
        extraction_batch = Extract_qualifier.objects.filter(
            extract__id=existed_extract.id, key='batch',
            value=extract['batch'])
        if not extraction_batch.exists():
            raise IntegrityError('Extract '
                           + str(extract['name'])
                           +' already exist in another batch!')

        if extract['sample'] != str(existed_extract.sample):
            if (extract['sample'] != control['negCtl']
                    or (existed_extract.etype != 'extneg'
                    and existed_extract.etype != 'PCRPos')):
                raise IntegrityError('Extract '
                               + str(extract['name'])
                               +' already exist with another sample!')

        qualifiers_list = Extract_qualifier.objects.all().filter(
            extract__id=existed_extract.id).exclude(key='batch')
        keys = dict()
        for q in qualifiers_list:
            if q.key not in extract['qualifier']:
                raise IntegrityError('Extract ' + str(extract['name'])
                                     +' already exist with more qualifiers'
                                     + '(missing qualifier "'+ q.key + '")!')
            keys[q.key] = q.value

        for k, v in extract['qualifier'].items():
            if k not in keys:
                raise IntegrityError('Extract ' + str(extract['name'])
                               +' already exist with less qualifiers!')
            else:
                if str(v) != str(keys[k]):
                    raise IntegrityError('Extract ' + str(extract['name'])
                                         + ' already exist with another value'
                                         + ' for qualifier "' + str(k) + '"!')


def check_imported_extract(extract, imported_data):
    imported_extracts = imported_data['extracts']
    if extract['name'] in imported_extracts:
        existed_extract = imported_extracts[extract['name']]
        if existed_extract['batch'] != str(extract['batch']):
            raise IntegrityError('Extract '
                           + str(extract['name'])
                           + ' already exist in another batch!')

        if extract['sample'] != str(existed_extract['sample']):
            raise IntegrityError('Extract '
                           + str(extract['name'])
                           +' already exist with another sample!')

        for key, value in existed_extract['qualifiers'].items():
            if key not in extract['qualifier']:
                raise IntegrityError('Extract ' + str(extract['name'])
                                     + ' already exist with more qualifiers'
                                     + '(missing qualifier "' + key + '")!')

        for key, value in extract['qualifier'].items():
            if key not in existed_extract['qualifiers']:
                raise IntegrityError('Extract ' + str(extract['name'])
                               +' already exist with less qualifiers!')
            else:
                if str(value) != str(existed_extract['qualifiers'][key]):
                    raise IntegrityError('Extract ' + str(extract['name'])
                                         + ' already exist with another value'
                                         + ' for qualifier "' + str(key) + '"!')
    else:
        qualifiers_list = list()
        import_extract = instanciate_imported_data('extname' + str(extract['name']),
                                                   'extracts', 'name',
                                                   extract['name'],
                                                   imported_data['project'])
        qualifiers_list.append(add_qualifier('extracts', 'batch',
                                  extract['batch'], import_extract))
        qualifiers_list.append(add_qualifier('extracts', 'sample',
                                  extract['sample'], import_extract))
        for key, value in extract['qualifier'].items():
            qualifiers_list.append(add_qualifier("extracts", key, value,
                                                import_extract))
        return {"qualifiers": qualifiers_list,
                "extract": import_extract}
    return None


def check_extract_unicity_db(extracts, control, existed_extract_list,
                             imported_data):
    qualifiers_list = list()
    extracts_list = list()
    for extract in extracts:
        check_extract_unicity(extract, existed_extract_list, control)
        if (extract['sample'] == control['negPCRctl']
                or extract['sample'] == control['blkPCRctl']):
            raise IntegrityError('The sample ' + str(extract['sample'])
                                 + ' must not be linked with a extract!')
        tmp_data = check_imported_extract(extract, imported_data)
        if tmp_data is not None:
            qualifiers_list.extend(tmp_data["qualifiers"])
            extracts_list.append(tmp_data["extract"])
    push_qualifiers(extracts_list)
    push_qualifiers(qualifiers_list)


def check_positive_control_db(positive_controls, existed_extract_list):
    for ctl in positive_controls:
        if ctl['name'] in existed_extract_list:
            existed_extract = existed_extract_list[ctl['name']]
            qualifiers_list = Extract_qualifier.objects.filter(
                extract__id=existed_extract.id)
            keys = dict()
            for q in qualifiers_list:
                if q.key not in ctl['abundance']:
                    raise IntegrityError('positive control ' + str(ctl['name'])
                                         + ' already exist with more taxid'
                                         + ' (missing taxid "'+ str(q.key) + '")!')
                keys[q.key] = q.value
            for k, v in ctl['abundance'].items():
                if k not in keys:
                    raise IntegrityError('positive control ' + str(ctl['name'])
                                         + ' already exist with less taxid'
                                         + ' (taxid "'+ str(k) + '" not exist)!')
                else:
                    if str(v) != str(keys[k]):
                        raise IntegrityError('positive control ' + str(ctl['name'])
                                             + ' already exist with the taxid '
                                             + 'with another abundance value!')


def check_imported_positive_control(positive_controls, imported_data):
    imported_extract = imported_data['controls']
    qualifiers_list = list()
    ctl_list = list()
    for ctl in positive_controls:
        if ctl['name'] in imported_extract:
            existed_extract = imported_extract[ctl['name']]
            for key, value in existed_extract.items():
                if key not in ctl['abundance']:
                    raise IntegrityError('positive control ' + str(ctl['name'])
                                         + ' already exist with more taxid'
                                         + ' (missing taxid "'+ str(key) + '")!')
            for key, value in ctl['abundance'].items():
                if key not in existed_extract:
                    raise IntegrityError('positive control ' + str(ctl['name'])
                                         + ' already exist with less taxid'
                                         + ' (taxid "'+ str(key) + '" not exist)!')
                else:
                    if str(value) != str(existed_extract[key]):
                        raise IntegrityError('positive control ' + str(ctl['name'])
                                             + ' already exist with the taxid '
                                             + 'with another abundance value!')
        else:
            import_control = instanciate_imported_data( 'ctlname' + str(ctl['name']),
                                                       'controls', 'name',
                                                       ctl['name'],
                                                       imported_data['project'])
            ctl_list.append(import_control)
            for key, value in ctl['abundance'].items():
                qualifiers_list.append(add_qualifier('controls', key, value,
                                                     import_control))
    push_qualifiers(ctl_list)
    push_qualifiers(qualifiers_list)


def check_extraction_sample_id_integrity(extract_list, checking_dict):
    warning_messages = list()
    extract_sample = list()
    for extract in extract_list:
        sample_name = extract['sample']
        if sample_name == checking_dict['ext_neg']:
            continue
        extract_sample.append(sample_name)
        if sample_name not in checking_dict['samples']:
            raise InternalError('The sample ID"' + str(sample_name)
                                    + '" not exist in the sample sheet '
                                    + 'or control sheet!')

    for sample in checking_dict['samples']:
        if sample not in extract_sample:
            warning_messages.append('The sample "' + str(sample)
                                    + '" is not used in the extraction sheet!')
    return warning_messages


def populate_extract_qualifier_table(qualifier, extract):
    for key, value in qualifier.items():
        extract_qualifier = Extract_qualifier()
        extract_qualifier.key = key
        extract_qualifier.value = value
        extract_qualifier.extract = extract
        extract_qualifier.save()


def populate_extract_table(extracts, control, sampleList, existedExtractList):
    used_sample = list()
    for dictExt in extracts:
        if dictExt['name'] not in existedExtractList:
            extract = Extract()
            extract.name = dictExt['name']
            if dictExt['sample'] == control['negCtl']:
                extract.etype = 'extneg'
                extract.save()
                populate_extract_qualifier_table({'batch':dictExt['batch']},
                                                 extract)
            else:
                extract.sample = sampleList[dictExt['sample']]
                extract.etype = 'sample'
                extract.save()
                populate_extract_qualifier_table({'batch':dictExt['batch']},
                                                 extract)
                populate_extract_qualifier_table(dictExt['qualifier'], extract)
                used_sample.append(sampleList[dictExt['sample']].id)
            existedExtractList[extract.name] = extract
    return used_sample


def add_ctl_pos(control, existed_extract, submission_file):
    for ctl in control:
        if ctl['name'] not in existed_extract:
            extract = Extract(name=ctl['name'])
            extract.etype = 'PCRPos'
            extract.save()
            populate_extract_qualifier_table(ctl['abundance'],
                                            extract)
            existed_extract[ctl['name']] = extract
        else:
            extract = existed_extract[ctl['name']]
        unused_extract = Unused_extract()
        unused_extract.extract = extract
        unused_extract.submission_file = submission_file
        unused_extract.save()



def add_extract(extracts, control, sampleList, existedExtractList,
                submission_file):
    used_sample = list()
    for dictExt in extracts:
        if dictExt['name'] not in existedExtractList:
            extract = Extract()
            extract.name = dictExt['name']
            if dictExt['sample'] == control['negCtl']:
                extract.etype = 'extneg'
                extract.save()
                populate_extract_qualifier_table({'batch':dictExt['batch']},
                                                 extract)
            else:
                extract.sample = sampleList[dictExt['sample']]
                extract.etype = 'sample'
                extract.save()
                populate_extract_qualifier_table({'batch':dictExt['batch']},
                                                 extract)
                populate_extract_qualifier_table(dictExt['qualifier'], extract)
                used_sample.append(sampleList[dictExt['sample']].id)
            existedExtractList[extract.name] = extract
        else:
            extract = existedExtractList[dictExt['name']]
            if extract.etype == 'sample':
                used_sample.append(sampleList[dictExt['sample']].id)
        unused_extract = Unused_extract()
        unused_extract.extract = extract
        unused_extract.submission_file = submission_file
        unused_extract.save()
    return used_sample


def clear_unused_extract(submission_file, used_extracts):
    Unused_extract.objects.filter(submission_file=submission_file,
                                 extract__id__in=used_extracts).delete()


def identify_unused_extract(plates, extracts, submission_file):
    plate_extract = list()
    for plate_id, plate_obj in plates.items():
        for column_id, col_obj in plate_obj.items():
            for row_id, row_obj in col_obj.items():
                if row_obj['extractId'] not in plate_extract:
                    plate_extract.append(row_obj['extractId'])
    for extract_id, extract in extracts.items():
        if extract_id not in plate_extract:
            count_unused_extract = Unused_extract.objects.filter(
                submission_file=submission_file, extract=extract).count()
            if count_unused_extract == 0:
                unused_extract = Unused_extract()
                unused_extract.extract = extract
                unused_extract.submission_file = submission_file
                unused_extract.save()


def get_project_extracts(project_id):
    return Extract.objects.all().filter(
        Q(well__plate__pcr__tube__project__id=project_id) |
        Q(unused_extract__submission_file__tube_mix__project__id=project_id)
    ).distinct()


def get_dict_of_project_extract(project_id):
    extract_list = dict()
    extract_query = get_project_extracts(project_id)
    for query in extract_query:
        extract_list[query.name] = query
    return extract_list


def get_project_pos_control(project_id):
    return Extract.objects.filter(
        Q(well__plate__pcr__tube__project__id=project_id) |
        Q(unused_extract__submission_file__tube_mix__project__id=project_id),
        etype='PCRPos'
    ).distinct()


def get_dict_of_project_pos_control(project_id):
    ctl_pos_list = dict()
    ctl_query = get_project_pos_control(project_id)
    for query in ctl_query:
        ctl_pos_list[query.name] = query
    return ctl_pos_list


def get_extneg_unused_extracts_for_deletion(submission_file_id):
    extract_filter = Count('well__plate__pcr__tube__submission_file',
        filter=~Q(
            well__plate__pcr__tube__submission_file__id=submission_file_id),
            distinct=True)

    extracts = Extract.objects.annotate(
        submission_file_count=extract_filter).filter(
            etype='extneg',
            well__plate__pcr__tube__submission_file__id=submission_file_id
    ).exclude(Q(submission_file_count__gt=0) | Q(unused_extract__isnull=False))

    unused_extract_filter = Count('unused_extract__submission_file',
                filter=~Q(
                    unused_extract__submission_file__id=submission_file_id),
                distinct=True)

    unused_extracts = Extract.objects.annotate(
        submission_file_count=unused_extract_filter).filter(
        unused_extract__submission_file__id=submission_file_id
    ).exclude(Q(submission_file_count__gt=0) | Q(well__isnull=False))

    return (extracts | unused_extracts).distinct()


def get_control_positive_for_deletion(submission_file_id):
    extract_filter = Count('well__plate__pcr__tube__submission_file',
        filter=~Q(
            well__plate__pcr__tube__submission_file__id=submission_file_id),
            distinct=True)

    extracts = Extract.objects.annotate(
            submission_file_count=extract_filter).filter(
                etype='PCRPos',
                well__plate__pcr__tube__submission_file__id=submission_file_id
        ).exclude(
            Q(submission_file_count__gt=0) | Q(unused_extract__isnull=False))

    return extracts.distinct()


def get_submission_file_extracts_for_deletion(submission_file_id):
    extract_filter = Count('well__plate__pcr__tube__submission_file',
        filter=~Q(
            well__plate__pcr__tube__submission_file__id=submission_file_id),
            distinct=True)

    extracts = Extract.objects.annotate(
        submission_file_count=extract_filter).filter(
            well__plate__pcr__tube__submission_file__id=submission_file_id
        ).exclude(
            Q(submission_file_count__gt=0) | Q(unused_extract__isnull=False))

    unused_extract_filter = Count('unused_extract__submission_file',
                filter=~Q(
                    unused_extract__submission_file__id=submission_file_id),
                distinct=True)

    unused_extracts = Extract.objects.annotate(
        submission_file_count=unused_extract_filter).filter(
        unused_extract__submission_file__id=submission_file_id
        ).exclude(Q(submission_file_count__gt=0) | Q(well__isnull=False))

    return (extracts | unused_extracts).distinct()


def remove_extracts(extract_ids, submission_file_id):
    submission_file = Submission_file.objects.get(id=submission_file_id)
    sample_ids = Extract.objects.filter(
            id__in=extract_ids).values_list("sample__id", flat=True).distinct()
    samples = Sample.objects.filter(id__in=sample_ids).annotate(
            extract_count=Count("extract"),
            unused_sample_count=Count("unused_sample")
            ).filter(unused_sample_count=0, extract_count__lte=1).distinct()
    for sample in samples:
        unused_sample = Unused_sample()
        unused_sample.sample = sample
        unused_sample.submission_file = submission_file
        unused_sample.save()

    Extract.objects.filter(id__in=extract_ids).delete()


def get_deleted_project_extracts(project_id):
    w_parentProject_count = Count('well__plate__pcr__tube__project__parentProject',
                                distinct=True)
    ue_parentProject_count = Count('unused_extract__submission_file__tube_mix__project__parentProject',
                                distinct=True)
    return Extract.objects.annotate(
        w_parentProject_count=w_parentProject_count,
        ue_parentProject_count=ue_parentProject_count
    ).filter(
        Q(well__plate__pcr__tube__project__id=project_id) |
        Q(unused_extract__submission_file__tube_mix__project__id=project_id) |
        (Q(well__plate__pcr__tube__project__parentProject__id=project_id) &
         Q(w_parentProject_count__lt=2)) |
        (Q(unused_extract__submission_file__tube_mix__project__parentProject__id=project_id) &
         Q(ue_parentProject_count__lt=2))
    ).distinct()
