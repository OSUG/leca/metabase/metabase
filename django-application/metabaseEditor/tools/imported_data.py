from ..models import Imported_data_tmp
import time

def get_imported_samples_dict(project):
    osample = dict()
    sample_data = Imported_data_tmp.objects.filter(type='samples')
    sample_names = sample_data.filter(key='name', parent=project)
    for sample_name in sample_names:
        sample = osample[sample_name.value] = dict()
        qualifier_data = sample_data.filter(parent=sample_name)
        for data in qualifier_data:
            sample[data.key] = data.value
    return osample


def get_imported_extracts_dict(project):
    oextract = dict()
    extract_data = Imported_data_tmp.objects.filter(type='extracts')
    extract_names = extract_data.filter(key='name', parent=project)
    for extract_name in extract_names:
        extract = oextract[extract_name.value] = dict()
        extract['qualifiers'] = dict()
        qualifier_data = extract_data.filter(parent=extract_name)
        for data in qualifier_data:
            if data.key == 'batch':
                extract[data.key] = data.value
            elif data.key == 'sample':
                extract[data.key] = data.value
            else:
                extract['qualifiers'][data.key] = data.value
    return oextract


def get_imported_controls_dict(project):
    ocontrols = dict()
    control_data = Imported_data_tmp.objects.filter(type='controls')
    control_names = control_data.filter(key='name', parent=project)
    for control_name in control_names:
        control = ocontrols[control_name.value] = dict()
        qualifier_data = control_data.filter(parent=control_name)
        for data in qualifier_data:
            control[data.key] = data.value
    return ocontrols


def get_imported_pcrs_dict(project):
    opcr = dict()
    pcr_data = Imported_data_tmp.objects.filter(type='pcrs')
    pcr_runs = pcr_data.filter(key='run', parent=project)
    for pcr_run in pcr_runs:
        opcr[pcr_run.value] = pcr_data.filter(
                parent=pcr_run).values_list('value', flat=True)
    return opcr


def get_imported_plates_dict(project):
    oplate = dict()
    plate_data = Imported_data_tmp.objects.filter(type='plates', parent=project)
    plate_names = plate_data.filter(key='name')
    for plate_name in plate_names:
        oplate[plate_name.value] = dict()
    column_ids = plate_data.filter(key='column_id')
    for column_id in column_ids:
        oplate[column_id.parent.value][column_id.value] = dict()
    row_ids = plate_data.filter(key='row_id').select_related("parent__parent")
    for row_id in row_ids:
        well = oplate[row_id.parent.parent.value][row_id.parent.value][row_id.value] = dict()
        well_data = plate_data.filter(parent=row_id)
        for data in well_data:
            well[data.key] = data.value
    return oplate


def get_imported_tubes_list(project):
    return list(Imported_data_tmp.objects.filter(
            type='tubes',
            parent=project).values_list('value', flat=True))


def instanciate_imported_data(id, type, name, value, parent=None):
    import_data = Imported_data_tmp()
    millis = int(round(time.time() * 1000))
    import_data.id = str(millis) + str(id)
    import_data.type = type
    import_data.key = name
    import_data.value = value
    if parent is not None:
        import_data.parent = parent
    # import_data.save()
    return import_data


def project_imported_data(project_id):
    if Imported_data_tmp.objects.filter(type="project",
                                        value=project_id).exists():
        return Imported_data_tmp.objects.get(type="project", value=project_id)
    import_project = Imported_data_tmp()
    millis = int(round(time.time() * 1000))
    import_project.id = str(millis) + "project" + str(project_id)
    import_project.type = "project"
    import_project.key = "id"
    import_project.value = project_id
    import_project.save()
    return import_project


def submit_file_imported_data(submission_file_id, project_id):
    if Imported_data_tmp.objects.filter(type="submission_file",
                                        value=submission_file_id).exists():
        return Imported_data_tmp.objects.get(type="submission_file",
                                             value=submission_file_id)
    submit_file = instanciate_imported_data("submit_file" + str(submission_file_id),
                              "submission_file", project_id, submission_file_id)
    submit_file.save()
    return submit_file


def get_submission_file_by_project_id(project_id):
    if Imported_data_tmp.objects.filter(type="submission_file",
                                        key=project_id).exists():
        return Imported_data_tmp.objects.get(type="submission_file",
                                             key=project_id)
    return None


def get_ID_submission_files():
    if Imported_data_tmp.objects.filter(type="submission_file").count() > 0:
        return list(Imported_data_tmp.objects.filter(type="submission_file"))
    return None


def add_qualifier(type, key, value, parent):
    millis = int(round(time.time() * 1000))
    id = "".join([str(millis), type, str(key),
                 str(value), str(parent.value)])
    if parent.parent and parent.parent.value:
        id += str(parent.parent.value)
    return Imported_data_tmp(
            id=id,
            type=type,
            key=key,
            value=value,
            parent=parent)


def push_qualifiers(qualifier_list):
    Imported_data_tmp.objects.bulk_create(qualifier_list)


def clear_imported_table(project_id):
    Imported_data_tmp.objects.filter(value=str(project_id)).delete()


def clear_imported_table_type(project_id, type):
    Imported_data_tmp.objects.filter(type=type,
                                     parent__value=str(project_id)).delete()
    if Imported_data_tmp.objects.filter(
            parent__value=str(project_id)).count() < 1:
        Imported_data_tmp.objects.filter(value=str(project_id)).delete()
