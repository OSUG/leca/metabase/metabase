from django.db import IntegrityError

from ..models import PCR
from .imported_data import instanciate_imported_data, push_qualifiers

def check_run_unicity(pcrs, pcr_list):
    for run_id, run_obj in pcrs['runs'].items():
        if run_id not in pcr_list:
            continue
        for run_position in run_obj:
            if run_position in pcr_list[run_id]:
                raise IntegrityError('The run id ' + str(run_id)
                                    + ' with the position '
                                    + str(run_position)
                                    + ' already exist in the database'
                                    + ' for this project!')


def check_imported_run(pcrs, imported_data):
    pcr_run_list = list()
    pcr_pos_list = list()
    imported_pcr = imported_data['pcrs']
    for run_id, run_obj in pcrs['runs'].items():
        run_id = str(run_id)
        if run_id in imported_pcr:
            for run_position in run_obj:
                run_position = str(run_position)
                if str(run_position) in imported_pcr[run_id]:
                    raise IntegrityError('The run id ' + str(run_id)
                                        + ' with the position '
                                        + str(run_position)
                                        + ' already exist in the database'
                                        + ' for this project!')
        else:
            run = instanciate_imported_data('pcrsrun' + str(run_id), 'pcrs',
                                            'run', run_id,
                                            imported_data['project'])
            pcr_run_list.append(run)
            for run_position in run_obj:
                position = instanciate_imported_data('pcrsposition' + str(run) + str(run_position),
                                          'pcrs', 'position',
                                          run_position, run)
                pcr_pos_list.append(position)
    push_qualifiers(pcr_run_list)
    push_qualifiers(pcr_pos_list)


def check_pcr_plate_integrity(pcr_plate, plate_list):
    for plate_id in pcr_plate:
        if plate_id in plate_list:
            raise IntegrityError('The plate "' + str(plate_id)
                                + '" already exist in the database '
                                + 'for this project!')


def insert_pcr_information_into_db(pcrs, tubes, plate_list):
    for run_id, run_obj in pcrs.items():
        for run_position, pcr_obj in run_obj.items():
            pcr = PCR()
            pcr.run = run_id
            pcr.run_position = run_position
            pcr.cycles = pcr_obj['cycle']
            pcr.denaturation_time = pcr_obj['denatTime']
            pcr.hybridation_time = pcr_obj['hybridTime']
            pcr.synthesis_time = pcr_obj['synthTime']
            pcr.final_synthesis_time = pcr_obj['finalSynthTime']
            pcr.denaturation_temperature = pcr_obj['denatTemp']
            pcr.hybridation_temperature = pcr_obj['hybridTemp']
            pcr.synthesis_temperature = pcr_obj['synthTemp']
            pcr.final_synthesis_temperature = pcr_obj['finalSynthTemp']
            pcr.machine_id = pcr_obj['machineId']
            plate_id = pcrs[run_id][run_position]['plate']
            pcr.plate = plate_list[plate_id]
            pcr.save()
            for tube_info in tubes[plate_id]:
                pcr.tube.add(tube_info['tube'])


def get_all_project_PCR(project_id):
    pcr_list = dict()
    pcr_query = PCR.objects.all().filter(
            tube__project__id=project_id
        ).distinct()
    for pcr in pcr_query:
        if pcr.run not in pcr_list:
            pcr_list[pcr.run] = dict()
        pcr_list[pcr.run][pcr.run_position] = pcr
    return pcr_list
