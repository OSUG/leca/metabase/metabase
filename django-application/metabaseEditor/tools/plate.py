from django.db import InternalError, IntegrityError
from django.db.models import Count, Q

from ..models import Well, Plate_96, Extract, Unused_extract, Submission_file

from .primerPair import *
from .imported_data import (instanciate_imported_data, add_qualifier,
                            push_qualifiers)

def check_pcr_plate_extract_id_integrity(extracts, checking_dict):
    warning_messages = list()
    for extract in extracts:
        if (extract == checking_dict['blk']
                or extract == checking_dict['PCR_neg']):
            continue
        if (extract not in checking_dict['extracts'] and
            extract not in checking_dict['controls']):
            raise InternalError('The extract "' + str(extract)
                                + '" used in the "PCR Plate" sheet'
                                + ' is not referenced in the'
                                + ' extraction or controls sheets!')

    for extract in checking_dict['extracts']:
        if extract not in extracts:
            warning_messages.append('The extract "' + str(extract)
                                    + '" referenced in "Extraction"'
                                    + ' and "Controls" sheets'
                                    + ' is not used in the pcr plate sheet!')
    return warning_messages


def check_well_unicity(row_obj, well, checking_dict):
    if well is None:
        return False
    if row_obj['tforward'] != well.barcode_forward:
        return False
    if row_obj['treverse'] != well.barcode_reverse:
        return False
    if row_obj['pforward'] != well.primer.forward_sequence:
        return False
    if row_obj['preverse'] != well.primer.reverse_sequence:
        return False
    extract_id = row_obj['extractId']
    if extract_id == checking_dict['PCR_neg'] and well.wtype != 'PCRNeg':
        return False
    elif extract_id == checking_dict['blk'] and well.wtype != 'blank':
        return False
    elif well.wtype == 'extract' and extract_id != well.extract.name:
        return False
    return True


def check_plate_identity(plates, plate_list, checking_dict):
    for plate_name, plate_obj in plates.items():
        if plate_name in plate_list:
            for column_id, col_obj in plate_obj.items():
                for row_id, row_obj in col_obj.items():
                    well = Well.objects.filter(
                        plate__id=plate_list[plate_name].id, row=row_id,
                        column=column_id).select_related('primer').first()
                    if not check_well_unicity(row_obj, well, checking_dict):
                        raise IntegrityError('The plate "' + str(plate_name)
                        + '" is not identical to the plate in MetaBase')


def check_imported_well(row_obj, well):
    if row_obj['tforward'] != well['barcode_forward']:
        return False
    if row_obj['treverse'] != well['barcode_reverse']:
        return False
    if row_obj['pforward'] != well['primer_forward']:
        return False
    if row_obj['preverse'] != well['primer_reverse']:
        return False
    if row_obj['extractId'] != well['extract']:
        return False
    return True


def check_imported_plate(plates, imported_data):
    imported_plates = imported_data['plates']
    qualifiers_list = list()
    plate_list = list()
    row_list = list()
    col_list = list()
    for plate_name, plate_obj in imported_plates.items():
        if plate_name in plates:
            for column_id, col_obj in plate_obj.items():
                column_id = int(column_id)
                if column_id not in plates[plate_name]:
                    raise IntegrityError('The plate "' + str(plate_name)
                    + '" is not identical to the plate in the import batch.'
                    + ' The column ' + str(column_id) + ' not exist')
                for row_id, row_obj in col_obj.items():
                    row_id = str(row_id)
                    if row_id not in plates[plate_name][column_id]:
                        raise IntegrityError('The plate "' + str(plate_name)
                        + '" is not identical to the plate in the import'
                        + ' batch. The row ' + str(row_id) + ' not exist')
    for plate_name, plate_obj in plates.items():
        if plate_name in imported_plates:
            for column_id, col_obj in plate_obj.items():
                column_id = str(column_id)
                if column_id not in imported_plates[plate_name]:
                    raise IntegrityError('The plate "' + str(plate_name)
                    + '" is not identical to the plate in the import batch.'
                    + ' The column ' + str(column_id) + ' not exist')
                for row_id, row_obj in col_obj.items():
                    row_id = str(row_id)
                    if row_id not in imported_plates[plate_name][column_id]:
                        raise IntegrityError('The plate "' + str(plate_name)
                        + '" is not identical to the plate in the import'
                        + ' batch. The row ' + str(row_id) + ' not exist')
                    well = imported_plates[plate_name][column_id][row_id]
                    if not check_imported_well(row_obj, well):
                        raise IntegrityError('The plate "' + str(plate_name)
                        + '" is not identical to the plate in the import batch.')
        else:
            plate = instanciate_imported_data('platesname' + str(plate_name),
                                              'plates', 'name', plate_name,
                                              imported_data['project'])
            plate_list.append(plate)
            for column_id, col_obj in plate_obj.items():
                col_id = instanciate_imported_data('platescol' + str(column_id),
                                                   'plates', 'column_id',
                                                   column_id, plate)
                col_list.append(col_id)
                for row_id, row_obj in col_obj.items():
                    row_id = instanciate_imported_data('platesrow' + str(row_id)
                                                       + str(col_id),
                                                       'plates', 'row_id',
                                                       row_id, col_id)
                    row_list.append(row_id)
                    qualifiers_list.append(add_qualifier('plates',
                                                         'barcode_forward',
                                                         row_obj['tforward'],
                                                         row_id))
                    qualifiers_list.append(add_qualifier('plates',
                                                         'barcode_reverse',
                                                         row_obj['treverse'],
                                                         row_id))
                    qualifiers_list.append(add_qualifier('plates',
                                                         'primer_forward',
                                                         row_obj['pforward'],
                                                         row_id))
                    qualifiers_list.append(add_qualifier('plates',
                                                         'primer_reverse',
                                                         row_obj['preverse'],
                                                         row_id))
                    qualifiers_list.append(add_qualifier('plates',
                                                         'extract',
                                                         row_obj['extractId'],
                                                         row_id))
    push_qualifiers(plate_list)
    push_qualifiers(col_list)
    push_qualifiers(row_list)
    push_qualifiers(qualifiers_list)


def add_plate_into_db(plates, plate_list):
    added_plate = dict()
    for plate_id in list(plates):
        if plate_id not in plate_list:
            plate_96 = Plate_96()
            plate_96.name = plate_id
            plate_96.save()
            added_plate[plate_id] = plate_96
            plate_list[plate_id] = plate_96
    return added_plate


def check_pcr_plate_id_integrity(plates, pcrs):
    for plate_id in plates:
        if plate_id not in pcrs:
            raise InternalError('The plate "' + str(plate_id)
                                + '" not exist in the PCR sheet!')
    for plate_id in pcrs:
        if plate_id not in plates:
            raise InternalError('The plate "' + str(plate_id)
                                + '" is not used in the '
                                + 'PCR plate sheet!')


def insert_pcr_plate_into_db(plates, plate_list, control, extract_list,
                             primer_pairs):
    used_extract = list()
    for plate_id, plate_obj in plates.items():
        if plate_id not in plate_list:
            continue
        for column_id, col_obj in plate_obj.items():
            for row_id, row_obj in col_obj.items():
                primer_forward = row_obj['primer']['pforward']
                primer_reverse = row_obj['primer']['preverse']
                extract_id = row_obj['extractId']
                well = Well()
                well.barcode_forward = row_obj['tforward']
                well.barcode_reverse = row_obj['treverse']
                well.plate = plate_list[plate_id]
                well.column = column_id
                well.row = row_id
                if extract_id == control['negPCRctl']:
                    well.wtype = 'PCRNeg'
                elif extract_id == control['blkPCRctl']:
                    well.wtype = 'blank'
                else:
                    well.extract = extract_list[extract_id]
                    well.wtype = 'extract'
                    used_extract.append(extract_list[extract_id].id)
                well.dilution = float(str(row_obj['dilution']).replace(",", "."))
                well.primer = primer_pairs[primer_forward][primer_reverse]
                well.save()
    return used_extract


def get_project_plates(project_id):
    return Plate_96.objects.filter(
            pcr__tube__project__id=project_id).distinct()


def get_dict_of_project_plate(project_id):
    plate_list = dict()
    plate_query = get_project_plates(project_id)
    for plate in plate_query:
        plate_list[plate.name] = plate
    return plate_list


def get_deleted_project_plates(project_id):
    parentProject_count = Count('pcr__tube__project__parentProject__id',
                                distinct=True)
    return Plate_96.objects.annotate(
            parentProject_count=parentProject_count
        ).filter(
            Q(pcr__tube__project__id=project_id) |
            (Q(pcr__tube__project__parentProject__id=project_id) &
             Q(parentProject_count__lt=2)))


def get_submission_file_plates_for_deletion(submission_file_id):
    submission_file_count = Count('pcr__tube__submission_file', filter=~Q(
            pcr__tube__submission_file__id=submission_file_id),
            distinct=True)
    plates = Plate_96.objects.annotate(
            submission_file_count=submission_file_count).filter(
                pcr__tube__submission_file__id=submission_file_id
                ).exclude(submission_file_count__gt=0)
    return plates


def remove_plates(plate_ids, submission_file_id):
    submission_file = Submission_file.objects.get(id=submission_file_id)
    extract_ids = Well.objects.filter(
            plate__id__in=plate_ids).values_list("extract__id",
                                                 flat=True).distinct()
    extracts = Extract.objects.filter(id__in=extract_ids).annotate(
            well_count=Count("well"),
            unused_extract_count=Count("unused_extract")
            ).filter(unused_extract_count=0, well_count__lte=1).distinct()
    for extract in extracts:
        unused_extract = Unused_extract()
        unused_extract.extract = extract
        unused_extract.submission_file = submission_file
        unused_extract.save()

    Plate_96.objects.filter(id__in=plate_ids).delete()
