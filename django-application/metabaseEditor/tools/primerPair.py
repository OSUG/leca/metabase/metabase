from django.db.models import Count, Q

from ..models import PrimerPair, Well


def populate_primer_pair_table(primers):
    primer_pairs = dict()
    for p_forward, pf_dict in primers.items():
        primer_pairs[p_forward] = dict()
        for p_reverse, primer_pair in pf_dict.items():
            primer = PrimerPair.objects.all().filter(
                forward_sequence = primer_pair['pforward'],
                reverse_sequence = primer_pair['preverse'])
            if primer.exists():
                primer = PrimerPair.objects.get(
                    forward_sequence = primer_pair['pforward'],
                    reverse_sequence = primer_pair['preverse'])
            else:
                primer = PrimerPair()
                primer.forward_sequence = primer_pair['pforward']
                primer.reverse_sequence = primer_pair['preverse']
                primer.save()
            primer_pairs[p_forward][p_reverse] = primer
    return primer_pairs


def get_submission_file_primer_for_deletion(submission_file_id):
    submission_file_count = Count('well__plate__pcr__tube__submission_file',
        filter=~Q(
            well__plate__pcr__tube__submission_file__id=submission_file_id),
            distinct=True)
    primers = PrimerPair.objects.annotate(
            submission_file_count=submission_file_count).filter(
                well__plate__pcr__tube__submission_file__id=submission_file_id
                ).exclude(submission_file_count__gt=0)
    return primers


def get_plate_primer_for_deletion(plate_ids):
    to_keep = Well.objects.exclude(
            plate__id__in=plate_ids).values_list("primer__id", flat=True).distinct()
    to_remove = Well.objects.filter(
            plate__id__in=plate_ids).values_list("primer__id", flat=True).distinct()
    primers = PrimerPair.objects.filter(id__in=to_remove).exclude(
            id__in=to_keep)
    return primers


def get_project_primers_for_deletion(project_id):
    project_count = Count('well__plate__pcr__tube__project__id',
        filter=~Q(well__plate__pcr__tube__project__id=project_id),
        distinct=True)
    primers = PrimerPair.objects.annotate(project_count=project_count).filter(
        well__plate__pcr__tube__project__id=project_id
        ).exclude(project_count__gt=0)
    return primers
