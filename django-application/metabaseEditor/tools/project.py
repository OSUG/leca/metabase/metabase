from django.db.models import Count

from ..models import Project, Submission_file, Library


def define_active_tab(project_id):
    parent_project = Project.objects.filter(parentProject__id=project_id)
    experiment = Submission_file.objects.all().filter(
        tube_mix__project__id=project_id)
    library = Library.objects.filter(tube__project__id=project_id)
    if parent_project.count() == 0:
        if experiment.count() == 0:
            return 'experiment'
        elif library.count() == 0:
            return 'sequencing'
    return 'project-information'


def is_parent_project(project_id):
    parent_project = Project.objects.filter(parentProject__id=project_id)
    if parent_project.count() > 0:
        return True
    return False


def get_project_link_to_sequencing(sequencing_file_id):
    return Project.objects.filter(
        tube_mix__libraries__sequencing_file__information__id=sequencing_file_id
        ).distinct()


def get_subproject(project_id):
    parentProject_count = Count('parentProject__id', distinct=True)
    return Project.objects.annotate(
        parentProject_count=parentProject_count
        ).filter(parentProject__id=project_id,
                 parentProject_count__lt=2).distinct()
