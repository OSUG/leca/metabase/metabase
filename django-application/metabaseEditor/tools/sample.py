from django.db import IntegrityError
from django.db.models import Count, Q

from ..models import Sample, Sample_qualifier, Unused_sample
from .imported_data import (instanciate_imported_data, add_qualifier,
                            push_qualifiers)

def check_sample_unicity(sample_name, qualifiers, sample_list):
    qualifiers_list = Sample_qualifier.objects.all().filter(
        sample__id=sample_list[sample_name].id)
    keys = dict()
    for q in qualifiers_list:
        if q.key not in qualifiers:
            raise IntegrityError('Sample ' + str(sample_name)
                                 +' already exist with more qualifiers'
                                 + '(missing qualifier "'+ str(q.key) + '")!')
        keys[q.key] = q.value
    for k, v in qualifiers.items():
        if k not in keys:
            return False
        else:
            if str(v) != str(keys[k]):
                return False
    return True


def check_imported_sample(sample_name, qualifiers, imported_sample):
    for key, value in imported_sample.items():
        if key not in qualifiers:
            raise IntegrityError('Sample ' + str(sample_name)
                                 + ' already exist with more qualifiers'
                                 + ' (missing qualifier "'+ str(key) + '")!')
    for key, value in qualifiers.items():
        if key not in imported_sample:
            raise IntegrityError('Sample ' + str(sample_name)
                                 + ' already exist with less qualifiers!'
                                 + ' (qualifier "' + str(key)
                                 + '" not exist)')
        elif str(value) != str(imported_sample[key]):
            raise IntegrityError('Sample ' + str(sample_name)
                                 + ' already exist with other qualifiers!'
                                 + ' (qualifier "' + str(key)
                                 + '" exist with another value "'
                                 + str(value) + '")')


def check_sample_unicity_db(samples, sample_list, imported_dict):
    qualifier_list = list()
    sample_list = list()
    for sample_id, qualifiers in samples.items():
        if sample_id in sample_list:
            if not check_sample_unicity(sample_id, qualifiers, sample_list):
                raise IntegrityError('Sample ' + str(sample_id)
                                     + ' already exist with other qualifiers!')
        elif sample_id in imported_dict['samples']:
            imported_sample = imported_dict['samples'][sample_id]
            check_imported_sample(sample_id, qualifiers, imported_sample)
        else:
            import_sample = instanciate_imported_data('samplename' + str(sample_id),
                                                      'samples', 'name',
                                                       sample_id,
                                                       imported_dict['project'])
            sample_list.append(import_sample)
            for key, value in qualifiers.items():
                qualifier_list.append(add_qualifier("samples", key, value,
                                                    import_sample))
    push_qualifiers(sample_list)
    push_qualifiers(qualifier_list)


def populate_sample_qualifier_table(qualifier, sample):
    for key, value in qualifier.items():
        sample_qualifier = Sample_qualifier()
        sample_qualifier.key = key
        sample_qualifier.value = value
        sample_qualifier.sample = sample
        sample_qualifier.save()


def populate_sample_table(samples, sample_list):
    for sample_id, qualifiers in samples.items():
        if sample_id not in sample_list:
            sample = Sample(name = sample_id)
            sample.save()
            sample_list[sample_id] = sample
            populate_sample_qualifier_table(qualifiers, sample)


def identify_unused_sample(extracts, sample_list, submission_file):
    extract_sample = list()
    for extract in extracts:
        if extract['sample'] not in extract_sample:
            extract_sample.append(extract['sample'])
    for sample_id, sample in sample_list.items():
        if sample_id not in extract_sample:
            count_unused_sample = Unused_sample.objects.filter(
                submission_file=submission_file, sample=sample).count()
            if count_unused_sample == 0:
                unused_sample = Unused_sample()
                unused_sample.sample = sample
                unused_sample.submission_file = submission_file
                unused_sample.save()


def add_sample(samples, sample_list, submission_file):
    for sample_id, qualifiers in samples.items():
        if sample_id not in sample_list:
            sample = Sample(name = sample_id)
            sample.save()
            sample_list[sample_id] = sample
            populate_sample_qualifier_table(qualifiers, sample)
        else:
            sample = sample_list[sample_id]
        unused_sample = Unused_sample()
        unused_sample.sample = sample
        unused_sample.submission_file = submission_file
        unused_sample.save()


def clear_unused_samples(submission_file, used_samples):
    Unused_sample.objects.filter(submission_file=submission_file,
                                 sample__id__in=used_samples).delete()


def get_project_samples(project_id):
    return Sample.objects.filter(
        Q(extract__well__plate__pcr__tube__project__id=project_id) |
        Q(unused_sample__submission_file__tube_mix__project__id=project_id) |
        Q(extract__unused_extract__submission_file__tube_mix__project__id=project_id)
    ).distinct()


def get_dict_of_project_sample(project_id):
    sample_list = dict()
    sample_query = get_project_samples(project_id)
    for sample in sample_query:
        sample_list[sample.name] = sample
    return sample_list


def get_submission_file_samples_for_deletion(submission_file_id):
    sample_filter = Count('extract__well__plate__pcr__tube__submission_file',
    filter=~Q(
        extract__well__plate__pcr__tube__submission_file__id=submission_file_id
        ), distinct=True)
    samples = Sample.objects.annotate(
            submission_file_count=sample_filter).filter(
        extract__well__plate__pcr__tube__submission_file__id=submission_file_id
    ).exclude(Q(submission_file_count__gt=0) | Q(unused_sample__isnull=False))

    unused_sample_filter = Count('unused_sample__submission_file',
                filter=~Q(
                    unused_sample__submission_file__id=submission_file_id),
                distinct=True)
    unused_samples = Sample.objects.annotate(
        submission_file_count=unused_sample_filter).filter(
        unused_sample__submission_file__id=submission_file_id
        ).exclude(Q(submission_file_count__gt=0) | Q(extract__isnull=False))

    # get the number of submission file without current submission file
    unused_extract_sample_filter = Count(
        'extract__unused_extract__submission_file',
        filter=~Q(
            extract__unused_extract__submission_file__id=submission_file_id),
            distinct=True)
    unused_extract_samples = Sample.objects.annotate(
        submission_file_count=unused_extract_sample_filter,
        used_sample_count=sample_filter,
        unused_sample_count=unused_sample_filter
    ).filter(
        extract__unused_extract__submission_file__id=submission_file_id,
        used_sample_count=0,
        unused_sample_count=0
    ).exclude(submission_file_count__gt=0)
    return (samples | unused_samples | unused_extract_samples).distinct()


def get_deleted_project_samples(project_id):
    e_parentProject_count = Count('extract__well__plate__pcr__tube__project__parentProject',
                                distinct=True)
    us_parentProject_count = Count('unused_sample__submission_file__tube_mix__project__parentProject',
                                distinct=True)
    ue_parentProject_count = Count('extract__unused_extract__submission_file__tube_mix__project__parentProject',
                                distinct=True)
    return Sample.objects.annotate(
        e_parentProject_count=e_parentProject_count,
        ue_parentProject_count=ue_parentProject_count,
        us_parentProject_count=us_parentProject_count
    ).filter(
        Q(extract__well__plate__pcr__tube__project__id=project_id) |
        Q(unused_sample__submission_file__tube_mix__project__id=project_id) |
        Q(extract__unused_extract__submission_file__tube_mix__project__id=project_id) |
        (Q(extract__well__plate__pcr__tube__project__parentProject__id=project_id) &
         Q(e_parentProject_count__lt=2)) |
        (Q(unused_sample__submission_file__tube_mix__project__parentProject__id=project_id) &
         Q(us_parentProject_count__lt=2)) |
        (Q(extract__unused_extract__submission_file__tube_mix__project__parentProject__id=project_id) &
         Q(us_parentProject_count__lt=2))
    ).distinct()
