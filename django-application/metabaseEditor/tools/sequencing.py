from django.db import transaction
from django.utils.timezone import localtime
from django.utils import timezone
from django.db.models.functions import Concat
from django.db.models import Value

import re
import base64
from datetime import datetime

from ..models import (Tube_mix, Library, Sequencing_information,
                      Sequencing_file)

from ..parser.sequencing import *

from .tube import add_sequencing_tube


def populate_sequencing_information(sequencing_file, project_information):
    sequencing_date = datetime.now().strftime("%y-%m-%d")
    if project_information['type'] == 'xml':
        temp_date = sequencing_file.name
    else:
        temp_date = project_information['project']['sequencing_date']
    m = re.search(r"(?P<dateYear>\d{4})\-"
                  r"(?P<dateMonth>\d{2})\-"
                  r"(?P<dateDay>\d{2})",str(temp_date))
    if m is not None:
        current_timezone = timezone.get_current_timezone()
        sequencing_date = datetime(int(m.group('dateYear')),
                                   int(m.group('dateMonth')),
                                   int(m.group('dateDay')),
                                   tzinfo=current_timezone)
    sequencing_information = Sequencing_information()
    sequencing_information.info_file = base64.b64encode(sequencing_file.read())
    sequencing_information.file_name = sequencing_file.name
    sequencing_information.file_type = project_information['type']
    sequencing_information.provider = project_information['project'][
        'facility_institution']
    if "customer_email" in project_information['project']:
        sequencing_information.contact = project_information[
                'project']['customer_email']
    elif 'customer_account' in project_information['project']:
        sequencing_information.contact = project_information[
                'project']['customer_account']
    else :
        sequencing_information.contact = "Unknown"

    sequencing_information.sequencing_date = sequencing_date
    sequencing_information.save()
    return sequencing_information


def populate_library(lib_name, tube):
    library = Library()
    library.name = lib_name
    library.tube = tube
    library.save()
    return library


@transaction.atomic
def add_library(library_name, tube_name, tube_mix={}):
    library = Library.objects.filter(name=library_name)
    if not library.exists():
        if tube_name not in tube_mix:
            new_tube = add_sequencing_tube(tube_name)
            tube_mix[tube_name] = new_tube
        else:
            new_tube = tube_mix[tube_name]
        library = populate_library(library_name, new_tube)
    else:
        library = Library.objects.get(name=library_name)
    return library


def populate_sequencing_file(file_name, run_info, sequencing_info, library):
    sequencing_file = Sequencing_file()
    sequencing_file.sequencingTechnology = run_info[
        'sequencingTechnology']
    sequencing_file.sequencingInstrument = run_info[
        'sequencingInstrument']
    sequencing_file.file_name = file_name
    sequencing_file.file_type = 'fastq.gz'
    sequencing_file.information = sequencing_info
    sequencing_file.library_name = library
    sequencing_file.save()
    return sequencing_file


@transaction.atomic
def insert_sequencing_information(sequencing_file):
    tube_mix = dict()
    parser_sequencing = sequencing_parsing(sequencing_file)
    sequencing_info = populate_sequencing_information(
        sequencing_file, parser_sequencing)
    for run in parser_sequencing['run']:
        for lib in run['libs']:
            library = add_library(lib['id'],
                                  lib['sampleName'], tube_mix)
            for file_name in lib['fileName']:
                sequencing_file = populate_sequencing_file(
                    file_name, run, sequencing_info, library)
    return sequencing_info


def get_available_libraries(project_id, sequencing_info_id):
    availabled_links = list()
    project_tube = list(Tube_mix.objects.filter(
        project__id=project_id, libraries__isnull=False
        ).values_list('name', flat=True))
    sequencing_libraries = list(Library.objects.filter(
        sequencing_file__information__id=sequencing_info_id
        ).exclude(tube__name__in=project_tube
        ).distinct().select_related('tube'))
    for library in sequencing_libraries:
        links = dict()
        links['id'] = library.tube.id
        links['library_name'] = library.name
        links['tube_name'] = library.tube.name
        availabled_links.append(links)
    return availabled_links


def sequencing_output(sequencing_info, sequencing_files, tubes, libraries):
    added_sequencing = dict()
    added_sequencing['id'] = sequencing_info.id
    added_sequencing['sequencing_date'] = localtime(
        sequencing_info.sequencing_date).strftime('%Y-%m-%d')
    added_sequencing['file_name'] = sequencing_info.file_name
    added_sequencing['sequencing_files'] = '; '.join(sequencing_files)
    added_sequencing['tubes'] = '; '.join(tubes)
    added_sequencing['libraries'] = '; '.join(libraries)
    return added_sequencing


def sequencing_import_information(sequencing_info):
    sequencing_files = list(Sequencing_file.objects.filter(
        information__id=sequencing_info.id).annotate(
            full_name=Concat('file_name', Value('.'), 'file_type')
        ).order_by('full_name').values_list('full_name', flat=True))
    libraries = list(Library.objects.filter(
        sequencing_file__information__id=sequencing_info.id
    ).distinct().order_by('name').values_list('name',flat=True))
    tubes = list(Tube_mix.objects.filter(
        libraries__in=libraries
    ).distinct().order_by('name').values_list('name',flat=True))
    added_sequencing = dict()
    added_sequencing['id'] = sequencing_info.id
    added_sequencing['sequencing_date'] = localtime(
        sequencing_info.sequencing_date).strftime('%Y-%m-%d')
    added_sequencing['file_name'] = sequencing_info.file_name
    added_sequencing['sequencing_files'] = '; '.join(sequencing_files)
    added_sequencing['tubes'] = '; '.join(tubes)
    added_sequencing['libraries'] = '; '.join(libraries)
    return added_sequencing


def get_project_sequecing_info_file(project_id):
    sequencing_info_file = Sequencing_information.objects.all().filter(
        sequencing_file__library_name__tube__project__id=project_id
        ).distinct()
    sif = list()
    for si in sequencing_info_file:
        sif.append(si.file_name)
    return sif


def get_unused_sequecing_info_file():
    sequencing_info_file = Sequencing_information.objects.all().filter(
        sequencing_file__library_name__tube__project__isnull=True
        ).distinct()
    sif = list()
    for si in sequencing_info_file:
        sif.append(si.file_name)
    return sif
