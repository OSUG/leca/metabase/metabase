from django.db import transaction
from django.db import InternalError
from django.db.models import Q, Count

from ..models import (Project, Submission_file, Tube_mix, Sample,
                      Extract, PrimerPair, Plate_96, PCR)

from .sample import get_submission_file_samples_for_deletion
from .extract import get_submission_file_extracts_for_deletion
from .plate import get_submission_file_plates_for_deletion
from .primerPair import get_submission_file_primer_for_deletion
from .project import is_parent_project


@transaction.atomic
def remove_submission_file(submission_file_id):
    samples = get_submission_file_samples_for_deletion(submission_file_id)
    sample_ids = list(samples.values_list('id', flat=True))

    extracts = get_submission_file_extracts_for_deletion(submission_file_id)
    extract_ids = list(extracts.values_list('id', flat=True))

    plates = get_submission_file_plates_for_deletion(submission_file_id)
    plate_ids = list(plates.values_list('id', flat=True))

    pcr_ids = list(PCR.objects.filter(
        tube__submission_file__id=submission_file_id
        ).values_list('id', flat=True))

    primers = get_submission_file_primer_for_deletion(submission_file_id)
    primer_ids = list(primers.values_list('id', flat=True))

    PrimerPair.objects.filter(id__in=primer_ids).delete()
    Sample.objects.filter(id__in=sample_ids).delete()
    Extract.objects.filter(id__in=extract_ids).delete()
    Plate_96.objects.filter(id__in=plate_ids).delete()
    PCR.objects.filter(id__in=pcr_ids).delete()

    sequencing_tube = list(Tube_mix.objects.filter(
        submission_file__id=submission_file_id, libraries__isnull=False))
    for tube in sequencing_tube:
        tube.submission_file = None
        tube.concentration = 0
        tube.quantity = 0
        tube.mtype = 'None'
        tube.save()
    Submission_file.objects.filter(id=submission_file_id).delete()


@transaction.atomic
def remove_on_fail(file_name, project_id):
    if file_name is None:
        query = Submission_file.objects.filter(
                tube_mix__project__id=project_id,
                tube_mix__pcr__isnull=True).distinct()
    else:
        query = Submission_file.objects.filter(
                tube_mix__project__id=project_id,
                file_name=file_name).distinct()
    if query.count() == 1:
        submission_file = query.first()
    else:
        project_name = Project.objects.get(id=project_id).name
        raise InternalError("Error, the submission file "
                            + str(file_name)
                            + " not exist or is duplicated for the project "
                            + str(project_name))

    samples = get_submission_file_samples_for_deletion(submission_file.id)
    sample_ids = list(samples.values_list('id', flat=True))

    extracts = get_submission_file_extracts_for_deletion(submission_file.id)
    extract_ids = list(extracts.values_list('id', flat=True))

    if len(sample_ids) > 0:
        Sample.objects.filter(id__in=sample_ids).delete()
    if len(extract_ids) > 0:
        Extract.objects.filter(id__in=extract_ids).delete()

    sequencing_tube = list(Tube_mix.objects.filter(
        submission_file__id=submission_file.id, libraries__isnull=False))
    for tube in sequencing_tube:
        tube.submission_file = None
        tube.concentration = 0
        tube.quantity = 0
        tube.mtype = 'None'
        tube.save()
    Submission_file.objects.filter(id=submission_file.id).delete()


def remove_only_submission_file(submission_file_id):
    sequencing_tube = list(Tube_mix.objects.filter(
        submission_file__id=submission_file_id, libraries__isnull=False))
    for tube in sequencing_tube:
        tube.submission_file = None
        tube.concentration = 0
        tube.quantity = 0
        tube.mtype = 'None'
        tube.save()
    Submission_file.objects.filter(id=submission_file_id).delete()


def get_project_submission_files(project_id):
    parentProject_count = Count('tube_mix__project__parentProject__id',
                                distinct=True)
    return Submission_file.objects.annotate(
        parentProject_count=parentProject_count
        ).filter(
        Q(tube_mix__project__id=project_id) |
        (Q(tube_mix__project__parentProject__id=project_id) &
        Q(parentProject_count__lt=2))).distinct()


def get_dict_of_project_submission_file(project_id):
    submission_file_project = list(get_project_submission_files(project_id))
    sfp = list()
    for sf in submission_file_project:
        sfp.append(sf.file_name)
    return sfp


def get_parentProject_children_submission_files(project_id):
    project_ids = Project.objects.filter(id=project_id).values_list(
            'parentProject__id', flat=True).distinct()
    return Submission_file.objects.filter(
        tube_mix__project__parentProject__id__in=project_ids).exclude(
                tube_mix__project__id=project_id).distinct()



def get_submission_file(file_name, project_id):
    query = Submission_file.objects.filter(
        file_name=file_name, tube_mix__project__id=project_id).distinct()
    if query.count() == 1:
        return query.first()
    return None;

def get_project_submission_file_by_name(file_name, project_id):
    query = Submission_file.objects.filter(
            tube_mix__project__id=project_id,
            file_name=file_name).distinct()
    if query.count() != 1:
        project_name = Project.objects.get(id=project_id).name
        raise InternalError("Error, the submission file "
                            + str(file_name)
                            + " not exist or is duplicated for the project "
                            + str(project_name))
    return query.first()
