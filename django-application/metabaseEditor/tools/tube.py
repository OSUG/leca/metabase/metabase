from django.db import InternalError
from django.db.models import Q, Count

from django.shortcuts import get_object_or_404

from ..models import Tube_mix


def add_sequencing_tube(tube_name):
    new_tube = Tube_mix()
    new_tube.name = tube_name
    new_tube.mtype = 'None'
    new_tube.save()
    return new_tube


def check_tube_plate_id_integrity(tubes, plates):
    for plate_id in tubes['plates']:
        if plate_id not in plates:
            raise InternalError('The plate id "' + str(plate_id)
                                + '" not exist in the sheet PCR plate!')

    for plate_id in plates:
        if plate_id not in tubes['plates']:
            raise InternalError('The plate id "' + str(plate_id)
                                + '" is not used in the '
                                + 'sheet mix (tube)!')


def populate_tube_table(tubes, project, submit_file):
    tube_list = list()
    for plate_id, tubes in tubes.items():
        for tube_info in tubes:
            tube_mix = Tube_mix.objects.filter(
                    submission_file__id=submit_file.id,
                    name=tube_info['tubeName'])
            if tube_mix.count() == 0:
                existed_tube = Tube_mix.objects.filter(name=tube_info['tubeName'],
                                                       project__id=project.id)
                if existed_tube.exists():
                    tube_mix = get_object_or_404(Tube_mix,
                                                 name=tube_info['tubeName'],
                                                 project__id=project.id)
                else:
                    tube_mix = Tube_mix()
                    tube_mix.name = tube_info['tubeName']
                    tube_mix.project = project
                tube_mix.submission_file = submit_file
                tube_mix.mtype = tube_info['type']
                tube_mix.quantity = tube_info['quantity']
                tube_mix.concentration = tube_info['concentration']
                tube_mix.save()
            else:
                tube_mix = get_object_or_404(Tube_mix,
                                             submission_file__id=submit_file.id,
                                             name=tube_info['tubeName'])
            tube_info['tube'] = tube_mix
            if tube_mix.name not in tube_list:
                tube_list.append(tube_mix.name)
    return tube_list


def add_submited_tubes(tubes, submit_file):
    for plate_id, tube_list in tubes.items():
        for tube_info in tube_list:
            tube_mix = get_object_or_404(Tube_mix,
                                         submission_file__id=submit_file.id,
                                         name=tube_info['tubeName'])
            tube_info['tube'] = tube_mix


def get_deleted_project_tubes(project_id):
    parentProject_count = Count('project__parentProject__id',
                                distinct=True)
    return Tube_mix.objects.annotate(
            parentProject_count=parentProject_count
        ).filter(Q(project__id=project_id) |
                 (Q(project__parentProject__id=project_id) &
                  Q(parentProject_count__lt=2)))


def get_all_unused_tube():
    project_tube = Tube_mix.objects.all().filter(project__isnull=True)
    tubes = dict()
    for pt in project_tube:
        tubes[pt.name] = pt
    return tubes


def get_all_used_tube():
    project_tube = Tube_mix.objects.all().filter(project__isnull=False)
    tubes = dict()
    for pt in project_tube:
        tubes[pt.name] = pt
    return tubes

def get_sequencing_tubes(sequencing_info_id):
    return Tube_mix.objects.all().filter(
        libraries__sequencing_file__information__id=sequencing_info_id
        ).distinct()
