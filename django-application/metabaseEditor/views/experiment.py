from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.db import transaction, IntegrityError, InternalError
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.utils.timezone import localtime
from django.template.loader import render_to_string
from django.contrib import messages

import base64
import openpyxl
import json as simplejson

from ..tools.decorator import check_user_login

from ..models import Submission_file, Tube_mix, Sample, Project, Library, Imported_data_tmp, Deleted_data_tmp

from ..forms import ExperimentForm

from ..parser.control import *
from ..parser.sample import *
from ..parser.extract import *
from ..parser.pcr_plate import *
from ..parser.pcr import *
from ..parser.tube import *

from ..tools.sample import *
from ..tools.extract import *
from ..tools.pcr import *
from ..tools.plate import *
from ..tools.submission_file import *
from ..tools.tube import *
from ..tools.primerPair import *
from ..tools.decorator import (check_user_login, check_user_right_js,
                               check_user_auth_js, check_user_right_exp_js)
from ..tools.imported_data import *
from ..tools.deleted_data import *

@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def get_project_experiment(request, project_id=None):
    experiments = list()
    if not project_id:
        return JsonResponse({'data': experiments},
                            content_type='application/json')
    try:
        submit_file_project = Submission_file.objects.all().filter(
            tube_mix__project__id = project_id).distinct()
        for sfp in submit_file_project:
            tube = Tube_mix.objects.all().filter(
                submission_file__id=sfp.id).values_list('name', flat=True)
            exp = dict()
            exp['id'] = sfp.id
            exp['file_name'] = sfp.file_name
            exp['submission_date'] = localtime(
                sfp.submission_date).strftime('%Y-%m-%d %H:%M')
            exp['tube_list'] = ' ; '.join(tube)
            experiments.append(exp)
        return JsonResponse({'data': experiments},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        messages.error(request, 'An error occur on the experiment loading.'
                       + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        html_msg = render_to_string(
            'parts/information-message.html', request=request)
        return JsonResponse({'data': experiments, 'html': html_msg},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def get_delete_info_libraries(request, submission_file_id):
    warnings = list()
    try:
        linked_library = Library.objects.filter(
            tube__submission_file__id=submission_file_id
            ).distinct().values_list('name', flat=True)
        if linked_library.count() > 0:
            warnings.append({'message': 'The following libraries wil be affected:',
                            'list': list(linked_library)})
        else:
            warnings.append('The experiment file has not linked libraries!')

        output = {'warnings' : warnings}
        return JsonResponse(output, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def get_delete_info_tubes(request, submission_file_id):
    warnings = list()
    try:
        tubes = Tube_mix.objects.filter(
            submission_file__id=submission_file_id
            ).values_list('name', flat=True)
        if tubes.count() > 0:
            warnings.append({'message': 'The following tubes wil be removed:',
                         'list': list(tubes)})
        else:
            warnings.append('The experiment file has not linked tubes!')

        output = {'warnings' : warnings}
        return JsonResponse(output, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def get_delete_info_plates(request, submission_file_id):
    warnings = list()
    try:
        plates = get_submission_file_plates_for_deletion(submission_file_id)
        plates_name = plates.values_list('name', flat=True)
        if plates_name.count() > 0:
            warnings.append({'message': 'The following plates wil be removed:',
                         'list': list(plates_name)})
        else:
            warnings.append('The experiment file has not linked plates!')

        primers = get_plate_primer_for_deletion(
                list(plates.values_list('id', flat=True))).count()
        warnings.append('Number of primer pairs that will be removed: '
                        + str(primers))

        output = {'warnings' : warnings}
        return JsonResponse(output, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def get_delete_info_samples(request, submission_file_id):
    warnings = list()
    try:
        samples = get_submission_file_samples_for_deletion(
            submission_file_id).count()
        warnings.append('Number of sample that will be removed: ' + str(samples))

        output = {'warnings' : warnings}
        return JsonResponse(output, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def get_delete_info_extracts(request, submission_file_id):
    warnings = list()
    try:
        extracts = get_submission_file_extracts_for_deletion(
            submission_file_id).count()
        warnings.append('Number of extract that will be removed: ' + str(extracts))

        output = {'warnings' : warnings}
        return JsonResponse(output, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def identify_deleted_submission_file(request, project_id, submission_file_id):
    try:
        deleted_project = get_project_deleted_data(project_id)
        submit_file_deleted_data(submission_file_id, deleted_project)
        return JsonResponse({'step': 'Identify experiment file to delete',
                             'step_number': "1/5"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def identify_deleted_tubes(request, submission_file_id):
    deleted_tubes = list()
    try:
        submit_file = get_delete_submission_file(submission_file_id)
        tube_ids = list(Tube_mix.objects.filter(
                submission_file__id=submission_file_id,
                libraries__isnull=False
                ).distinct().values_list('id', flat=True))
        for tube_id in tube_ids:
            deleted_tubes.append(instanciate_deleted_data('tubes',
                                                          tube_id,
                                                          submit_file))
        if len(deleted_tubes) > 0:
            push_deleted_data(deleted_tubes)

        return JsonResponse({'step': 'Identify tubes to delete',
                             'step_number': "2/5"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def identify_deleted_plates_primers(request, submission_file_id):
    deleted_data = list()
    try:
        submit_file = get_delete_submission_file(submission_file_id)

        plate_ids = list(get_submission_file_plates_for_deletion(
                submission_file_id).values_list('id', flat=True))
        for plate_id in plate_ids:
            deleted_data.append(instanciate_deleted_data("plates", plate_id,
                                                         submit_file))

        primer_ids = list(get_plate_primer_for_deletion(
                plate_ids).values_list('id', flat=True))

        for primer_id in primer_ids:
            deleted_data.append(instanciate_deleted_data("primers", primer_id,
                                                         submit_file))

        if len(deleted_data) > 0:
            push_deleted_data(deleted_data)

        return JsonResponse({'step': 'Identify plates and primers to delete',
                             'step_number': "3/5"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def identify_deleted_samples(request, submission_file_id):
    deleted_samples = list()
    try:
        submit_file = get_delete_submission_file(submission_file_id)
        sample_ids = list(get_submission_file_samples_for_deletion(
            submission_file_id).values_list('id', flat=True))

        for sample_id in sample_ids:
            deleted_samples.append(instanciate_deleted_data("samples",
                                                            sample_id,
                                                            submit_file))
        if len(deleted_samples) > 0:
            push_deleted_data(deleted_samples)

        return JsonResponse({'step': 'Identify samples to delete',
                             'step_number': "4/5"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def identify_deleted_extracts(request, submission_file_id):
    deleted_extracts = list()
    try:
        submit_file = get_delete_submission_file(submission_file_id)

        extract_ids = list(get_submission_file_extracts_for_deletion(
                submission_file_id).exclude(
                        etype="sample").values_list("id", flat=True))

        for extract_id in extract_ids:
            deleted_extracts.append(
                    instanciate_deleted_data("controls", extract_id,
                                             submit_file))
        if len(deleted_extracts) > 0:
            push_deleted_data(deleted_extracts)

        return JsonResponse({'step': 'Identify extracts to delete',
                             'step_number': "5/5"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def delete_plates(request, submission_file_id):
    try:
        plate_ids = list(get_deleted_id_by_type(submission_file_id, "plates"))
        Plate_96.objects.filter(id__in=plate_ids).delete()
        remove_deleted_data("plates", plate_ids)

        return JsonResponse({'step': 'Delete plates',
                             'step_number': "1/6"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def delete_primers(request, submission_file_id):
    try:
        primer_ids = list(get_deleted_id_by_type(submission_file_id,
                                                 "primers"))
        PrimerPair.objects.filter(id__in=primer_ids).delete()
        remove_deleted_data("primers", primer_ids)

        return JsonResponse({'step': 'Delete primers',
                             'step_number': "2/6"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def delete_samples(request, submission_file_id):
    try:
        sample_ids = list(get_deleted_id_by_type(submission_file_id,
                                                 "samples"))
        Sample.objects.filter(id__in=sample_ids).delete()
        remove_deleted_data("samples", sample_ids)

        return JsonResponse({'step': 'Delete samples',
                             'step_number': "3/6"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def delete_controls(request, submission_file_id):
    try:
        controls_ids = list(get_deleted_id_by_type(submission_file_id,
                                                   "controls"))
        Extract.objects.filter(id__in=controls_ids).delete()
        remove_deleted_data("controls", controls_ids)

        return JsonResponse({'step': 'Delete controls',
                             'step_number': "4/6"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_exp_js
def delete_tubes(request, submission_file_id):
    try:
        tubes_ids = list(get_deleted_id_by_type(submission_file_id, "tubes"))
        Tube_mix.objects.filter(id__in=tubes_ids).update(
                    submission_file=None, concentration=0, quantity=0,
                    mtype='None')
        remove_deleted_data("tubes", tubes_ids)

        return JsonResponse({'step': 'Delete tubes',
                             'step_number': "5/6"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def delete_submission_file(request, project_id, submission_file_id):
    try:
        Submission_file.objects.filter(id=submission_file_id).delete()
        clear_deleted_table(submission_file_id)

        return JsonResponse({'step': 'Delete submission file',
                             'step_number': "6/6"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def unlink_pcr_tube(request, project_id, submission_file_id):
    try:
        tubes_ids = list(get_deleted_id_by_type(submission_file_id, "tubes"))
        if len(tubes_ids) > 0:
            tubes = Tube_mix.objects.filter(id__in=tubes_ids)
        else:
            tubes = Tube_mix.objects.filter(
                    submission_file__id=submission_file_id,
                    libraries__isnull=False)
        for tube in tubes:
            tube.pcr_set.clear()

        return JsonResponse({'step': 'Unlink tube to pcr for remove experiment',
                             'step_number': "1/3"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg,
                       'errors_title': "- Clear Database"},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def unlink_tube_submission_file(request, project_id, submission_file_id):
    try:
        tubes_ids = list(get_deleted_id_by_type(submission_file_id, "tubes"))
        if len(tubes_ids) > 0:
            Tube_mix.objects.filter(id__in=tubes_ids).update(
                    submission_file=None, concentration=0, quantity=0,
                    mtype='None')
            remove_deleted_data("tubes", tubes_ids)
        else:
            Tube_mix.objects.filter(
                    submission_file__id=submission_file_id,
                    libraries__isnull=False
                    ).update(submission_file=None, concentration=0,
                             quantity=0, mtype='None')

        return JsonResponse({'step': 'Unlink tube to file for remove experiment',
                             'step_number': "2/3"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg,
                       'errors_title': "- Clear Database"},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def remove_exp_tube_wo_lib(request, project_id, submission_file_id):
    try:
        Tube_mix.objects.filter(
                submission_file__id=submission_file_id,
                libraries__isnull=True).delete()

        return JsonResponse({'step': 'Remove experiment tube without libraries',
                             'step_number': "3/3"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg,
                       'errors_title': "- Clear Database"},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def check_file_import(request, project_id):
    experiment_form = ExperimentForm(request.POST, request.FILES)
    file_name = None
    try:
        if experiment_form.is_valid():
            files = request.FILES.getlist('excelFilePaths')
            file_index = request.POST['file_index']
            file = files[int(file_index)]
            sfp = get_dict_of_project_submission_file(project_id)
            if file.name in sfp:
                raise IntegrityError('The submission file '
                                     + str(file.name) + ' already exist '
                                     + 'in this project!')

            sfpp = get_parentProject_children_submission_files(project_id)
            for sf in sfpp:
                if file.name == sf.file_name:
                    raise IntegrityError('The submission file '
                                         + str(file.name) + ' already exist '
                                         + 'in the parent project!')
                buff = file.read()
                if buff == base64.b64decode(sf.excel_file):
                    raise IntegrityError('The submission file '
                                         + str(file.name) + ' already exist '
                                         + 'with the name '
                                         + str(sf.file_name) + '!')

            return JsonResponse(
                    {'infos' : ['The submission file can be imported!']},
                    content_type='application/json')
        else:
            return JsonResponse(
                {'validatorErrors': experiment_form['excelFilePaths'].errors,
                 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except (IntegrityError, InternalError) as ie:
        clear_imported_table(project_id)
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': ie.args[0],
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'parsing'},
                            status=400)
    except Exception as e:
        clear_imported_table(project_id)
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': msg,
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def check_samples_import(request, project_id):
    experiment_form = ExperimentForm(request.POST, request.FILES)
    file_name = None
    try:
        if experiment_form.is_valid():
            infos = list()
            warnings = list()
            files = request.FILES.getlist('excelFilePaths')
            file_index = request.POST['file_index']
            file = files[int(file_index)]
            file_name = file.name
            sample_list = get_dict_of_project_sample(project_id)
            import_project = project_imported_data(str(project_id))
            imported_dict = {
                'project' : import_project,
                'samples' : get_imported_samples_dict(import_project),
            }
            excel_file = openpyxl.load_workbook(file, read_only = True,
                                                data_only = True)
            samples = excel_parsing_sample_checking(excel_file['Samples'])
            infos.append(samples["info"])
            samples = samples["data"]
            check_sample_unicity_db(samples, sample_list, imported_dict)
            if int(file_index) + 1 == len(files):
                clear_imported_table_type(project_id, "samples")
            output = {'infos' : infos}
            if len(warnings) > 0:
                output['warnings'] = warnings
            return JsonResponse(output, content_type='application/json')
        else:
            return JsonResponse(
                {'validatorErrors': experiment_form['excelFilePaths'].errors,
                 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except (IntegrityError, InternalError) as ie:
        clear_imported_table(project_id)
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': ie.args[0],
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'parsing'},
                            status=400)
    except Exception as e:
        clear_imported_table(project_id)
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': msg,
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def check_controls_import(request, project_id):
    experiment_form = ExperimentForm(request.POST, request.FILES)
    file_name = None
    try:
        if experiment_form.is_valid():
            infos = list()
            files = request.FILES.getlist('excelFilePaths')
            file_index = request.POST['file_index']
            file = files[int(file_index)]
            file_name = file.name
            ctl_pos_list = get_dict_of_project_pos_control(project_id)
            import_project = project_imported_data(str(project_id))
            imported_dict = {
                'project' : import_project,
                'controls' : get_imported_controls_dict(import_project)
            }

            excel_file = openpyxl.load_workbook(file, read_only = True,
                                                data_only = True)
            control = excel_parsing_control_checking(excel_file['Controls'])
            infos.extend(control["info"])
            control = control["data"]
            check_positive_control_db(control['ctlPos'], ctl_pos_list)
            check_imported_positive_control(control['ctlPos'], imported_dict)

            if int(file_index) + 1 == len(files):
                clear_imported_table_type(project_id, "controls")

            return JsonResponse(
                    {'infos' : infos},
                    content_type='application/json')
        else:
            return JsonResponse(
                {'validatorErrors': experiment_form['excelFilePaths'].errors,
                 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except (IntegrityError, InternalError) as ie:
        clear_imported_table(project_id)
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': ie.args[0],
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'parsing'},
                            status=400)
    except Exception as e:
        clear_imported_table(project_id)
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': msg,
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def check_extracts_import(request, project_id):
    experiment_form = ExperimentForm(request.POST, request.FILES)
    file_name = None
    try:
        if experiment_form.is_valid():
            infos = list()
            warnings = list()
            files = request.FILES.getlist('excelFilePaths')
            file_index = request.POST['file_index']
            file = files[int(file_index)]
            file_name = file.name
            extract_list = get_dict_of_project_extract(project_id)
            import_project = project_imported_data(str(project_id))
            imported_dict = {
                'project' : import_project,
                'extracts' : get_imported_extracts_dict(import_project)
            }
            checking_dict = {
                'samples' : list(),
                'ext_neg' : None
            }

            excel_file = openpyxl.load_workbook(file, read_only = True,
                                                data_only = True)
            extracts = excel_parsing_extract_checking(excel_file['Extraction'])
            infos.append(extracts["info"])
            extracts = extracts["data"]

            checking_dict['samples'] = excel_parsing_get_sample(
                    excel_file['Samples'])

            # get control identifier to avoid error
            controls = excel_parsing_get_control(excel_file['Controls'])
            checking_dict['ext_neg'] = controls['negCtl']

            check_extract_unicity_db(extracts, controls, extract_list,
                                     imported_dict)

            extract_warnings = check_extraction_sample_id_integrity(
                extracts, checking_dict)
            warnings.extend(extract_warnings)

            if int(file_index) + 1 == len(files):
                clear_imported_table_type(project_id, "extracts")

            output = {'infos' : infos}
            if len(warnings) > 0:
                output['warnings'] = warnings
            return JsonResponse(output, content_type='application/json')
        else:
            return JsonResponse(
                {'validatorErrors': experiment_form['excelFilePaths'].errors,
                 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except (IntegrityError, InternalError) as ie:
        clear_imported_table(project_id)
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': ie.args[0],
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'parsing'},
                            status=400)
    except Exception as e:
        clear_imported_table(project_id)
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': msg,
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def check_pcrs_import(request, project_id):
    experiment_form = ExperimentForm(request.POST, request.FILES)
    file_name = None
    try:
        if experiment_form.is_valid():
            infos = list()
            warnings = list()
            files = request.FILES.getlist('excelFilePaths')
            file_index = request.POST['file_index']
            file = files[int(file_index)]
            file_name = file.name
            pcr_list = get_all_project_PCR(project_id)
            plate_list = get_dict_of_project_plate(project_id)
            import_project = project_imported_data(str(project_id))
            imported_dict = {
                'project' : import_project,
                'pcrs' : get_imported_pcrs_dict(import_project),
                'plates' : get_imported_plates_dict(import_project)
            }

            excel_file = openpyxl.load_workbook(file, read_only = True,
                                                data_only = True)
            pcrs = excel_parsing_pcr_checking(excel_file['PCR'])
            infos.append(pcrs['info'])
            check_run_unicity(pcrs, pcr_list)
            check_imported_run(pcrs, imported_dict)
            check_pcr_plate_integrity(pcrs['plates'], plate_list)
            warnings.extend(pcrs['warnings'])

            if int(file_index) + 1 == len(files):
                clear_imported_table_type(project_id, "pcrs")

            output = {'infos' : infos}
            if len(warnings) > 0:
                output['warnings'] = warnings

            return JsonResponse(output, content_type='application/json')
        else:
            return JsonResponse(
                {'validatorErrors': experiment_form['excelFilePaths'].errors,
                 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except (IntegrityError, InternalError) as ie:
        clear_imported_table(project_id)
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': ie.args[0],
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'parsing'},
                            status=400)
    except Exception as e:
        clear_imported_table(project_id)
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': msg,
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def check_plates_import(request, project_id):
    experiment_form = ExperimentForm(request.POST, request.FILES)
    file_name = None
    try:
        if experiment_form.is_valid():
            infos = list()
            warnings = list()
            files = request.FILES.getlist('excelFilePaths')
            file_index = request.POST['file_index']
            file = files[int(file_index)]
            file_name = file.name
            plate_list = get_dict_of_project_plate(project_id)
            import_project = project_imported_data(str(project_id))
            imported_dict = {
                'project' : import_project,
                'plates' : get_imported_plates_dict(import_project)
            }
            checking_dict = {
                'extracts': list(),
                'controls': list(),
                'blk': None,
                'PCR_neg': None
            }
            excel_file = openpyxl.load_workbook(file, read_only = True,
                                                data_only = True)
            control = excel_parsing_get_control(excel_file['Controls'])
            checking_dict['blk'] = control['blkPCRctl']
            checking_dict['PCR_neg'] = control['negPCRctl']
            checking_dict["controls"] = excel_parsing_get_positif_control(
                    excel_file['Controls'])

            checking_dict['extracts'] = excel_parsing_get_extract(
                    excel_file['Extraction'])

            pcrs = excel_parsing_get_pcr(excel_file['PCR'])

            plates = excel_parsing_pcr_plate_checking(excel_file['PCR Plate'])
            infos.append(plates['info'])
            warnings.extend(plates['warnings'])

            check_plate_identity(plates['plates'], plate_list,
                                 checking_dict)
            check_imported_plate(plates['plates'], imported_dict)

            pcr_plate_warnings = check_pcr_plate_extract_id_integrity(
                plates['extracts'], checking_dict)
            warnings.extend(pcr_plate_warnings)

            check_pcr_plate_id_integrity(list(plates['plates']), pcrs)

            if int(file_index) + 1 == len(files):
                clear_imported_table_type(project_id, "plates")

            output = {'infos' : infos}
            if len(warnings) > 0:
                output['warnings'] = warnings
            return JsonResponse(output, content_type='application/json')

        else:
            return JsonResponse(
                {'validatorErrors': experiment_form['excelFilePaths'].errors,
                 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except (IntegrityError, InternalError) as ie:
        clear_imported_table(project_id)
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': ie.args[0],
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'parsing'},
                            status=400)
    except Exception as e:
        clear_imported_table(project_id)
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': msg,
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def check_tubes_import(request, project_id):
    experiment_form = ExperimentForm(request.POST, request.FILES)
    file_name = None
    try:
        if experiment_form.is_valid():
            infos = list()
            warnings = list()
            project = Project.objects.get(id=project_id)
            files = request.FILES.getlist('excelFilePaths')
            file_index = request.POST['file_index']
            file = files[int(file_index)]
            file_name = file.name
            existed_tube = list(Tube_mix.objects.filter(
                    project__id=project.id, submission_file__isnull=False
                ).values_list('name', flat=True))
            import_project = project_imported_data(str(project.id))
            imported_dict = {
                'project' : import_project,
                'tubes' : get_imported_tubes_list(import_project)
            }

            excel_file = openpyxl.load_workbook(file, read_only = True,
                                                data_only = True)
            plates = excel_parsing_get_pcr_plate(excel_file['PCR Plate'])

            tubes = excel_parsing_tube_checking(excel_file['mix (tube)'])
            infos.append(tubes['info'])
            warnings.extend(tubes['warnings'])

            tubes_list = list()
            for tube_name in tubes['tubes']:
                if tube_name in existed_tube:
                    raise IntegrityError('The tube mix ' + str(tube_name)
                                         + ' already exist '
                                         + 'for the project '
                                         + str(project.name) + '!')
                if tube_name in imported_dict['tubes']:
                    raise IntegrityError('The tube mix ' + str(tube_name)
                                         + ' already exist '
                                         + 'for the project '
                                         + str(project.name) + '!')
                else:
                    tubes_list.append(add_qualifier('tubes', 'name',
                                              tube_name, import_project))
                    imported_dict['tubes'].append(tube_name)
            push_qualifiers(tubes_list)

            check_tube_plate_id_integrity(tubes, plates)

            if int(file_index) + 1 == len(files):
                clear_imported_table_type(project.id, "tubes")

            output = {'infos' : infos}
            if len(warnings) > 0:
                output['warnings'] = warnings
            return JsonResponse(output, content_type='application/json')

        else:
            return JsonResponse(
                {'validatorErrors': experiment_form['excelFilePaths'].errors,
                 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except (IntegrityError, InternalError) as ie:
        clear_imported_table(project_id)
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': ie.args[0],
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'parsing'},
                            status=400)
    except Exception as e:
        clear_imported_table(project_id)
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': msg,
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def write_messages(request, project_id):
    try:
        project = Project.objects.get(id=project_id)
        JSONdata = request.POST['files']
        files = simplejson.JSONDecoder().decode(JSONdata)
        request_object = {'infos': {}}
        if "infos" in files:
            request_object['infos']['messages'] = []
            for message in files['infos']:
                request_object['infos']['messages'].append(message)
        else:
            for file, message in files.items():
                request_object['infos'][file] = []
                for info in message['infos']:
                    request_object['infos'][file].append(info);
                if 'warnings' in message and len(message['warnings']) > 0:
                    if 'warnings' not in request_object:
                        request_object['warnings'] = {}
                    request_object['warnings'][file] = []
                    for warning in message['warnings']:
                        request_object['warnings'][file].append(warning);

        return render(request, 'parts/checking-message-import.html',
                          request_object)
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)

@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def write_delete_messages(request, project_id=None):
    try:
        JSONdata = request.POST['infos']
        infos = simplejson.JSONDecoder().decode(JSONdata)
        request_object = {'warnings': []}
        if 'warnings' in infos:
            for warning in infos['warnings']:
                request_object['warnings'].append(warning);
        if "infos" in infos:
            request_object['infos'] = []
            for message in infos['infos']:
                request_object['infos'].append(message)

        return render(request, 'parts/checking-message.html',
                          request_object)
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def import_file_and_tube(request, project_id):
    file_name = None
    try:
        experiment_form = ExperimentForm(request.POST, request.FILES)
        if experiment_form.is_valid():
            with transaction.atomic():
                project = Project.objects.get(id=project_id)
                files = request.FILES.getlist('excelFilePaths')
                file_index = request.POST['file_index']
                file = files[int(file_index)]
                file_name = file.name

                submit_file = Submission_file()
                submit_file.file_name = file.name
                submit_file.excel_file = base64.b64encode(file.read())
                submit_file.save()

                submit_file_imported_data(submit_file.id, project_id)

                book = openpyxl.load_workbook(file, read_only = True,
                                              data_only = True)
                tubes = excel_parsing_tube(book['mix (tube)'])
                populate_tube_table(tubes, project, submit_file)

                return JsonResponse({'step': 'import file and tubes',
                                     'step_number': "1/4"},
                                    content_type='application/json')
        else:
            return JsonResponse(
                {'validatorErrors': experiment_form['excelFilePaths'].errors,
                 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except IntegrityError as ie:
        transaction.rollback()
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': ie.args[0],
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'db'},
                            status=400)
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': msg,
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def import_samples(request, project_id):
    file_name = None
    try:
        experiment_form = ExperimentForm(request.POST, request.FILES)
        if experiment_form.is_valid():
            with transaction.atomic():
                files = request.FILES.getlist('excelFilePaths')
                file_index = request.POST['file_index']
                file = files[int(file_index)]
                file_name = file.name
                sample_list = get_dict_of_project_sample(project_id)

                book = openpyxl.load_workbook(file, read_only = True,
                                              data_only = True)
                samples = excel_parsing_sample(book['Samples'])
                submission_file = get_submission_file(file_name, project_id)
                if submission_file is None:
                    raise Exception("Error: The submission file " + str(file_name)
                                    + " is duplicate or not exist in the database!")
                add_sample(samples, sample_list, submission_file)

                return JsonResponse({'step': 'import samples',
                                     'step_number': "2/4"},
                                    content_type='application/json')
        else:
            return JsonResponse(
                {'validatorErrors': experiment_form['excelFilePaths'].errors,
                 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except IntegrityError as ie:
        transaction.rollback()
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': ie.args[0],
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'db'},
                            status=400)
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': msg,
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def import_extracts(request, project_id):
    file_name = None
    try:
        experiment_form = ExperimentForm(request.POST, request.FILES)
        if experiment_form.is_valid():
            with transaction.atomic():
                files = request.FILES.getlist('excelFilePaths')
                file_index = request.POST['file_index']
                file = files[int(file_index)]
                file_name = file.name
                submission_file = get_submission_file(file_name, project_id)
                if submission_file is None:
                    raise Exception("Error: The submission file " + str(file_name) +
                                    " is duplicate or not exist in the database!")
                sample_list = get_dict_of_project_sample(project_id)
                extract_list = get_dict_of_project_extract(project_id)

                book = openpyxl.load_workbook(file, read_only = True,
                                              data_only = True)

                extracts = excel_parsing_extract(book['Extraction'])
                control = excel_parsing_control(book['Controls'])
                add_ctl_pos(control['ctlPos'], extract_list, submission_file)

                used_samples = add_extract(extracts, control, sample_list,
                                           extract_list, submission_file)
                clear_unused_samples(submission_file, used_samples)

                return JsonResponse({'step': 'import extracts and control',
                                     'step_number': "3/4"},
                                    content_type='application/json')
        else:
            return JsonResponse(
                {'validatorErrors': experiment_form['excelFilePaths'].errors,
                 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except IntegrityError as ie:
        transaction.rollback()
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': ie.args[0],
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'db'},
                            status=400)
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': msg,
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def import_pcr_plate(request, project_id):
    file_name = None
    try:
        experiment_form = ExperimentForm(request.POST, request.FILES)
        if experiment_form.is_valid():
            with transaction.atomic():
                files = request.FILES.getlist('excelFilePaths')
                file_index = request.POST['file_index']
                file = files[int(file_index)]
                file_name = file.name
                submission_file = get_submission_file(file_name, project_id)
                if submission_file is None:
                    raise Exception("Error: The submission file " + str(file_name) +
                                    " is duplicate or not exist in the database!")
                # get existed plate and extract for the project id
                plate_list = get_dict_of_project_plate(project_id)
                extract_list = get_dict_of_project_extract(project_id)

                book = openpyxl.load_workbook(file, read_only = True,
                                              data_only = True)

                # get information in the excel file
                tubes = excel_parsing_tube(book['mix (tube)'])
                control = excel_parsing_control(book['Controls'])
                plates = excel_parsing_pcr_plate(book['PCR Plate'])
                pcrs = excel_parsing_pcr(book['PCR'])

                # add tube model in the tube structure for next function
                add_submited_tubes(tubes, submission_file)

                # add primer in the database
                primers = plates.pop('primers')
                primerPairs = populate_primer_pair_table(primers)

                # add plate in the database and stocker their in list
                added_plate = add_plate_into_db(plates, plate_list)

                # add pcr program in the database
                insert_pcr_information_into_db(pcrs, tubes, plate_list)

                # add well in the database and get the used extract
                # to remove in the next function
                used_extracts = insert_pcr_plate_into_db(plates, added_plate,
                                                        control, extract_list,
                                                        primerPairs)
                clear_unused_extract(submission_file, used_extracts)

                submit_file_imported_data(submission_file.id,
                                          project_id).delete()

                return JsonResponse({'step': 'import pcr plate',
                                     'step_number': "4/4"},
                                    content_type='application/json',
                                    safe=False)
        else:
            return JsonResponse(
                {'validatorErrors': experiment_form['excelFilePaths'].errors,
                 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except IntegrityError as ie:
        transaction.rollback()
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': ie.args[0],
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'db'},
                            status=400)
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message-import.html',
                      {'errors': msg,
                       'file': file_name},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def remove_extracts_on_fail(request, project_id):
    file_name = None
    try:
        with transaction.atomic():
            submission_file = get_submission_file_by_project_id(project_id)
            extracts = get_submission_file_extracts_for_deletion(submission_file.value)
            extract_ids = list(extracts.values_list('id', flat=True))
            if len(extract_ids) > 0:
                remove_extracts(extract_ids, submission_file.value)

            return JsonResponse({'step': 'remove extracts',
                                 'step_number': "1/4"},
                                content_type='application/json')
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        html_msg = render_to_string('parts/checking-message_import.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'status': 'FAIL',
                             'errorType': 'database'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def remove_samples_on_fail(request, project_id):
    file_name = None
    try:
        with transaction.atomic():
            submission_file = get_submission_file_by_project_id(project_id)

            samples = get_submission_file_samples_for_deletion(submission_file.value)
            sample_ids = list(samples.values_list('id', flat=True))
            if len(sample_ids) > 0:
                Sample.objects.filter(id__in=sample_ids).delete()

            return JsonResponse({'step': 'remove samples',
                                 'step_number': "2/4"},
                                content_type='application/json')
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        html_msg = render_to_string('parts/checking-message_import.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'status': 'FAIL',
                             'errorType': 'database'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def remove_plate_on_fail(request, project_id):
    file_name = None
    try:
        with transaction.atomic():
            submission_file = get_submission_file_by_project_id(project_id)

            plates = get_submission_file_plates_for_deletion(submission_file.value)
            plate_ids = list(plates.values_list('id', flat=True))
            if len(plate_ids) > 0:
                remove_plates(plate_ids, submission_file.value)

            return JsonResponse({'step': 'remove plates',
                                 'step_number': "3/4"},
                                content_type='application/json')
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        html_msg = render_to_string('parts/checking-message_import.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'status': 'FAIL',
                             'errorType': 'database'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def remove_file_tube_on_fail(request, project_id):
    file_name = None
    try:
        with transaction.atomic():
            submission_file = get_submission_file_by_project_id(project_id)

            remove_only_submission_file(submission_file.value)
            submission_file.delete()
            return JsonResponse({'step': 'remove experiment file and tubes',
                                 'step_number': "4/4"},
                                content_type='application/json')
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        html_msg = render_to_string('parts/checking-message_import.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'status': 'FAIL',
                             'errorType': 'database'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def remove_imported_data_on_fail(request, project_id):
    file_name = None
    try:
        with transaction.atomic():
            clear_imported_table(project_id)
            return JsonResponse({'step': 'remove imported data',
                                 'step_number': "1/1"},
                                content_type='application/json')
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        html_msg = render_to_string('parts/checking-message_import.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'status': 'FAIL',
                             'errorType': 'database'},
                            status=500)
