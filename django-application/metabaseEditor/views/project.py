from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.views.decorators.http import require_http_methods
from django.db import transaction
from django.db.models import Q, Count
from django.http import JsonResponse
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required

from django.contrib.auth.models import User
from ..models import Project, Submission_file, Library, Tube_mix

from ..forms import (ProjectForm, UpdateForm, ExperimentForm,
                     SequencingForm, selectSequencingForm)

from ..tools.decorator import (check_user_login, check_user_right,
                               check_user_right_js, check_user_auth,
                               check_user_auth_js)
from ..tools.project import *
from ..tools.submission_file import *
from ..tools.plate import get_deleted_project_plates
from ..tools.sample import get_deleted_project_samples
from ..tools.extract import get_deleted_project_extracts
from ..tools.tube import get_deleted_project_tubes
from ..tools.primerPair import get_plate_primer_for_deletion
from ..tools.deleted_data import *



@require_http_methods(["POST", "GET"])
@login_required
@check_user_auth
@permission_required('metabaseEditor.add_project')
def create_project(request):
    if request.method == 'GET':
        return redirect('metabaseEditor:manage_project')
    project_form = ProjectForm(request.POST)
    if project_form.is_valid():
        project = project_form.save()
        for superuser in User.objects.filter(is_superuser=True):
            project.users.add(superuser)
        project.save()
        messages.success(request, 'The project was created with success!')
        return redirect('metabaseEditor:manage_project',
                            project_id=project.id)
    return render(request, 'manage_project.html', {
        'update_form': UpdateForm(user=request.user),
        'projectInformation_form': project_form,
        'experiment_form': ExperimentForm(),
        'sequencing_form': SequencingForm(),
        'selectSequencing_form': selectSequencingForm(),
        'active_tab': 'project-information'
        })


@require_http_methods(["GET"])
@login_required
@check_user_right
@check_user_auth
def manage_project(request, project_id=None):
    if project_id:
        project = Project.objects.get(id=project_id)
        active_tab = define_active_tab(project_id)
        return render(request, 'manage_project.html', {
            'update_form': UpdateForm(initial={'project': project_id},
                                      user=request.user),
            'projectInformation_form': ProjectForm(instance=project),
            'experiment_form': ExperimentForm(),
            'sequencing_form': SequencingForm(),
            'selectSequencing_form': selectSequencingForm(),
            'project': project_id,
            'active_tab': active_tab,
            'parentProject': is_parent_project(project_id)
        })
    return render(request, 'manage_project.html', {
        'update_form': UpdateForm(user=request.user),
        'projectInformation_form': ProjectForm(
            initial = {'users' : [request.user]}),
        'experiment_form': ExperimentForm(),
        'sequencing_form': SequencingForm(),
        'selectSequencing_form': selectSequencingForm(),
        'active_tab': 'project-information'
    })


@require_http_methods(["POST"])
def load_project(request):
    if request.method == 'POST':
        update_form = UpdateForm(request.POST, user=request.user)
        if update_form.is_valid():
            project_id = update_form.cleaned_data['project'].id
            return redirect('metabaseEditor:manage_project',
                            project_id=project_id)
    return redirect('metabaseEditor:manage_project')


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def get_project_users(request, project_id=None):
    users_id = list()
    if project_id:
        project_users = Project.objects.filter(
            id=project_id).values_list('users__id', flat=True)
        users_id = list(User.objects.filter(id__in=project_users).exclude(
            id=request.user.id).values_list('id', flat=True))
    return JsonResponse(users_id, content_type='application/json', safe=False)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def get_deleted_submission_files(request, project_id):
    try:
        submission_files = list(get_project_submission_files(project_id).values(
            'file_name', 'id'))

        output = {'nb_files': len(submission_files),
                  'files_list': submission_files}
        return JsonResponse(output, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An internal servor error is raised: ' + error_name + ': '
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_right_js
@check_user_auth_js
def identify_deleted_project(request, project_id):
    try:
        deleted_project = project_deleted_data(project_id)
        for sbp in list(get_subproject(project_id).values_list('id', flat=True)):
            project_deleted_data(sbp, deleted_project)
        return JsonResponse({'step': 'Identify project to delete',
                             'step_number': '1/1'},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An internal servor error is raised: ' + error_name + ': '
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_right_js
@check_user_auth_js
def delete_project(request, project_id):
    try:
        for sbp in list(get_subproject(project_id).values_list('id', flat=True)):
            Project.objects.filter(id=sbp).delete()
            remove_deleted_data("project", sbp)

        Project.objects.filter(id=project_id).delete()
        remove_deleted_data("project", project_id)

        messages.success(request, 'The project was deleted with success!')
        return JsonResponse({'step': 'Delete project',
                             'step_number': '1/1'},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An internal servor error is raised: ' + error_name + ': '
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_right_js
@check_user_auth_js
def identify_deleted_parent_project(request, project_id):
    try:
        deleted_project = project_deleted_data(project_id)
        return JsonResponse({'step': 'Identify project to delete',
                             'step_number': '1/1'},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An internal servor error is raised: ' + error_name + ': '
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_right_js
@check_user_auth_js
def delete_parent_project(request, project_id):
    try:
        Project.objects.filter(id=project_id).delete()
        remove_deleted_data("project", project_id)

        messages.success(request, 'The project was deleted with success!')
        return JsonResponse({'step': 'Delete project',
                             'step_number': '1/1'},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An internal servor error is raised: ' + error_name + ': '
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def unlink_pcr_tube(request, project_id):
    try:
        tubes_ids = list(get_project_deleted_id_by_type(project_id, 'tubes'))
        if len(tubes_ids) > 0:
            tubes = Tube_mix.objects.filter(id__in=tubes_ids)
        else:
            project_list = list(get_subproject(project_id).values_list('id',
                                                                       flat=True))
            project_list.append(project_id)
            tubes = Tube_mix.objects.filter(
                    project__id__in=project_list,
                    libraries__isnull=False)
        for tube in tubes:
            tube.pcr_set.clear()

        return JsonResponse({'step': 'Unlink tube to pcr for remove project',
                             'step_number': '1/3'},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An internal servor error is raised: ' + error_name + ': '
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg,
                       'errors_title': '- Clear Database'},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def unlink_project_tube(request, project_id):
    try:
        tubes_ids = list(get_project_deleted_id_by_type(project_id, 'tubes'))
        if len(tubes_ids) > 0:
            Tube_mix.objects.filter(id__in=tubes_ids).update(project=None,
                    submission_file=None, concentration=0, quantity=0,
                    mtype='None')
            remove_deleted_data("tubes", tubes_ids)
        else:
            project_list = list(get_subproject(project_id).values_list('id',
                                                                       flat=True))
            project_list.append(project_id)
            Tube_mix.objects.filter(
                    project__id__in=project_list,
                    libraries__isnull=False
                    ).update(project=None, submission_file=None,
                             concentration=0, quantity=0, mtype='None')

        return JsonResponse({'step': 'Unlink tube to file for remove experiment',
                             'step_number': "2/3"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg,
                       'errors_title': "- Clear Database"},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def remove_project_tube_wo_lib(request, project_id):
    try:
        project_list = list(get_subproject(project_id).values_list('id',
                                                                   flat=True))
        project_list.append(project_id)
        Tube_mix.objects.filter(
                project__id__in=project_list,
                libraries__isnull=True).delete()

        return JsonResponse({'step': 'Remove project tube without libraries',
                             'step_number': "3/3"},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg,
                       'errors_title': "- Clear Database"},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_right_js
@check_user_auth_js
def get_delete_info_subproject(request, project_id):
    warnings = list()
    try:
        subproject = list(get_subproject(project_id).values_list(
            'name', flat=True))
        if len(subproject) > 0:
            warnings.append(
                {'message': 'The following sub-project wil be removed:',
                'list': subproject})
        else:
            warnings.append('The project has no sub-project!')

        output = {'warnings' : warnings}
        return JsonResponse(output, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An internal servor error is raised: ' + error_name + ': '
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_right_js
@check_user_auth_js
def get_delete_info_submission_file(request, project_id):
    warnings = list()
    try:
        submission_file = list(get_project_submission_files(project_id).values_list(
            'file_name', flat=True))
        if len(submission_file) > 0:
            warnings.append(
                {'message': 'The following experiment files wil be removed:',
                'list': submission_file})
        else:
            warnings.append('The project has not linked experiment file!')

        output = {'warnings' : warnings}
        return JsonResponse(output, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An internal servor error is raised: ' + error_name + ': '
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_right_js
@check_user_auth_js
def get_delete_info_libraries(request, project_id):
    warnings = list()
    try:
        parentProject_count = Count('tube__project__parentProject__id',
                                    distinct=True)
        linked_library = list(Library.objects.annotate(
            parentProject_count=parentProject_count
            ).filter(
            Q(tube__project__id=project_id) |
            (Q(tube__project__parentProject__id=project_id) &
               Q(parentProject_count__lt=2))
            ).distinct().values_list('name', flat=True))
        if len(linked_library) > 0:
            warnings.append({'message': 'The following libraries wil be affected:',
                         'list': linked_library})
        else:
            warnings.append('The project has not linked libraries!')

        output = {'warnings' : warnings}
        return JsonResponse(output, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An internal servor error is raised: ' + error_name + ': '
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_right_js
@check_user_auth_js
def get_delete_info_tubes(request, project_id):
    warnings = list()
    try:
        tubes = list(get_deleted_project_tubes(project_id).values_list('name', flat=True))
        if len(tubes) > 0:
            warnings.append({'message': 'The following tubes wil be removed:',
                             'list': list(tubes)})
        else:
            warnings.append('The project has not linked tubes!')

        output = {'warnings' : warnings}
        return JsonResponse(output, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An internal servor error is raised: ' + error_name + ': '
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_right_js
@check_user_auth_js
def get_delete_info_plates(request, project_id):
    warnings = list()
    try:
        plates = get_deleted_project_plates(project_id)
        plates_name = list(plates.values_list('name', flat=True))
        if len(plates_name) > 0:
            warnings.append({'message': 'The following plates wil be removed:',
                             'list': list(plates_name)})
        else:
            warnings.append('The project has not linked plates!')

        primers = get_plate_primer_for_deletion(
                list(plates.values_list('id', flat=True))).count()
        warnings.append('Number of primer pairs that will be removed: '
                        + str(primers))

        output = {'warnings' : warnings}
        return JsonResponse(output, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An internal servor error is raised: ' + error_name + ': '
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_right_js
@check_user_auth_js
def get_delete_info_samples(request, project_id):
    warnings = list()
    try:
        samples = get_deleted_project_samples(project_id).count()
        warnings.append('Number of sample that will be removed: ' + str(samples))

        output = {'warnings' : warnings}
        return JsonResponse(output, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An internal servor error is raised: ' + error_name + ': '
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_right_js
@check_user_auth_js
def get_delete_info_extracts(request, project_id):
    warnings = list()
    try:
        extracts = get_deleted_project_extracts(project_id).count()
        warnings.append('Number of extract that will be removed: ' + str(extracts))

        output = {'warnings' : warnings}
        return JsonResponse(output, content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ('An internal servor error is raised: ' + error_name + ': '
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST", "GET"])
@login_required
@check_user_right
@check_user_auth
def update_project_information(request, project_id):
    if request.method == 'GET':
        return redirect('metabaseEditor:manage_project')
    project = get_object_or_404(Project, id = project_id)
    project_info_form = ProjectForm(request.POST, instance=project)
    if project_info_form.is_valid():
        if project_info_form.has_changed():
            project = project_info_form.save()
            messages.success(request,
                'The project information was updated with success!')
        else:
            messages.info(request,
                'The project information was not change!')
        return redirect('metabaseEditor:manage_project', project_id=project_id)
    return render(request, 'manage_project.html', {
        'update_form': UpdateForm(initial={'project': project_id},
                                  user=request.user),
        'projectInformation_form': project_info_form,
        'experiment_form': ExperimentForm(),
        'sequencing_form': SequencingForm(),
        'selectSequencing_form': selectSequencingForm(),
        'project':project_id,
        'active_tab': 'project-information',
        'parentProject': is_parent_project(project_id)
    })
