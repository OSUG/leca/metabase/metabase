from django.views.decorators.http import require_http_methods
from django.template.loader import render_to_string
from django.shortcuts import render
from django.db import transaction, IntegrityError, InternalError
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from django.utils.timezone import localtime
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.db.models import Q

import json as simplejson
import re

from ..models import (Project, Tube_mix, Library,
                      Sequencing_information, Sequencing_file)

from ..forms import SequencingForm, selectSequencingForm

from ..parser.sequencing import *

from ..tools.decorator import (check_user_login, check_user_right_js,
                               check_user_auth_js, check_user_auth)
from ..tools.sequencing import *
from ..tools.tube import (get_all_unused_tube, get_all_used_tube,
                          get_sequencing_tubes)
from ..tools.project import get_project_link_to_sequencing


@require_http_methods(["GET"])
@login_required
@check_user_auth
def manage_sequencing(request):
    return render(request, 'manage_sequencing.html', {
        'sequencing_form': SequencingForm(),
    })


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
def get_information(request, sequencing_file_id):
    warnings = list()
    try:
        affected_project = get_project_link_to_sequencing(sequencing_file_id)
        project_names = affected_project.values_list('name', flat=True)
        if affected_project.count() > 0:
            warnings.append({'message': 'The following projects wil be affected:',
                             'list': project_names})
        else:
            warnings.append('The sequencing file is not link to a project!')

        sequencing_tubes = get_sequencing_tubes(sequencing_file_id)
        removed_tubes = sequencing_tubes.exclude(
            submission_file__isnull=False).values_list('name', flat=True)
        warnings.append({'message': 'The following tubes wil be removed:',
                         'list': removed_tubes})

        removed_library = Library.objects.filter(
            sequencing_file__information__id=sequencing_file_id
            ).distinct().values_list('name', flat=True)
        warnings.append({'message': 'The following libraries wil be removed:',
                         'list': removed_library})

        full_sequencing_files =list()
        sequencing_files = Sequencing_file.objects.filter(
            information__id=sequencing_file_id)
        for sf in sequencing_files:
            full_sequencing_files.append(sf.file_name + '.' + sf.file_type)
        warnings.append(
            {'message': 'The following sequencing files wil be removed:',
             'list': full_sequencing_files})
        request_object = {'warnings': warnings}
        return render(request, 'parts/checking-message.html',
                      request_object)
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def get_library_and_tube_information(request, project_id):
    sequencings = dict()
    sequencings['data'] = list()
    try :
        select_sequencing_form = selectSequencingForm(request.POST)
        if select_sequencing_form.is_valid():
            sequencing_file = select_sequencing_form.cleaned_data[
                'sequencingInfoFile']
            libraries = Library.objects.filter(tube__project__isnull=True,
                sequencing_file__information__id=sequencing_file.id
                ).select_related('tube').distinct('name')
            for library in libraries:
                links = dict()
                links['id'] = library.tube.id
                links['library_name'] = library.name
                links['tube_name'] = library.tube.name
                sequencings['data'].append(links)
            return JsonResponse(sequencings, content_type='application/json')
        else:
            return JsonResponse({'validatorErrors': select_sequencing_form[
                'sequencingInfoFile'].errors, 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def link_library(request, project_id):
    sequencings = list()
    try:
        with transaction.atomic():
            JSONdata = request.POST['rows']
            rows = simplejson.JSONDecoder().decode(JSONdata)
            project = Project.objects.get(id=project_id)
            for row in rows:
                lib = Library.objects.get(name=row['library'])
                sequencing_tube = Tube_mix.objects.get(id=row['id'])
                tube = Tube_mix.objects.filter(name=sequencing_tube.name,
                                               project__id=project_id)
                if tube.exists():
                    experiment_tube = Tube_mix.objects.get(
                        name=sequencing_tube.name, project__id=project_id)
                    lib.tube = experiment_tube
                    lib.save()
                    if Library.objects.filter(tube__id=row['id']).count() == 0:
                        sequencing_tube.delete()
                else:
                    sequencing_tube.project = project
                    sequencing_tube.save()
                links = dict()
                links['id'] = row['library']
                links['library_name'] = row['library']
                links['tube_name'] = row['tube']
                sequencing_info = Sequencing_information.objects.filter(
                    sequencing_file__library_name=row['library']
                    ).distinct('file_name').values_list('file_name',flat=True)
                links['file_name'] = '; '.join(sequencing_info)
                is_link = True if lib.tube.submission_file else False
                links['experiment'] = is_link
                sequencings.append(links)
                message = ('The library ' + str(row['library'])
                           + ' was linked to the project ' + str(project.name)
                           + ' with success!')
                messages.success(request, message)
            html_form = render_to_string(
                'parts/sequencing-file-selection.html',
                {'selectSequencing_form': selectSequencingForm()},
                request=request)
            html_msg = render_to_string(
                'parts/information-message.html', request=request)
            return JsonResponse({'data': sequencings,
                                 'html_message': html_msg,
                                 'html_form': html_form},
                                content_type='application/json', safe=False)
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'status': 'FAIL',
                             'errorType': 'database'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def unlink_library(request, project_id):
    try:
        with transaction.atomic():
            JSONdata = request.POST['libraries']
            libraries = simplejson.JSONDecoder().decode(JSONdata)
            project = Project.objects.get(id=project_id)
            for library_id in libraries:
                library = Library.objects.get(name=library_id)
                if library.tube.submission_file:
                    new_tube = Tube_mix()
                    new_tube.name = library.tube.name
                    new_tube.mtype = 'None'
                    new_tube.save()
                    library.tube = new_tube
                    library.save()
                else:
                    library.tube.project = None
                    library.tube.save()
                message = ('The library ' + library_id
                           + ' was unlinked to the project '
                           + str(project.name) + ' with success!')
                messages.success(request, message)

            html_form = render_to_string(
                'parts/sequencing-file-selection.html',
                {'selectSequencing_form': selectSequencingForm()},
                request=request)
            html_msg = render_to_string('parts/information-message.html',
                                        request=request)
            return JsonResponse({'status': 'SUCCESS',
                                 'html_form': html_form,
                                 'html_message': html_msg},
                                    content_type='application/json')
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'status': 'FAIL',
                             'errorType': 'database'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
def get_sequencing_information(request):
    sequencings = list()
    try:
        unused_sequencing_info_file = Sequencing_file.objects.select_related(
                'information').distinct('information')
        for sif in unused_sequencing_info_file:
            seq = dict()
            tubes = Tube_mix.objects.filter(
                libraries__sequencing_file__information__id=sif.information.id
                ).values_list('name', flat=True).distinct('name')
            library = Library.objects.filter(
                sequencing_file__information__id=sif.information.id
                ).values_list('name', flat=True).distinct('name')
            full_seq_files =list()
            seq_files = list(Sequencing_file.objects.all().filter(
                information__id = sif.information.id))
            for sf in seq_files:
                full_seq_files.append(sf.file_name + '.' + sf.file_type)
            seq['id'] = sif.information.id
            seq['file_name'] = sif.information.file_name
            seq['sequencing_date'] = localtime(
                sif.information.sequencing_date).strftime('%Y-%m-%d')
            seq['tubes'] = '; '.join(tubes)
            seq['libraries'] = '; '.join(library)
            seq['sequencing_files'] = '; '.join(full_seq_files)
            sequencings.append(seq)
        return JsonResponse({'data': sequencings},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        messages.error(request,
               'An error occur on the sequencing information file loading.'
               + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        html_msg = render_to_string(
            'parts/information-message.html', request=request)
        return JsonResponse({'data': sequencings, 'html': html_msg},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
def get_project_sequencing(request, project_id=None):
    sequencings = list()
    if not project_id:
        return JsonResponse({'data': sequencings},
                            content_type='application/json')
    try:
        if not Project.objects.filter(Q(id=project_id), Q(available=True) | Q(
        available=False, users__username=request.user.username)).exists():
            messages.warning(request,
                "Your account doesn't have access to this page. "
                + "To proceed, please login with an account that has access.")
            return JsonResponse({'status': 'FAIL', 'not_authenticated': True,
                                 'redirect_url': settings.LOGIN_URL, 'data':[]},
                                status=401)
        libraries = Library.objects.filter(
            tube__project__id=project_id,
            ).select_related('tube').distinct('name')
        for library in libraries:
            links = dict()
            sequencing_info = Sequencing_information.objects.filter(
                sequencing_file__library_name__name=library.name
                ).distinct('file_name').values_list('file_name', flat=True)
            links['id'] = library.name
            links['file_name'] = '; '.join(sequencing_info)
            links['library_name'] = library.name
            links['tube_name'] = library.tube.name
            links['experiment'] = True if library.tube.submission_file else False
            sequencings.append(links)
        return JsonResponse({'data': sequencings},
                            content_type='application/json')
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        messages.error(request,
                       'An error occur on the sequencing information loading.'
                       + ' (Error: ' + error_name + ': ' + e.args[0] +')')
        html_msg = render_to_string(
            'parts/information-message.html', request=request)
        return JsonResponse({'data': sequencings, 'html': html_msg},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
@check_user_right_js
def add_sequencing(request, project_id):
    sequencings = dict()
    sequencings['data'] = list()
    try:
        sequencing_form = SequencingForm(request.POST, request.FILES)
        project_tube = Tube_mix.objects.filter(project__id=project_id,
            libraries__isnull=False).values_list('name', flat=True)
        if sequencing_form.is_valid():
            with transaction.atomic():
                files = request.FILES.getlist('sequencingFile')
                for file in files:
                    sequencing_info = insert_sequencing_information(file)
                    links = get_available_libraries(project_id,
                                                    sequencing_info.id)
                    sequencings['data'].extend(links)
                    message = ('The sequencing information file '
                               + str(sequencing_info.file_name)
                               + ' was imported with success!')
                    messages.success(request, message)
            html_msg = render_to_string('parts/information-message.html',
                                        request=request)
            return JsonResponse({'data': sequencings,
                                 'html_message': html_msg},
                                content_type='application/json', safe=False)
        else:
            return JsonResponse(
                {'validatorErrors': sequencing_form['sequencingFile'].errors,
                 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except (IntegrityError, InternalError) as ie:
        transaction.rollback()
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': ie.args[0]},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'db'},
                            status=400)
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
def check_sequencing_import(request):
    try:
        sequencing_form = SequencingForm(request.POST, request.FILES)
        if sequencing_form.is_valid():
            sequencing_file_list = get_unused_sequecing_info_file()
            files = request.FILES.getlist('sequencingFile')
            unused_tube = get_all_unused_tube()
            used_tube = get_all_used_tube()
            warnings = list()
            for f in files:
                if f.name in sequencing_file_list:
                    raise IntegrityError('The sequencing information file '
                                         + f.name + ' already exist!')
                sequencing_info = sequencing_parsing(f)
                if sequencing_info['type'] == 'xml':
                    m = re.search(r"(?P<dateYear>\d{4})\-"
                                  r"(?P<dateMonth>\d{2})\-"
                                  r"(?P<dateDay>\d{2})",f.name)
                    if m is None:
                        warnings.append(
                            'The sequencing date is not find in the xml file name.')
                if "customer_email" not in sequencing_info['project']:
                    warnings.append(
                        'The customer email is not find in the xml file name.')
                elif "customer_account" not in sequencing_info['project']:
                    warnings.append(
                        'The customer account is not find in the xml file name.')
                for run in sequencing_info['run']:
                    for lib in run['libs']:
                        for file_name in lib['fileName']:
                            seq_file_count = Sequencing_file.objects.filter(
                                file_name=file_name,
                                library_name=lib['id']).count()
                            if seq_file_count > 0:
                                raise InternalError('The sequencing file '
                                                     + file_name
                                                     + '.fastq.gz'
                                                     + ' already exist!')
                        if lib['sampleName'] in used_tube:
                            projects = Project.objects.filter(
                                tube_mix__name=lib['sampleName']
                                ).values_list('name', flat=True)
                            warnings.append({'message': 'The tube mix '
                                + lib['sampleName']
                                + ' already exist in the project: ',
                                'list': projects})
                        elif lib['sampleName'] in unused_tube:
                            warnings.append('The tube mix '
                                             + lib['sampleName']
                                             + ' already exist but '
                                             + 'is not assign to a project')

            infos = list()
            infos.append('The sequencing file is ready for the import.')
            request_object = {'warnings': warnings, 'infos': infos}

            return render(request, 'parts/checking-message.html',
                          request_object)
        else:
            return JsonResponse(
                {'validatorErrors': sequencing_form['sequencingFile'].errors,
                 'errorType': 'validator'}, content_type='application/json',
                status=400)
    except (IntegrityError, InternalError) as ie:
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': ie.args[0]},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'parsing'},
                            status=400)
    except ObjectDoesNotExist as one:
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': one.args[0]},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'db'},
                            status=400)
    except Exception as e:
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + str(e.args[0]))
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
def import_sequencing(request):
    new_sequencing = list()
    try:
        sequencing_form = SequencingForm(request.POST, request.FILES)
        if sequencing_form.is_valid():
            with transaction.atomic():
                files = request.FILES.getlist('sequencingFile')
                for file in files:
                    sequencing_info = insert_sequencing_information(file)
                    added_sequencing = sequencing_import_information(
                        sequencing_info)
                    new_sequencing.append(added_sequencing)
                    message = ('The sequencing information file '
                               + str(sequencing_info.file_name)
                               + ' was imported with success!')
                    messages.success(request, message)
                html_msg = render_to_string('parts/information-message.html',
                                            request=request)
            return JsonResponse({'data': new_sequencing, 'html': html_msg},
                                content_type='application/json', safe=False)
        else:
            return JsonResponse(
                {'validatorErrors': sequencing_form['sequencingFile'].errors,
                 'errorType': 'validator'},
                content_type='application/json',
                status=400)
    except (IntegrityError, InternalError) as ie:
        transaction.rollback()
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': ie.args[0]},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'db'},
                            status=400)
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'errorType': 'other'},
                            status=500)


@require_http_methods(["POST"])
@check_user_login
@check_user_auth_js
def remove_sequencing(request, sequencing_file_id):
    try:
        with transaction.atomic():
            sequencing_information = Sequencing_information.objects.get(
                id=sequencing_file_id)
            library_name = Sequencing_file.objects.filter(
                information__id=sequencing_file_id
                ).distinct('library_name').values_list('library_name',flat=True)
            Tube_mix.objects.filter(libraries__name__in=library_name,
                                    submission_file__isnull=True).delete()
            Library.objects.filter(name__in=library_name).delete()
            Sequencing_information.objects.filter(id=sequencing_file_id).delete()
            message = ('The sequencing information file '
                       + str(sequencing_information.file_name)
                       + ' was removed!')
            messages.success(request, message)
            html_msg = render_to_string('parts/information-message.html',
                                        request=request)
            return JsonResponse({'status': 'SUCCESS', 'html': html_msg},
                                content_type='application/json')
    except Exception as e:
        transaction.rollback()
        error_name = type(e).__name__
        msg = ("An internal servor error is raised: " + error_name + ": "
               + e.args[0])
        html_msg = render_to_string('parts/checking-message.html',
                      {'errors': msg},
                      request=request)
        return JsonResponse({'html': html_msg, 'status': 'FAIL',
                             'errorType': 'database'},
                            status=500)
