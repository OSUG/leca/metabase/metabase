from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.utils.http import urlquote
from django.db import transaction
from django.contrib.auth.decorators import login_required

import base64

from ..models import Submission_file

from ..tools.submission_file import *
from ..tools.decorator import (check_user_right_exp, check_user_auth)

@require_http_methods(["GET"])
@login_required
@check_user_auth
@check_user_right_exp
def export_submission_file(request, submission_file_id):
    submission_file = get_object_or_404(Submission_file, id=submission_file_id)
    file = base64.b64decode(submission_file.excel_file)
    file_name = submission_file.file_name
    response = HttpResponse(file, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = ('attachment; filename={}'.format(urlquote(str(file_name))))
    return response
