from django.apps import AppConfig


class MetabasehomeConfig(AppConfig):
    name = 'metabaseHome'
