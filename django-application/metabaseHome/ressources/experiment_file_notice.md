# Notice d'utilisation de fichier excel template.xlsx

L'ajout de donnée dans la base de donnée est réalisé par l'intermédiaire d'un fichier excel.
Ce fichier contient 6 onglets pré-remplis facilitant l'insertion des données par l'utilisateur à l'aide d'un code couleur :
- bleu clair : titre des onglets (obligatoirement la première ligne de chaque onglet)
- violet foncé : titre de colonne ou de ligne obligatoires. La position et le contenu de ces cellules ne doit pas être modifiés.
- violet clair : nom de colonne facultatifs. Permet l'insertion de données supplémentaires comme des métadonnées pour les échantilons et les extraits ou les noms de contrôle positif de PCR.
- orange : champs de valeurs obligatoire. Cellules que l'utilisateur doit remplir avec ses valeurs.


## Onglet Samples
### Description
Cet onglet doit contenir toutes les informations relatives aux échantillons. Il est composé :
- d'une colonne 'Sample ID' : colonne obligatoire qui contiendra l'identifiant de l'échantillon.
- d'une ou plusieurs colonne(s) facultative(s) : qui auront comme nom de colonne le nom de la métadonnée et comme valeur associée à l'échantillon la valeur de la métadonnée.

Pour information les identifiants des échantillons doivent être uniques car ils seront ensuite utilisés dans l'onglet 'Extraction'.

### Règle d'utilisation
1. Les identifiants des échantillons sont uniques. La colonne les contenants ne doit pas avoir de doublon.
2. Tous les identifiants d'échantillon utilisés dans l'onglet 'Extraction' doivent être décrits dans cet onglet.
3. De préférence tous les identifiants d'échantillon de cet onglet doivent être utilisés dans l'onglet 'Extraction'. Dans le cas où un échantillon n'est pas utilisé, un message warning sera affiché lors de l'import. Si vous souhaitez tout de même ajouter le fichier, l'échantillons non utilisé sera inséré différemment dans la base de façon à le retrouver pour les futurs imports.
4. Pour les colonnes facultatives, leurs cellules peuvent être vides, signifiant qu'elles n'auront pas de valeurs pour cette métadonnée.


## Onglet Controls
### Description
Cet onglet doit contenir toutes les informations concernant les contrôles techniques de manipulation d'extraction et de PCR. Cette onglet a une organisation différente des autres onglets car tous les champs à remplir ne se présentent pas sous forme d'un tableau.
Trois contrôles se présentent en ligne sous la forme '&lt;libellé&gt;:&lt;valeur&gt;' :
- le contrôle négatif d'extraction qui a pour valeur par défaut 'coExt'
- le contrôle négatif de PCR qui a pour valeur par défaut 'ctl-'
- le blanc de PCR qui a pour valeur par défaut 'blank'

Ces trois valeurs peuvent être modifiées. Elles correspondent aux noms donnés à ces différents contrôles pour l'expérience décrite dans ce fichier excel.

Le dernier contrôle à renseigner, le contrôle positif de PCR, se présente sous la forme d'un tableau avec :
- une colonne 'taxid' : colonne obligatoire qui contiendra l'identifiant taxonomique de l'un des organismes présent dans un des contrôles.
- une ou plusieurs colonne(s) facultative(s) : qui auront comme nom de colonne le ou les nom(s) du ou des contrôle(s) positif(s) utilisé(s) pour cette expérience. Les valeurs d'abondances (ou de proportions) relatives aux taxids sont reportées dans les colonnes correspondant à chaque contrôle. Celles-ci peuvent être vides lorsque le contrôle ne contient pas le taxid correspond à la ligne.

Dans le cas où aucun contrôle positif n'est utilisé il n'est pas nécessaire de compléter le tableau des contrôles positifs.

### Règle d'utilisation
1. les noms des contrôles positifs sont uniques. L'entête des contrôles positif ne doit pas avoir de doublon.
2. De préférence tous les noms de contrôles positifs doivent être utilisés dans l'onglet 'PCR Plate'. Dans le cas où un contrôle positif n'est pas utilisé, un message warning sera affiché lors de l'import. Si vous souhaitez tout de même ajouter le fichier le contrôle non utilisé sera inséré différemment dans la base de façon à le retrouver pour les futurs imports.
3. les taxids sont uniques. La colonne les contenants ne doit pas avoir de doublon.
4. chaque colonne des contrôles positifs doit être égale à 100.
5. tous les noms de contrôle qui sont renseignés dans l'onglet 'PCR Plate' doivent être décrits dans cet onglet.
6. Dans le cas où les organismes ne sont pas connus, vous devez mettre le rang le plus élévé c'est à dire le 'root' (taxid: '1') et mettre 100 dans la cellule du contrôle concerné.


## Onglet Extraction
### Description
Cet onglet doit contenir toutes les informations relatives aux extraits. Il est composé :
- d'une colonne 'Extract ID' : colonne obligatoire qui contiendra l'identifiant de l'extrait.
- d'une colonne 'Sample ID' : colonne obligatoire qui contiendra un des 'sample id' de l'onglet 'Samples'. Cette colonne lie un extrait à un échantillon décrit dans le premier onglet.
- d'une colonne 'Batch' : colonne obligatoire qui contiendra le numéro du run d'extraction (ou 'cochon').
- d'une ou plusieurs colonne(s) facultative(s) : qui auront comme nom de colonne le nom de la métadonnée et comme valeur associée à l'extrait la valeur associée de la métadonnée.

### Règle d'utilisation
1. Les identifiants des extraits sont uniques. La colonne les contenant ne doit pas avoir de doublon.
2. Tous les identifiants d'extrait utilisés dans l'onglet suivant (onglet 'PCR Plate') doivent être décrits dans cet onglet.
3. De préférence tous les identifiants d'extrait de cet onglet doivent être utilisés dans l'onglet suivant (onglet 'PCR Plate'). Dans le cas où un échantillon n'est pas utilisé, un message warning sera affiché lors de l'import. Si vous souhaitez tout de même ajouter le fichier l'extrait non utilisé sera inséré différemment dans la base de façon à le retrouver pour les futurs imports.
4. Pour les colonnes facultatives, leurs cellules peuvent être vides, signifiant qu'elles n'auront pas de valeurs pour cette métadonnée.
5. Tous les identifiants d'échantillons renseignés dans cet onglet doivent être décrits dans l'onglet 'Samples'.
6. La colonne 'batch' doit obligatoirement contenir un entier.


## Onglet PCR Plate
### Description
Cet onglet doit contenir toutes les informations relatives aux plans de plaque 96. Il est composé de 9 colonnes obligatoires:
- d'une colonne 'Plate ID' : colonne qui contiendra l'identifiant (ou nom) de la plaque 96. Ce même identifiant sera utilisé dans les 2 derniers onglets ('PCR' et 'mix (tube)')
- d'une colonne 'Column' : colonne qui contiendra la coordonnée horizontale du puits. Les valeurs contenues dans cette colonne doivent être comprises entre 1 et 12.
- d'une colonne 'Row' : colonne qui contiendra la coordonnée verticale du puits.
- d'une colonne 'Extract ID' : colonne qui contiendra un des 'extract id' de l'onglet 'Extraction'. Cette colonne lie un puits à un extrait décrit dans l'onglet 'Extraction'.
- d'une colonne 'Dilution' : colonne qui contiendra la valeur du facteur de dilution.
- d'une colonne 'Tag forward' : colonne qui contiendra la séquence du tag forward.
- d'une colonne 'Tag reverse' : colonne qui contiendra la séquence du tag reverse.
- d'une colonne 'Primer forward' : colonne qui contiendra la séquence du primer forward. Cette séquence correspond à l'amorce de séquençage forward pour un organisme donnée.
- d'une colonne 'Primer reverse' : colonne qui contiendra la séquence du primer reverse. Cette séquence correspond à l'amorce de séquençage reverse pour un organisme donnée.

Chaque ligne de l'onglet correspond à un puits de la plaque 96.

### Règle d'utilisation
1. Les identifiants de plaque 96 renseignés dans cet onglet doivent être les mêmes que ceux utilisés dans les onglets 'PCR' et 'mix (tube)'.
2. De préférence tous les puits doivent être décrits. Dans le cas où tous les puits ne sont pas renseignés, un message d'alerte est affiché lors de l'importation et les puits manquants sont considérés comme 'blanc' et ne sont pas affichés dans la partie exploratoire de l'application.
3. Tous les identifiants d'extraits renseignés dans cet onglet doivent être décrits dans l'onglet précédent (onglet 'Extraction').
4. Toutes les valeurs contenues dans la colonne 'Column' doivent être comprises entre 1 et 12.
5. les cellules de la colonne 'Row' doivent obligatoirement contenir une de ces valeurs: A, B, C, D, E, F, G, H.
6. La colonne 'Dilution' doit contenir une valeur entre 0 et 1. Si l'extrait n'est pas dilué la valeur renseignée est 1.
7. Aucun ligne ne doit être partiellement remplies. Aucun cellule ne doit être vide.


## Onglet PCR
### Description
Cet onglet doit contenir toutes les informations relatives aux programmes PCR. Il est composé de 13 colonnes obligatoires:
- d'une colonne 'Run' : colonne qui contiendra le numéro du run (ou batch) PCR. Cette colonne doit impérativement contenir un entier.
- d'une colonne 'Position' : colonne qui contiendra la position de la plaque 96 dans la machine PCR pour le run défini. Cette colonne doit impérativement contenir un entier compris entre 1 et 4.
- d'une colonne 'Plate' : colonne qui contiendra l'identifiant de la plaque 96 renseigné dans l'onglet 'PCR plate'. Cette colonne lie une plaque 96 à un programme PCR, indiquant ainsi quel programme PCR à été utilisé pour la plaque renseignée.
- d'une colonne 'Cycles' : colonne qui contiendra le nombre de cycles effectués par la machine PCR choisi pour le programme PCR.
- d'une colonne 'Denaturation time (s)' : colonne qui contiendra la valeur en secondes du temps de dénaturation de l'ADN choisi pour le programme PCR.
- d'une colonne 'Hybridation time (s)' : colonne qui contiendra la valeur en secondes du temps d'hybridation de l'ADN choisi pour le programme PCR.
- d'une colonne 'Synthesis time (s)' : colonne qui contiendra la valeur en secondes de temps de synthèse de l'ADN choisi pour le programme PCR.
- d'une colonne 'Final synthesis time (min)' : colonne qui contiendra la valeur en minutes du temps de synthèse finale de l'ADN choisi pour le programme PCR.
- d'une colonne 'Denaturation temp' : colonne qui contiendra la valeur en degrés Celsius de la température de dénaturation de l'ADN choisi pour le programme PCR.
- d'une colonne 'Hybridation temp' : colonne qui contiendra la valeur en degrés Celsius de la température d'hybridation de l'ADN choisi pour le programme PCR.
- d'une colonne 'Synthesis temp' : colonne qui contiendra la valeur en degrés Celsius de la température de synthèse de l'ADN choisi pour le programme PCR.
- d'une colonne 'Final synthesis temp' : colonne qui contiendra la valeur en degré Celsius de la température de synthèse finale de l'ADN choisi pour le programme PCR.
- d'une colonne 'Machine id' : colonne qui contiendra le nom (ou l'identifiant) de la machine PCR utilisée pour le programme PCR.

### Règle d'utilisation
1. Les identifiants de plaque 96 renseignés dans cet onglet doivent être les mêmes que ceux utilisés dans les onglets 'PCR Plate' et 'mix (tube)'.
2. Pas de doublon de position au sein d'un même run.
3. La valeur renseignée pour le nombre de cycle doit être un entier positif.
4. Les valeurs de température à renseigner doivent être des entiers positifs. L'unité souhaité est le degré Celsius.
5. Les valeurs de temps à renseigner pour les colonnes 'denaturation time' et 'hybridation time' sont en minutes et doivent être des entiers positifs.
6. Les valeurs de temps à renseigner pour les colonnes 'synthesis time' et 'final synthesis time' sont en secondes et doivent être des entiers positifs.
7. Aucun ligne ne doit être partiellement remplies. Aucun cellule ne doit être vide.

## Onglet mix (tube)
### Description
Cet onglet doit contenir toutes les informations relatives aux mix PCR (ou tube) qui sont ensuite envoyés au séquençage. Il est composé de 5 onglets obligatoires :
- d'une colonne 'Mix (tube)' : colonne qui contiendra l'identifiant (ou le nom) du tube. C'est à partir de cet identifiant que le lien entre les informations contenues dans ce fichier et les informations de séquençage est réalisé. cet identifiant correspond au nom donné au tube envoyé à Fasteris.
- d'une colonne 'Plate id' : colonne qui contiendra l'identifiant de la plaque 96 renseigné dans l'onglet 'PCR plate'. Cette colonne indique dans quel mix PCR se trouve la plaque 96.
- d'une colonne 'Type' : colonne qui contiendra le type de la méthode utilisée pour amplifier le matériel génétique. Pour l'instant seul le type 'PCR' est disponible.
- d'une colonne 'Quantity (µl)' : colonne qui contiendra la valeur en microlitre de la quantité de produit PCR de la plaque 96 contenu dans le mix.
- d'une colonne 'Concentration (ng/µl)' : colonne qui contiendra la valeur en nanogramme par microlitre de la concentration d'ADN de la plaque 96 contenu dans le mix.

### Règle d'utilisation
1. Les identifiants de plaque 96 renseignés dans cet onglet doivent être les mêmes que ceux utilisés dans les onglets 'PCR Plate' et 'PCR'.
2. En l'état de la base, la colonne 'type' contient obligatoirement 'PCR'.
3. Un plaque 96 ne peut pas être présente en double dans un tube.
4. Les valeurs de la colonne 'Quantity' doivent être en microlitre et positives. Dans le cas où cette valeur n'est pas connue, veuillez renseigner la valeur 1.
5. Les valeurs de la colonne 'Concentration' doivent être en nanogramme par microlitre et positives. Dans le cas où cette valeur n'est pas connue, veuillez renseigner la valeur 1.
6. Aucun ligne ne doit être partiellement remplies. Aucun cellule ne doit être vide.
