# Notice d'utilisation de fichier excel template_sequencing.xlsx

Ce fichier excel est mis à disposition pour insérer les informations de séquençage lorsque le fichier xml d'information de séquençage de Fasterix n'est pas disponible. Donc vous devez utiliser ce fichier pour tous les autres prestataires de séquençage qui n'ont pas la même structure xml des données.
Ce fichier contient 3 onglets pré-remplis facilitant l'insertion des données par l'utilisateur à l'aide d'un code couleur :
- bleu clair : titre des onglets (obligatoirement la première ligne de chaque onglet)
- violet : titre de colonne ou de ligne obligatoires. La position et le contenu de ces cellules ne doit pas être modifiés.
- orange : champs de valeurs obligatoire. Cellules que l'utilisateur doit remplir avec ses valeurs.


## Onglet Sequencing information
Cet onglet doit contenir toutes les informations concernant le séquençage. Il est composé de 5 lignes :
- Provider : le champs à renseigner doit contenir le nom du prestataire de séquençage.
- Contact : le champs à renseigner doit contenir l'adresse email de la personne du laboratoire qui est en contact avec le prestataire (ou celui qui a passé la commande).
- Sequencing date : le champs à renseigner doit contenir la date du séquençage au format YYYY-MM-DD.
- Sequencing technology : le champs à renseigner doit contenir le nom de la technologie de séquençage utilisée. Ex : Illumina.
- Sequencing instrument : le champs à renseigner doit contenir le nom de l'appareil de séquençage utilisée. Ex : NextSeq.


## Onglet Libraries
Cet onglet doit contenir les informations pour lier les librairies de séquençage aux tubes. Il est composé de 2 colonnes obligatoires :
- Library : colonne qui contiendra les identifiants (ou noms) des librairies
- Tube : colonne qui contiendra les identifiants (ou noms) des tubes (ou mix).

Cet onglet permet de faire le lien entre les librairies et les tubes qui ont été préalablement renseignés dans "l'experiment file".

Remarque :
- Toutes les Librairies décrites dans cet onglet doivent être utilisées dans l'onglet suivant (onglet Sequencing files).
- C'est à partir des noms des tubes que les données de séquençage seront rattachées aux données du projet.

## Onglet Sequencing files
Cet onglet doit contenir les informations pour lier les fichiers de séquençage aux librairies. Il est composé de 2 colonnes obligatoires :
- Sequencing file : colonne qui contiendra le nom des fichiers de séquençage.
- Library : colonne qui contiendra les identifiants des librairies détaillées dans l'onglet précédent (onglet libraries).

Cet onglet permet de faire le lien entre les librairies et les fichiers de séquençage.

Remarque : toutes les Librairies renseignées dans cet onglet doivent être décrites dans l'onglet précédent (onglet Libraries).
