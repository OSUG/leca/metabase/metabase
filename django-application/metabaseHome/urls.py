from django.urls import re_path
from . import views

app_name = 'metabaseHome'

urlpatterns = [
    re_path(r'^$',
        views.index,
        name='index'),
    re_path(r'^myaccount',
        views.myaccount,
        name='myaccount'),
    re_path(r'^experiment/file/template/download',
        views.export_template,
        name='export_experiment_template'),
    re_path(r'^experiment/file/template-with-macro/download',
        views.export_template_with_macro,
        name='export_experiment_template_with_macro'),
    re_path(r'^experiment/file/notice/download',
        views.export_experiment_notice,
        name='export_experiment_notice'),
    re_path(r'^sequencing/file/template/download',
        views.export_sequencing_template,
        name='export_sequencing_template'),
    re_path(r'^sequencing/file/notice/download',
        views.export_sequencing_notice,
        name='export_sequencing_notice'),
]
