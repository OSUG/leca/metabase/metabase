from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.conf import settings
from django.http import HttpResponse
from rest_framework.authtoken.models import Token

from pathlib import Path


@require_http_methods(["GET"])
@login_required
def index(request):
    return render(request, 'index.html')


@require_http_methods(["GET", "POST"])
@login_required
def myaccount(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(
                request, 'Your password was successfully updated!')
            return redirect('metabaseHome:myaccount')
        else:
            messages.error(request,
                           'The new password could not be saved because some fields have errors.')
    else:
        token = Token.objects.get(user_id=request.user.id)
        form = PasswordChangeForm(request.user)
    return render(request, 'myaccount.html', {
        'form': form,
        'token': token
    })


@require_http_methods(["GET"])
def lost_password(request):
    return render(request, 'registration/password_lost.html',
                  {'contact_email': settings.CONTACT_EMAIL})


@require_http_methods(["GET"])
@login_required(redirect_field_name="/home")
def export_template(request):
    file_path = Path(settings.BASE_DIR +
                     "/metabaseHome/ressources/template.xlsx").resolve()
    file = open(file_path, 'rb')
    response = HttpResponse(file.read(),
                            content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = ('attachment; filename="template.xlsx"')
    return response

@require_http_methods(["GET"])
@login_required(redirect_field_name="/home")
def export_template_with_macro(request):
    file_path = Path(settings.BASE_DIR +
                     "/metabaseHome/ressources/template-with-macro.xlsm").resolve()
    file = open(file_path, 'rb')
    response = HttpResponse(file.read(),
                            content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = (
            'attachment; filename="template-with-macro.xlsm"')
    return response


@require_http_methods(["GET"])
@login_required(redirect_field_name="/home")
def export_experiment_notice(request):
    file_path = Path(settings.BASE_DIR +
        "/metabaseHome/ressources/experiment_file_notice.pdf").resolve()
    file = open(file_path, 'rb')
    response = HttpResponse(file.read(),
                            content_type='application/pdf')
    response['Content-Disposition'] = (
        'attachment; filename="experiment_file_notice.pdf"')
    return response


@require_http_methods(["GET"])
@login_required(redirect_field_name="/home")
def export_sequencing_template(request):
    file_path = Path(settings.BASE_DIR +
        "/metabaseHome/ressources/template_sequencing.xlsx").resolve()
    file = open(file_path, 'rb')
    response = HttpResponse(file.read(),
                            content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = (
        'attachment; filename="sequencing_template.xlsx"')
    return response


@require_http_methods(["GET"])
@login_required(redirect_field_name="/home")
def export_sequencing_notice(request):
    file_path = Path(settings.BASE_DIR +
        "/metabaseHome/ressources/sequencing_file_notice.pdf").resolve()
    file = open(file_path, 'rb')
    response = HttpResponse(file.read(),
                            content_type='application/pdf')
    response['Content-Disposition'] = (
        'attachment; filename="sequencing_file_notice.pdf"')
    return response
