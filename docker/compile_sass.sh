#!/bin/bash

set -e

if [ $# -eq 0 ]
then
  SASSDIRS=$(find . -name "sass" | grep "static/sass")
  for SASSDIR in ${SASSDIRS}
  do
    STATICDIR=$(dirname ${SASSDIR})
    mkdir -p ${STATICDIR}/css
    sass --update ${SASSDIR}:${STATICDIR}/css
    USERID=$(stat -c '%u' ${SASSDIR})
    GROUPID=$(stat -c '%g' ${SASSDIR})
    chown -R ${USERID}:${GROUPID} ${STATICDIR}/css
  done
fi

exec "$@"
