#!/bin/bash
envsubst '${METABASE_URL},${METABASE_PORT}' < /etc/nginx/conf.d/metabase.conf.template > /etc/nginx/conf.d/default.conf
