Django==3.2.14
psycopg2-binary==2.9.3
openpyxl==3.0.2
djangorestframework==3.13.1
django-widget-tweaks==1.4.8
