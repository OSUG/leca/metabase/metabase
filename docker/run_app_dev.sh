#!/bin/bash

set -e

/bin/sh /app/wait-for ${POSTGRES_HOST}:${POSTGRES_PORT}

if [ $# -eq 0 ]
then
  if [ ${MIGRATION} == true ]
  then
    python /app/djangoApp/manage.py makemigrations
    python /app/djangoApp/manage.py makemigrations metabaseEditor
    python /app/djangoApp/manage.py migrate
  fi
  python /app/djangoApp/manage.py runserver 0.0.0.0:8000
fi

exec "$@"
