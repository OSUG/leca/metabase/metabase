#!/bin/bash


###########################################################################
# Initialize the database metaBase with the last version of database      #
# This script apply migrations of django database and add dump data.      #
# argument:                                                               #
#   - path of file which contains database data (this file was created    #
#     with django command: dumpdata)                                      #
###########################################################################


set -e

if [ $# -ne 1 ]
then
  echo "The command not working, please try: "
  echo "bash init_metabase_db.sh dumpdata.json (to initialize database with data)"
  exit 1
else
  if [ ! -f $1 ]
  then
    echo "The argument is not a file!"
    exit 1
  fi
fi

# Get full path of docker directory and migration directory
DOCKERDIR=$(dirname $(readlink -f "${0}"))"/docker"

MIGRATIONDIR=${DOCKERDIR}"/migration"
# Build file name
DBFILE=$(date +%Y%m%d)"_db.json"
# copy the imported file in the migration directory which is contains by shared directory
cat $1 > ${MIGRATIONDIR}"/"${DBFILE}
sudo docker-compose -f ${DOCKERDIR}/docker-compose.dev.yml run --rm web python3 /app/djangoApp/manage.py loaddata /app/migration/${DBFILE}

sudo docker-compose -f ${DOCKERDIR}/docker-compose.dev.yml down
