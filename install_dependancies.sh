#!/bin/bash

set -e

usage() {
  echo 'Usage: install_dependancies.sh'
  echo 'This script install docker and docker-compose, add docker to os group,'
  echo 'add the group docker to user, and configure docker daemon'
}

if [ $# -gt 0 ]
then
  usage
  exit 1
fi

# install docker and docker-compose
sudo apt install -y docker docker-compose

# check if group 'docker' does not exist
if ! grep -q docker /etc/group
then
  # add group docker if not exist
  sudo groupadd docker
fi

# check if user is not in the group 'docker'
if ! groups ${USER} | grep -qw 'docker'
then
  # assign groups docker to the user
  sudo usermod -aG docker ${USER}
fi

# copy the docker daemon file to configure dns for internet connexion
DNSIPS=$(nmcli dev show | grep DNS | sed "s/\s//g" | cut -d":" -f2)
DNS=$(echo ${DNSIPS} | sed 's/ /", "/g')
echo '{
  "dns": ["'${DNS}'", "8.8.8.8"]
}' > docker/deamon.json
sudo cp docker/daemon.json /etc/docker/

# restart docker service to apply daemon configuration
sudo service docker restart

echo 'Docker and docker-compose installation is completed.'
echo 'Please, log out and log back in to re-evaluate your group membership.'
echo 'Note: If testing on a virtual machine, it may be necessary to restart the virtual machine for changes to take effect.'
