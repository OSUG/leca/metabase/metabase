#!/bin/bash

set -e

if [ $# -eq 1 ] && [ $1 == 'dev' ]
then
  DOCKERCOMPOSEFILE=docker/docker-compose.dev.yml
elif [ $# -eq 1 ] && [ $1 == 'prod' ]
then
  DOCKERCOMPOSEFILE=docker/docker-compose.prod.yml
else
  echo 'Usage: run_docker_compose.sh distrib'
  echo 'required argument:'
  echo '  distrib: prod or dev:'
  echo '    prod: run production server'
  echo '    dev: run development server'
  exit 1
fi

if groups ${USER} | grep -qw 'docker'
then
  docker-compose -f ${DOCKERCOMPOSEFILE} up
  docker-compose -f ${DOCKERCOMPOSEFILE} down
else
  sudo docker-compose -f ${DOCKERCOMPOSEFILE} up
  sudo docker-compose -f ${DOCKERCOMPOSEFILE} down
fi
