#!/bin/bash

set -e

usage() {
  echo 'Usage: setup_metabase_project.sh [db_init_file]'
  echo 'Optional argument:'
  echo '  db_init_file: json file produce by command cmd manage.py dumpdata.'
  echo '                This file is use to initialize db and fill the db '
  echo '                with data of old database backup.'
  echo 'This script install metabase project, configure environmental file '
  echo 'and initialize the database.'
}

if [ $# -gt 1 ]
then
  usage
  exit 1
elif [ $# -eq 1 ] && [ ! -f $1 ]
then
  echo "The argument is not a file!"
  usage
  exit 1
fi

# create the require directory
mkdir docker/migration
mkdir -p docker/pg-data/db-data
mkdir docker/backup

# build docker images
sudo docker-compose -f docker/docker-compose.dev.yml build
sudo docker-compose -f docker/docker-compose.dev.yml run web python3 /app/djangoApp/manage.py makemigrations
sudo docker-compose -f docker/docker-compose.dev.yml run web python3 /app/djangoApp/manage.py makemigrations metabaseEditor
sudo docker-compose -f docker/docker-compose.dev.yml run web python3 /app/djangoApp/manage.py migrate
# stop and remove docker-compose service
sudo docker-compose -f docker/docker-compose.dev.yml down

# Initialize the database with file to add backup database
if [ $# -eq 1 ]
then
  bash init_metabase_db.sh $1
fi

echo 'Would you like to add superuser (yes | no):'
read SUPERUSER
while [ ${SUPERUSER} != "yes" ] && [ ${SUPERUSER} != "no" ]
do
  echo 'Would you like to add superuser (yes | no):'
  read SUPERUSER
done
# add superuser
if [ ${SUPERUSER} == "yes" ]
then
  echo "If you want to cancel the superuser adding, you can use ctl + c."
  set +e
  export PYTHONIOENCODING="UTF-8"
  sudo docker-compose -f docker/docker-compose.dev.yml run web python3 /app/djangoApp/manage.py createsuperuser
  # stop and remove docker-compose service
  sudo docker-compose -f docker/docker-compose.dev.yml down
  set -e
fi

echo 'The project installation is completed.'
