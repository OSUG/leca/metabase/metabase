#!/bin/bash

set -e

if [ $# -eq 1 ] && [ $1 != 'all' ]
then
  echo 'usage: uninstall_project.sh [all]'
  echo 'optional argument:'
  echo '  all: uninstall docker and docker-compose'
  echo 'If argument "all" is not used, project files and docker images are'
  echo 'removed but the softwares docker and docker-compose are not removed.'
  exit 1
fi

sudo docker-compose -f docker/docker-compose.dev.yml down

sudo docker rmi -f $(sudo docker images -q metabase_dev:lastest)
sudo docker rmi -f $(sudo docker images -q postgres:11.1-alpine)
sudo docker rmi -f $(sudo docker images -q sass_compiler:lastest)
sudo docker rmi -f $(sudo docker images -q ruby:lastest)
sudo docker rmi -f $(sudo docker images -q python:3.7)

sudo rm /etc/docker/daemon.json

sudo rm -r docker/migration
sudo rm -r docker/pg-data
sudo rm -r docker/backup

if [ $# -eq 1 ] && [ $1 == 'all' ]
then
  sudo docker rmi -f $(sudo docker images -q)
  if grep -q docker /etc/group
  then
    sudo deluser ${USER} docker
    sudo groupdel docker
  fi
  sudo apt autoremove docker docker-compose
fi
